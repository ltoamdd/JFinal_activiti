package com.qysoft.rapid.aop.Interceptor;

import com.jfinal.aop.Enhancer;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.qysoft.rapid.aop.annotations.AutoService;
import com.qysoft.rapid.exceptions.BizException;
import com.qysoft.rapid.exceptions.RapidException;
import com.qysoft.rapid.utils.ClassUtil;

import java.lang.reflect.Field;

/**
 * Created by shenjinxiang on 2017/11/30.
 */
public class ActionFieldInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation invocation) {
        Class clazz = invocation.getMethod().getDeclaringClass();
        Object target = invocation.getTarget();
        try {
            Field[] fields = clazz.getDeclaredFields();
            for (Field field: fields) {
                if (field.isAnnotationPresent(AutoService.class)) {
                    generateServiceField(field, target);
                    continue;
                }
            }
            invocation.invoke();
        } catch (Exception e) {
            if (e instanceof BizException) {
                throw new BizException(e.getMessage());
            } else {
                throw new RapidException(e);
            }
        }
    }


    private void generateServiceField(Field field, Object target) throws Exception {
        field.setAccessible(true);
        if (null != field.get(target)) {
            return;
        }
        AutoService autoService = field.getAnnotation(AutoService.class);
        Class clazz = autoService.value().equals(Object.class) ? field.getType() : autoService.value();
        if (!ClassUtil.canNewInstance(clazz)) {
            throw new RapidException(getErrorMsg(field, target));
        }
        Object obj = Enhancer.enhance(clazz);
        if (null == obj) {
            throw new RapidException(getErrorMsg(field, target));
        }
        field.set(target, obj);
    }

    private String getErrorMsg(Field field, Object target) {
        return target.getClass().getName() + "." + field.getName() + "无法初始化";
    }

}
