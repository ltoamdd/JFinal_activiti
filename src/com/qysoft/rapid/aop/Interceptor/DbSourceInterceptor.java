package com.qysoft.rapid.aop.Interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.StrKit;
import com.qysoft.rapid.aop.annotations.MyBatisDbConn;
import com.qysoft.rapid.aop.annotations.MyBatisDbConnTx;
import com.qysoft.rapid.consts.RapidConsts;
import com.qysoft.rapid.exceptions.BizException;
import com.qysoft.rapid.exceptions.RapidException;
import com.qysoft.rapid.plugin.mybatis.MyBatisSessionManager;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import java.lang.reflect.Method;

/**
 * Created by shenjinxiang on 2017/11/30.
 */
public class DbSourceInterceptor implements Interceptor {

//    private Class clazz;
//    private Method method;
    private final Logger log = Logger.getLogger(DbSourceInterceptor.class);

    @Override
    public void intercept(Invocation invocation) {
        Method method = invocation.getMethod();
        Class clazz = invocation.getMethod().getDeclaringClass();
        if (method.isAnnotationPresent(MyBatisDbConnTx.class)) {
            MyBatisDbConnTx myBatisDbConnTx = method.getAnnotation(MyBatisDbConnTx.class);
            generateMyBatisDbConnTx(invocation, myBatisDbConnTx);
            return;
        }
        if (method.isAnnotationPresent(MyBatisDbConn.class)) {
            MyBatisDbConn myBatisDbConn = method.getAnnotation(MyBatisDbConn.class);
            generateMyBatisDbConn(invocation, myBatisDbConn);
            return;
        }
        if (clazz.isAnnotationPresent(MyBatisDbConnTx.class)) {
            MyBatisDbConnTx myBatisDbConnTx = (MyBatisDbConnTx) clazz.getAnnotation(MyBatisDbConnTx.class);
            generateMyBatisDbConnTx(invocation, myBatisDbConnTx);
            return;
        }
        if (clazz.isAnnotationPresent(MyBatisDbConn.class)) {
            MyBatisDbConn myBatisDbConn = (MyBatisDbConn) clazz.getAnnotation(MyBatisDbConn.class);
            generateMyBatisDbConn(invocation, myBatisDbConn);
            return;
        }
        invocation.invoke();
    }

    private void generateMyBatisDbConnTx(Invocation invocation, MyBatisDbConnTx myBatisDbConnTx) {
//        MyBatisDbConnTx myBatisDbConnTx = this.method.getAnnotation(MyBatisDbConnTx.class);
        boolean createSession = false;
        SqlSession session = MyBatisSessionManager.getSession();
        String dbSourceKey = RapidConsts.getDEFAULT_DBSOURCE_KEY();
        if (null == session) {
            dbSourceKey = StrKit.isBlank(myBatisDbConnTx.dbSourceKey()) ?
                    RapidConsts.getDEFAULT_DBSOURCE_KEY() :
                    myBatisDbConnTx.dbSourceKey();
            MyBatisSessionManager.setSession(dbSourceKey, false);
            createSession = true;
            doLog("已获取数据库连接，数据库key：" + dbSourceKey);
            doLog("数据库连接事务已开启...");
        }
        try {
            if (createSession) {
                doLog("准备执行业务...");
            }
            invocation.invoke();
            if (createSession) {
                doLog("业务已执行...");
                MyBatisSessionManager.commit();
                doLog("事务已提交...");
            }

        } catch (BizException e) {
            MyBatisSessionManager.rollback();
            doLog("事务已回滚...");
            doLog("BizException: "+e.getMessage());
            throw e;
        } catch (Exception e) {
            MyBatisSessionManager.rollback();
            doLog("事务已回滚...");
            e.printStackTrace();
            throw new RapidException(e);
        } finally {
            if (createSession) {
                MyBatisSessionManager.closeSession();
                doLog("连接已释放，数据库key：" + dbSourceKey);
            }
        }

    }

    private void generateMyBatisDbConn(Invocation invocation, MyBatisDbConn myBatisDbConn) {

        boolean createSession = false;
        SqlSession session = MyBatisSessionManager.getSession();
        String dbSourceKey = RapidConsts.getDEFAULT_DBSOURCE_KEY();
        if (null == session) {
            dbSourceKey = StrKit.isBlank(myBatisDbConn.dbSourceKey()) ?
                    RapidConsts.getDEFAULT_DBSOURCE_KEY() :
                    myBatisDbConn.dbSourceKey();
            MyBatisSessionManager.setSession(dbSourceKey, true);
            createSession = true;
            doLog("已获取数据库连接，数据库key：" + dbSourceKey);
        }
        try {
            if (createSession) {
                doLog("准备执行业务...");
            }
            invocation.invoke();
            if (createSession) {
                doLog("业务已执行...");
            }

        } catch (BizException e) {
            MyBatisSessionManager.rollback();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RapidException(e);
        } finally {
            if (createSession) {
                MyBatisSessionManager.closeSession();
                doLog("连接已释放，数据库key：" + dbSourceKey);
            }
        }
    }


    private void doLog(String message){
        if (RapidConsts.isIS_DEV_MODE()) {
            log.warn(message);
        }
    }
}
