package com.qysoft.rapid.utils;

import com.jfinal.kit.HttpKit;
import com.qysoft.accountingCloud.platform.entity.FWH;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by shenjinxiang on 2017-07-24.
 */
public class WxKit {

    public static void getJsApiToken(FWH fwh){
        WxKit.getAccessToken(fwh);
        Date nowdate = new Date();
        System.out.println("jsapitoken已生成 时间戳"+fwh.getJsapi_ticket_timestamp());
        System.out.println("jsapi_ticket_timestamp判断时间戳是否有效 值为"+nowdate.after(new Date(fwh.getJsapi_ticket_timestamp())));
        if(nowdate.after(new Date(fwh.getJsapi_ticket_timestamp()))){
            System.out.println("需更新token");
            //时间戳失效需要生成token

            String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="+fwh.getAccess_token()+"&type=jsapi";
            System.out.println("access_token="+url);
            String str = HttpKit.get(url);
            JSONObject js = new JSONObject(str);
            System.out.println("jsapitokenjson ="+js.toString());
            Integer errcode = (Integer)js.getInt("errcode") ;
            String errmsg = String.valueOf(js.get("errmsg"));
            if(errcode!=null && errcode.equals(0) && errmsg!=null && errmsg.equals("ok")){
                //调用正常
//				jsapi_ticket = js.getString("ticket");
                fwh.setJsapi_ticket(js.getString("ticket"));
                int expires_in = js.getInt("expires_in");
                Calendar cal = Calendar.getInstance();
                System.out.println("cal时间错access_timestamp3="+cal.getTimeInMillis());
                cal.add(Calendar.SECOND, expires_in);
                System.out.println("cal时间错access_timestamp4="+cal.getTimeInMillis());
//				jsapi_ticket_timestamp = cal.getTimeInMillis();
                fwh.setJsapi_ticket_timestamp(cal.getTimeInMillis());
                System.out.println("jsapitoken已生成 时间戳"+fwh.getJsapi_ticket_timestamp());

            }else{
                System.out.println("token失效报错信息 errcode = "+errcode);
                System.out.println("token失效报错信息 errmsg = "+errmsg);
            }
        }else{
            System.out.println("不需要更新token");
        }

    }

    /**
     * access_Token
     */
    public static void getAccessToken(FWH fwh){
        //https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
        Date nowdate = new Date();
//		FWH fwh = getSessionAttr("fwh");
        //System.out.println("微信号：" + fwh.getName());
        //System.out.println("access_timestamp已生成 时间戳"+fwh.getAccess_timestamp());
        //System.out.println("判断access_token时间戳是否有效 值为"+nowdate.after(new Date(fwh.getAccess_timestamp())));
        if(nowdate.after(new Date(fwh.getAccess_timestamp()))){
            //System.out.println("需更新access_token");
            //时间戳失效需要生成token

            String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+fwh.getAppid()+"&secret="+ fwh.getAppsecret();
            System.out.println("access_token="+url);
            String str = HttpKit.get(url.toString());
            JSONObject js = new JSONObject(str);
            System.out.println("tokenjson ="+js.toString());
            if(js.isNull("errcode")&&js.isNull("errmsg")){
                //调用正常
//				this.access_token = js.getString("access_token");
                fwh.setAccess_token(js.getString("access_token"));
                int expires_in = js.getInt("expires_in");
                Calendar cal = Calendar.getInstance();
                //System.out.println("cal时间戳="+cal.getTimeInMillis());
                cal.add(Calendar.SECOND, expires_in - 10);
                //System.out.println("cal时间戳2="+cal.getTimeInMillis());
//				access_timestamp = cal.getTimeInMillis();
                fwh.setAccess_timestamp(cal.getTimeInMillis());
                //System.out.println("access_timestamp已生成 时间戳"+fwh.getAccess_timestamp());
//				WeiXinConsts.ACCESS_TOKEN = this.access_token;

            }else{
                Integer errcode = (Integer)js.get("errcode") ;
                //System.out.println("errcode="+errcode);
                //String errmsg = String.valueOf(js.get("errmsg"));
                //System.out.println("errcode="+errcode);
                //System.out.println("token失效报错信息 errcode = "+errcode);
                //System.out.println("token失效报错信息 errmsg = "+errmsg);
            }
        }else{
            //System.out.println("不需要更新token");
//			WeiXinConsts.ACCESS_TOKEN = this.access_token;
        }

    }
}
