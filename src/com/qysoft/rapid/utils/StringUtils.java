/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qysoft.rapid.utils;

import com.qysoft.rapid.consts.RapidConsts;
import org.apache.shiro.crypto.hash.Md5Hash;

/**
 *
 * @author huangwei
 */
public class StringUtils {

    public static String getMD5_SALT(String password) {
        return stringToMD5(password,RapidConsts.MD5_SALT);
    }
    private static String stringToMD5(String str,Object salt){
        return new Md5Hash(str, salt).toString();
    }

}
