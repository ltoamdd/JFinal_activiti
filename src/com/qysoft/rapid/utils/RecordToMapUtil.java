package com.qysoft.rapid.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

public class RecordToMapUtil {

	public static Page<HashMap<String, Object>> convertRecordToMap(Page<Record> records){
		List<HashMap<String, Object>> result = new ArrayList<HashMap<String,Object>>();
		List<Record> recordList = records.getList();
		for(int i=0;i<recordList.size();i++){
			HashMap<String, Object> hm = (HashMap<String, Object>) recordList.get(i).getColumns();
			result.add(hm);
		} 
		Page<HashMap<String, Object>> pageRecords = new Page<HashMap<String,Object>>(result, records.getPageNumber(), records.getPageSize(), records.getTotalPage(), records.getTotalRow());
		return pageRecords;
	}


	public static List<HashMap<String, Object>> convertRecordToMap(List<Record> records){
		List<HashMap<String, Object>> result = new ArrayList<HashMap<String,Object>>();
		for(int i=0;i<records.size();i++){
			HashMap<String, Object> hm = (HashMap<String, Object>) records.get(i).getColumns();
			result.add(hm);
		} 
		return result;
	}
	
}
