
package com.qysoft.rapid.domain;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.ModelBuilder;

/**
 * BeanBuilder.
 */
public class BeanBuilder {


	public static final List<Bean> build(ResultSet rs) throws Exception {
		List<Bean> result = new ArrayList<Bean>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		String[] labelNames = new String[columnCount + 1];
		int[] types = new int[columnCount + 1];
		buildLabelNamesAndTypes(rsmd, labelNames, types);
		while (rs.next()) {
			Bean bean = new Bean();
			for (int i=1; i<=columnCount; i++) {
				Object value;
				if (types[i] < Types.BLOB)
					value = rs.getObject(i);
				else if (types[i] == Types.CLOB)
					value = new ModelBuilder().handleClob(rs.getClob(i));
				else if (types[i] == Types.NCLOB)
					value = new ModelBuilder().handleClob(rs.getNClob(i));
				else if (types[i] == Types.BLOB)
					value = new ModelBuilder().handleBlob(rs.getBlob(i));
				else
					value = rs.getObject(i);
				
				bean.put(labelNames[i], value);
			}
			result.add(bean);
		}
		return result;
	}
	
	private static final void buildLabelNamesAndTypes(ResultSetMetaData rsmd, String[] labelNames, int[] types) throws SQLException {
		for (int i=1; i<labelNames.length; i++) {
			labelNames[i] = rsmd.getColumnLabel(i);
			types[i] = rsmd.getColumnType(i);
		}
	}
	
}





