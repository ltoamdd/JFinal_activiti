package com.qysoft.rapid.core;

import java.net.URL;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.ActionReporter;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;
import com.qysoft.accountingCloud.platform.plugins.DataCachePlugin;
import com.qysoft.accountingCloud.platform.plugins.FWHPlugin;
import com.qysoft.rapid.aop.Interceptor.ActionFieldInterceptor;
import com.qysoft.rapid.consts.RapidConsts;
import com.qysoft.rapid.plugin.druid.DruidPlugin;
import com.qysoft.rapid.plugin.druid.DruidStatViewHandler;
import com.qysoft.rapid.aop.Interceptor.DbSourceInterceptor;
import com.qysoft.rapid.plugin.mybatis.MyBatisPlugin;
import com.qysoft.rapid.utils.DbSourcePlugin;

/**
 * rapid平台配置工具
 * @author liugong
 */
public abstract class RapidConifg extends JFinalConfig{
	
	private Routes routes;
	
	/**
	 * 配置数据库工具
	 */
	public abstract int configDbTool();
	
	/**
	 * 配置action
	 * @param me
	 */
	public abstract void configAction(Routes me);
	
	
	/**
	 * 配置action
	 */
	public abstract String configErrorMSg();
	
	/**
	 * 设置数据库版本
	 * @return
	 */
	public abstract int configDbType();
	
	
	@Override
	public final void configConstant(Constants me) {
		//设置错误页
		me.setError401View("/pages/common/401.html");
		me.setError404View("/pages/common/404.html"); 
		me.setError403View("/pages/common/403.html");
		me.setError500View("/pages/common/500.html");
		me.setMaxPostSize(50 * 1024 * 1024);
		//读取rapid配置
		loadPropertyFile("config/rapid.properties");
		RapidConsts.setIS_ALL_SCR(getPropertyToBoolean("isAllScr"));
		RapidConsts.setIS_DEV_MODE(getPropertyToBoolean("devMode"));
        ActionReporter.setReportAfterInvocation(false);
		me.setDevMode(RapidConsts.isIS_DEV_MODE());
		me.setViewType(ViewType.JSP);
		//配置全局错误提示
		RapidConsts.setERROR_MSG(configErrorMSg());
	}

	@Override
	public final void configRoute(Routes me) {
		this.routes = me;
		configAction(this.routes);
	}

	@Override
	public final void configHandler(Handlers me) {
		//druid监控
		DruidStatViewHandler dvh =  new DruidStatViewHandler("/druid");
		me.add(dvh);
	} 

	@Override
	public final void configPlugin(Plugins me) {
		URL url = getClass().getResource("/config/ehcache.xml");
		me.add(new EhCachePlugin(url));
		DbSourcePlugin.configPlugin(me);
		me.add(new DataCachePlugin());
		me.add(new FWHPlugin());
	}

    @Override
    public final void configInterceptor(Interceptors me) {
        me.addGlobalActionInterceptor(new ActionFieldInterceptor());

        // 让 模版 可以使用session
        me.add(new SessionInViewInterceptor());
        me.addGlobalServiceInterceptor(new DbSourceInterceptor());
    }


	private final void configOrlace(Plugins me){
		//读取数据库配置
		loadPropertyFile("config/oracle.properties");
		DruidPlugin dp = new DruidPlugin(getProperty("jdbcUrl"),getProperty("user"),getProperty("password"));
		dp.setDriverClass(getProperty("driverClass"));
		dp.addFilter(new StatFilter());
		dp.setMaxActive(getPropertyToInt("dbMaxActive"));
		if (RapidConsts.isIS_DEV_MODE()) {
			dp.setInitialSize(1);
		}else {
			dp.setInitialSize(getPropertyToInt("dbInitSize"));
		}
		WallFilter wall = new WallFilter(); 
		wall.setDbType(getProperty("dbType")); 
		dp.addFilter(wall);
		me.add(dp);		
		//配置数据库操作工具
		int dbToll = configDbTool();
		RapidConsts.setCURRENT_DB_TOOL(dbToll);
		switch (dbToll) {
		case RapidConsts.DB_TOOL_ACTIVERECORD:
			//配置ActiveRecord插件
			ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
			me.add(arp);
			arp.setShowSql(RapidConsts.isIS_DEV_MODE());
			// 配置Oracle方言
			arp.setDialect(new OracleDialect());
			// 配置属性名(字段名)大小写不敏感容器工厂
			arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
			break;
		case RapidConsts.DB_TOOL_MYBATIS:
			MyBatisPlugin myBatisPlugin = new MyBatisPlugin(dp);
			me.add(myBatisPlugin);
			myBatisPlugin.setShowSql(RapidConsts.isIS_DEV_MODE());
			break;
		}
	}
	
	
	private final void configMySql(Plugins me){
		//读取数据库配置
		loadPropertyFile("config/mysql.properties");
		DruidPlugin dp = new DruidPlugin(getProperty("jdbcUrl"),getProperty("user"),getProperty("password"));
		dp.setDriverClass(getProperty("driverClass"));
		dp.addFilter(new StatFilter());
		dp.setMaxActive(getPropertyToInt("dbMaxActive"));
		if (RapidConsts.isIS_DEV_MODE()) {
			dp.setInitialSize(1);
		}else {
			dp.setInitialSize(getPropertyToInt("dbInitSize"));
		}
		WallFilter wall = new WallFilter(); 
		wall.setDbType(getProperty("dbType")); 
		dp.addFilter(wall);
		me.add(dp);		
		//配置数据库操作工具
		int dbToll = configDbTool();
		RapidConsts.setCURRENT_DB_TOOL(dbToll);
		switch (dbToll) {
		case RapidConsts.DB_TOOL_ACTIVERECORD:
			//配置ActiveRecord插件
			ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
			me.add(arp);
			arp.setShowSql(RapidConsts.isIS_DEV_MODE());
			break;
		case RapidConsts.DB_TOOL_MYBATIS:
			MyBatisPlugin myBatisPlugin = new MyBatisPlugin(dp);
			me.add(myBatisPlugin);
			myBatisPlugin.setShowSql(RapidConsts.isIS_DEV_MODE());
			break;
		}
	}
	

	
}
