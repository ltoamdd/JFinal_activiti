package com.qysoft.rapid.consts;

import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * rapid平台常量
 * @author liugong
 *
 */
public final class RapidConsts {
	
	/**
	 * 平台名称
	 */
	public final static String RAPID_NAME = "财税服务后台管理系统";
	
	/**
	 * 平台版本
	 */
	public final static String RAPID_VERSION = "1.0";
	
	/**
	 * ORACLE数据库
	 */
	public final static int DB_TYPE_ORACLE = 0;

	/**
	 * MYSQL数据库
	 */
	public final static int DB_TYPE_MYSQL = 1;
	
	/**
	 * 数据库操作工具ActiveRecord
	 */
	public final static int DB_TOOL_ACTIVERECORD = 0;

	/**
	 * 数据库操作工具MyBatis
	 */
	public final static int DB_TOOL_MYBATIS = 1;
	
	
	/**
	 * 图标路径
	 */
	public final static String ICONS_PATH = "/static/images/icons/";
	
	/**
	 * excel导出最大行数
	 */
	public final static int MAX_EXCEL_ROWS = 4000;
	
	
	/**
	 * 是否全屏
	 */
	private static boolean IS_ALL_SCR = false;
	
	/**
	 * 是否开发模式
	 */
	private static boolean IS_DEV_MODE = false;
	
	/**
	 * 数据库操作工具
	 */
	private static int CURRENT_DB_TOOL = 0;
	
	/**
	 * 加密盐值
	 */
	public final static String MD5_SALT = "SXQYRJ_ACCOUNTCLOUD_2015";
	
	public static int getCURRENT_DB_TOOL() {
		return CURRENT_DB_TOOL;
	}

	public static void setCURRENT_DB_TOOL(int cURRENT_DB_TOOL) {
		CURRENT_DB_TOOL = cURRENT_DB_TOOL;
	}

	/**
	 * 当前数据库版本
	 */
	private static int CURRENT_DB_TYPE = 0;
	
	
	/**
	 * 错误提示
	 */
	private static String ERROR_MSG = "操作失败，请联系管理员！";	
	
	/**
	 * 获取默认错误提示
	 * @return
	 */
	public static String getERROR_MSG() {
		return ERROR_MSG;
	}

	/**
	 * 设置错误提示
	 * @param eRROR_MSG
	 */
	public static void setERROR_MSG(String eRROR_MSG) {
		ERROR_MSG = eRROR_MSG;
	}

	/**
	 * 是否全屏
	 * @return
	 */
	public static boolean isIS_ALL_SCR() {
		return IS_ALL_SCR;
	}

	/**
	 * 设置是否全屏
	 * @param iS_ALL_SCR
	 */
	public static void setIS_ALL_SCR(boolean iS_ALL_SCR) {
		IS_ALL_SCR = iS_ALL_SCR;
	}

	/**
	 * 是否开发模式
	 * @return
	 */
	public static boolean isIS_DEV_MODE() {
		return IS_DEV_MODE;
	}

	/**
	 * 设置是否开发模式
	 * @param iS_DEV_MODE
	 */
	public static void setIS_DEV_MODE(boolean iS_DEV_MODE) {
		IS_DEV_MODE = iS_DEV_MODE;
	}

	/**
	 * 获取当前数据库版本
	 * @return
	 */
	public static int getCURRENT_DB_TYPE() {
		return CURRENT_DB_TYPE;
	}

	/**
	 * 设置当前数据库版本
	 * @param cURRENT_DB_TYPE
	 */
	public static void setCURRENT_DB_TYPE(int cURRENT_DB_TYPE) {
		CURRENT_DB_TYPE = cURRENT_DB_TYPE;
	}
	
	public static void main(String[] args) {
		String str = new Md5Hash("111111","BETA1").toString();
		System.out.println(str);
	}

	private static String DEFAULT_DBSOURCE_KEY ;

	public static void setDEFAULT_DBSOURCE_KEY(String default_dbsource_key) {
		DEFAULT_DBSOURCE_KEY = default_dbsource_key;
	}

	public static String getDEFAULT_DBSOURCE_KEY() {
		return DEFAULT_DBSOURCE_KEY;
	}

}
