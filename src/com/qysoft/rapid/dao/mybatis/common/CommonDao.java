package com.qysoft.rapid.dao.mybatis.common;

import com.qysoft.rapid.dao.mybatis.MyBatisDao;

public class CommonDao extends MyBatisDao {

	public static CommonDao dao = null;
	
	private CommonDao(){}
	
	static{
		dao = new CommonDao();
	}
}
