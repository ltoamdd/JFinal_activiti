package com.qysoft.rapid.dao.mybatis;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.jfinal.plugin.activerecord.Page;
import com.qysoft.rapid.consts.RapidConsts;
import com.qysoft.rapid.exceptions.RapidException;
import com.qysoft.rapid.plugin.mybatis.MyBatisPlugin;
import com.qysoft.rapid.plugin.mybatis.MyBatisSessionManager;

/**
 * 封装mybatis数据库操作
 * @author liugong
 *
 */
public class MyBatisDao {
	
	/**
	 * 获取sqlsession
	 * @return
	 */
	private static SqlSession getSqlSession(){
		return MyBatisPlugin.getSqlSessionFactory().openSession(true);
	}

	private static SqlSession getSqlSession(String dbSourceKey){
		return MyBatisPlugin.getSqlSessionFactory(dbSourceKey).openSession(true);
	}


	/**
	 * 分页查询
	 * @param countSqlid
	 * @param dataSqlid
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public static Page<HashMap<String, Object>> paginateByXml(int page,int limit,String countSqlid,String dataSqlid,HashMap<String, Object> parameters) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			int start = (page-1)*limit;
			parameters.put("start", start); 
			int count = session.selectOne(countSqlid, parameters);
			List<HashMap<String, Object>> datas = session.selectList(dataSqlid, parameters);
			int totalPage = (int) (count / limit);
			if (count % limit != 0) {
				totalPage++;
			}
			return new Page<HashMap<String,Object>>(datas, page, limit, totalPage, count);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}			
		}
	}
	
	/**
	 * selectOne
	 * @param sqlId
	 * @return
	 * @throws Exception
	 */
	public static <T> T selectOneByXml(String sqlId) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			return session.selectOne(sqlId);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}
		}		
	}
	
	/**
	 * selectOne
	 * @param sqlId
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public static <T> T selectOneByXml(String sqlId,Object parameters) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			return session.selectOne(sqlId,parameters);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}
		}		
	}
	
	/**
	 * selectList
	 * @param sqlId
	 * @return
	 * @throws Exception
	 */
	public static <T>List<T> selectListByXml(String sqlId) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			List<T> result = session.selectList(sqlId);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}
		}		
	}
	
	/**
	 * selectList
	 * @param sqlId
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public static <T>List<T> selectListByXml(String sqlId,Object parameters) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			List<T> result = session.selectList(sqlId,parameters);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}
		}		
	}	
	
	/**
	 * insert
	 * @param sqlId
	 * @throws Exception
	 */
	public static int insertByXml(String sqlId) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			int primaryKey = session.insert(sqlId);
			return primaryKey;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}
		}
	}

	
	/**
	 * insert
	 * @param sqlId
	 * @param parameters
	 * @throws Exception
	 */
	public static int insertByXml(String sqlId,Object parameters) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			int primaryKey = session.insert(sqlId,parameters);
			if (!isTx) {
				session.commit();
			}
			return primaryKey;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}
		}
	}
	
	/**
	 * delete
	 * @param sqlId
	 * @return
	 * @throws Exception
	 */
	public static int deleteByXml(String sqlId) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			int deleteCount = session.delete(sqlId);
			if (!isTx) {
				session.commit();
			}
			return deleteCount;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}
		}
	}


	/**
	 * update
	 * @param sqlId
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public static int deleteByXml(String sqlId,Object parameters) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			int deleteCount = session.delete(sqlId,parameters);
			if (!isTx) {
				session.commit();
			}
			return deleteCount;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}
		}
	}	
	
	/**
	 * update
	 * @param sqlId
	 * @return
	 * @throws Exception
	 */
	public static int updateByXml(String sqlId) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			int updateCount = session.update(sqlId);
			if (!isTx) {
				session.commit();
			}
			return updateCount;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}
		}
	}


	/**
	 * update
	 * @param sqlId
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public static int updateByXml(String sqlId,Object parameters) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			int updateCount = session.update(sqlId,parameters);
			if (!isTx) {
				session.commit();
			}
			return updateCount;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}
		}
	}
	
	/**
	 * 分页查询
	 * @param countSqlid
	 * @param dataSqlid
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public static Page<HashMap<String, Object>> paginateForExcelByXml(int page,int limit,String countSqlid,String dataSqlid,HashMap<String, Object> parameters) throws Exception{
		boolean isTx = false; //是否存在事务拦截
		SqlSession session = MyBatisSessionManager.getSession();
		if (session!=null) {
			isTx = true;
		}else {
			session = getSqlSession(RapidConsts.getDEFAULT_DBSOURCE_KEY());
		}
		try {
			int start = (page-1)*limit;
			parameters.put("start", start);
			int count = session.selectOne(countSqlid, parameters);
			if (count>=RapidConsts.MAX_EXCEL_ROWS) {
				throw new RapidException("导出excel超出最大行数要求，每次导出应小于"+RapidConsts.MAX_EXCEL_ROWS+"行");
			}
			List<HashMap<String, Object>> datas = session.selectList(dataSqlid, parameters);
			return new Page<HashMap<String,Object>>(datas, page, limit, 0, count);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally{
			if (session!=null) {
				if (!isTx) {
					session.close();
				}
			}			
		}
	}	

}
