package com.qysoft.rapid.actions;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.druid.support.json.JSONUtils;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.qysoft.accountingCloud.platform.entity.FWH;
import com.qysoft.accountingCloud.platform.uitl.*;
import com.qysoft.accountingCloud.platform.services.article.ArticleService;
import com.qysoft.accountingCloud.platform.uitl.Config;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.domain.CurrentRyxx;
import com.qysoft.rapid.exceptions.BizException;

/**s
 * Rapid平台Action类，开发时继承于它
 * @author liugong
 *
 */
public abstract class RapidAction extends Controller {
	
	/**
	 * 获取所有请求中的参数并封装成MAP
	 * @return
	 */
	protected Map<String, Object> getParams() { 
		HttpServletRequest request = getRequest();
		Map<String, Object> params = new HashMap<String, Object>(); 
		for (Enumeration<String> iter = request.getParameterNames(); iter.hasMoreElements();) {
			String paraName = (String) iter.nextElement();
			params.put(paraName, request.getParameter(paraName));
		}
		return params;
	}

	/**
	 * 获取所有请求中的参数并封装成MAP
	 * @return
	 */
	protected Bean getBean() { 
		HttpServletRequest request = getRequest();
		Bean bean = new Bean();
		for (Enumeration<String> iter = request.getParameterNames(); iter.hasMoreElements();) {
			String paraName = (String) iter.nextElement();
			bean.put(paraName, request.getParameter(paraName));
		}
		return bean;
	}
	/**
	 * 清除无效的存库参数（用于公用的save和update）
	 * @param parameters
	 * @param parNames
	 */
	public static void clearPar(HashMap<String, Object> parameters,String... parNames){
		for (String string : parNames) {
			parameters.remove(string);
		}
	}
	protected final void setAttrWxEwmSrc(String wx_key) {
		FWH fwh = Config.fwhMap.get(wx_key);
		String ewm_src;
		if (null == fwh || StrKit.isBlank(fwh.getEwm_src())) {
			ewm_src = "/static/wx_ewm/csthyfw.jpg";
		} else {
			ewm_src = fwh.getEwm_src();
		}
		setAttr("ewm_src", ewm_src);
	}
	protected boolean isHasQxByRank(int _rank, String articleId) throws Exception {
		String referer = getRequest().getHeader("Referer");
		if (!StrKit.isBlank(referer)) {
			referer = URLEncoder.encode(referer, "utf-8");
		}
		String qxPath = getRequest().getContextPath() + "/common/qxtx";

		String openid = getOpenid();
		Bean param = new Bean();
		param.set("openid", openid);
		param.set("wx_key", getWxKey());
		Bean hybd = RapidDao.selectOneByXml("HYBD.queryWxBdyhByWxkeyAndOpenid", param);
//		if (hybd == null) {
////			hybd = RapidDao.selectOneByXml("HYBD.queryUserInfoByOpid", param);
//			hybd = RapidDao.selectOneByXml("HYBD.queryWxBdyhByWxkeyAndOpenid", param);
//			CacheKit.put("wxyhbd", param.getStr("wx_key") + "_" + openid, hybd);
//		}
		boolean hasQx = true;
		if (_rank != 0) {
			// 没有配置权限的不进行拦截
			//System.out.println("------------------");
			//System.out.println("------------------");
			//System.out.println(hybd == null);
			//System.out.println("------------------");
			if (hybd == null) {
				hasQx = false;

				getResponse().sendRedirect(qxPath + "?errorCode=1" + "&wx_key=" + getWxKey() + (StrKit.isBlank(referer) ? "" : ("&backUrl=" + referer)));
				return false;
			} else {

				int gid = hybd.getInt("gid");
				if (gid < _rank) {
					hasQx = false;
					getResponse().sendRedirect(qxPath + "?errorCode=3" + "&wx_key=" + getWxKey() + (StrKit.isBlank(referer) ? "" : ("&backUrl=" + referer)));
					return false;
				}
			}

		}

		ArticleService.updateClick(articleId);
		return true;

	}
	/**
	 * 设置json结果，用于下级拦截器返回客户端
	 * @param object
	 */
	public void setJson(Object object){
		setAttr("result", object);
	}
	
	/**
	 * 获取登陆用户
	 */
	protected CurrentRyxx getCurrentRyxx(){
		CurrentRyxx currentRyxx = getSessionAttr("currentRyxx");
		return currentRyxx;
	}
	
	/**
	 * 绑定当前登录用户到页面
	 */
	protected void bindCurrentRyxxToPage() {
		setAttr("currentRyxx", getCurrentRyxx());
	}
	
	/**
	 * 绑定当前登录用户到bean
	 * @param bean
	 */
	protected void bindCurretRyxxToBean(Bean bean) {
		CurrentRyxx currentRyxx = getCurrentRyxx();
		bean.set("currentRyxx_rydm", currentRyxx.getRydm());
		bean.set("currentRyxx_rymc", currentRyxx.getRymc());
		bean.set("currentRyxx_dh", currentRyxx.getDh());
		bean.set("currentRyxx_ssjgid", currentRyxx.getSsjgid());
		bean.set("currentRyxx_ssjgmc", currentRyxx.getSsjgmc());
		bean.set("currentRyxx_ssgsid", currentRyxx.getSsgsid());
		bean.set("currentRyxx_ssgsmc", currentRyxx.getSsgsmc());
		bean.set("currentRyxx_scsjqx", currentRyxx.getScsjqx());
		bean.set("currentRyxx_xssjqx", currentRyxx.getXssjqx());
	}
	

	/**
	 * 绑定当前登录用户到map
	 * @param parameters
	 */
	protected void bindCurrentRyxxToMap(Map<String, Object> parameters) {
		CurrentRyxx currentRyxx = getCurrentRyxx();
		parameters.put("currentRyxx_rydm", currentRyxx.getRydm());
		parameters.put("currentRyxx_rymc", currentRyxx.getRymc());
		parameters.put("currentRyxx_dh", currentRyxx.getDh());
		parameters.put("currentRyxx_ssjgid", currentRyxx.getSsjgid());
		parameters.put("currentRyxx_ssjgmc", currentRyxx.getSsjgmc());
		parameters.put("currentRyxx_ssgsid", currentRyxx.getSsgsid());
		parameters.put("currentRyxx_ssgsmc", currentRyxx.getSsgsmc());
		parameters.put("currentRyxx_scsjqx", currentRyxx.getScsjqx());
		parameters.put("currentRyxx_xssjqx", currentRyxx.getXssjqx());
		parameters.put("currentRyxx_ssprovince", currentRyxx.getSsprovince());
		parameters.put("currentRyxx_ssarea", currentRyxx.getSsarea());
	}
	protected boolean isHasQxByRank1(int _rank, String articleId,String openid,String wx_key) throws Exception {
		String referer = getRequest().getHeader("Referer");
		if (!StrKit.isBlank(referer)) {
			referer = URLEncoder.encode(referer, "utf-8");
		}
		String qxPath = getRequest().getContextPath() + "/common/qxtx";

		Bean param = new Bean();
		param.set("openid", openid);
		param.set("wx_key", wx_key);
		setAttr("backUrl", referer);
		Bean hybd = RapidDao.selectOneByXml("HYBD.queryWxBdyhByWxkeyAndOpenid", param);
		ArticleService.updateClick(articleId);
		if (_rank != 0) {
			if (hybd == null) {
				setAttr("errorCode", 1);
				return false;
			} else {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				String endtime = hybd.getStr("endtime");
				Date endDate = df.parse(endtime);
				Date newDate = new Date();
				if (endDate.getTime() < newDate.getTime()) {
					setAttr("errorCode", 2);
					return false;
				}
				int gid = hybd.getInt("gid");
				if (gid < _rank) {
					setAttr("errorCode", 3);
					return false;
				}
			}

		}
		return true;
	}



	protected final String getOpenid() {
		String wx_key = getWxKey();
		String openid = getSessionAttr(wx_key + "_openid");
		return openid;
	}

	protected final String getWxKey() {
		String wx_key = getRequest().getParameter("wx_key");
		if (StrKit.isBlank(wx_key)) {
			FWH fwh = getSessionAttr("fwh");
			wx_key = fwh == null ? "csthyfw" : fwh.getKey();
		}
		return wx_key;
	}

	protected final Object getJKMessage(String url) throws Exception {
		return getJKMessage(url, null, null);
	}

	protected final Object getJKMessage(String url, Map<String, Object> requestBodyMap) throws Exception {
		return getJKMessage(url, null, requestBodyMap);
	}

	protected final Object getJKMessage(String url, Map<String, String> queryParas, Map<String, Object> requestBodyMap) throws Exception {
		String data = null;
		if (null != requestBodyMap && !requestBodyMap.isEmpty()) {
			data = DlfwConfig.isEncryptData ? DESUtil.encrypt(JsonKit.toJson(requestBodyMap)) : JsonKit.toJson(requestBodyMap);
		}
		String result = HttpUtil.post(url, queryParas, data);
		Map<String, Object> resultMap = (Map<String, Object>) JSONUtils.parse(result);
		boolean success = (boolean) resultMap.get("success");
		if (!success) {
			throw new BizException("获取数据失败");
		}
		String messageStr = (String) resultMap.get("message");
		messageStr = DlfwConfig.isEncryptData ? DESUtil.decrypt(messageStr) : messageStr;
		Object message = JSONUtils.parse(messageStr);
		return message;
	}
}
