package com.qysoft.accountingCloud.platform.interceptors;

import com.jfinal.aop.Duang;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.qysoft.accountingCloud.platform.services.statistical.KCTJService;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.utils.DateUtil;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Map;


/**拦截器用户统计访问的课程
 * @author mdd
 *
 */
public class StatisticsInterceptor implements Interceptor {
	private static Prop qxConfig = new Prop("config/statistical.properties");
	@Override
	public void intercept(Invocation ai) {
		// TODO Auto-generated method stub
		ai.invoke();
		HttpSession session = ai.getController().getSession();
		Object openid="";
		Object object=null;
		Map mapinfo=null;
		//String actionkey = ai.getActionKey();
		//String modelkey=StringUtils.substringBeforeLast(actionkey,"/"); 
		String actionUrlKey=session.getAttribute("actionUrlKey")==null?"":session.getAttribute("actionUrlKey").toString();
		if(qxConfig.containsKey(actionUrlKey)){
			if(qxConfig.get(actionUrlKey).equals("275")){
				object=ai.getController().getAttr("ts");
				mapinfo=(Map) object;
				mapinfo.put("cid", "275");
				openid=mapinfo.get("openid");
			}else if(qxConfig.get(actionUrlKey).equals("12")||qxConfig.get(actionUrlKey).equals("32")){
               				object=ai.getController().getAttr("videoInfo");
				openid=ai.getController().getAttr("openid");
				mapinfo=(Map) object;
			}
		}
		if(mapinfo!=null){
			Date newdate= DateUtil.getCurrentDate();
			 Bean bean = new Bean();
			 bean.set("openid", openid);
			 bean.set("createtime", newdate);
			 bean.set("kcid", mapinfo.get("id").toString());
			 bean.set("moduleid", mapinfo.get("cid"));
			 bean.set("modulename", getCidName(mapinfo.get("cid")==null?"":mapinfo.get("cid").toString()));
			 bean.set("title", mapinfo.get("title"));
			 bean.set("status", "1");
			 try {
				 // 使用Duang.duang方法在任何地方对目标进行增强 
				 KCTJService service = Duang.duang(KCTJService.class);
				 service.insertStatistical(bean);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		 
	}
	public String getCidName(String cid){
		String a="";
		if(cid.equals("12")){
			a= "直播课程";
		}else if(cid.equals("32")){
			a= "网络课程";
		}else if(cid.equals("275")){
			a= "每日听税";
		}else if(cid.equals("33")){
			a= "试听课程";
		}else{
			a="其他课程";
		}
		return a;
	}
	public static void main(String[] args) {
		Prop qxConfig= PropKit.use("config/statistical.properties");
		String a=qxConfig.get("/hyfw/ts");
		System.out.println("22222222222222222"+a);
		String key="/hyfw/zbkc/zbkcDetail";
		String aa= StringUtils.substringBeforeLast(key,"/");
		System.out.println("--------------"+aa);
	}
}
