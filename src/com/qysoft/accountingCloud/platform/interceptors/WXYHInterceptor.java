package com.qysoft.accountingCloud.platform.interceptors;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.Prop;
import com.jfinal.kit.StrKit;
import com.qysoft.accountingCloud.platform.entity.FWH;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.Date;

/**
 * Created by shenjinxiang on 2017-03-15.
 */
public class WXYHInterceptor implements Interceptor {

    private static Prop qxConfig = new Prop("config/qxConfig.properties");

    @Override
    public void intercept(Invocation invocation) {
        try {
            Controller controller = invocation.getController();
            String actionkey = invocation.getActionKey();
            HttpServletRequest request = invocation.getController().getRequest();
            String referer = request.getHeader("Referer");
            if (!StrKit.isBlank(referer)) {
                referer = URLEncoder.encode(referer, "utf-8");
            }
            String qxPath = request.getContextPath() + "/common/qxtx";

            String openid = request.getParameter("openid");
            // 没有openid
            if (StrKit.isBlank(openid)) {
                // 跳转页面
                controller.getResponse().sendRedirect(qxPath);
            }
            int qx = qxConfig.getInt(actionkey, -1);
            // 没有配置权限的不进行拦截
            if (qx != -1) {
//                controller.getResponse().sendRedirect(qxPath + "?errorCode=2" + "&openid=" + openid + (StrKit.isBlank(referer) ? "" : ("&backUrl=" + referer)));

//                controller.getResponse().sendRedirect(referer);

                FWH fwh = controller.getSessionAttr("fwh");
                String wx_key = fwh == null ? "csthyfw" : fwh.getKey();
                Bean bean = new Bean();
                bean.set("openid", openid);
                bean.set("wx_key", wx_key);
                Bean hybd = RapidDao.selectOneByXml("HYBD.queryUserInfoByOpid", bean);
//                if (null == hybd) {
//                    hybd = RapidDao.selectOneByXml("HYBD.queryUserInfoByOpid", bean);
//                }
                if (hybd == null) {
                    controller.getResponse().sendRedirect(qxPath + "?errorCode=1" + "&wx_key=" + wx_key + (StrKit.isBlank(referer) ? "" : ("&backUrl=" + referer)));
                } else {
                    Date endTime = hybd.getDate("endtime1");
                    if ((new Date()).after(endTime)) {
                        controller.getResponse().sendRedirect(qxPath + "?errorCode=2" + "&wx_key=" + wx_key + (StrKit.isBlank(referer) ? "" : ("&backUrl=" + referer)));

                    } else {
                        int gid = hybd.getInt("gid");
                        if (gid < qx) {
                            controller.getResponse().sendRedirect(qxPath + "?errorCode=3" + "&wx_key=" + wx_key + (StrKit.isBlank(referer) ? "" : ("&backUrl=" + referer)));
                        }
                    }

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        invocation.invoke();
    }

}
