package com.qysoft.accountingCloud.platform.interceptors;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpSession;

public class ActionUrlInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation invocation) {
		// TODO Auto-generated method stub
		invocation.invoke();
		HttpSession session = invocation.getController().getSession();
		String modelkey= StringUtils.substringBeforeLast(invocation.getActionKey(),"/");
		session.setAttribute("actionUrlKey", modelkey);
		
	}

}
