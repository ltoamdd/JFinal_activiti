package com.qysoft.accountingCloud.platform.plugins;

import com.jfinal.plugin.IPlugin;
import com.qysoft.accountingCloud.platform.timer.MrtsCacheTimer;
import com.qysoft.accountingCloud.platform.timer.ZbkcCacheTimer;
import com.qysoft.accountingCloud.platform.uitl.ThreadPool;

/**
 * Created by shenjinxiang on 2018-03-02.
 */
public class DataCachePlugin implements IPlugin {

    @Override
    public boolean start() {
        // 缓存微信绑定信息
        // ThreadPool.getThread().execute(new WxbdCacheTimer());
        // 直播预告列表数据
        ThreadPool.getThread().execute(new ZbkcCacheTimer());
        // 每日听税列表数据
        ThreadPool.getThread().execute(new MrtsCacheTimer());
        // ThreadPool.getThread().execute(new UserCacheTimer());
        return true;
    }

    @Override
    public boolean stop() {
        return true;
    }
}
