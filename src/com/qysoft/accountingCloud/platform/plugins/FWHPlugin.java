package com.qysoft.accountingCloud.platform.plugins;

import com.jfinal.aop.Enhancer;
import com.jfinal.plugin.IPlugin;
import com.qysoft.accountingCloud.platform.entity.FWH;
import com.qysoft.accountingCloud.platform.services.common.CommonService;
import com.qysoft.accountingCloud.platform.uitl.Config;
import com.qysoft.rapid.consts.RapidConsts;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.plugin.mybatis.MyBatisSessionManager;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Created by shenjinxiang on 2017-07-21.
 */
public class FWHPlugin implements IPlugin {

    private final Logger log = Logger.getLogger(FWHPlugin.class);

    @Override
    public boolean start() {
        MyBatisSessionManager.setSession(RapidConsts.getDEFAULT_DBSOURCE_KEY(),true);
        doLog("FWHPlugin：已获取数据库连接...");
        try {
            List<Bean> fwhList = CommonService.queryFwhList();
            for(Bean bean : fwhList) {
                FWH fwh = new FWH();
                fwh.setKey(bean.getStr("key"));
                fwh.setAppid(bean.getStr("appid"));
                fwh.setName(bean.getStr("name"));
                fwh.setAppsecret(bean.getStr("appsecret"));
                fwh.setYsid(bean.getStr("ysid"));
                fwh.setJiekou_token(bean.getStr("jiekou_token"));
                fwh.setEwm_src(bean.getStr("ewm_src"));
                Config.fwhMap.put(fwh.getKey(), fwh);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            MyBatisSessionManager.closeSession();
            doLog("FWHPlugin：数据库 连接已释放...");
        }
    }

    @Override
    public boolean stop() {
        return true;
    }

    private void doLog(String message){
        if (RapidConsts.isIS_DEV_MODE()) {
            log.warn(message);
        }
    }
}
