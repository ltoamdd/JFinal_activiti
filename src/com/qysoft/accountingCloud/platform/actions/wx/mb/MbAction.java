package com.qysoft.accountingCloud.platform.actions.wx.mb;

import com.jfinal.aop.Clear;
import com.jfinal.aop.Before;
import com.qysoft.accountingCloud.platform.services.mb.MbService;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import java.util.List;
import com.qysoft.rapid.domain.Bean;

/**
 * 获取所有模板信息
 */
public class MbAction extends RapidAction {

    /**
     * 接口 http://localhost:8888/csfw/jiekou/mb
     */
    @Before(JsonResultInterceptor.class)
    public void index() throws Exception {
        List<Bean> mb = MbService.selectAllMb();
        System.out.println("-----------------------------------------"+mb);
        setJson(mb);
    }

}