package com.qysoft.accountingCloud.platform.actions.wx.wxyh;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.qysoft.accountingCloud.platform.entity.FWH;
import com.qysoft.accountingCloud.platform.uitl.Config;
import com.qysoft.accountingCloud.platform.uitl.WxyhUtil;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import java.util.List;
import java.util.ArrayList;
import com.alibaba.fastjson.JSONObject;
import com.qysoft.rapid.utils.WxKit;

/**
 * 获取微信所有关注用户OPENID的列表
 */
public class WxyhAction extends RapidAction {

    /**
     * 接口 https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&next_openid=NEXT_OPENID
     */
    @Before(JsonResultInterceptor.class)
    public static List<String> index() {
        FWH fwh = Config.fwhMap.get(Config.DEFAULT_WX_KEY);
        if(fwh != null){
            List<String> openidList = new ArrayList<String>();
            WxKit.getAccessToken(fwh);
            openidList = WxyhUtil.findWeiXinUserList(openidList, fwh.getAccess_token(), null);
            return openidList;
        }
        return null;
    }

}
