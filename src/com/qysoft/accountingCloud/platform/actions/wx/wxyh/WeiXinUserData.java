package com.qysoft.accountingCloud.platform.actions.wx.wxyh;

import java.util.List;

/**
 * 单个openid的WeiXinUserData.java
 */
public class WeiXinUserData {

    private List<String> openid;

    public List<String> getOpenid() {
        return openid;
    }

    public void setOpenid(List<String> openid) {
        this.openid = openid;
    }

}
