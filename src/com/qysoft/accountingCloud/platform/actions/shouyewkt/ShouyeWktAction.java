package com.qysoft.accountingCloud.platform.actions.shouyewkt;

import com.jfinal.aop.Before;
import com.qysoft.accountingCloud.platform.services.hyfw.WLKCService;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;

public class ShouyeWktAction extends RapidAction{
	public void wlkc_XhssIndexPage() {
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        setAttr("openid",openid);
        setAttr("wx_key",wx_key);
        renderJsp("/WEB-INF/pages/shouyewkt/wlkc_xhss_list.jsp");
    }
	@Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public void queryAllWlkc_Xhss() throws  Exception{
        Bean params = getBean();
        String hot_label = params.get("hot_label") == null ? null : String.valueOf(params.get("hot_label"));
        if(hot_label!=null && !hot_label.equals("0") && !hot_label.equals("")){
            String[] hot_labelArray = hot_label.split(",");
            params.set("hot_label",hot_labelArray);

        }else{
            params.remove("hot_label");
        }
        Bean result = WLKCService.queryAllWlkcXhss(params);
        setJson(result);
    }
}
