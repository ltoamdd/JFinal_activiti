package com.qysoft.accountingCloud.platform.actions.common;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;

import com.qysoft.accountingCloud.platform.entity.FWH;
import com.qysoft.accountingCloud.platform.services.common.CommonService;
import com.qysoft.accountingCloud.platform.uitl.Config;
import com.qysoft.accountingCloud.platform.uitl.Decript;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.mybatis.BaseCommonAction;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.exceptions.BizException;
import com.qysoft.rapid.utils.WxKit;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CommonAction extends BaseCommonAction {
	@Override
	public void handlerPar(HashMap<String, Object> parameters) {
		bindCurrentRyxxToMap(parameters);
		parameters.put("wx_key", getWxKey());
	}

	@Override
	public String rendererCellData(String sqlid, String dataIndex,
								   String value) {
		return null;
	}

	/**
	 * 获取收藏点赞信息
	 * @throws Exception
	 */
	@Before({RapidDbConnTx.class})
	public void queryArticleMetaInfo() throws Exception {
		Bean bean = getBean();
		bean.set("wx_key", getWxKey());
		bean.set("openid", getOpenid());
		Bean result = CommonService.queryArticleMetaInfo(bean);
		setJson(result);
	}

	/**
	 * 收藏
	 * @throws Exception
	 */
	@Before({RapidDbConnTx.class})
	public void shCArticle() throws Exception {
		Bean bean = getBean();
		Bean result = CommonService.insertShCInfo(bean);
		setJson(result);
	}

	/**
	 * 取消收藏
	 * @throws Exception
	 */
	@Before({RapidDbConnTx.class})
	public void qxShCArticle() throws Exception {
		Bean bean = getBean();
		Bean result = CommonService.delShCInfo(bean);
		setJson(result);
	}

	/**
	 * 点赞
	 * @throws Exception
	 */
	@Before({RapidDbConnTx.class})
	public void dzArticle() throws Exception {
		Bean bean = getBean();
		Bean result = CommonService.dzArticle(bean);
		setJson(result);
	}

	/**
	 * 取消点赞
	 * @throws Exception
	 */
	@Before({RapidDbConnTx.class})
	public void qxdzArticle() throws Exception {
		Bean bean = getBean();
		Bean result = CommonService.qxdzArticle(bean);
		setJson(result);
	}

	/**
	 * 提交评论信息
	 * @throws Exception
	 */
	@Before({RapidDbConnTx.class})
	public void tjPinglun() throws Exception {
		Bean bean = getBean();
		String ip = getRequest().getRemoteAddr();
		bean.set("ip", ip);
		Bean result = CommonService.insertCommentInfo(bean);
		setJson(result);
	}

	/**
	 * 获取评论列表
	 * @throws Exception
	 */
	@Before({RapidDbConnTx.class})
	public void queryPinglunData() throws Exception {
		Bean bean = getBean();
		Bean result = CommonService.queryCommentPage(bean);
		setJson(result);
	}

	/**
	 * 跳转至搜索页面
	 * @throws Exception
	 */
	@Clear(JsonResultInterceptor.class)
	public void search() throws Exception {
		String openid = getPara("openid");
		String wx_key = getPara("wx_key");
		String search = getPara("search");
		setAttr("openid", openid);
		setAttr("wx_key", wx_key);
		setAttr("search", search);
		renderJsp("/WEB-INF/pages/common/search.jsp");
	}

	/**
	 * 搜索分页
	 * @throws Exception
	 */
	@Before({RapidDbConnTx.class})
	public void searchList() {
		Bean params = getBean();
		params.set("openid", getOpenid());
		Bean result = CommonService.querySearchList(params);
		setJson(result);
	}

	/**
	 * 微信接入action
	 * 用户同意授权，获取code
	 *
	 * 参考文档地址：
	 * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140183&token=&lang=zh_CN （微信网页开发菜单下 微信网页授权）
	 */
	@Clear({JsonResultInterceptor.class})
	public void wxApi() {
//		https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect
		String action = getPara("action");
		String fwhname = getPara("fwhname");
		if (StrKit.isBlank(fwhname)) {
			fwhname = "csthyfw";
		}
		FWH fwh = Config.fwhMap.get(fwhname);
		setSessionAttr("fwh", fwh);
		String appid = fwh.getAppid();
		String redirect_uri = URLEncoder.encode(Config.FILE_HOST + getRequest().getContextPath() + "/common/getCodeInfo?action=" + action);
		String response_type = "code";
		String scope = "snsapi_base";
		String state = "";
		StringBuffer url = new StringBuffer(Config.AUTHORIZE_URL);
		url.append("?appid=").append(appid)
				.append("&redirect_uri=").append(redirect_uri)
				.append("&response_type=").append(response_type)
				.append("&scope=").append(scope)
				.append("&state=12345#wechat_redirect");

		System.out.println(url.toString());
		redirect(url.toString());
	}

	/**
	 * 用户同意授权后，回跳的action
	 * 微信穿过来的参数：
	 * code=CODE 作为换取access_token的票据,每次用户授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期
	 * state=STATE
	 */
	@Clear({JsonResultInterceptor.class})
	public void getCodeInfo() {
		//	https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
		String action = getPara("action");
		FWH fwh = getSessionAttr("fwh");
		String appid = fwh.getAppid();
		String secret = fwh.getAppsecret();

		// 通过code获取access_token和openid信息
		StringBuffer url = new StringBuffer(Config.ACCESS_TOKEN_URL);
		url.append("?appid=").append(appid)
				.append("&secret=").append(secret)
				.append("&code=").append(getPara("code"))
				.append("&grant_type=").append("authorization_code");
		String str = HttpKit.get(url.toString());
		JSONObject js = new JSONObject(str);
		String openid = (String) js.get("openid");
		setSessionAttr("openid", openid);
		redirect("/" + action + "?openid=" + openid);
	}

	/**
	 * 以流的形式返回音频文件给客户端
	 * @throws Exception
	 */
//	@Before(RapidDbConn.class)
	@Clear({JsonResultInterceptor.class})
	public void getFileByPath() throws Exception {
		String path = getPara("path");
		if (StrKit.isBlank(path)) {
			throw new BizException("文件不存在!");
		}
		if (path.startsWith("http://") || path.startsWith("https://")) {
			renderUrlFileIO(path);
		} else if ("0".equals(path)) {
			throw new BizException("文件不存在!");
		} else {
			String filePath = Config.FILE_HOST + path;   //服务器
//			String filePath = Config.FILE_LOCALHOT + path;  //本地

			renderUrlFileIO(filePath);
		}

	}

	private void renderUrlFileIO(String url) throws IOException {
		URL _url = new URL(url);
		HttpURLConnection conn = (HttpURLConnection)_url.openConnection();
		OutputStream output = null;
		InputStream input = null;
		try {
			conn.connect();
			output = getResponse().getOutputStream();
			input = conn.getInputStream();
			byte byte1[]= new byte[1024];
			int length = 0;
			while((length=input.read(byte1))!=-1){
				output.write(byte1,0,length);
			}
			output.flush();

		} catch (Exception var10) {
			throw new RuntimeException(var10);
		} finally {
			if(output!=null){

				output.close();
			}
			if(input!=null){

				input.close();
			}
			if(conn != null) {
				conn.disconnect();
			}

		}
		renderNull();

	}

	@Clear({JsonResultInterceptor.class})
	public void qxtx() {
		setAttr("openid", getOpenid());
		setAttr("wx_key", getWxKey());
		setAttr("errorCode", getPara("errorCode"));
		setAttr("backUrl", getPara("backUrl"));
		renderJsp("/WEB-INF/pages/common/qxtx.jsp");
	}




	/**
	 * 获得微信js签名信息
	 */
	@Before(JsonResultInterceptor.class)
	public void getWxSignature() throws UnsupportedEncodingException {
		String path1 = getPara("path1");
		FWH fwh = getSessionAttr("fwh");
		String url = fwh.getLocal() + getRequest().getContextPath()+path1;
		url = path1;
		System.out.println("url:" + url);
		url = URLDecoder.decode(url, "utf-8");
		System.out.println("url路径="+url);
		WxKit.getJsApiToken(fwh);
		//生成随机字符串
		String noncestr = getRandomString(20);
//		String jsapi_ticket = this.jsapi_ticket;
		String jsapi_ticket = fwh.getJsapi_ticket();
		System.out.println("jsapi_ticket = "+jsapi_ticket);
//		long timestamp = this.jsapi_ticket_timestamp;
		long timestamp = fwh.getJsapi_ticket_timestamp();
		System.out.println("timestamp = "+timestamp);
		String appid = fwh.getAppid();
		StringBuilder sha1Str = new StringBuilder();
		sha1Str.append("jsapi_ticket=").append(jsapi_ticket).append("&noncestr=").append(noncestr).append("&timestamp=").append(timestamp).append("&url=").append(url);
		//String str1="jsapi_ticket=sM4AOVdWfPE4DxkXGEs8VMCPGGVi4C3VM0P37wVUCFvkVAy_90u5h9nbSlYy3-Sl-HhTdfl2fzFy1AOcHKP7qg&noncestr=Wm3WZYTPz0wzccnW&timestamp=1414587457&url=http://mp.weixin.qq.com";
		String signature = Decript.SHA1(sha1Str.toString());
		System.out.println("sha1Str = "+sha1Str.toString());
//        String signature = new SHA1().getDigestOfString(sha1Str.toString().getBytes());
		System.out.println("signature = "+signature);
		Map map = new HashMap();
		map.put("appid", appid);
		map.put("noncestr", noncestr);
		map.put("timestamp", timestamp);
		map.put("signature", signature);
		map.put("jsapi_ticket", jsapi_ticket);
		map.put("url",url);
		System.out.println(map);
		setJson(map);
//        signature=sha1(string1)。 示例：
//•	noncestr=Wm3WZYTPz0wzccnW
//•	jsapi_ticket=sM4AOVdWfPE4DxkXGEs8VMCPGGVi4C3VM0P37wVUCFvkVAy_90u5h9nbSlYy3-Sl-HhTdfl2fzFy1AOcHKP7qg
//•	jsapi_ticket_timestamp=1414587457
//•	url=http://mp.weixin.qq.com
//jsapi_ticket=sM4AOVdWfPE4DxkXGEs8VMCPGGVi4C3VM0P37wVUCFvkVAy_90u5h9nbSlYy3-Sl-HhTdfl2fzFy1AOcHKP7qg&noncestr=Wm3WZYTPz0wzccnW&jsapi_ticket_timestamp=1414587457&url=http://mp.weixin.qq.com
	}

	/**
	 * access_Token
	 */
	/*private void getAccessToken(FWH fwh){
		//https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
		Date nowdate = new Date();
//		FWH fwh = getSessionAttr("fwh");
		System.out.println("access_timestamp已生成 时间戳"+fwh.getAccess_timestamp());
		System.out.println("判断access_token时间戳是否有效 值为"+nowdate.after(new Date(fwh.getAccess_timestamp())));
		if(nowdate.after(new Date(fwh.getAccess_timestamp()))){
			System.out.println("需更新access_token");
			//时间戳失效需要生成token

			String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+fwh.getAppid()+"&secret="+ fwh.getAppsecret();
			System.out.println("access_token="+url);
			String str = HttpKit.get(url.toString());
			JSONObject js = new JSONObject(str);
			System.out.println("tokenjson ="+js.toString());
			if(js.isNull("errcode")&&js.isNull("errmsg")){
				//调用正常
//				this.access_token = js.getString("access_token");
				fwh.setAccess_token(js.getString("access_token"));
				int expires_in = js.getInt("expires_in");
				Calendar cal = Calendar.getInstance();
				System.out.println("cal时间错="+cal.getTimeInMillis());
				cal.add(Calendar.SECOND, expires_in);
				System.out.println("cal时间错2="+cal.getTimeInMillis());
//				access_timestamp = cal.getTimeInMillis();
				fwh.setAccess_timestamp(cal.getTimeInMillis());
				System.out.println("access_timestamp已生成 时间戳"+fwh.getAccess_timestamp());
//				WeiXinConsts.ACCESS_TOKEN = this.access_token;

			}else{
				Integer errcode = (Integer)js.get("errcode") ;
				System.out.println("errcode="+errcode);
				String errmsg = String.valueOf(js.get("errmsg"));
				System.out.println("errcode="+errcode);
				System.out.println("token失效报错信息 errcode = "+errcode);
				System.out.println("token失效报错信息 errmsg = "+errmsg);
			}
		}else{
			System.out.println("不需要更新token");
//			WeiXinConsts.ACCESS_TOKEN = this.access_token;
		}

	}*/

	/**
	 * jsapi_ticket赋值
	 */
	/*private void getJsApiToken(FWH fwh){
		getAccessToken(fwh);
		Date nowdate = new Date();
		System.out.println("jsapitoken已生成 时间戳"+fwh.getJsapi_ticket_timestamp());
		System.out.println("jsapi_ticket_timestamp判断时间戳是否有效 值为"+nowdate.after(new Date(fwh.getJsapi_ticket_timestamp())));
		if(nowdate.after(new Date(fwh.getJsapi_ticket_timestamp()))){
			System.out.println("需更新token");
			//时间戳失效需要生成token

			String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="+fwh.getAccess_token()+"&type=jsapi";
			System.out.println("access_token="+url);
			String str = HttpKit.get(url);
			JSONObject js = new JSONObject(str);
			System.out.println("jsapitokenjson ="+js.toString());
			Integer errcode = (Integer)js.getInt("errcode") ;
			String errmsg = String.valueOf(js.get("errmsg"));
			if(errcode!=null && errcode.equals(0) && errmsg!=null && errmsg.equals("ok")){
				//调用正常
//				jsapi_ticket = js.getString("ticket");
				fwh.setJsapi_ticket(js.getString("ticket"));
				int expires_in = js.getInt("expires_in");
				Calendar cal = Calendar.getInstance();
				System.out.println("cal时间错access_timestamp3="+cal.getTimeInMillis());
				cal.add(Calendar.SECOND, expires_in);
				System.out.println("cal时间错access_timestamp4="+cal.getTimeInMillis());
//				jsapi_ticket_timestamp = cal.getTimeInMillis();
				fwh.setJsapi_ticket_timestamp(cal.getTimeInMillis());
				System.out.println("jsapitoken已生成 时间戳"+fwh.getJsapi_ticket_timestamp());

			}else{
				System.out.println("token失效报错信息 errcode = "+errcode);
				System.out.println("token失效报错信息 errmsg = "+errmsg);
			}
		}else{
			System.out.println("不需要更新token");
		}

	}*/

	public static String getRandomString(int length){
		String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		Random random = new Random();
		StringBuffer sb = new StringBuffer();

		for(int i = 0 ; i < length; ++i){
			int number = random.nextInt(62);//[0,62)

			sb.append(str.charAt(number));
		}
		return sb.toString();
	}

	public static String getRandomString2(int length){
		Random random = new Random();

		StringBuffer sb = new StringBuffer();

		for(int i = 0; i < length; ++i){
			int number = random.nextInt(3);
			long result = 0;

			switch(number){
				case 0:
					result = Math.round(Math.random() * 25 + 65);
					sb.append(String.valueOf((char)result));
					break;
				case 1:
					result = Math.round(Math.random() * 25 + 97);
					sb.append(String.valueOf((char)result));
					break;
				case 2:
					sb.append(String.valueOf(new Random().nextInt(10)));
					break;
			}
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		String sha1Str="jsapi_ticket=sM4AOVdWfPE4DxkXGEs8VPlWmNmwpxFhnmUe2vpbiMgJ4ixZKZspvFY5D4Sp2nnq-LW1afkCLYCIMw7jhiac9Q&noncestr=ujrOXpYcwrcm40z1Ei2k&timestamp=1492239068&url=http://mobile.csfw360.com/csfw_weix/hyfw/ts/tsDetail?id=93fa42af-1e68-11e7-9c79-02004c4f4f50&openid=ox1Ltt68yPAOG-oCa3x_CJO7i-Qk";
		System.out.println(Decript.SHA1(sha1Str.toString()));;
	}


}