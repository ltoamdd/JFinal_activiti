package com.qysoft.accountingCloud.platform.actions.ztxl;

import com.jfinal.aop.Before;
import com.qysoft.accountingCloud.platform.services.ztxl.ZtxlService;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;


public class ZtxlAction extends RapidAction{
	public void index(){
	    String openid=getPara("openid");
        String wx_key=getPara("wx_key");
        setAttr("openid",openid);
        setAttr("wx_key",wx_key);
        renderJsp("/WEB-INF/pages/ztxl/ztxl.jsp");
	}
	@Before({JsonResultInterceptor.class, RapidDbConnTx.class})
	public void queryAllKcb() throws Exception{
		Bean bean=getBean();
		Bean result= ZtxlService.queryAllKcb(bean);
		setJson(result);
	}
	public void kcindex() throws Exception{
        String openid=getPara("openid");
        String wx_key=getPara("wx_key");
        setAttr("openid",openid);
        setAttr("wx_key",wx_key);
        String kcbid=getPara("kcbid");
        setAttr("kcbid",kcbid);
        Bean bean=ZtxlService.queryKcbById(kcbid);
        setAttr("kcbbt",bean.get("kcbbt"));
        setAttr("kcbid",kcbid);
        setAttr("wx_key",getWxKey());
        renderJsp("/WEB-INF/pages/ztxlkc/ztxlkc.jsp");
	}
	@Before({JsonResultInterceptor.class, RapidDbConnTx.class})
	public void queryKcbKc() throws Exception{
		Bean bean=getBean();
		Bean result=ZtxlService.queryKcbKc(bean);
		setJson(result);
	}
}
