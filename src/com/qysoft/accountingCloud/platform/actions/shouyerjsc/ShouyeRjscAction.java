package com.qysoft.accountingCloud.platform.actions.shouyerjsc;

import com.jfinal.aop.Before;
import com.qysoft.accountingCloud.platform.services.hyfw.RjscService;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;

public class ShouyeRjscAction extends RapidAction{
	public void index(){
		String openid =getPara("openid");
		String wx_key =getPara("wx_key");
        setAttr("openid", openid);
        setAttr("wx_key", wx_key);
        renderJsp("/WEB-INF/pages/shouyerjsc/rjsc_list.jsp");
	}
	@Before({JsonResultInterceptor.class, RapidDbConnTx.class})
	public void queryRjsc() throws Exception{
		  Bean params = getBean();
		  params.put("wx_key", getWxKey());
	      Bean result = RjscService.queryAllRjsc(params);
	      setJson(result);
	}
}
