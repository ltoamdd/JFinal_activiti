package com.qysoft.accountingCloud.platform.actions.lbt;

import com.jfinal.aop.Before;
import com.qysoft.accountingCloud.platform.services.lbt.LbtService;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;

import java.util.List;
public class LbtAction extends RapidAction{
	/**
	 * @throws Exception
	 * 轮播图
	 */
	@Before({JsonResultInterceptor.class, RapidDbConnTx.class})
	public void index() throws Exception{
		List<Bean> result= LbtService.queryLbt();
		setJson(result);
	}
}
