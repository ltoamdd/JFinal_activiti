package com.qysoft.accountingCloud.platform.actions.wode;

import com.jfinal.aop.Before;
import com.qysoft.accountingCloud.platform.services.wode.HYBDService;
import com.qysoft.accountingCloud.platform.uitl.ZdfsMbUtil;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;


/**
 * 会员绑定
 * Created by shenjinxiang on 2017-03-13.
 */
public class HYBDAction extends RapidAction {

    /**
     * 跳转至会员绑定页面或解绑页面
     */
    @Before(RapidDbConnTx.class)
    public void index() throws Exception {
        String toUrl = getPara("toUrl");
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        Bean param = getBean();
        Bean hybd = HYBDService.queryHybd(param);
//        if (hybd == null) {
//            hybd = HYBDService.queryHybd(param);
//        }
        setAttr("wx_key", wx_key);
        setAttr("openid", openid);
        if (hybd == null) {
            setAttr("openid", openid);
            setAttr("toUrl", toUrl);
            renderJsp("/WEB-INF/pages/wode/hybd.jsp");
        } else {
            setAttr("hybd", hybd);

            renderJsp("/WEB-INF/pages/wode/hyjb.jsp");
        }
    }

    /**
     * 绑定会员
     * 修改规则：会员卡号和纳税人识别号二选一   yxy    1
     */
    @Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public void hybd() {
        Bean bean = getBean();
        bean.set("ip", getRequest().getRemoteAddr());
        Bean result = new Bean();
        try {
            if ("gh_18a9a687b51a".equals(getWxKey())) {
                result = HYBDService.hybdSgxy(bean);
            } else {
                result = HYBDService.hybd(bean);
            }
            if(result.getStr("msg").equals("绑定成功!")) {
                //用户绑定会员成功推送模板消息
                ZdfsMbUtil.sendMbxx(bean);
            }
            setJson(result);
        } catch (Exception e) {
            e.printStackTrace();
            result.set("result", false);
            result.set("msg", "绑定失败");
            setJson(result);
        }
    }

    /**
     * 解除绑定
     */
    @Before({JsonResultInterceptor.class})
    public void jchbd() {
        Bean bean = getBean();
        try {
            HYBDService.jchbd(bean);
            setJson("解绑成功");
        } catch (Exception e) {
            e.printStackTrace();
            setJson("解绑失败");
        }
    }

    @Before(JsonResultInterceptor.class)
    public void queryHybd() throws Exception {
        Bean result = new Bean();
        Bean param = getBean();
        param.set("openid", getOpenid());
        param.set("wx_key", getWxKey());
        Bean hybd = HYBDService.queryHybd(param);
        if(hybd == null) {
            result.set("result", false);
            result.set("msg", "未绑定会员");
            setJson(result);
        } else {
            result.set("result", true);
            result.set("msg", "已绑定会员");
            result.set("hybd", hybd);
            setJson(result);
        }
    }

}
