package com.qysoft.accountingCloud.platform.actions.hyfw;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.ehcache.CacheKit;
import com.qysoft.accountingCloud.platform.entity.ChatModel;
import com.qysoft.accountingCloud.platform.services.hyfw.ZBKCService;
import com.qysoft.accountingCloud.platform.uitl.Config;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by WangPengfei on 2017/3/9.
 */
public class ZBKCAction extends RapidAction {

    public void index() {
        String openid = getOpenid();
        setAttr("openid", openid);
        setAttr("wx_key", getWxKey());
        renderJsp("/WEB-INF/pages/hyfw/zbkc_list.jsp");
    }

    public void zbkcList() {
        String openid = getOpenid();
        setAttr("openid", openid);
        setAttr("wx_key", getWxKey());
        renderJsp("/WEB-INF/pages/hyfw/zbkc_list.jsp");
    }

    /**
     * 回看
     */
    public void zbkcHkList() {
        String openid = getOpenid();
        setAttr("openid", openid);
        setAttr("wx_key", getWxKey());
        renderJsp("/WEB-INF/pages/hyfw/zbkc_hk_list.jsp");
    }

    @Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public void queryAllZbkc() throws Exception {

        Bean params = getBean();
        params.put("wx_key", getWxKey());
        params.put("openid", getOpenid());
        Bean result = ZBKCService.queryAllZbkc(params);
        setJson(result);
    }

    @Before(JsonResultInterceptor.class)
    public void queryAllZbkcYg() throws Exception {

        Bean params = getBean();
        params.put("wx_key", getWxKey());
        params.put("openid", getOpenid());
        Bean result = ZBKCService.queryAllZbkcYg(params);
        result.set("wx_key", getWxKey());
        setJson(result);
    }

    @Before({RapidDbConnTx.class})
    public void zbkcDetail() throws Exception {
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        setAttr("openid", openid);
        setAttr("wx_key", wx_key);
        String id = getPara("id");
        Bean params = getBean();
        Map map = ZBKCService.findVideoById(params);
        String url = (String) map.get("url");
        // if (!StrKit.isBlank(url) && url.endsWith(",")) {
        // url = url.substring(0, url.length() - 1);
        // }
        map.put("url", url);
        int _rank = (int) map.get("rank");
        setAttrWxEwmSrc(wx_key);

        //互动所需信息
        //		Bean bean = WDTQService.queryWdtq(param);
//        Bean bean = CacheKit.get("wxyhbd", wxKey + "_" + openid);
//        if (null == bean) {
//            bean = RapidDao.selectOneByXml("HYBD.queryWxBdyhByWxkeyAndOpenid", param);
//            CacheKit.put("wxyhbd", wxKey + "_" + openid, bean);
//        }
        Bean bean = RapidDao.selectOneByXml("HYBD.queryWxBdyhByWxkeyAndOpenid", params);
        String userid = "";
        String clientContext = "";
        if (bean != null) {
            setAttr("hasBinded", 0);
            String contacts = bean.getStr("contacts");
            if (contacts != null && !"0".equals(contacts) && !"".equals(contacts)) {
                clientContext = bean.getStr("contacts");
            } else {
                clientContext = bean.getStr("username");
            }

            userid = String.valueOf(bean.getInt("uid"));
        } else {
            setAttr("hasBinded", 1);
            clientContext = "游客";
            userid = UUID.randomUUID().toString();
        }

        String pubkey = Config.PUBKEY;
        String subkey = Config.SUBKEY;

        ChatModel cm = new ChatModel();
        cm.setPubkey(pubkey);
        cm.setSubkey(subkey);
        cm.setClientid(userid);
        cm.setClientContext(clientContext);
        cm.setTopicId(id);

        setAttr("chatModel", cm);

        if (isHasQxByRank(_rank, id)) {
            System.out.println(map.get("url"));
            setAttr("id", id);
            setAttr("videoInfo", map);
            // 增加判断 是否属于章节课程
            Long video_id = (Long) map.get("video_id");
            int count = ZBKCService.queryWlkcZhangjCount(map.get("id"));
            if (video_id != null && video_id != 0) {
                List mlList = ZBKCService.queryWlkcZhangjList(video_id);
                setAttr("kcmlList", mlList);
                renderJsp("/WEB-INF/pages/hyfw/zbkc_detailForMl.jsp");
            } else if (count > 1) {
                // 获取目录列表
                List mlList = ZBKCService.queryWlkcZhangjList(map.get("id"));
                setAttr("kcmlList", mlList);
                renderJsp("/WEB-INF/pages/hyfw/zbkc_detailForMl.jsp");
            } else {
                renderJsp("/WEB-INF/pages/hyfw/zbkc_detail.jsp");
            }

        }
    }

    public void zbkcygDetail() throws Exception {
        String openid =getPara("openid");
        String wx_key = getPara("wx_key");
        setAttr("openid", openid);
        setAttr("wx_key", wx_key);
        String id = getPara("id");
        Bean params = getBean();
        List<Bean> list = CacheKit.get("zbkcData", "list");
        Map map = null;
        for (Bean zbkc : list) {
            if (id.equals(zbkc.get("id") + "")) {
                map = zbkc;
            }
        }
//		Map map = CacheKit.get("zbkcData", id);
        if (null == map) {
            map = ZBKCService.findVideoByIdCache(params);
        }
        String url = (String) map.get("url");
        int _rank = (int) map.get("rank");
        setAttrWxEwmSrc(wx_key);

        //互动所需信息
        Bean param = getBean();
        param.set("openid", openid);
        param.set("wx_key", wx_key);
//		Bean bean = WDTQService.queryWdtq(param);
        Bean bean = RapidDao.selectOneByXml("HYBD.queryWxBdyhByWxkeyAndOpenid", param);
//        if (null == bean) {
//            bean = RapidDao.selectOneByXml("HYBD.queryWxBdyhByWxkeyAndOpenid", param);
//            CacheKit.put("wxyhbd", wxKey + "_" + openid, bean);
//        }
        String userid = "";
        String clientContext = "";
        if (bean != null) {
            setAttr("hasBinded", 0);
            String contacts = bean.getStr("contacts");
            if (contacts != null && !"0".equals(contacts) && !"".equals(contacts)) {
                clientContext = bean.getStr("contacts");
            } else {
                clientContext = bean.getStr("username");
            }

            userid = String.valueOf(bean.getInt("uid"));
        } else {
            setAttr("hasBinded", 1);
            clientContext = "游客";
            userid = UUID.randomUUID().toString();
        }

        String pubkey = Config.PUBKEY;
        String subkey = Config.SUBKEY;

        ChatModel cm = new ChatModel();
        cm.setPubkey(pubkey);
        cm.setSubkey(subkey);
        cm.setClientid(userid);
        cm.setClientContext(clientContext);
        cm.setTopicId(id);

        setAttr("chatModel", cm);

        if (isHasYgQxByRank(_rank, id)) {
            map.put("click", Integer.valueOf(map.get("click") + "") + 1);
            setAttr("id", id);
            setAttr("videoInfo", map);
            renderJsp("/WEB-INF/pages/hyfw/zbkc_detail.jsp");
        }
    }

    protected boolean isHasYgQxByRank(int _rank, String articleId) throws Exception {
        String referer = getRequest().getHeader("Referer");
        if (!StrKit.isBlank(referer)) {
            referer = URLEncoder.encode(referer, "utf-8");
        }
        String qxPath = getRequest().getContextPath() + "/common/qxtx";

        String openid = getOpenid();
        Bean param = new Bean();
        param.set("openid", openid);
        param.set("wx_key", getWxKey());
        Bean hybd = RapidDao.selectOneByXml("HYBD.queryWxBdyhByWxkeyAndOpenid", param);
//        if (hybd == null) {
////			hybd = RapidDao.selectOneByXml("HYBD.queryUserInfoByOpid", param);
//            hybd = RapidDao.selectOneByXml("HYBD.queryWxBdyhByWxkeyAndOpenid", param);
//            CacheKit.put("wxyhbd", param.getStr("wx_key") + "_" + openid, hybd);
//        }
        boolean hasQx = true;
        if (_rank != 0) {
            // 没有配置权限的不进行拦截
            if (hybd == null) {
                hasQx = false;

                getResponse().sendRedirect(qxPath + "?errorCode=1" + "&wx_key=" + getWxKey() + (StrKit.isBlank(referer) ? "" : ("&backUrl=" + referer)));
                return false;
            } else {

                int gid = hybd.getInt("gid");
                if (gid < _rank) {
                    hasQx = false;
                    getResponse().sendRedirect(qxPath + "?errorCode=3" + "&wx_key=" + getWxKey() + (StrKit.isBlank(referer) ? "" : ("&backUrl=" + referer)));
                    return false;
                }
            }

        }

//		ArticleService.updateClick(articleId);
        return true;

    }
}
