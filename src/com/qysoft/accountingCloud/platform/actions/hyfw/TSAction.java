package com.qysoft.accountingCloud.platform.actions.hyfw;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.kit.StrKit;
import com.qysoft.accountingCloud.platform.services.hyfw.TSService;
import com.qysoft.accountingCloud.platform.uitl.Config;
import com.qysoft.accountingCloud.platform.uitl.FileUtil;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConn;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.exceptions.BizException;

import java.io.File;
import java.util.List;

/**
 * 每日听税
 * Created by shenjinxiang on 2017-03-06.
 */
public class TSAction extends RapidAction {

    /**
     * 跳转至听税列表页面
     */
//    @Before(WXYHInterceptor.class)
//	@Clear(OpenidInterceptor.class)
    public void index() {
        String openid = getOpenid();
        setAttr("openid", openid);
        setAttr("wx_key", getWxKey());
        renderJsp("/WEB-INF/pages/hyfw/tsList.jsp");
    }

    /**
     * 每日听税所需的数据
     * @throws Exception
     */
    @Before(JsonResultInterceptor.class)
    public void queryTsDataList() throws Exception {
        Bean params = getBean();
        params.set("openid", getOpenid());
        Bean result = TSService.queryTsDatas(params);
        setJson(result);
    }

    /**
     * 跳转到听税详细页面
     */
    @Before({RapidDbConnTx.class})
    public void tsDetail() throws Exception {
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        Bean bean = TSService.queryTsById(getPara("id"));
        bean.set("wx_key", wx_key);
        TSService.logTsInfo(bean);
        bean.set("openid", openid);
        bean.set("playTimes", bean.getLong("playTimes") + 1);
        setAttr("ts", bean);
        setAttrWxEwmSrc(wx_key);
        String ts_content = bean.getStr("content");
        ts_content = StrKit.isBlank(ts_content) ? "" : ts_content.replace("\n", "");
        ts_content = StrKit.isBlank(ts_content) ? "" : ts_content.replace("\r", "");
        setAttr("ts_content", ts_content);
        setAttr("file_host", Config.FILE_HOST);
        setAttr("wx_key", getWxKey());
        setAttr("openid", getOpenid());
        renderJsp("/WEB-INF/pages/hyfw/tsDetail.jsp");
    }

    /**
     * 以流的形式返回音频文件给客户端
     * @throws Exception
     */
    @Before(RapidDbConn.class)
    public void queryAudioById() throws Exception {
        String id = getPara("id");
        Bean bean = TSService.queryTsById(id);
        if (StrKit.isBlank(bean.getStr("audio_src"))) {
            throw new BizException("音频文件不存在");
        }
        String audioPath = Config.TS_BASE_PATH + File.separator + bean.getStr("audio_src");
        File srcFile = null;
        try {
            srcFile = new File(audioPath);
//        getResponse().setHeader("Content-Type", "audio/x-mpeg");
//        getResponse().setHeader("Accept-Ranges", "bytes");
//        getResponse().setHeader("Content-Length", srcFile.length() + "");
//        getResponse().setHeader("Content-Range", "bytes 0-"+(srcFile.length()-1)+"/"+ srcFile.length() + "");
//        FileUtil.outputFileToResponse(srcFile, getResponse().getOutputStream());
//        renderNull();
            renderFile(srcFile);
        }finally{
            srcFile=null;
        }
    }

    /**
     * 流的形式返回音频对应的图片给客户端
     * @throws Exception
     */
    @Before(RapidDbConn.class)
    public void queryAudioImgById() throws Exception {
        String id = getPara("id");
        Bean bean = TSService.queryTsById(id);
        if (StrKit.isBlank(bean.getStr("image"))) {
            throw new BizException("图片不存在");
        }
        String imgPath = Config.TS_BASE_PATH + File.separator + bean.getStr("image");
        File srcFile = new File(imgPath);
//        FileUtil.outputFileToResponse(srcFile, getResponse().getOutputStream());
//        renderNull();
        renderFile(srcFile);
    }

    @Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public void queryTsSrcsInfo() throws Exception {
        String id = getPara("id");
        String contextPath = getRequest().getServletContext().getRealPath("temp");
        Bean bean = TSService.queryTsSrcsInfo(id, contextPath);
        setJson(bean);
    }

    @Before(RapidDbConn.class)
    public void queryAudioInfo() throws Exception {
        String id = getPara("id");
        Bean bean = TSService.queryTsById(id);
        if (StrKit.isBlank(bean.getStr("audio_src"))) {
            throw new BizException("音频文件不存在");
        }
        String audioPath = Config.TS_BASE_PATH + File.separator + bean.getStr("audio_src");
        File srcFile = null;
        try {
            srcFile = new File(audioPath);
        getResponse().setHeader("Content-Type", "audio/x-mpeg");
        getResponse().setHeader("Accept-Ranges", "bytes");
        getResponse().setHeader("Content-Length", srcFile.length() + "");
        getResponse().setHeader("Content-Range", "bytes 0-"+(srcFile.length()-1)+"/"+ srcFile.length() + "");
        FileUtil.outputFileToResponse(srcFile, getResponse().getOutputStream());
        renderNull();
//            renderFile(srcFile);
        }finally{
            srcFile=null;
        }
    }


    /**
     * 获取听税详细信息
     * @throws Exception
     */
//    @Before({JsonResultInterceptor.class, RapidDbConn.class})
//    public void queryTsInfo() throws Exception {
//        String id = getPara("id");
//        String contextPath = getRequest().getServletContext().getRealPath("temp");
//        Bean bean = TSService.queryTs(id, contextPath);
//        setJson(bean);
//    }

    @Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public void queryTsTabs() throws Exception {
        Bean params = getBean();
        params.set("wx_key", getWxKey());
        List<Bean> list = TSService.queryTsTabs(params);
        setJson(list);
    }

}
