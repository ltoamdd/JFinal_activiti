package com.qysoft.accountingCloud.platform.actions.hyfw;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.kit.JsonKit;
import com.qysoft.accountingCloud.platform.services.hyfw.WLKCService;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by WangPengfei on 2017/3/9.
 */
public class WLKCAction extends RapidAction{


    public void index() throws Exception{
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        setAttr("openid",openid);
        setAttr("wx_key",getWxKey());
        List kindsList = WLKCService.queryWlkcHotLabel_Kinds();
        for(int i=0;i<kindsList.size();i++){
            Map kinds = (Map) kindsList.get(i);
//            kinds.get("name");
            Map params = new HashMap();
            params.put("wx_key", getWxKey());
            params.put("kinds",kinds.get("kindsvalue"));
            List flList = WLKCService.queryWlkcHotLabel(params);
            kinds.put("fllist",flList);
            kinds.put("fllistCount",flList.size());
            int count = flList.size();
            int rows = count/3;
            int rowsmod = count%3;
            if(rowsmod==0){
                count = (rows-1)*3+1;
            }else{
                count = rows*3+1;
            }
            kinds.put("fllistCount",count);
        }
        System.out.println(kindsList);
        System.out.println(JsonKit.toJson(kindsList));
        setAttr("kindsList",kindsList);
        renderJsp("/WEB-INF/pages/hyfw/wlkc_list_new.jsp");
    }
    public void rjsc() throws Exception{
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        setAttr("openid",openid);
        setAttr("wx_key",getWxKey());
        List kindsList = WLKCService.queryWlkcHotLabel_Kinds();
        for(int i=0;i<kindsList.size();i++){
            Map kinds = (Map) kindsList.get(i);
//            kinds.get("name");
            Map params = new HashMap();
            params.put("kinds",kinds.get("kindsvalue"));
            List flList = WLKCService.queryWlkcHotLabel(params);
            kinds.put("fllist",flList);
            kinds.put("fllistCount",flList.size());
            int count = flList.size();
            int rows = count/3;
            int rowsmod = count%3;
            if(rowsmod==0){
                count = (rows-1)*3+1;
            }else{
                count = rows*3+1;
            }
            kinds.put("fllistCount",count);


        }
        System.out.println(kindsList);
        System.out.println(JsonKit.toJson(kindsList));
        setAttr("kindsList",kindsList);
        renderJsp("/WEB-INF/pages/hyfw/rjsc_list.jsp");
    }

    public void test() throws Exception{
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        setAttr("openid",openid);
        setAttr("wx_key",getWxKey());
        List kindsList = WLKCService.queryWlkcHotLabel_Kinds();
        for(int i=0;i<kindsList.size();i++){
            Map kinds = (Map) kindsList.get(i);
//            kinds.get("name");
            Map params = new HashMap();
            params.put("kinds",kinds.get("kindsvalue"));
            List flList = WLKCService.queryWlkcHotLabel(params);
            kinds.put("fllist",flList);
            kinds.put("fllistCount",flList.size());
            int count = flList.size();
            int rows = count/3;
            int rowsmod = count%3;
            if(rowsmod==0){
                count = (rows-1)*3+1;
            }else{
                count = rows*3+1;
            }
            kinds.put("fllistCount",count);


        }
        System.out.println(kindsList);
        System.out.println(JsonKit.toJson(kindsList));
        setAttr("kindsList",kindsList);
        renderJsp("/WEB-INF/pages/hyfw/wlkc_list_new1.jsp");
    }
    @Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public void queryAllWlkc() throws  Exception{
        Bean params = getBean();
        String hot_label = String.valueOf(params.get("hot_label"));
        if(hot_label!=null && !hot_label.equals("0") && !hot_label.equals("")){
            String[] hot_labelArray = hot_label.split(",");
            params.set("hot_label",hot_labelArray);

        }else{
            params.remove("hot_label");
        }

        Bean result = WLKCService.queryAllWlkc(params);
        setJson(result);
    }

    public void wlkc_XhssIndexPage() {
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        setAttr("openid",openid);
        setAttr("wx_key",getWxKey());
        renderJsp("/WEB-INF/pages/hyfw/wlkc_xhss_list.jsp");
    }
    @Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public void queryAllWlkc_Xhss() throws  Exception{

        Bean params = getBean();
        String hot_label = params.get("hot_label") == null ? null : String.valueOf(params.get("hot_label"));
        if(hot_label!=null && !hot_label.equals("0") && !hot_label.equals("")){
            String[] hot_labelArray = hot_label.split(",");
            params.set("hot_label",hot_labelArray);

        }else{
            params.remove("hot_label");
        }
        Bean result = WLKCService.queryAllWlkcXhss(params);
        setJson(result);
    }

    public static void main(String[] args) {
        System.out.println(4/3);
    }

}
