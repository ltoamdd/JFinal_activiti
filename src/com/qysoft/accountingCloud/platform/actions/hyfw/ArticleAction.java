package com.qysoft.accountingCloud.platform.actions.hyfw;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.qysoft.accountingCloud.platform.services.hyfw.TSService;
import com.qysoft.accountingCloud.platform.uitl.Config;
import com.qysoft.accountingCloud.platform.uitl.FileUtil;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConn;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.exceptions.BizException;

import java.io.File;

/**
 * 文章
 * Created by huangwei on 2017-03-06.
 */

public class ArticleAction extends RapidAction {

    /**
     * 跳转至听税列表页面
     */
    public void index() {
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        setAttr("openid",openid);
        setAttr("wx_key",getWxKey());
        renderJsp("/WEB-INF/pages/hyfw/tsList.jsp");
    }

    /**
     * 每日听税所需的数据
     * @throws Exception
     */
    @Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public void queryTsDataList() throws Exception {
        Bean params = getBean();
        Bean result = TSService.queryTsDataList(params);
        setJson(result);
    }

    /**
     * 跳转到听税详细页面
     */
    @Before(RapidDbConn.class)
    public void tsDetail() throws Exception {
        Bean bean = TSService.queryTsById(getPara("id"));
        bean.set("openid", getOpenid());
//        TSService.logTsInfo(bean);
        setAttr("ts", bean);
        setAttr("wx_key", getWxKey());
        renderJsp("/WEB-INF/pages/hyfw/tsDetail.jsp");
    }

    /**
     * 以流的形式返回音频文件给客户端
     * @throws Exception
     */
    @Before(RapidDbConn.class)
    public void queryAudioById() throws Exception {
        String id = getPara("id");
        Bean bean = TSService.queryTsById(id);
        if (StrKit.isBlank(bean.getStr("audio_src"))) {
            throw new BizException("音频文件不存在");
        }
        String audioPath = Config.TS_BASE_PATH + File.separator + bean.getStr("audio_src");
        File srcFile = new File(audioPath);
        FileUtil.outputFileToResponse(srcFile, getResponse().getOutputStream());
        renderNull();
    }

    /**
     * 流的形式返回音频对应的图片给客户端
     * @throws Exception
     */
    @Before(RapidDbConn.class)
    public void queryAudioImgById() throws Exception {
        String id = getPara("id");
        Bean bean = TSService.queryTsById(id);
        if (StrKit.isBlank(bean.getStr("image"))) {
            throw new BizException("图片不存在");
        }
        String imgPath = Config.TS_BASE_PATH + File.separator + bean.getStr("image");
        File srcFile = new File(imgPath);
        FileUtil.outputFileToResponse(srcFile, getResponse().getOutputStream());
        renderNull();
    }


    /**
     * 获取听税详细信息
     * @throws Exception
     */
    @Before({JsonResultInterceptor.class, RapidDbConn.class})
    public void queryTsInfo() throws Exception {
        String id = getPara("id");
        String contextPath = getRequest().getServletContext().getRealPath("temp");
        Bean bean = TSService.queryTs(id, contextPath);
        setJson(bean);
    }

}
