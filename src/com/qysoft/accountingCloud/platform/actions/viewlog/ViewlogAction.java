package com.qysoft.accountingCloud.platform.actions.viewlog;

import com.jfinal.aop.Before;
import com.qysoft.accountingCloud.platform.services.viewlog.ViewlogService;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;

public class ViewlogAction extends RapidAction{
	@Before(RapidDbConnTx.class)
	public void index() throws Exception{
		Bean params=getBean();
		Integer yhdj= ViewlogService.queryYhdjByOpenid(params);
		if(yhdj==null){
			params.put("yhdj", "0");
		}else{
			params.put("yhdj", yhdj.toString());
		}
		ViewlogService.addViewLog(params);
		renderNull();
	}
}
