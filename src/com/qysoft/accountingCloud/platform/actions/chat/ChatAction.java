package com.qysoft.accountingCloud.platform.actions.chat;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.qysoft.accountingCloud.platform.entity.ChatModel;
import com.qysoft.accountingCloud.platform.services.chat.ChatService;
import com.qysoft.accountingCloud.platform.services.wode.WDTQService;
import com.qysoft.accountingCloud.platform.uitl.Config;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.aop.annotations.AutoService;
import com.qysoft.rapid.core.RapidDbConn;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.utils.DateUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by shenjinxiang on 2017-05-03.
 */
public class ChatAction extends RapidAction {

    @AutoService
    private ChatService chatService;

    /**
     * 保存聊天记录
     * @throws Exception
     */
    @Before({ JsonResultInterceptor.class })
    public void saveChat() throws Exception {

        Bean params = getBean();
        Bean result = chatService.insertChat(params);
        setJson(result);
    }



    /**
     * 聊天 历史记录查询
     * sort 排序方式 createTime click
     *
     * @throws Exception
     */
    @Before({ JsonResultInterceptor.class })
    public void queryChatHistory() throws Exception {
        Bean params = getBean();
        Bean result = chatService.queryChatList(params);
        setJson(result);
    }



    /**
     * * 网络课程 互动 html
     * @throws Exception
     *
     */
    @Before(RapidDbConn.class)
    public void wlkcDetailChat() throws Exception {
        String openid = getPara("openid");
        setAttr("openid",openid);
        Bean param = getBean();
        param.set("wx_key", getWxKey());
        Bean bean = WDTQService.queryWdtq(param);
        String userid = "";
        String clientContext ="";
        if (bean != null) {
            String contacts = bean.getStr("contacts");
            if(contacts != null && !"0".equals(contacts) && !"".equals(contacts)){
                clientContext = bean.getStr("contacts");
            }else{
                clientContext = bean.getStr("username");
            }

            userid = String.valueOf(bean.getInt("uid"));
        }

        String pubkey = Config.PUBKEY;
        String subkey = Config.SUBKEY;

        ChatModel cm = new ChatModel();
        cm.setPubkey(pubkey);
        cm.setSubkey(subkey);
        cm.setClientid(userid);
        cm.setClientContext(clientContext);
        cm.setTopicId("zbj1");

        setAttr("chatModel",cm);


        renderJsp("/WEB-INF/pages/chat/chat.jsp");
    }




}
