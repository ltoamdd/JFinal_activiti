package com.qysoft.accountingCloud.platform.actions.shouyezbkc;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.qysoft.accountingCloud.platform.entity.ChatModel;
import com.qysoft.accountingCloud.platform.services.hyfw.ZBKCService;
import com.qysoft.accountingCloud.platform.services.wode.WDTQService;
import com.qysoft.accountingCloud.platform.uitl.Config;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;

import java.util.List;
import java.util.Map;


public class ShouyeZbkcAction extends RapidAction {
//	@Clear(OpenidInterceptor.class)
	public void index() {
		String openid = getPara("openid");
		String wx_key = getPara("wx_key");
		setAttr("openid", openid);
		setAttr("wx_key", wx_key);
		renderJsp("/WEB-INF/pages/shouyezbkc/zbkc_list.jsp");
	}

	public void zbkcList() {
		String openid = getPara("openid");
		String wx_key = getPara("wx_key");
		setAttr("openid", openid);
		setAttr("wx_key", wx_key);
		renderJsp("/WEB-INF/pages/shouyezbkc/zbkc_list.jsp");
	}

	/**
	 * 回看
	 */
	public void zbkcHkList() {
		String openid = getOpenid();
		setAttr("openid", openid);
		setAttr("wx_key", getWxKey());
		renderJsp("/WEB-INF/pages/shouyezbkc/zbkc_hk_list.jsp");
	}

	@Before({ JsonResultInterceptor.class, RapidDbConnTx.class })
	public void queryAllZbkc() throws Exception {
		Bean params = getBean();
		params.put("wx_key", getWxKey());
		params.put("openid", getOpenid());
		Bean result = ZBKCService.queryAllZbkc(params);
		setJson(result);
	}

	public void zbkcDetail() throws Exception {
		String openid = getOpenid();
		setAttr("openid", openid);
		String id = getPara("id");
		setAttr("wx_key", getWxKey());
		Bean params = getBean();
		Map map = ZBKCService.findVideoById(params);
		String url = (String) map.get("url");
		// if (!StrKit.isBlank(url) && url.endsWith(",")) {
		// url = url.substring(0, url.length() - 1);
		// }
		map.put("url", url);
		int _rank = (int) map.get("rank");

        //互动所需信息
        Bean param = getBean();
        param.set("openid", openid);
        param.set("wx_key", getWxKey());
        Bean bean = WDTQService.queryWdtq(param);
        String userid = "";
        String clientContext ="";
        if (bean != null) {
            String contacts = bean.getStr("contacts");
            if(contacts != null && !"0".equals(contacts) && !"".equals(contacts)){
                clientContext = bean.getStr("contacts");
            }else{
                clientContext = bean.getStr("username");
            }

            userid = String.valueOf(bean.getInt("uid"));
        }

        String pubkey = Config.PUBKEY;
        String subkey = Config.SUBKEY;

        ChatModel cm = new ChatModel();
        cm.setPubkey(pubkey);
        cm.setSubkey(subkey);
        cm.setClientid(userid);
        cm.setClientContext(clientContext);
        cm.setTopicId(id);

        setAttr("chatModel",cm);

		if (isHasQxByRank(_rank, id)) {
			System.out.println(map.get("url"));
			setAttr("id", id);
			setAttr("videoInfo", map);
			// 增加判断 是否属于章节课程
			Long video_id = (Long) map.get("video_id");
			int count = ZBKCService.queryWlkcZhangjCount(map.get("id"));
			if (video_id != null && video_id != 0) {
				List mlList = ZBKCService.queryWlkcZhangjList(video_id);
				setAttr("kcmlList", mlList);
				renderJsp("/WEB-INF/pages/shouyezbkc/zbkc_detailForMl.jsp");
			} else if (count > 1) {
				// 获取目录列表
				List mlList = ZBKCService.queryWlkcZhangjList(map.get("id"));
				setAttr("kcmlList", mlList);
				renderJsp("/WEB-INF/pages/shouyezbkc/zbkc_detailForMl.jsp");
			} else {
				renderJsp("/WEB-INF/pages/shouyezbkc/zbkc_detail.jsp");
			}

		}
	}
}
