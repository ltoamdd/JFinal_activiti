package com.qysoft.accountingCloud.platform.actions.cszx;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.qysoft.accountingCloud.platform.services.cszx.CSZXService;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConn;
import com.qysoft.rapid.dao.mybatis.common.CommonDao;
import com.qysoft.rapid.domain.Bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 财税咨询
 * Created by huangwei on 2017/3/20.
 */
public class CszxAction extends RapidAction {

    public void cszxList(){
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        setAttr("openid",openid);
        setAttr("wx_key",wx_key);
        renderJsp("/WEB-INF/pages/cszx/cszx_list.jsp");
    }

    public void cszxListNew(){
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        setAttr("openid",openid);
        setAttr("wx_key",wx_key);
        renderJsp("/WEB-INF/pages/cszx/cszx_list_new.jsp");
    }

    public void cszxListMine() throws Exception {
//        String pid = getPara("pid");
        String openid = getPara("openid");
//        String openid = getOpenid();
        setAttr("openid",openid);
        Map params = new HashMap();
        params.put("openid",openid);
        params.put("wx_key", getWxKey());
        Map userinfo = CommonDao.dao .selectOneByXml("HYBD.queryUserInfoByOpid",params);
        setAttr("userinfo",userinfo);
        setAttr("wx_key",getWxKey());
//        setAttr("ztid",pid);
        renderJsp("/WEB-INF/pages/cszx/cszx_list_mine.jsp");
    }

    public void cszxListRm() throws Exception {
        String pid = getPara("pid");
        String openid = getPara("openid");
//        String openid = getOpenid();
        setAttr("openid",openid);
        Map params = new HashMap();
        params.put("openid",openid);
        params.put("wx_key", getWxKey());
        Map userinfo = CommonDao.dao.selectOneByXml("HYBD.queryUserInfoByOpid",params);
        setAttr("userinfo",userinfo);
        setAttr("wx_key",getWxKey());
//        setAttr("ztid",pid);
        renderJsp("/WEB-INF/pages/cszx/cszx_list_rm.jsp");
    }

    @Before(RapidDbConn.class)
    public void cszxDetailIndex() throws Exception {
        String id = getPara("id");
        String openid = getPara("openid");
        Map params = getParams();
        params.put("id",id);
        Map cszxInfo = CSZXService.queryCszxInfoById(id);
//        Map article = ArticleService.queryList(params);
        List reCszxList = CSZXService.selelctCszxInfoByReplyId(id);
        CSZXService.updateReplyClick(id);
        setAttr("cszxInfo",cszxInfo);
        setAttr("reCszxList",reCszxList);
        setAttr("id",id);
//        String openid = getOpenid();
        setAttr("openid",openid);
        setAttr("wx_key",getWxKey());
        renderJsp("/WEB-INF/pages/cszx/cszx_detail.jsp");
    }

    public void cszxZxzj() throws Exception {
//        String pid = getPara("pid");
        String openid = getPara("openid");
//        String openid = getOpenid();
        setAttr("openid",openid);
        Map params = new HashMap();
        params.put("openid",openid);
        params.put("wx_key", getWxKey());
        Map userinfo = CommonDao.dao.selectOneByXml("HYBD.queryUserInfoByOpid",openid);
        setAttr("userinfo",userinfo);
        setAttr("wx_key",getWxKey());
//        setAttr("ztid",pid);
        renderJsp("/WEB-INF/pages/cszx/cszx_zxzj.jsp");
    }

    @Before(RapidDbConn.class)
    public void cszx_addzxzj() throws Exception{
        String openid = getPara("openid");
//        String openid = getOpenid();
        setAttr("openid",openid);
        setAttr("wx_key",getWxKey());
        String uid = getPara("uid");
        String content = getPara("content");
        if(content!=null&&!content.equals("")){
            Map param = new HashMap();
            param.put("uid",uid);
            param.put("content",content);
            param.put("typename",getPara("typename"));
            param.put("type",getPara("type"));

            CSZXService.saveCszxZxzj(param);

            redirect("/cszx/cszxList?openid="+openid);
        }
        /*Map params = new HashMap();
        params.put("openid",openid);
        Map userinfo = CommonDao.dao.selectOneByXml("HYBD.queryUserInfoByOpid",openid);
        setAttr("userinfo",userinfo);
//        setAttr("ztid",pid);
        renderJsp("/WEB-INF/pages/cszx/cszx_zxzj.jsp");*/
    }

    @Before({JsonResultInterceptor.class, RapidDbConn.class})
    public void addcszxzxzj() {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            Bean bean = getBean();
            String openid = getPara("openid");
//            bean.set("openid", getOpenid());
            bean.set("openid", openid);
            bean.put("wx_key", getWxKey());
            CSZXService.saveCszxZxzj(bean);
            map.put("result", true);
            map.put("msg", "提交成功");
            setJson(map);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", false);
            map.put("msg", "提交失败");
            setJson(map);
        }
    }


}
