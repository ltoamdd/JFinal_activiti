package com.qysoft.accountingCloud.platform.actions.dlfw;

import com.alibaba.druid.support.json.JSONUtils;
import com.jfinal.aop.Before;
import com.jfinal.kit.JsonKit;
import com.qysoft.accountingCloud.platform.uitl.DlfwConfig;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;

import java.util.List;
import java.util.Map;

@Before(JsonResultInterceptor.class)
public class DlfwDmAction extends RapidAction {

    public void queryAllXzqy() throws Exception {
        String url = DlfwConfig.DLFW_BASE_URL + DlfwConfig.DLFW_ALL_XZQY_URL;
        List<Map<String, Object>> xzqyList = (List<Map<String, Object>>) getJKMessage(url);
        setJson(xzqyList);
    }

    public void queryAllZclx() throws Exception {
        String url = DlfwConfig.DLFW_BASE_URL + DlfwConfig.DLFW_ALL_ZCLX_URL;
        List<Map<String, Object>> zclxList = (List<Map<String, Object>>) getJKMessage(url);
        setJson(zclxList);
    }
}
