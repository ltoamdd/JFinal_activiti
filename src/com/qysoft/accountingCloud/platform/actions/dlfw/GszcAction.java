package com.qysoft.accountingCloud.platform.actions.dlfw;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.qysoft.accountingCloud.platform.uitl.DlfwConfig;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.domain.Bean;

@Before(JsonResultInterceptor.class)
public class GszcAction extends RapidAction {

    @Clear(JsonResultInterceptor.class)
    public void index() {
        setAttr("openid", getPara("openid"));
        renderJsp("/WEB-INF/pages/dlfw/gszc.jsp");
    }

    public void addGszc() throws Exception {
        Bean params = getBean();
        params.set("fwlx", "zcgs");
        params.set("fwlxmc", "注册公司");
        params.set("wx_type", "dyh");
        String url = DlfwConfig.DLFW_BASE_URL + DlfwConfig.DLFW_ADD_FANGWEN_QUERY_PHONE_URL;
        String phone = (String) getJKMessage(url, params);
        setJson(phone);
    }

    @Clear(JsonResultInterceptor.class)
    public void xiayibu() {
        setAttr("phone", getPara("phone"));
        renderJsp("/WEB-INF/pages/dlfw/liuchen.jsp");
    }
}
