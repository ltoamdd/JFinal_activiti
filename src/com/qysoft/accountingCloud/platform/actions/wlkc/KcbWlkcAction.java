package com.qysoft.accountingCloud.platform.actions.wlkc;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.kit.JsonKit;
import com.qysoft.accountingCloud.platform.services.hyfw.WLKCService;
import com.qysoft.accountingCloud.platform.services.hyfw.ZBKCService;
import com.qysoft.accountingCloud.platform.services.wlkc.WlkcService;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;

import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KcbWlkcAction extends RapidAction{

    @Before(RapidDbConnTx.class)
	public void index() throws Exception{
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        String dj="";
        setAttr("openid",openid);
        Bean bean= WlkcService.findYhbdByOpenid(openid);
        if(bean!=null){
        	String gid=bean.get("gid")==null?"":bean.get("gid").toString();
        	if(Integer.parseInt(gid)>1){
        		dj=gid;
        		setAttr("dj",gid);
        	}else{
        		setAttr("dj",0);
        		dj="0";
        	}
        }else{
        	dj="0";
        	setAttr("dj",0);
        }
        List kindsList = WLKCService.queryWlkcHotLabel_Kinds();
        for(int i=0;i<kindsList.size();i++){
            Map kinds = (Map) kindsList.get(i);
            Map params = new HashMap();
            params.put("dj", dj);
            params.put("kinds",kinds.get("kindsvalue"));
            params.put("wx_key", wx_key);
            List flList = WLKCService.queryWlkcHotLabel(params);
            kinds.put("fllist",flList);
            kinds.put("fllistCount",flList.size());
            int count = flList.size();
            int rows = count/3;
            int rowsmod = count%3;
            if(rowsmod==0){
                count = (rows-1)*3+1;
            }else{
                count = rows*3+1;
            }
            kinds.put("fllistCount",count);
        }
        System.out.println(kindsList);
        System.out.println(JsonKit.toJson(kindsList));
        setAttr("kindsList",kindsList);
        setAttr("kclx","zxkc");
        setAttr("wx_key",wx_key);
        renderJsp("/WEB-INF/pages/wlkc/wlkc.jsp");
    }
	/**
	 * @throws Exception
	 * 网络课程内容
	 */
    public void wlkcDetail() throws Exception {
        String openid = getPara("openid");
        String wx_key = getPara("wx_key");
        setAttr("openid",openid);
        setAttr("wx_key", wx_key);
        String id = getPara("id");
        Bean params = getBean();
        Map map = ZBKCService.findVideoById(params);
        String url = (String) map.get("url");
        map.put("url", url);
        setAttrWxEwmSrc(wx_key);
        int _rank = (int)map.get("rank");
        isHasQxByRank1(_rank, id,openid,wx_key);
    	System.out.println(map.get("url"));
        setAttr("id", id);
        setAttr("videoInfo",map);
        //增加判断 是否属于章节课程
        Long video_id = (Long)map.get("video_id");
        int count = ZBKCService.queryWlkcZhangjCount(map.get("id"));
        if(video_id!=null && video_id!=0){
            Bean param = new Bean();
            param.set("video_id", video_id);
            param.set("wx_key", wx_key);
            List mlList = ZBKCService.queryWlkcZhangjListWithWxKey(param);
            setAttr("kcmlList",mlList);
            renderJsp("/WEB-INF/pages/ztxlkc/kcbnr.jsp");
        }else if(count>1){
            //获取目录列表
            Bean param = new Bean();
            param.set("video_id", map.get("id"));
            param.set("wx_key", wx_key);
            List mlList = ZBKCService.queryWlkcZhangjListWithWxKey(param);
            setAttr("kcmlList",mlList);
           renderJsp("/WEB-INF/pages/ztxlkc/kcbnr.jsp");
        }else{
        	List mlList = new ArrayList();
        	mlList.add(map);
            setAttr("kcmlList",mlList);
        	renderJsp("/WEB-INF/pages/ztxlkc/kcbnr.jsp");
        }
    }
	@Before({JsonResultInterceptor.class, RapidDbConnTx.class})
	public void queryWlkcXgkc() throws Exception{
		Bean params = getBean();
		Bean result=WlkcService.findWlkcXgkc(params);
	    setJson(result);
	}
}
