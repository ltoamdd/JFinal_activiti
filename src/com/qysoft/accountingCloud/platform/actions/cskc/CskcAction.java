package com.qysoft.accountingCloud.platform.actions.cskc;

import com.jfinal.aop.Before;
import com.qysoft.accountingCloud.platform.services.cskc.CskcService;
import com.qysoft.accountingCloud.platform.services.cskc.KcbTool;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.actions.RapidAction;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.domain.Bean;

public class CskcAction extends RapidAction{
    public void index(){
        String openid =getPara("openid");
        String wx_key=getPara("wx_key");
        setAttr("openid", openid);
        setAttr("wx_key", wx_key);
        renderJsp("/WEB-INF/pages/cskc/cskc.jsp");
    }
    @Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public void findKcbRmkcList() throws Exception{
        Bean params = getBean();
        int[] cids=KcbTool.CIDS;
        params.put("cids", cids);
        Bean result=CskcService.findKcbRmkcList(params);
        setJson(result);
    }
    @Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public void findKcbZxkcList() throws Exception{
        Bean params = getBean();
        int[] cids= KcbTool.CIDS;
        params.put("cids", cids);
        Bean result=CskcService.findKcbZxkcList(params);
        setJson(result);
    }
    @Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public void findKcbMfkcList() throws Exception{
        Bean params = getBean();
        int[] cids=KcbTool.MFKC;
        params.put("cids", cids);
        Bean result= CskcService.findKcbMfkcList(params);
        setJson(result);
    }
}
