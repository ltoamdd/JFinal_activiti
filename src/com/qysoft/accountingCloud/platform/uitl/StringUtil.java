package com.qysoft.accountingCloud.platform.uitl;

import java.io.UnsupportedEncodingException;

/**
 * Created by shenjinxiang on 2017-08-16.
 */
public class StringUtil {

    public static int getLength(String text) throws UnsupportedEncodingException {
        int length = 0;
        for (int i = 0; i < text.length(); i++) {
            if (new String(text.charAt(i) + "").getBytes("utf-8").length > 1) {
                length += 2;
            } else {
                length += 1;
            }
        }
        return length / 2;
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        String text1 = "中华人民共和国";
        String text2 = "张三1阿a";
        System.out.println(getLength(text1));
        System.out.println(getLength(text2));
    }
}
