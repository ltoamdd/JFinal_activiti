package com.qysoft.accountingCloud.platform.uitl;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by shenjinxiang on 2017-08-15.
 */
public class ImgUtil {

    public static void main(String[] args) throws Exception {
        String src = "D:\\work\\Z\\001.jpg";
        String dest = "D:\\work\\Z\\002.jpg";
        drawBottomTitle(src, dest, "中华人民共和国", "宋体", Color.black,  Font.BOLD, 24, 1.0f);

    }

    /**
     * 图片底部添加文本
     * @param srcImgPath 原图片路径
     * @param destImgPath 目标图片路径
     * @param text 添加的文本内容
     * @param fontName 字体
     * @param color 文本颜色
     * @param fontStyle 文本样式
     * @param fontSize 字体大小
     * @param alpha 透明度 0.0-1.0
     */
    public static void drawBottomTitle(String srcImgPath, String destImgPath, String text, String fontName, Color color, int fontStyle, int fontSize, float alpha) {
        try {
            int addHeight = fontSize * 2;
            File srcImg = new File(srcImgPath);
            File destImg = new File(destImgPath);
            Image src = ImageIO.read(srcImg);
            int width = src.getWidth(null);
            int height = src.getHeight(null);
            BufferedImage image = new BufferedImage(width, height + addHeight, BufferedImage.TYPE_INT_BGR);
            Graphics2D g = image.createGraphics();
            g.drawImage(src, 0, 0, width, height, null);
            g.setBackground(Color.WHITE);
            g.clearRect(0, height, width, addHeight);
            g.setColor(color);
            g.setFont(new Font(fontName, fontStyle, fontSize));
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha));
            g.drawString(text, (width - StringUtil.getLength(text) * fontSize) / 2, height + addHeight / 2);
            g.dispose();
            ImageIO.write((BufferedImage)image, "JPEG", destImg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
