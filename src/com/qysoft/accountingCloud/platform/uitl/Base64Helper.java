package com.qysoft.accountingCloud.platform.uitl;



import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;

public class Base64Helper {
	
	private static final String CHARSET = "UTF-8";
	
	public static String encode(String res){
		try{
			Base64 base = new Base64();
			if(res != null && !"".equals(res)){ // 即将解密串不为null并且不为“”
				return new String(base.encode(res.getBytes(CHARSET)));
			}else {
				return "";
			}
		}catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static byte[] encode(byte[] res){
		try{
			Base64 base = new Base64();
			return base.encode(res);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String decode(String str) {
		
		try{
			return new String(new Base64().decode(str.getBytes()),CHARSET);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static byte[] decodeByte(byte[] str) {
		return new Base64().decode(str);
	}
	

	public static byte[] decode(byte[] str) throws UnsupportedEncodingException {
		return new Base64().decode(str);
	}
}
