package com.qysoft.accountingCloud.platform.uitl;

import com.alibaba.fastjson.JSONObject;
import com.qysoft.accountingCloud.platform.actions.wx.mbxx.HttpClient;
import com.qysoft.accountingCloud.platform.actions.wx.wxyh.WeiXinUserList;

import java.util.ArrayList;
import java.util.List;

/**
 * 获取公众号关注用户列表util
 */
public class WxyhUtil {

    /**
     * 获取用户列表
     * @param token
     * @return
     */
    public static List<String> findWeiXinUserList(List<String> openidList, String token, String nextOpenid){
        WeiXinUserList weixinUserList = null;
        String requestUrl= Config.YHLB_URL + token;//小于10000条的用户列表url
        if(null != nextOpenid){
            requestUrl = requestUrl + "&next_openid=" + nextOpenid;//大于10000条的用户列表url
        }
        JSONObject jsonResult = HttpClient.httpsRequest(requestUrl,"GET");
        weixinUserList = JSONObject.toJavaObject(jsonResult,WeiXinUserList.class);
        weixinUserList.getTotal();
        if(weixinUserList.getCount() == 10000){
            openidList.addAll(weixinUserList.getData().getOpenid());
            //如果大于10000的,继续查询
            findWeiXinUserList(openidList, token, weixinUserList.getNext_openid());
        }
        else{
            openidList.addAll(weixinUserList.getData().getOpenid());
        }
        if(jsonResult!=null){
            //System.out.println("获取公众号关注用户列表成功");
        }
        return openidList;
    }

}
