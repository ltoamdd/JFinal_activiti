package com.qysoft.accountingCloud.platform.uitl;

import com.jfinal.kit.StrKit;
import com.qysoft.rapid.exceptions.RapidException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by shenjinxiang on 2018-01-16.
 */
public class HttpUtil {

    private static final String CHARSET = "UTF-8";

    public static String post(String urlPath, String postData) {
        return post(urlPath, null, postData);
    }

    public static String post(String urlPath, Map<String, String> queryParas, String postData) {
        BufferedReader reader = null;
        try {
            urlPath = buildUrlWithQueryString(urlPath, queryParas);
            URL url = new URL(urlPath);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setConnectTimeout(19000);
            conn.setReadTimeout(19000);
            conn.setRequestProperty("Content-Type", "application/json");
            if (StrKit.notBlank(postData)) {
                OutputStream e = conn.getOutputStream();
                e.write(postData.getBytes(CHARSET));
                e.flush();
                e.close();
            }
            conn.connect();
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line = reader.readLine();
            String result;
            if(line == null) {
                result = "";
            } else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(line);
                while((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                result = stringBuilder.toString();
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RapidException("发送数据发生错误：" + e.getMessage());
        } finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e ) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static String buildUrlWithQueryString(String url, Map<String,String> queryParas) throws UnsupportedEncodingException {
        if (queryParas != null && !queryParas.isEmpty()) {
            StringBuilder sb = new StringBuilder(url);
            boolean isFirst = (url.indexOf(63) == -1) ? true : false;

            String key;
            String value;
            for(Iterator iterator = queryParas.entrySet().iterator(); iterator.hasNext(); sb.append(key).append('=').append(value)) {
                Map.Entry<String, String> entry = (Map.Entry)iterator.next();
                if (isFirst) {
                    isFirst = false;
                    sb.append("?");
                } else {
                    sb.append('&');
                }

                key = entry.getKey();
                value = entry.getValue();
                if (StrKit.notBlank(value)) {
                    value = URLEncoder.encode(value, CHARSET);
                }
            }

            return sb.toString();
        } else {
            return url;
        }
    }

}
