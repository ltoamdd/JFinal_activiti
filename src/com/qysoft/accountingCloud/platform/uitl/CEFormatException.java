package com.qysoft.accountingCloud.platform.uitl;

import java.io.IOException;

public class CEFormatException extends IOException
{
	public CEFormatException(String s)
	{
		super(s);
	}
}
