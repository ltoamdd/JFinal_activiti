package com.qysoft.accountingCloud.platform.uitl;

import com.alibaba.fastjson.JSONObject;
import com.qysoft.accountingCloud.platform.actions.wx.mbxx.Template;
import com.qysoft.accountingCloud.platform.services.mb.MbService;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.utils.DateUtil;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自动发送模板消息util
 */
public class ZdfsMbUtil {

    /**
     * 发送绑定成功模板消息
     */
    public static void sendMbxx(Bean bean) throws Exception{
        Bean mb = MbService.selectMbByName("mb1");
        mb.set("keyword1",bean.getStr("username"));//会员卡号
        mb.set("keyword2", DateUtil.getCurrentDateTimeStr());//绑定时间 当前时间
        Template template = new Template();
        Map<String,String> map = new HashMap<>();
        map.put("first",mb.getStr("first")+"\\n");
        map.put("firstcolor",mb.getStr("firstcolor"));
        map.put("keyword1",mb.getStr("keyword1"));
        map.put("keyword1color",mb.getStr("keyword1color"));
        map.put("keyword2",mb.getStr("keyword2"));
        map.put("keyword2color",mb.getStr("keyword2color"));
        map.put("remark","\\n"+mb.getStr("remark"));
        map.put("remarkcolor",mb.getStr("remarkcolor"));
        MbxxtsUtil.xxts(MbxxtsUtil.setTemplate(template,map,mb,bean.getStr("openid")));//设置单个模板内容
    }

    /**
     * 发送会员到期通知模板消息
     */
    public static void sendHydqMbxx(List<Bean> list, int day) throws Exception{
        //Bean hydq = list.get(0);
        //list.removeAll(list);
        //list.add(hydq);
        System.out.println("会员到期openidList"+list.size());
        Template template = new Template();
        Map<String,String> map = new HashMap<>();
        Bean mb = MbService.selectMbByName("mb3");
        map.put("firstcolor",mb.getStr("firstcolor"));
        map.put("keyword1color",mb.getStr("keyword1color"));
        map.put("keyword2color",mb.getStr("keyword2color"));
        map.put("remark","\\n"+mb.getStr("remark"));
        map.put("remarkcolor",mb.getStr("remarkcolor"));
        if(day == 30){
            map.put("first",mb.getStr("first")+"剩余时间"+day+"天!\\n");
        }
        if(day == 7){
            map.put("first",mb.getStr("first")+"剩余时间"+day+"天!\\n");
        }
        if(day == 1){
            map.put("first",mb.getStr("first")+"剩余时间"+day+"天!\\n");
        }
        for(Bean bean :list) {
            map.put("keyword1",bean.getDate("createtime").toString());//会员开通时间
            map.put("keyword2",bean.getDate("endtime").toString());//会员到期时间
            MbxxtsUtil.xxts(MbxxtsUtil.setTemplate(template,map,mb,bean.getStr("openid")));//设置单个模板内容
            //MbxxtsUtil.xxts(MbxxtsUtil.setTemplate(template,map,mb,"otLtU0xwfeUP6I0hL7lgSjs429u8"));//设置单个模板内容
        }
    }

}
