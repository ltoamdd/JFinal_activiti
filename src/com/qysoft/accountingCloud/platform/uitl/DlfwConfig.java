package com.qysoft.accountingCloud.platform.uitl;

import com.jfinal.kit.Prop;

public class DlfwConfig {

    private static final Prop prop = new Prop("config/dlfw.properties");

    public static final boolean isEncryptData;
    public static final String DLFW_BASE_URL;
    public static final String DLFW_ALL_XZQY_URL;
    public static final String DLFW_ALL_ZCLX_URL;
    public static final String DLFW_ADD_FANGWEN_QUERY_PHONE_URL;

    static {
        isEncryptData = getBoolean("isEncryptData");
        DLFW_BASE_URL = getString("dlfw_base_url");
        DLFW_ALL_XZQY_URL = getString("dlfw_all_xzqy_url");
        DLFW_ALL_ZCLX_URL = getString("dlfw_all_zclx_url");
        DLFW_ADD_FANGWEN_QUERY_PHONE_URL = getString("dlfw_add_fangwen_query_phone_url");
    }

    public static final boolean getBoolean(String key) {
        return prop.getBoolean(key, false);
    }

    public static final int getInt(String key) {
        return prop.getInt(key, 0);
    }

    public static final String getString(String key) {
        return prop.get(key);
    }
}
