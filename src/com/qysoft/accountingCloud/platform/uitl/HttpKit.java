package com.qysoft.accountingCloud.platform.uitl;

/**
 * Created by gw on 2018/3/19.
 */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.qysoft.rapid.exceptions.BizException;

import javax.net.ssl.*;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.Map;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

public class HttpKit {
    private static final String GET = "GET";
    private static final String POST = "POST";
    private static final String CHARSET = "UTF-8";
    private static final SSLSocketFactory sslSocketFactory = initSSLSocketFactory();
    private static final TrustAnyHostnameVerifier trustAnyHostnameVerifier = new HttpKit().new TrustAnyHostnameVerifier((TrustAnyHostnameVerifier) null);

    private HttpKit() {
    }

    private static SSLSocketFactory initSSLSocketFactory() {
        try {
            TrustManager[] tm = new TrustManager[]{new HttpKit().new TrustAnyTrustManager((TrustAnyTrustManager) null)};
            SSLContext sslContext = SSLContext.getInstance("TLS", "SunJSSE");
            sslContext.init((KeyManager[]) null, tm, new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (Exception var2) {
            throw new RuntimeException(var2);
        }
    }

    private static HttpURLConnection getHttpConnection(String url, String method, Map<String, String> headers) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyManagementException {
        URL _url = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) _url.openConnection();
        if (conn instanceof HttpsURLConnection) {
            ((HttpsURLConnection) conn).setSSLSocketFactory(sslSocketFactory);
            ((HttpsURLConnection) conn).setHostnameVerifier(trustAnyHostnameVerifier);
        }

        conn.setRequestMethod(method);
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setConnectTimeout(120000);
        conn.setReadTimeout(120000);
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36");
        if (headers != null && !headers.isEmpty()) {
            Iterator var6 = headers.entrySet().iterator();

            while (var6.hasNext()) {
                Map.Entry<String, String> entry = (Map.Entry) var6.next();
                conn.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
            }
        }

        return conn;
    }

    public static String get(String url, Map<String, String> queryParas, Map<String, String> headers) {
        HttpURLConnection conn = null;

        String var6;
        try {
            conn = getHttpConnection(buildUrlWithQueryString(url, queryParas), "GET", headers);
            conn.connect();
            var6 = readResponseString(conn);
        } catch (Exception var9) {
            throw new RuntimeException(var9);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }

        }

        return var6;
    }

    public static String get(String url, Map<String, String> queryParas) {
        return get(url, queryParas, (Map) null);
    }

    public static String get(String url) {
        return get(url, (Map) null, (Map) null);
    }

    public static String post(String url, Map<String, String> queryParas, String data, Map<String, String> headers) {
        HttpURLConnection conn = null;

        String var7;
        try {
            conn = getHttpConnection(buildUrlWithQueryString(url, queryParas), "POST", headers);
            conn.connect();
            OutputStream out = conn.getOutputStream();
            if(data != null) {
                out.write(data.getBytes("UTF-8"));
            }
            out.flush();
            out.close();
            var7 = readResponseString(conn);
        } catch (Exception var10) {
            throw new RuntimeException(var10);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }

        }

        return var7;
    }

    /**
     * 返回图片url
     * @param url
     * @param queryParas
     * @param headers
     * @return
     */
    public static JSONObject post(String url, Map<String, String> queryParas, Map<String, String> headers) {
        HttpURLConnection conn = null;
        JSONObject result = new JSONObject();
        JSONObject json = new JSONObject();
        try {
            conn = getHttpConnection(buildUrlWithQueryString(url, queryParas), "POST", headers);
            conn.connect();
            OutputStream out = conn.getOutputStream();
            out.flush();
            out.close();
            String r = readResponseString(conn);
            System.out.println(r);
            json = JSONObject.parseObject(r);
            if(json.getString("success").equals("true")){
                if(json.getString("message").contains(",")){
                    result = JSONObject.parseObject(json.getString("message"));
                }else {
                    result = json;
                }
            }
        } catch (Exception var10) {
            throw new RuntimeException(var10);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }

        }
        return result;
    }

    public static String post(String url, Map<String, String> queryParas, String data) {
        return post(url, queryParas, data, (Map) null);
    }

    public static String post(String url, String data, Map<String, String> headers) {
        return post(url, (Map) null, data, headers);
    }

    public static String post(String url, String data) {
        return post(url, (Map) null, data, (Map) null);
    }

    private static String readResponseString(HttpURLConnection conn) {
        StringBuilder sb = new StringBuilder();
        InputStream inputStream = null;

        try {
            inputStream = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }

            String var6 = sb.toString();
            return var6;
        } catch (Exception var13) {
            throw new RuntimeException(var13);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException var12) {
                    var12.printStackTrace();
                }
            }

        }
    }

    private static String buildUrlWithQueryString(String url, Map<String, String> queryParas) {
        if (queryParas != null && !queryParas.isEmpty()) {
            StringBuilder sb = new StringBuilder(url);
            boolean isFirst;
            if (url.indexOf("?") == -1) {
                isFirst = true;
                sb.append("?");
            } else {
                isFirst = false;
            }

            String key;
            String value;
            for (Iterator var5 = queryParas.entrySet().iterator(); var5.hasNext(); sb.append(key).append("=").append(value)) {
                Map.Entry<String, String> entry = (Map.Entry) var5.next();
                if (isFirst) {
                    isFirst = false;
                } else {
                    sb.append("&");
                }

                key = (String) entry.getKey();
                value = (String) entry.getValue();
                if (StrKit.notBlank(value)) {
                    try {
                        value = URLEncoder.encode(value, "UTF-8");
                    } catch (UnsupportedEncodingException var9) {
                        throw new RuntimeException(var9);
                    }
                }
            }

            return sb.toString();
        } else {
            return url;
        }
    }

    public static String readIncommingRequestData(HttpServletRequest request) {
        BufferedReader br = null;

        try {
            StringBuilder result = new StringBuilder();
            br = request.getReader();
            String line = null;

            while ((line = br.readLine()) != null) {
                result.append(line).append("\n");
            }

            String var5 = result.toString();
            return var5;
        } catch (IOException var12) {
            throw new RuntimeException(var12);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException var11) {
                    var11.printStackTrace();
                }
            }

        }
    }

    private class TrustAnyHostnameVerifier implements HostnameVerifier {
        private TrustAnyHostnameVerifier(TrustAnyHostnameVerifier trustAnyHostnameVerifier) {
        }

        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    private class TrustAnyTrustManager implements X509TrustManager {
        private TrustAnyTrustManager(TrustAnyTrustManager trustAnyTrustManager) {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }
    }
    public static Map<String, String> getMap(HttpServletRequest request) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader (new InputStreamReader(request.getInputStream(),"UTF-8"));
            StringBuffer stringBuffer = new StringBuffer();
            String line = null;
            while((line = reader.readLine()) != null) {
                stringBuffer.append(line + "\n");
            }
            String text = stringBuffer.toString();
            if (StrKit.isBlank(text)) {
                return null;
            }
            Map map = (Map) JSON.parse(text);
            return map;
        } catch (Exception e) {
            throw new BizException(e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}


