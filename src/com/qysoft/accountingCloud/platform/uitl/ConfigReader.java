package com.qysoft.accountingCloud.platform.uitl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * Created by shenjinxiang on 2017-03-09.
 */
public class ConfigReader extends Properties {

    private static ConfigReader _instance = new ConfigReader();

    private ConfigReader() {
        InputStream inputStream = null;
        try {
            inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("config/config.properties");
            this.load(new InputStreamReader(inputStream, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static ConfigReader getConfigReader() {
        return _instance;
    }

    public String get(String key) {
        return this.getProperty(key, "");
    }
}
