package com.qysoft.accountingCloud.platform.uitl;


import com.qysoft.accountingCloud.platform.entity.FWH;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shenjinxiang on 2017-03-09.
 */
public class Config {

    public static final String TS_BASE_PATH;
    public static final String TPSC_BASE_PATH;
    public static final String TWNTP_BASE_PATH;
    public static final Map<String, FWH> fwhMap;
    public static final String ZDYCDCJ_URL;
    public static final String ZDYCDCJ_CX_URL;
    public static final String TWSC_URL;
    public static final String SC_URL;
    public static final String PIC_URL;
    public static final String SEND_MSG;
    public static final String FILE_HOST;
    public static final String AUTHORIZE_URL;
    public static final String ACCESS_TOKEN_URL;
    public static final String PUBKEY;
    public static final String SUBKEY;
    public static final String DEFAULT_WX_KEY;
    public static final String YHLB_URL;
    public static final String MBXXTS_URL;

    static {
        TS_BASE_PATH = get("ts_base_path");
        TPSC_BASE_PATH = get("tpsc_base_path");
        TWNTP_BASE_PATH = get("twntp_base_path");
        fwhMap = new HashMap();
        FILE_HOST = get("file_host");
        ZDYCDCJ_URL = get("zdycdcj_url");
        ZDYCDCJ_CX_URL = get("zdycdcj_cx_url");
        TWSC_URL = get("twsc_url");
        SC_URL = get("sc_url");
        PIC_URL = get("pic_url");
        SEND_MSG = get("send_msg");
        AUTHORIZE_URL = get("authorize_url");
        ACCESS_TOKEN_URL = get("access_token_url");
        PUBKEY=get("pubKey");
        SUBKEY=get("subKey");
        DEFAULT_WX_KEY=get("default_wx_key");
        YHLB_URL = get("yhlb_url");
        MBXXTS_URL=get("mbxxts_url");
    }
    private static String get(String key) {
        return ConfigReader.getConfigReader().get(key);
    }
}
