package com.qysoft.accountingCloud.platform.uitl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.qysoft.accountingCloud.platform.actions.wx.mbxx.HttpClient;
import com.qysoft.accountingCloud.platform.actions.wx.mbxx.Template;
import com.qysoft.accountingCloud.platform.actions.wx.mbxx.TemplateParam;
import com.qysoft.accountingCloud.platform.actions.wx.wxyh.WxyhAction;
import com.qysoft.accountingCloud.platform.entity.FWH;
import com.qysoft.accountingCloud.platform.services.mb.MbService;
import com.qysoft.accountingCloud.platform.services.message.TextMessageService;
import com.qysoft.accountingCloud.platform.services.qt.QtService;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.exceptions.BizException;
import com.qysoft.rapid.utils.WxKit;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 模板消息推送工具类
 */
public class MbxxtsUtil {

    /**
     * 判断单发接口参数不为空
     */
    public static void checkdf(String[] params,Bean bean) throws Exception{
        for (String param: params) {
            String val = bean.getStr(param);
            if (StrKit.isBlank(val)) {
                throw new BizException("参数：" + param + "不能为空");
            }
        }
        //判断接口参数中mb是否存在  qt是否存在
        if(!MbService.selectMbName().contains(bean.getStr("mb"))){
            throw new BizException("模板：" + bean.getStr("mb") + "不存在");
        }
    }

    /**
     * 判断群发接口参数不为空
     */
    public static void checkqf(String[] params,Bean bean) throws Exception{
        for (String param: params) {
            String val = bean.getStr(param);
            if (StrKit.isBlank(val)) {
                throw new BizException("参数：" + param + "不能为空");
            }
        }
        //判断接口参数中mb是否存在  qt是否存在
        if(!MbService.selectMbName().contains(bean.getStr("mb"))){
            throw new BizException("模板：" + bean.getStr("mb") + "不存在");
        }
        if(!QtService.selectQtName().contains(bean.getStr("qt"))){
            throw new BizException("群体：" + bean.getStr("qt") + "不存在");
        }
    }

    /**
     * 判断模板参数不为空
     */
    public static void checkMbcs(String[] params,Map<String,String> map){
        for (String param: params) {
            String val = map.get(param);
            if (StrKit.isBlank(val)) {
                throw new BizException("参数：" + param + "不能为空");
            }
        }
    }

    /**
     * 获取群体openid
     */
    public static List<String> selectQt(Bean bean,Bean mb) throws Exception{
        List<String> openidList = new ArrayList<String>();
        if("cs".equals(bean.getStr("qt"))){
            openidList.add("ox1Ltt5wxM3g9LPMxMmDavpN1OZQ");
            openidList.add("ox1Ltt2DqwDrGqKjbWLPOEfszKRs");
            openidList.add("ox1Ltt7k5Ah5RBBEWL6xXMHdnKcI");
            openidList.add("ox1Ltt9FPxmKpJrNIaXvKaXj9IFk");
            openidList.add("ox1Ltt87AATmarMyGNDh_g4nDihA");
            openidList.add("otLtU0xwfeUP6I0hL7lgSjs429u8");
        }
        if(bean.getStr("qt").equals("qb")){
            openidList = WxyhAction.index();//公众号所有用户列表
        }
        if(bean.getStr("qt").equals("wbd")){ //未绑定用户列表
            openidList = WxyhAction.index();//公众号所有用户列表
            List<String> pt = QtService.selectQtpthy();//普通绑定会员列表
            List<String> yk = QtService.selectQtykhy();//银卡绑定会员列表
            List<String> jk = QtService.selectQtjkhy();//金卡绑定会员列表
            openidList.removeAll(pt);
            openidList.removeAll(yk);
            openidList.removeAll(jk);
        }
        if(bean.getStr("qt").equals("ptbd")){
            openidList = QtService.selectQtpthy();//普通绑定会员列表
        }
        if(bean.getStr("qt").equals("ykbd")){
            openidList = QtService.selectQtykhy();//银卡绑定会员列表
        }
        if(bean.getStr("qt").equals("jkbd")){
            openidList = QtService.selectQtjkhy();//金卡绑定会员列表
        }
        //查询退订的openid 根据每个模板的退订标识和模板名称
        List<String> mbtd = TextMessageService.selectMbtdByMb(mb);
        if(mbtd != null) {
            openidList.removeAll(mbtd);
        }
        return openidList;
    }


    /*
     * 设置模板内容
     */
    public static Template setTemplate(Template template, Map<String,String> map, Bean bean, String openid){
        template.setTemplateId(bean.getStr("mbid"));//设备模板ID
        template.setToUser(openid);//设置openid
        template.setTopColor("#EB1C27");
        if(null != bean.getStr("url")) {
            template.setUrl(bean.getStr("url"));
        }
        List<TemplateParam> list = new ArrayList<TemplateParam>();
        TemplateParam param1 = new TemplateParam("first", map.get("first"), map.get("firstcolor"));
        list.add(param1);
        TemplateParam param2 = new TemplateParam("keyword1", map.get("keyword1"), map.get("keyword1color"));
        list.add(param2);
        TemplateParam param3 = new TemplateParam("keyword2", map.get("keyword2"), map.get("keyword2color"));
        list.add(param3);
        if (null != map.get("keyword3")) {
            TemplateParam param4 = new TemplateParam("keyword3", map.get("keyword3"), map.get("keyword3color"));
            list.add(param4);
        }
        if (null != map.get("keyword4")) {
            TemplateParam param5 = new TemplateParam("keyword4", map.get("keyword4"), map.get("keyword4color"));
            list.add(param5);
        }
        if (null != map.get("keyword5")) {
            TemplateParam param6 = new TemplateParam("keyword5", map.get("keyword5"), map.get("keyword5color"));
            list.add(param6);
        }
        TemplateParam param7 = new TemplateParam("remark", map.get("remark"), map.get("remarkcolor"));
        list.add(param7);
        template.setTemplateParamList(list);
        return template;
    }

    /**
     * 消息推送
     * @param template
     * @return
     */
    public static JSONObject xxts(Template template){
        FWH fwh = Config.fwhMap.get(Config.DEFAULT_WX_KEY);
        WxKit.getAccessToken(fwh);
        JSONObject jsonResult=sendTemplateMsg(fwh.getAccess_token(),template);
        return jsonResult;
    }

    /**
     * 模板信息发送
     * @param token
     * @param template
     * @return
     */
    public static JSONObject sendTemplateMsg(String token,Template template){
        String requestUrl=Config.MBXXTS_URL + token;
        System.out.println("requestUrl"+requestUrl);
        JSONObject jsonResult= HttpClient.httpsRequest(requestUrl, "POST", template.toJSON());
        System.out.println("jsonResult"+jsonResult);
        if(jsonResult!=null){
            int errorCode=jsonResult.getIntValue("errcode");
            String errorMessage=jsonResult.getString("errmsg");
            if(errorCode==0){
                //System.out.println("模板消息发送成功:"+errorCode+","+errorMessage);
            }else{
                //System.out.println("模板消息发送失败:"+errorCode+","+errorMessage);
            }
        }
        return jsonResult;
    }

    /**
     * body请求体封装为map
     * @param request
     * @return
     */
    public static Map<String, String> getMap(HttpServletRequest request) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader (new InputStreamReader(request.getInputStream(),"UTF-8"));
            StringBuffer stringBuffer = new StringBuffer();
            String line = null;
            while((line = reader.readLine()) != null) {
                stringBuffer.append(line + "\n");
            }
            String text = stringBuffer.toString();
            if (StrKit.isBlank(text)) {
                return null;
            }
            Map map = (Map) JSON.parse(text);
            return map;
        } catch (Exception e) {
            throw new BizException(e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
