package com.qysoft.accountingCloud.platform.uitl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPool {

	private static ExecutorService fixedThreadPool;
	
	private ThreadPool() {}
	
	static {
		fixedThreadPool = Executors.newFixedThreadPool(20);
	}
	
	public static ExecutorService getThread() {
		return fixedThreadPool;
	}
	
	public void down() {
		fixedThreadPool.shutdown();
	}
}
