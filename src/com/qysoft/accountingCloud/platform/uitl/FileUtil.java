package com.qysoft.accountingCloud.platform.uitl;

import org.apkinfo.api.util.AXmlResourceParser;
import org.apkinfo.api.util.TypedValue;
import org.apkinfo.api.util.XmlPullParser;

import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by shenjinxiang on 2017-03-10.
 */
public class FileUtil {

    private static final int BYLENGTH = 1024 * 1024;

    /**
     * 复制文件至指定目录，文件名不变
     * @param srcFile 原文件
     * @param newPath 目标目录
     * @return 新生成的文件
     */
    public static File copyFile(File srcFile, String newPath) {
        if (srcFile == null) {
            throw new RuntimeException("文件不存在");
        }
        if (!srcFile.isFile()) {
            throw new RuntimeException("不是文件");
        }
        File toPFile = new File(newPath);
        if (!toPFile.exists()) {
            toPFile.mkdirs();
        }
        File toFile = new File(toPFile.getAbsolutePath(), srcFile.getName());
        return _copyFile(srcFile, toFile);
    }

    /**
     * 复制文件至指定目录，新生成的文件名为uuid生成
     * @param srcFile 原文件
     * @param newPath 目标目录
     * @return 新生成的文件
     */
    public static File copyFileUUID(File srcFile, String newPath) {
        if (srcFile == null) {
            throw new RuntimeException("文件不存在");
        }
        if (!srcFile.isFile()) {
            throw new RuntimeException("不是文件");
        }
        File toPFile = new File(newPath);
        if (!toPFile.exists()) {
            toPFile.mkdirs();
        }
        File toFile = new File(toPFile.getAbsolutePath(), UUID.randomUUID().toString() + "." + getSuffix(srcFile));
        return _copyFile(srcFile, toFile);
    }

    /**
     * 复制文件至指定目录 文件名不变
     * @param srcPath 原文件 目录
     * @param newPath 目标目录
     * @return 新生成的文件
     */
    public static File copyFile(String srcPath, String newPath) {
        File srcFile = new File(srcPath);
        if (srcFile == null) {
            throw new RuntimeException("文件不存在");
        }
        if (!srcFile.isFile()) {
            throw new RuntimeException("不是文件");
        }
        File toPFile = new File(newPath);
        if (!toPFile.exists()) {
            toPFile.mkdirs();
        }
        File toFile = new File(toPFile.getAbsolutePath(), srcFile.getName());
        return _copyFile(srcFile, toFile);
    }


    /**
     * 复制文件至指定目录 新生成的文件名为uuid生成
     * @param srcPath 原文件 目录
     * @param newPath 目标目录
     * @return 新生成的文件
     */
    public static File copyFileUUID(String srcPath, String newPath) {
        File srcFile = new File(srcPath);
        if (srcFile == null) {
            throw new RuntimeException("文件不存在");
        }
        if (!srcFile.isFile()) {
            throw new RuntimeException("不是文件");
        }
        File toPFile = new File(newPath);
        if (!toPFile.exists()) {
            toPFile.mkdirs();
        }
        File toFile = new File(toPFile.getAbsolutePath(), UUID.randomUUID().toString() + "." + getSuffix(srcFile));
        return _copyFile(srcFile, toFile);
    }

    public static String getSuffix(File file) {
        String fileName = file.getName();
        int dot = fileName.lastIndexOf(".");
        String subffix = "";
        if ((dot > -1) && (dot < (fileName.length() - 1))) {
            subffix = fileName.substring(dot + 1);
        }
        return subffix;
    }

    private static File _copyFile(File srcFile, File toFile) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            inputStream = new FileInputStream(srcFile);
            outputStream = new FileOutputStream(toFile);
            byte[] buffer = new byte[BYLENGTH];
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }
            outputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (outputStream != null ) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (inputStream != null ) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return toFile;
    }

    /**
     * 将一个文件写入到一个输出流中
     * @param file
     * @param outputStream
     * @throws IOException
     */
    public static void outputFileToResponse(File file, OutputStream outputStream) throws IOException {
        InputStream inputStream = null;
        try {

            byte[] buffer = new byte[BYLENGTH];
            int length = 0;
            inputStream = new FileInputStream(file);
            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }
            outputStream.flush();
        } catch (Exception e) {
            throw new RuntimeException("获取文件失败");
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    /**
     * 将一个文件写入到一个输出流中
     * @param path 文件在硬盘上的路径
     * @param outputStream
     * @throws IOException
     */
    public static void outputFileToResponse(String path, OutputStream outputStream) throws IOException {
        InputStream inputStream = null;
        try {
            byte[] buffer = new byte[BYLENGTH];
            int length = 0;
            File file = new File(path);
            inputStream = new FileInputStream(file);
            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }
            outputStream.flush();
        } catch (Exception e) {
            throw new RuntimeException("获取文件失败");
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    public static String getExtName(File file) {
        if (file == null) {
            return "";
        }
        String name = file.getName();
        String ext = name.substring(name.lastIndexOf(".") + 1);
        return ext;
    }

    public static String getUUIDPath(File file) {
        if (file == null) {
            return "";
        }
        String path = file.getAbsolutePath();
        String name = file.getName();
        path = path.replace(name, UUID.randomUUID().toString() + name.substring(name.lastIndexOf(".")));
        return path;
    }


    public static Map<String, Object> readAPK(String apkUrl) {
        ZipFile zipFile = null;
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            zipFile = new ZipFile(apkUrl);
            Enumeration enumeration = zipFile.entries();
            ZipEntry zipEntry = null;
            while(enumeration.hasMoreElements()) {
                zipEntry = (ZipEntry) enumeration.nextElement();
                if (!zipEntry.isDirectory()) {
                    if ("androidmanifest.xml".equals(zipEntry.getName().toLowerCase())) {
                        AXmlResourceParser parser = new AXmlResourceParser();
                        parser.open(zipFile.getInputStream(zipEntry));
                        while (true) {
                            int type = parser.next();
                            if (type == XmlPullParser.END_DOCUMENT) {
                                break;
                            }
                            String name = parser.getName();
                            if (null != name && "manifest".equals(name.toLowerCase())) {
                                for (int i = 0; i != parser.getAttributeCount(); i++) {
                                    if ("versionName".equals(parser.getAttributeName(i))) {
                                        String versionName = getAttributeValue(parser, i);
                                        versionName = (null == versionName) ? "" : versionName;
                                        result.put("versionName", versionName);
                                    } else if ("package".equals(parser.getAttributeName(i))) {
                                        String packageName  = getAttributeValue(parser, i);
                                        packageName  = (null == packageName ) ? "" : packageName ;
                                        result.put("package", packageName );
                                    } else if ("versionCode".equals(parser.getAttributeName(i))) {
                                        String versionCode = getAttributeValue(parser, i);
                                        versionCode = (null == versionCode) ? "" : versionCode;
                                        result.put("versionCode", versionCode);
                                    }
                                }
                                if (checkApkResult(result, "versionName")
                                        && checkApkResult(result, "package")
                                        && checkApkResult(result, "versionCode")) {
                                    result.put("result", true);
                                } else {
                                    result.put("result", false);
                                    result.put("error", "读取apk文件失败");
                                }
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            result.put("result", false);
            result.put("error", "读取apk文件失败");
        } finally {
            if (null != zipFile) {
                try {
                    zipFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    private static boolean checkApkResult(Map<String, Object> result, String key) {
        if (null == result.get(key)) {
             return false;
        }
        if ("".equals(String.valueOf(result.get("key")))) {
            return false;
        }
        return true;
    }

    private static String getAttributeValue(AXmlResourceParser parser, int index) {
        int type = parser.getAttributeValueType(index);
        int data = parser.getAttributeValueData(index);
        if (type == TypedValue.TYPE_STRING) {
            return parser.getAttributeValue(index);
        }
        if (type == TypedValue.TYPE_ATTRIBUTE) {
            return String.format("?%s%08X", getPackage(data), data);
        }
        if (type == TypedValue.TYPE_REFERENCE) {
            return String.format("@%s%08X", getPackage(data), data);
        }
        if (type == TypedValue.TYPE_FLOAT) {
            return String.valueOf(Float.intBitsToFloat(data));
        }
        if (type == TypedValue.TYPE_INT_HEX) {
            return String.format("0x%08X", data);
        }
        if (type == TypedValue.TYPE_INT_BOOLEAN) {
            return data != 0 ? "true" : "false";
        }
        if (type == TypedValue.TYPE_DIMENSION) {
            return Float.toString(complexToFloat(data)) + DIMENSION_UNITS[data & TypedValue.COMPLEX_UNIT_MASK];
        }
        if (type == TypedValue.TYPE_FRACTION) {
            return Float.toString(complexToFloat(data)) + FRACTION_UNITS[data & TypedValue.COMPLEX_UNIT_MASK];
        }
        if (type >= TypedValue.TYPE_FIRST_COLOR_INT && type <= TypedValue.TYPE_LAST_COLOR_INT) {
            return String.format("#%08X", data);
        }
        if (type >= TypedValue.TYPE_FIRST_INT && type <= TypedValue.TYPE_LAST_INT) {
            return String.valueOf(data);
        }
        return String.format("<0x%X, type 0x%02X>", data, type);
    }

    private static String getPackage(int id) {
        if (id >>> 24 == 1) {
            return "android:";
        }
        return "";
    }

    public static float complexToFloat(int complex) {
        return (float) (complex & 0xFFFFFF00) * RADIX_MULTS[(complex >> 4) & 3];
    }

    private static final float RADIX_MULTS[] =
            {
                    0.00390625F, 3.051758E-005F,
                    1.192093E-007F, 4.656613E-010F
            };
    private static final String DIMENSION_UNITS[] = { "px", "dip", "sp", "pt", "in", "mm", "", "" };
    private static final String FRACTION_UNITS[] = { "%", "%p", "", "", "", "", "", "" };

    public static void main(String[] args) {
        String url = "D:\\work\\财税服务网\\app-release.apk";
        Map<String, Object> result = readAPK(url);
        System.out.println(result);
    }

}
