package com.qysoft.accountingCloud.platform.uitl;

import com.jfinal.kit.StrKit;
import com.qysoft.accountingCloud.platform.entity.FWH;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.utils.WxKit;

import java.util.HashMap;
import java.util.Map;

/**
 * 素材util
 */
public class ScUtil {

    /**
     * 上传图文素材
     * @param params
     */
    public static String scTwsc(Bean params){
        FWH fwh = Config.fwhMap.get(params.getStr("wx_key"));
        WxKit.getAccessToken(fwh);
        String url = Config.TWSC_URL + fwh.getAccess_token();//+ TokenUtil.getAccessToken(params.getStr("wx_key"));
        StringBuffer buffer = new StringBuffer();
        buffer.append("{\"articles\":[{");
        if(!StrKit.isBlank(params.getStr("title"))) {
            buffer.append(String.format("\"title\":\"%s\"", params.getStr("title"))).append(",");
            buffer.append(String.format("\"thumb_media_id\":\"%s\"", params.getStr("tp_media_id"))).append(",");
            buffer.append(String.format("\"author\":\"%s\"", params.getStr("author"))).append(",");
            buffer.append(String.format("\"digest\":\"%s\"", "")).append(",");
            buffer.append(String.format("\"show_cover_pic\":\"%s\"", params.getStr("show_cover_pic"))).append(",");
            buffer.append(String.format("\"content\":\"%s\"", params.getStr("content"))).append(",");
            buffer.append(String.format("\"content_source_url\":\"%s\"", params.getStr("content_source_url")));
        }
        buffer.append("}]}");
        System.out.println(buffer.toString());
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        return HttpKit.post(url,null,buffer.toString(),headers);
    }

}
