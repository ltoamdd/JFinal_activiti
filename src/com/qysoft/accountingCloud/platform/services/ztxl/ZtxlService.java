package com.qysoft.accountingCloud.platform.services.ztxl;


import java.util.List;
import java.util.Map;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

public class ZtxlService {
	public static Bean queryAllKcb(Map<String,Object> parms) throws Exception{
		Bean result = new Bean();
        List<Map<String,Object>> list =  RapidDao.selectListByXml("ZTXL.queryAllKcb",parms);
        Integer count=RapidDao.selectOneByXml("ZTXL.queryKcbCount",parms);
        result.put("list",list);
        result.put("total", count);
        return result;
	}
	//课程包下面课程查询
	public static Bean queryKcbKc(Map<String,Object> parms) throws Exception{
		Bean result = new Bean();
        List<Map<String,Object>> list =  RapidDao.selectListByXml("ZTXL.queryAllKcbKc",parms);
        Integer count= RapidDao.selectOneByXml("ZTXL.queryKcbKcCount",parms);
        result.put("list",list);
        result.put("total", count);
        return result;
	}
	public static List<Map<String,Object>> queryKcbKcNoPage(Map<String,Object> parms) throws Exception{
        List<Map<String,Object>> list =  RapidDao.selectListByXml("ZTXL.queryAllKcbKcNoPage",parms);
        return list;
	}
	public static Bean queryKcbById(String kcbid) throws Exception{
		Bean bean=RapidDao.selectOneByXml("ZTXL.queryKcbById", kcbid);
		return bean;
	}
 	 
}
