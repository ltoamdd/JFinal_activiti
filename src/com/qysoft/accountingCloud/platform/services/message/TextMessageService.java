package com.qysoft.accountingCloud.platform.services.message;


import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.List;
import java.util.Map;

/**
 * 接收文本消息Service
 */
public class TextMessageService {

    /**
     * 保存文本消息
     * @param param
     * @throws Exception
     */
    public static void saveTextMessage(Map<String, Object> param, String mbName) throws Exception {
        System.out.println("文本消息退订某一个模板消息");
        Bean textMessageBean= new Bean();
        textMessageBean.set("wx_key", param.get("ToUserName"));
        textMessageBean.set("openid", param.get("FromUserName"));
        textMessageBean.set("createTime", param.get("CreateTime"));
        textMessageBean.set("msg_type", param.get("MsgType"));
        textMessageBean.set("content", param.get("Content"));
        textMessageBean.set("msgid", param.get("MsgId"));
        textMessageBean.set("mbname", mbName);
        RapidDao.insertByXml("MESSAGES.insertText",textMessageBean);
    }

    /**
     * 删除退订openid  重新订阅模板消息
     * @param param
     * @throws Exception
     */
    public static void deleteTextMessage(Map<String, Object> param, String mbName) throws Exception {
        System.out.println("文本消息重新订阅某一个模板消息");
        Bean textMessageBean= new Bean();
        textMessageBean.set("wx_key", param.get("ToUserName"));
        textMessageBean.set("openid", param.get("FromUserName"));
        textMessageBean.set("createTime", param.get("CreateTime"));
        textMessageBean.set("msg_type", param.get("MsgType"));
        textMessageBean.set("content", param.get("Content"));
        textMessageBean.set("msgid", param.get("MsgId"));
        textMessageBean.set("mbname", mbName);
        //RapidDao.insertByXml("MESSAGES.insertText",textMessageBean);
        RapidDao.deleteByXml("MESSAGES.deleteText",textMessageBean);
    }

    /**
     * 根据模板查询退订文本消息
     * @throws Exception
     */
    public static List<String> selectMbtdByMb(Bean mb) throws Exception {
        List<String> tdlist = RapidDao.selectListByXml("MESSAGES.selectMbtdByMb",mb);
        return tdlist;
    }

}
