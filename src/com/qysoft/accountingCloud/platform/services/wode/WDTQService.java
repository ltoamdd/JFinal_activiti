package com.qysoft.accountingCloud.platform.services.wode;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

/**
 * Created by WangPengfei on 2017/3/17.
 */
public class WDTQService {

    public static Bean queryWdtq(Bean params) throws Exception{
        return RapidDao.selectOneByXml("HYBD.queryUserInfoByOpid",params);
    }
}
