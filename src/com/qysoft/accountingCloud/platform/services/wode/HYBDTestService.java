package com.qysoft.accountingCloud.platform.services.wode;

import com.jfinal.kit.StrKit;
import com.qysoft.accountingCloud.platform.uitl.MD5Util;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by shenjinxiang on 2018-03-08.
 */
public class HYBDTestService {

    public static Bean hybd(Bean bean) throws Exception {
        Bean result = new Bean();
        bean.set("password", MD5Util.MD5(bean.getStr("password")));
//        String nsrsbh = bean.getStr("nsrsbh");
        //纳税人识别号或会员卡号
        String username = bean.getStr("username");
        if (StrKit.isBlank(username)) {
            result.set("result", false);
            result.set("msg", "纳税人识别号或卡号或手机号不能为空!");
            return result;
        }

        List<Bean> userList = RapidDao.selectListByXml("HYBDTEST.queryUserByUsernamOrTaxpayer1", bean);

        if (userList.size() <= 0) {
            result.set("result", false);
            result.set("msg", "绑定失败，您输入的信息有误!");
            return result;
        }
        // 验证纳税人识别号
        Iterator<Bean> it = userList.iterator();

        // 验证用户来源类型
        it = userList.iterator();
        while (it.hasNext()) {
            Bean user = it.next();
            int type = user.getInt("type");
            if (type == 7) {
                it.remove();
            }
        }
        if (userList.size() <= 0) {
            result.set("result", false);
            result.set("msg", "绑定失败，您输入的信息有误!");
            return result;
        }


        // 验证endTime
        it = userList.iterator();
        Date currentDate = new Date();
        while(it.hasNext()) {
            Bean user = it.next();
            Date endtime = user.getDate("endtime");
            if (endtime == null || endtime.before(currentDate)) {
                it.remove();
            }
        }
        if (userList.size() <= 0) {
            result.set("result", false);
            result.set("msg", "绑定失败，会员服务时间已到期!");
            return result;
        }

        // 验证status
        it = userList.iterator();
        while(it.hasNext()) {
            Bean user = it.next();
            boolean status = user.getBoolean("status");
            if (!status) {
                it.remove();
            }
        }
        if (userList.size() <= 0) {
            result.set("result", false);
            result.set("msg", "绑定失败，被禁止使用!");
            return result;
        }

//        Bean oldHy = new Bean();
//        oldHy.set("openid", bean.getStr("openid"));
//        oldHy.set("yxbz", 0);
        // 将该openid绑定的记录都改成无效的
//        RapidDao.updateByXml("HYBD.updateHybdYxbz", oldHy);

        Bean hybd = new Bean();
        int uid = userList.get(0).getInt("userid");
        hybd.set("uid", uid);
        hybd.set("openid", bean.getStr("openid"));
        hybd.set("taxpayer", userList.get(0).getStr("taxpayer"));
        hybd.set("yxbz", 1);
        hybd.set("wx_key", bean.getStr("wx_key"));
        // 判断username是否被绑定
        int count = RapidDao.selectOneByXml("HYBDTEST.queryIsHaveBind", hybd);
        if(count>0){
            result.set("result", false);
            result.set("msg", "绑定失败，纳税人识别号或会员卡号已被其他用户绑定!");
            return result;
        }

        // wx_yhbd中用户id为当前uid的状态都设置为无效
        RapidDao.updateByXml("HYBDTEST.updateWxYhbdStatusForZeroByUserId", hybd);
        // 新增一条绑定记录
        RapidDao.insertByXml("HYBDTEST.insertHybd", hybd);

        // 验证用户激活状态，如未激活则激活用户，设定创建结束时间 记录日志
        Bean user = RapidDao.selectOneByXml("HYBDTEST.queryUserById", uid);
        boolean activate = user.getBoolean("activate");
        if (!activate) {
            RapidDao.updateByXml("HYBDTEST.updateUserActivate", user);
            Bean log = new Bean();
            log.set("uid", user.get("id"));
            log.set("username", user.get("username"));
            log.set("ip", bean.get("ip"));
            log.set("type", 2);
            log.set("yys_id", user.get("yys_id"));
            RapidDao.insertByXml("HYBDTEST.insertUserActivateLog", log);
        }


        // 更新对应的缓存信息
//        Bean wxyh = RapidDao.selectOneByXml("HYBDTEST.queryWxBdyhByWxkeyAndOpenid", hybd);
//        CacheKit.put("wxyhbd", hybd.getStr("wx_key") + "_" + hybd.getStr("openid"), wxyh);


        result.set("result", true);
        result.set("msg", "绑定成功!");
        return result;
    }

    /**
     * 税干学苑会员绑定
     * @param bean
     * @return
     * @throws Exception
     */
    public static Bean hybdSgxy(Bean bean) throws Exception {
        Bean result = new Bean();
        bean.set("password", MD5Util.MD5(bean.getStr("password")));

        String username = bean.getStr("username");
        if (StrKit.isBlank(username)) {
            result.set("result", false);
            result.set("msg", "账号不能为空!");
            return result;
        }

        List<Bean> userList = RapidDao.selectListByXml("HYBDTEST.queryUserByUsernamForSgxy", bean);

        if (userList.size() <= 0) {
            result.set("result", false);
            result.set("msg", "绑定失败，您输入的信息有误!");
            return result;
        }
        Iterator<Bean> it = userList.iterator();

        it = userList.iterator();
        Date currentDate = new Date();
        while(it.hasNext()) {
            Bean user = it.next();
            Date endtime = user.getDate("endtime");
            if (endtime == null || endtime.before(currentDate)) {
                it.remove();
            }
        }
        if (userList.size() <= 0) {
            result.set("result", false);
            result.set("msg", "绑定失败，服务时间已到期!");
            return result;
        }

        // 验证status
        it = userList.iterator();
        while(it.hasNext()) {
            Bean user = it.next();
            boolean status = user.getBoolean("status");
            if (!status) {
                it.remove();
            }
        }
        if (userList.size() <= 0) {
            result.set("result", false);
            result.set("msg", "绑定失败，被禁止使用!");
            return result;
        }
        Bean hybd = new Bean();
        int uid = userList.get(0).getInt("userid");
        hybd.set("uid", uid);
        hybd.set("openid", bean.getStr("openid"));
        hybd.set("taxpayer", userList.get(0).getStr("taxpayer"));
        hybd.set("yxbz", 1);
        hybd.set("wx_key", bean.getStr("wx_key"));
        // 判断username是否被绑定
        int count = RapidDao.selectOneByXml("HYBDTEST.queryIsHaveBind", hybd);
        if(count>0){
            result.set("result", false);
            result.set("msg", "绑定失败，纳税人识别号或会员卡号已被其他用户绑定!");
            return result;
        }
        // wx_yhbd中用户id为当前uid的状态都设置为无效
        RapidDao.updateByXml("HYBDTEST.updateWxYhbdStatusForZeroByUserId", hybd);
        // 新增一条绑定记录
        RapidDao.insertByXml("HYBDTEST.insertHybd", hybd);

        // 更新对应的缓存信息
//        Bean wxyh = RapidDao.selectOneByXml("HYBDTEST.queryWxBdyhByWxkeyAndOpenid", hybd);
//        CacheKit.put("wxyhbd", hybd.getStr("wx_key") + "_" + hybd.getStr("openid"), wxyh);

        result.set("result", true);
        result.set("msg", "绑定成功!");
        return result;
    }
}
