package com.qysoft.accountingCloud.platform.services.wode;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.List;

/**
 * Created by shenjinxiang on 2017-04-13.
 */
public class GJXService {

    public static Bean queryClassifInfo(String id) throws Exception {
        return RapidDao.selectOneByXml("GJX.queryClassifInfo", id);
    }

    public static Bean queryGjDataPage(Bean params) throws Exception {
        int pageNum = Integer.parseInt(params.getStr("pageNum"));
        int pageSize = Integer.parseInt(params.getStr("pageSize"));
        pageNum = (pageNum <= 1) ? 1 : pageNum;

        int total = RapidDao.selectOneByXml("GJX.queryGjPageCount", params);
        int totalPage = (total % pageSize == 0) ? (total / pageSize) : ((total / pageSize) + 1);

        params.set("start", (pageNum - 1) * pageSize);
        List<Bean> list = RapidDao.selectListByXml("GJX.queryGjPageList", params);

        Bean result = new Bean();
        result.set("pageNum", pageNum);
        result.set("pageSize", pageSize);
        result.set("total", total);
        result.set("data", list);
        result.set("totalPage", totalPage);
        result.set("result", true);
        return result;
    }
}
