package com.qysoft.accountingCloud.platform.services.article;

import com.qysoft.rapid.dao.mybatis.RapidDao;

import java.util.Map;

/**
 * Created by shenjinxiang on 2017-03-09.
 */
public class ArticleService {

    /**
     * 获取听税列表数据
     * 返回结果包括 count 总播放次数 total 列表条数 data 获取到的分页数据
     * @param params
     * @return
     * @throws Exception
     */
    public static Map queryOne(Map params) throws Exception {

        Map map = RapidDao.selectOneByXml("ARTICLE.findByArticle", params);

        return map;
    }


    public static void updateClick(String articleId) throws Exception {
        RapidDao.updateByXml("ARTICLE.updateClick", articleId);
    }
}
