package com.qysoft.accountingCloud.platform.services.cszx;

import com.jfinal.kit.StrKit;
import com.qysoft.accountingCloud.platform.uitl.Config;
import com.qysoft.accountingCloud.platform.uitl.FileUtil;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.dao.mybatis.common.CommonDao;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.exceptions.BizException;
import com.qysoft.rapid.utils.DateUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by huangwei on 2017-03-09.
 */
public class CSZXService {

    public static Bean queryCszxInfoById(String id) throws Exception {
        Map params = new HashMap();
        params.put("id",id);
        Bean bean = RapidDao.selectOneByXml("REPLY.findByReply",params);
        return bean;
    }

    /**
     * 获取听税列表数据
     * 返回结果包括 count 总播放次数 total 列表条数 data 获取到的分页数据
     * @param params
     * @return
     * @throws Exception
     */
    public static Bean queryTsDataList(Bean params) throws Exception {
        Bean result = new Bean();
        long count = RapidDao.selectOneByXml("MRTS.queryCountTs", params);
        int total = RapidDao.selectOneByXml("MRTS.queryCountTsList", params);
        List<Bean> list = RapidDao.selectOneByXml("ARTICLE.queryTsListForPage", params);
        result.put("count", count);
        result.put("total", total);
        result.put("data", list);
        return result;
    }

    public static Bean queryTsById(String id) throws Exception {
        Bean result = RapidDao.selectOneByXml("MRTS.queryTSBean", id);
        return result;
    }


    /**
     * 获取一条听税信息
     * @param id
     * @return
     * @throws Exception
     */
    public static Bean queryTs(String id, String contextPath) throws Exception {
        // 添加日志信息

        // 修改播放次数

        // 获取听税信息
        Bean result = RapidDao.selectOneByXml("MRTS.queryTSBean", id);
        if (result == null || StrKit.isBlank(result.getStr("audio_src"))) {
            throw new BizException("未找到音频文件");
        }
        String audioPath = Config.TS_BASE_PATH + File.separator + result.getStr("audio_src");
//        String tempPath = contextPath + File.separator + "temp";
        File tempFile = FileUtil.copyFileUUID(audioPath, contextPath);
        System.out.println(tempFile.getAbsolutePath());
        result.set("src", tempFile.getName());
        return result;
    }


    /**
     * 记录听税日志表信息
     * @param bean
     * @throws Exception
     */
    public static void logTsInfo(Bean bean) throws Exception {
        // 通过openid 获取当前用户
        Bean currentUser = RapidDao.selectOneByXml("HYBD.queryUserInfoByOpid", bean);

        Bean tsLog = new Bean();
        tsLog.set("ts_id", bean.getStr("id"));
        tsLog.set("openid", bean.getStr("openid"));
        if (currentUser != null) {
            tsLog.set("u_id", currentUser.getInt("uid"));
        }
        RapidDao.insertByXml("MRTS.insertTsLog", tsLog);

        Bean ts = new Bean();
        ts.set("id", bean.getStr("id"));
        ts.set("playTimes", bean.getLong("playTimes") + 1);
        ts.set("lastTime", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss SSS"));
        RapidDao.updateByXml("MRTS.updateTsPlayTimes", ts);
    }


    public static Bean queryTsSrcsInfo(String id, String contextPath) {
        Bean result = new Bean();
        try {
            Bean bean = RapidDao.selectOneByXml("MRTS.queryTSBean", id);
            if (bean == null) {
                result.set("result", false);
                result.set("msg", "获取信息失败");
                return result;
            }
            if (StrKit.isBlank(bean.getStr("audio_src"))) {
                result.set("audio_src", "");
            }
            if (StrKit.isBlank(bean.getStr("image"))) {
                result.set("image_src", "");
            }
            String audioPath = Config.TS_BASE_PATH + File.separator + bean.getStr("audio_src");
            File tempAudioFile = FileUtil.copyFileUUID(audioPath, contextPath);
            String imgPath = Config.TS_BASE_PATH + File.separator + bean.getStr("image");
            File tempImgFile = FileUtil.copyFileUUID(imgPath, contextPath);
            result.set("audio_src", tempAudioFile.getName());
            result.set("image_src", tempImgFile.getName());
            result.set("result", true);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            result.put("result", false);
            result.put("msg", "获取信息失败");
            return result;
        }
    }

    public static List selelctCszxInfoByReplyId(String repId) throws Exception {
        Map params = new HashMap();
        params.put("repid",repId);
        List replyList = RapidDao.selectListByXml("REPLY.findByReply",params);
        return replyList;
    }

    public static int saveCszxZxzj(Map params) throws Exception {
        params.put("click","1");
        params.put("addid","1");
        params.put("status","0");
        Date date = new Date(111);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        params.put("replytime",sdf.format(new Date()));
        String openid=params.get("openid")==null?"":params.get("openid").toString();
        
        Map<String,Object> hybdMap=new HashMap<String,Object>();
        hybdMap.put("openid", openid);
        hybdMap.put("wx_key", params.get("wx_key"));
        Map userinfo = CommonDao.dao.selectOneByXml("HYBD.queryUserInfoByOpid",hybdMap);
        params.put("uid", userinfo.get("uid"));
        List<Map> topUserMetaList = CommonDao.dao.selectListByXml("REPLY.queryOneTopUserMetaByUid",userinfo.get("uid"));
        if(topUserMetaList!=null&&topUserMetaList.size()>0){
        	params.put("username", topUserMetaList.get(0).get("meta_val"));
        }
        int i = RapidDao.insertByXml("REPLY.saveReply",params);
        return i;
    }

    public static void updateReplyClick(String id) throws Exception {
        RapidDao.updateByXml("REPLY.updateReplyClick", id);
    }
}
