package com.qysoft.accountingCloud.platform.services.hyfw;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.List;
import java.util.Map;

/**
 * Created by WangPengfei on 2017/3/10.
 */
public class WLKCService {

    public static Bean queryAllWlkc(Bean para) throws  Exception{

        Bean result = new Bean();
        int total = RapidDao.selectOneByXml("WLZBKC.queryCountWlkc",para);
        System.out.println("-----------------------------------------------------------------------------------");
        List list =  RapidDao.selectListByXml("WLZBKC.queryWlkcListForPage",para);
        result.put("total",total);
        result.put("list",list);
        return result;
    }

    public static Bean queryAllWlkcXhss(Bean para) throws  Exception{

        Bean result = new Bean();
        int total = RapidDao.selectOneByXml("WLZBKC.queryCountWlkcXhss",para);
        System.out.println("-----------------------------------------------------------------------------------");
        List list =  RapidDao.selectListByXml("WLZBKC.queryWlkcXhssListForPage",para);
        result.put("total",total);
        result.put("list",list);
        return result;
    }

    /**
     * 获得网络课程分类信息
     * @return
     */
    public static List queryWlkcHotLabel_Kinds() throws Exception {
        List list = RapidDao.selectListByXml("WLZBKC.queryWlkcHotLabel_Kinds");
        return list;
    }

    /**
     * 获得网络课程分类信息
     * @return
     */
    public static List queryWlkcHotLabel(Map params) throws Exception {
        List list = RapidDao.selectListByXml("WLZBKC.queryWlkcHotLabel",params);
        return list;
    }
    /**
     * 
     * 税干学苑查询微课堂数据
     * @param para
     * @return
     * @throws Exception
     */
    public static Bean querySfdbkc(Bean para) throws  Exception{

        Bean result = new Bean();
        int total = RapidDao.selectOneByXml("WLZBKC.queryCountSfdbkc",para);
        System.out.println("-----------------------------------------------------------------------------------");
        List list =  RapidDao.selectListByXml("WLZBKC.querySfdbkcListForPage",para);
        result.put("total",total);
        result.put("list",list);
        return result;
    }

}
