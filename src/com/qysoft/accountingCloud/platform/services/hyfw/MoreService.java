package com.qysoft.accountingCloud.platform.services.hyfw;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.List;
import java.util.Map;

/**
 * Created by WangPengfei on 2017/3/20.
 */
public class MoreService {

    public static List<Bean> queryFgList(Bean para) throws  Exception{

        List<Bean> fgFl = RapidDao.selectListByXml("MORE.queryFgFlList",para);
        return fgFl;
    }

    public  static List<Bean> querySzList(Bean para) throws  Exception{
        List<Bean> fgSz = RapidDao.selectListByXml("MORE.queryFgSzList",para);
        return fgSz;
    }

    public static List queryFgArray() throws  Exception{
        List list = RapidDao.selectListByXml("MORE.queryArrayFg");
        return list;
    }

    public static Bean queryFgLists(Map map) throws  Exception{
        List ids = (List) map.get("ids");
        Bean result = new Bean();
        long total = RapidDao.selectOneByXml("MORE.queryCount", map);
        List<Bean> list  = RapidDao.selectListByXml("MORE.queryFgLists", map);
        result.put("total",total);
        result.put("list",list);
        return result;
    }



    public static Bean queryByFgId(Bean bean) throws  Exception{
        Bean result = new Bean();
        long total = RapidDao.selectOneByXml("MORE.queryFgCount",bean);
        List<Bean> list = RapidDao.selectListByXml("MORE.queryFgForIdList",bean);
        result.put("total",total);
        result.put("list",list);
        return result;
    }


    public static Bean queryJdListForPage(Bean bean) throws  Exception{
        Bean result = new Bean();
        long total = RapidDao.selectOneByXml("MORE.queryJdCountForPage");
        List<Bean> list = RapidDao.selectListByXml("MORE.queryJdListForPage",bean);
        result.put("total",total);
        result.put("list",list);
        return result;
    }


    public static Bean queryQkListForPage(Bean bean) throws  Exception{
        Bean result = new Bean();
        long total = RapidDao.selectOneByXml("MORE.queryQkCountForPage");
        List<Bean> list = RapidDao.selectListByXml("MORE.queryQkListForPage",bean);
        result.put("total",total);
        result.put("list",list);
        return result;
    }


    public static Bean querySwListForPage(Bean bean) throws  Exception{
        Bean result = new Bean();
        long total = RapidDao.selectOneByXml("MORE.querySwCountForPage",bean);
        List<Bean> list = RapidDao.selectListByXml("MORE.querySwListForPage",bean);
        result.put("total",total);
        result.put("list",list);
        return result;
    }
    public static Bean queryFxListForPage(Bean bean) throws  Exception{
        Bean result = new Bean();
        long total = RapidDao.selectOneByXml("MORE.queryFxCountForPage",bean);
        List<Bean> list = RapidDao.selectListByXml("MORE.queryFxListForPage",bean);
        result.put("total",total);
        result.put("list",list);
        return result;
    }


    /**
     * 由法规倒推解读ID
     * @return
     * @throws Exception
     */
    public static int queryJdIdForFg(String url) throws Exception{
        String sql = "select a.id from top_article a where a.url = ? ORDER BY a.createtime desc LIMIT 0,1";
        return (int)RapidDao.queryOne(sql,url);
    }

    /**
     * 获取推荐列表信息
     * @param bean
     * @return
     * @throws Exception
     */
    public  static Bean queryTjlist(Bean bean) throws  Exception{
        Bean result = new Bean();
        long total = RapidDao.selectOneByXml("MORE.queryTjCountForPage",bean);
        List<Bean> list = RapidDao.selectListByXml("MORE.queryTjListForPage",bean);
        result.put("total",total);
        result.put("list",list);
        return result;
    }

}
