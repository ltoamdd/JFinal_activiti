package com.qysoft.accountingCloud.platform.services.hyfw;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.ehcache.CacheKit;
import com.qysoft.accountingCloud.platform.uitl.Config;
import com.qysoft.accountingCloud.platform.uitl.FileUtil;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.exceptions.BizException;
import com.qysoft.rapid.utils.DateUtil;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Created by shenjinxiang on 2017-03-09.
 */
public class TSService {

    /**
     * 获取听税列表数据
     * 返回结果包括 count 总播放次数 total 列表条数 data 获取到的分页数据
     * @param params
     * @return
     * @throws Exception
     */
    public static Bean queryTsDataList(Bean params) throws Exception {
        Bean result = new Bean();
        int pageNum = Integer.parseInt(params.getStr("pageNum"));
        int pageSize = Integer.parseInt(params.getStr("pageSize"));
        pageNum = (pageNum <= 1) ? 1 : pageNum;
        int total = RapidDao.selectOneByXml("MRTS.queryCountTsList", params);
        int totalPage = (total % pageSize == 0) ? (total / pageSize) : ((total / pageSize) + 1);

        long count = RapidDao.selectOneByXml("MRTS.queryCountTs", params);
        params.set("start", (pageNum - 1) * pageSize);
        List<Bean> list = RapidDao.selectListByXml("MRTS.queryTsListForPage", params);

        result.put("count", count);
        result.set("pageNum", pageNum);
        result.set("pageSize", pageSize);
        result.set("total", total);
        result.set("data", list);
        result.set("totalPage", totalPage);
        result.set("result", true);
        return result;
    }

    public static Bean queryTsDatas(Bean params) throws Exception {
        Bean result = new Bean();
        List<Bean> list = CacheKit.get("MrtsListData", "tab_" + params.get("tab"));
        if (null == list) {
            list = queryTsListForPageCache(params);
            CacheKit.put("MrtsListData", "tab_" + params.get("tab"), list);
        }
//        long totalPlayTimes = 0;
//        for(Bean ts : list) {
//            long playTimes = ts.getLong("playTimes");
//            totalPlayTimes += playTimes;
//        }
        result.set("result", true);
        result.set("data", list);
//        result.set("totalPlayTimes", totalPlayTimes);
        result.set("total", list.size());
        return result;
    }

    public static Bean queryTsById(String id) throws Exception {
        Bean result = RapidDao.selectOneByXml("MRTS.queryTSBean", id);
        return result;
    }

    /**
     * 获取一条听税信息
     * @param id
     * @return
     * @throws Exception
     */
    public static Bean queryTs(String id, String contextPath) throws Exception {
        // 添加日志信息

        // 修改播放次数

        // 获取听税信息
        Bean result = RapidDao.selectOneByXml("MRTS.queryTSBean", id);
        if (result == null || StrKit.isBlank(result.getStr("audio_src"))) {
            throw new BizException("未找到音频文件");
        }
        String audioPath = Config.TS_BASE_PATH + File.separator + result.getStr("audio_src");
//        String tempPath = contextPath + File.separator + "temp";
        File tempFile = FileUtil.copyFileUUID(audioPath, contextPath);
        System.out.println(tempFile.getAbsolutePath());
        result.set("src", tempFile.getName());
        return result;
    }


    /**
     * 记录听税日志表信息
     * @param bean
     * @throws Exception
     */
    public static void logTsInfo(Bean bean) throws Exception {
        // 通过openid 获取当前用户
        Bean currentUser = RapidDao.selectOneByXml("HYBD.queryUserInfoByOpid", bean);

        Bean tsLog = new Bean();
        tsLog.set("ts_id", bean.getStr("id"));
        tsLog.set("openid", bean.getStr("openid"));
        if (currentUser != null) {
            tsLog.set("u_id", currentUser.getInt("uid"));
        }
        RapidDao.insertByXml("MRTS.insertTsLog", tsLog);

        Bean ts = new Bean();
        ts.set("id", bean.getStr("id"));
        ts.set("playTimes", bean.getLong("playTimes") + 1);
        ts.set("lastTime", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss SSS"));
        RapidDao.updateByXml("MRTS.updateTsPlayTimes", ts);
    }


    public static Bean queryTsSrcsInfo(String id, String contextPath) {
        Bean result = new Bean();
        try {
            Bean bean = RapidDao.selectOneByXml("MRTS.queryTSBean", id);
            if (bean == null) {
                result.set("result", false);
                result.set("msg", "获取信息失败");
                return result;
            }
            if (StrKit.isBlank(bean.getStr("audio_src"))) {
                result.set("audio_src", "");
            }
//            if (StrKit.isBlank(bean.getStr("image"))) {
//                result.set("image_src", "");
//            }
            String audioPath = Config.TS_BASE_PATH + File.separator + bean.getStr("audio_src");
            File tempAudioFile = FileUtil.copyFileUUID(audioPath, contextPath);
//            String imgPath = Config.TS_BASE_PATH + File.separator + bean.getStr("image");
//            File tempImgFile = FileUtil.copyFileUUID(imgPath, contextPath);
            result.set("audio_src", tempAudioFile.getName());
//            result.set("image_src", tempImgFile.getName());
            result.set("result", true);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            result.put("result", false);
            result.put("msg", "获取信息失败");
            return result;
        }
    }

    public static List<Bean> queryTsTabs(Bean params) throws Exception {
        List<Bean> list = RapidDao.selectListByXml("MRTS.queryTsTabs", params);
        return list;
    }

    public static List<Bean> queryAllTsTabs() throws Exception {
        List<Bean> list = RapidDao.selectListByXml("MRTS.queryAllTsTabs");
        return list;
    }

    public static List<Bean> queryTsListForPageCache(Bean params) throws Exception {
        return RapidDao.selectListByXml("MRTS.queryTsListForPageCache", params);
    }
}
