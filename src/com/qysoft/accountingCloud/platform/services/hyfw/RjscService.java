package com.qysoft.accountingCloud.platform.services.hyfw;

import java.util.List;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

public class RjscService {
	public static Bean queryAllRjsc(Bean para) throws  Exception{
        Bean result = new Bean();
        int total = RapidDao.selectOneByXml("RJSC.queryCountRjsc",para);
        System.out.println("-----------------------------------------------------------------------------------");
        List list =  RapidDao.selectListByXml("RJSC.queryRjscListForPage",para);
        result.put("total",total);
        result.put("list",list);
        return result;
    }
}
