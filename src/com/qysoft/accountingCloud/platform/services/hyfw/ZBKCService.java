package com.qysoft.accountingCloud.platform.services.hyfw;

import com.jfinal.aop.Before;
import com.jfinal.plugin.ehcache.CacheKit;
import com.qysoft.rapid.actions.JsonResultInterceptor;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by WangPengfei on 2017/3/10.
 */

public class ZBKCService {

    public static Bean queryAllZbkc(Bean para) throws  Exception{

        Bean result = new Bean();
        int total = RapidDao.selectOneByXml("WLZBKC.queryCountZbkc",para);
        System.out.println("-----------------------------------------------------------------------------------");
        List list =  RapidDao.selectListByXml("WLZBKC.queryZbkcListForPage",para);
        result.put("total",total);
        result.put("list",list);
        return result;
    }
    public static Bean queryAllZbkcYg(Bean para) throws  Exception{

        Bean result = new Bean();
        List<Bean> list = CacheKit.get("zbkcData", "list");
        if (null == list) {
            list =  RapidDao.selectListByXml("WLZBKC.queryZbkcListForPageCache");
            CacheKit.put("zbkcData", "list", list);
        }
        int total = list.size();

        result.put("total",total);
        result.put("list",list);
        return result;
    }

    @Before({JsonResultInterceptor.class, RapidDbConnTx.class})
    public static Map findVideoById(Map params) throws Exception {
        Map result =  RapidDao.selectOneByXml("WLZBKC.findVideoById",params);
        return result;
    }
    public static Map findVideoByIdCache(Map params) throws Exception {
        Map result =  RapidDao.selectOneByXml("WLZBKC.findVideoByIdCache",params);
        return result;
    }

    public static int queryWlkcZhangjCount(Object id) throws Exception {
        Map params = new HashMap();
        params.put("video_id",id);
        int  i = RapidDao.selectOneByXml("WLZBKC.queryWlkcZhangjCount",id);
        return i;
    }

    public static List queryWlkcZhangjList(Object id) throws Exception {
        Map params = new HashMap();
        params.put("video_id",id);
        List list = RapidDao.selectListByXml("WLZBKC.queryWlkcZhangjList",id);
        return list;
    }

    public static List queryWlkcZhangjListWithWxKey(Bean param) throws Exception {
        List list = RapidDao.selectListByXml("WLZBKC.queryWlkcZhangjListWithWxKey", param);
        return list;
    }
}
