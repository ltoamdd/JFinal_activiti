package com.qysoft.accountingCloud.platform.services.chat;

import com.qysoft.rapid.aop.annotations.MyBatisDbConn;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by guowei on 2017-05-03.
 */
public class ChatService {

    /**
     * 插入聊天记录
     * @param params
     * @return
     * @throws Exception
     */
    @MyBatisDbConn(dbSourceKey = "zkx_log")
    public Bean insertChat(Map<String, Object> params) throws Exception {

        params.put("createtime", new Date());
        Bean result = new Bean();
        try {
            RapidDao.insertByXml("CHAT.insertChat", params);
            result.set("result", true);
            result.set("msg", "保存记录成功。");
        }catch (Exception e){
            e.printStackTrace();
            result.set("result", false);
            result.set("msg", "保存记录失败。");
        }

        return result;
    }


    /**
     * 获取聊天记录列表
     *
     * @param params
     * @return
     */
    @MyBatisDbConn(dbSourceKey = "zkx_log")
    public Bean queryChatList(Map<String, Object> params) throws Exception {
        int pageSize = Integer.parseInt((String) params.get("pageSize"));
        int total = RapidDao.selectOneByXml("CHAT.queryChatHistoryCount", params);
        int totalPage = (total % pageSize == 0) ? (total / pageSize) : ((total / pageSize) + 1);
        if(totalPage >0){
            params.put("start", (totalPage - 1) * pageSize);
        }else{
            params.put("start", (totalPage) * pageSize);
        }

        List<Bean> list = RapidDao.selectListByXml("CHAT.queryChatHistory", params);

        Bean result = new Bean();
        result.set("pageSize", pageSize);
        result.set("total", total);
        result.set("data", list);
        result.set("totalPage", totalPage);
        return result;
    }
}
