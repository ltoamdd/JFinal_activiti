package com.qysoft.accountingCloud.platform.services.cache;

import com.jfinal.plugin.ehcache.CacheKit;
import com.qysoft.accountingCloud.platform.services.wode.HYBDService;
import com.qysoft.rapid.aop.annotations.MyBatisDbConn;
import com.qysoft.rapid.consts.RapidConsts;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.plugin.mybatis.MyBatisPlugin;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

/**
 * Created by shenjinxiang on 2018-03-10.
 */
public class CacheService {

    public static void wxbdCache() throws Exception {
        List<Bean> wxyhList = HYBDService.queryWxBdyh();
        for (Bean wxyh : wxyhList) {
            if (checkNotNull(wxyh.getStr("wx_key")) && checkNotNull(wxyh.getStr("openid"))) {
                CacheKit.put("wxyhbd",
                        wxyh.getStr("wx_key") + "_" + wxyh.getStr("openid"),
                        wxyh);
            }
        }
    }

    public static void mrtsCache() throws Exception {
    	
    	
    	SqlSession sqlSession=null;
    	try {
    		SqlSessionFactory sqlSessionFactory = MyBatisPlugin.getSqlSessionFactory(RapidConsts.getDEFAULT_DBSOURCE_KEY());
    		sqlSession = sqlSessionFactory.openSession(false);
    		List<Bean> tabList=sqlSession.selectList("MRTS.queryAllTsTabs");
    		for (Bean tabBean : tabList) {
                Bean params = new Bean();
                int tab = tabBean.getInt("id");
                params.set("tab", tab);
                List<Bean> tsList = sqlSession.selectList("MRTS.queryTsListForPageCache", params);;
                CacheKit.put("MrtsListData", "tab_" + tab, tsList);
            }
    		sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback(true);
			e.printStackTrace();
		}finally {
			if(sqlSession!=null){
				sqlSession.close();
			}
			
		}
        
        
    }

    public static void zbkcCache() throws Exception {
    	SqlSession sqlSession=null;
    	try {
    		SqlSessionFactory sqlSessionFactory = MyBatisPlugin.getSqlSessionFactory(RapidConsts.getDEFAULT_DBSOURCE_KEY());
    		sqlSession = sqlSessionFactory.openSession(false);
    		 List<Bean> list = CacheKit.get("zbkcData", "list");
	        if (list != null) {
	            for(Bean zbkc : list) {
	            	sqlSession.update("updateZbkcClickById", zbkc);
	            }
	        }
    	    list = sqlSession.selectList("WLZBKC.queryZbkcListForPageCache");
    	    CacheKit.put("zbkcData", "list", list);
    	    sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback(true);
			e.printStackTrace();
		}finally{
			if(sqlSession!=null){
				sqlSession.close();
			}
			
		}
    	
       
        
    }

    private static boolean checkNotNull(String str) {
        return null != str && !"".equals(str) && !"null".equals(str) && !"undefined".equals(str);
    }


    @MyBatisDbConn
    public void mrtsCacheMul() throws Exception {
    	SqlSession sqlSession=null;
    	try {
    		SqlSessionFactory sqlSessionFactory = MyBatisPlugin.getSqlSessionFactory(RapidConsts.getDEFAULT_DBSOURCE_KEY());
    		sqlSession = sqlSessionFactory.openSession(false);
            List<Bean> tabList = sqlSession.selectList("MRTS.queryAllTsTabs");
            for (Bean tabBean : tabList) {
                Bean params = new Bean();
                int tab = tabBean.getInt("id");
                params.set("tab", tab);
                List<Bean> tsList = sqlSession.selectList("MRTS.queryTsListForPageCache", params);
                CacheKit.put("MrtsListData", "tab_" + tab, tsList);
            }
            sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback(true);
			e.printStackTrace();
		}finally {
			if(sqlSession!=null){
				sqlSession.close();
			}
		}
    	
    }

    @MyBatisDbConn
    public void zbkcCacheMul() throws Exception {
    	SqlSession sqlSession=null;
    	try {
    		SqlSessionFactory sqlSessionFactory = MyBatisPlugin.getSqlSessionFactory(RapidConsts.getDEFAULT_DBSOURCE_KEY());
    		sqlSession = sqlSessionFactory.openSession(false);
    		List<Bean> list = CacheKit.get("zbkcData", "list");
            if (list != null) {
                for(Bean zbkc : list) {
                	sqlSession.update("updateZbkcClickById", zbkc);
                }
            }
            list = sqlSession.selectList("WLZBKC.queryZbkcListForPageCache");
            CacheKit.put("zbkcData", "list", list);
            sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback(true);
			e.printStackTrace();
		}finally {
			if(sqlSession!=null){
				sqlSession.close();
			}
		}
        
    }


}
