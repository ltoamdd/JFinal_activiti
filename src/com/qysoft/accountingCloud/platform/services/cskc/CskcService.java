package com.qysoft.accountingCloud.platform.services.cskc;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.List;

public class CskcService {
    public static Bean findKcbRmkcList(Bean para) throws  Exception{
        Bean result = new Bean();
        int total = RapidDao.selectOneByXml("CSKC.findKcbRmkcCount",para);
        System.out.println("-----------------------------------------------------------------------------------");
        List list =  RapidDao.selectListByXml("CSKC.findKcbRmkc",para);
        result.put("total",total);
        result.put("list",list);
        return result;
    }

    public static Bean findKcbZxkcList(Bean para) throws  Exception{
        Bean result = new Bean();
        int total = RapidDao.selectOneByXml("CSKC.findKcbZxkcCount",para);
        System.out.println("-----------------------------------------------------------------------------------");
        List list =  RapidDao.selectListByXml("CSKC.findKcbZxkc",para);
        result.put("total",total);
        result.put("list",list);
        return result;
    }
    public static Bean findKcbMfkcList(Bean para) throws  Exception{
        Bean result = new Bean();
        int total = RapidDao.selectOneByXml("CSKC.findKcbMfkcCount",para);
        System.out.println("-----------------------------------------------------------------------------------");
        List list =  RapidDao.selectListByXml("CSKC.findKcbMfkc",para);
        result.put("total",total);
        result.put("list",list);
        return result;
    }
}
