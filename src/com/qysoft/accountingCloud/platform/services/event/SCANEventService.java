package com.qysoft.accountingCloud.platform.services.event;

import com.jfinal.kit.JsonKit;
import com.qysoft.accountingCloud.platform.uitl.HttpKit;
import com.qysoft.accountingCloud.platform.uitl.HttpUtil;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by shenjinxiang on 2017-07-27.
 */
public class SCANEventService extends IWxEventService {
    @Override
    public Bean doService(Map<String, Object> param) throws Exception {
        System.out.println("用户已关注时扫描二维码事件");
        String jsonParams= JsonKit.toJson(param);
        String wx_key=param.get("ToUserName").toString();
        String actionurl=RapidDao.selectOneByXml("WXGZHGL.selectActionUrlByWxkey",wx_key);
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        HttpKit.post(actionurl,null,jsonParams,headers);
        return null;
    }
    public static void main(String[] args) {
		String a="0101";
		String b=a.substring(0,6);
		System.out.println(b);
	}
}
