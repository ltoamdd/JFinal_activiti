package com.qysoft.accountingCloud.platform.services.event;

import com.jfinal.kit.JsonKit;
import com.qysoft.accountingCloud.platform.uitl.HttpKit;
import com.qysoft.accountingCloud.platform.uitl.HttpUtil;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;
import netscape.javascript.JSObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shenjinxiang on 2017-07-29.
 */
public class ClickEventService extends IWxEventService {

    @Override
    public Bean doService(Map<String, Object> param) throws Exception {
        System.out.println("点击菜单事件");
        String jsonParams=JsonKit.toJson(param);
        String wx_key=param.get("ToUserName").toString();
        String actionurl=RapidDao.selectOneByXml("WXGZHGL.selectActionUrlByWxkey",wx_key);
        System.out.println(actionurl);
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        HttpKit.post(actionurl,null,jsonParams,headers);
        return null;
    }
}
