package com.qysoft.accountingCloud.platform.services.event;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.Map;

/**
 * Created by shenjinxiang on 2017-07-27.
 */
public abstract class IWxEventService {

    protected abstract Bean doService(Map<String, Object> param) throws Exception;

    public Bean invoke(Map<String, Object> param) throws Exception {
//        insertEventLog(param);
        return doService(param);
    }

    private void insertEventLog(Map<String, Object> param) throws Exception {
        RapidDao.insertByXml("EVENT.insertEventLog", param);
    }

    public static int queryCountByOpenidAndCreateTime(Map<String, Object> param) throws Exception {
        return RapidDao.selectOneByXml("EVENT.queryCountByOpenidAndCreateTime", param);
    }
}
