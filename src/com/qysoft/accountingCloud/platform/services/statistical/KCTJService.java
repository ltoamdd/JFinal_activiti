package com.qysoft.accountingCloud.platform.services.statistical;

import com.jfinal.aop.Before;
import com.qysoft.rapid.core.RapidDbConnTx;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

public class KCTJService {
	@Before(RapidDbConnTx.class)
	public  void insertStatistical(Bean params)throws Exception{
		 RapidDao.insertByXml("STATISTICAL.inertStatisticalinfo", params);
	}
}
