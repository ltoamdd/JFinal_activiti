package com.qysoft.accountingCloud.platform.services.mb;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.exceptions.BizException;

import java.util.List;

/**
 * 微信模板消息Service
 */
public class MbService {

    /*
     * 通过模板名称查询模板信息
     */
    public static Bean selectMbByName(String name) throws Exception {
        Bean mb = RapidDao.selectOneByXml("MB.selectMbByName", name);
        if (null == mb) {
            throw new BizException("模板信息不存在");
        }
        return mb;
    }

    /*
     * 查询所有模板名称
     */
    public static List<String> selectMbName() throws Exception {
        List<String> mb = RapidDao.selectListByXml("MB.selectMbName");
        if (null == mb) {
            throw new BizException("模板名称不存在");
        }
        return mb;
    }

    /*
     * 查询所有模板信息
     */
    public static List<Bean> selectAllMb() throws Exception {
        List<Bean> mb =  RapidDao.selectListByXml("MB.selectAllMb");
        if (null == mb) {
            throw new BizException("模板信息不存在");
        }
        return mb;
    }

    /**
     * 根据退订关键字查询退订的模板名称
     * @throws Exception
     */
    public static String selectMbByTd(String td) throws Exception {
        String mbName = RapidDao.selectOneByXml("MB.selectMbByTd",td);
        return mbName;
    }

    /**
     * 根据订阅关键字查询订阅的模板名称
     * @throws Exception
     */
    public static String selectMbByDy(String dy) throws Exception {
        String mbName = RapidDao.selectOneByXml("MB.selectMbByDy",dy);
        return mbName;
    }

}
