package com.qysoft.accountingCloud.platform.services.common;

import com.qysoft.accountingCloud.platform.entity.FWH;
import com.qysoft.rapid.aop.annotations.MyBatisDbConn;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.Date;
import java.util.List;

/**
 * Created by shenjinxiang on 2017-03-14.
 */
@MyBatisDbConn
public class CommonService {

    /**
     * 获取文章的点赞次数
     * @param params
     *  type: wx_article_type 中的id
     *  article_id: 需要查询的文章的id
     * @return
     */
    public static long queryDZCount(Bean params) throws Exception {
        return RapidDao.selectOneByXml("COMMON.queryDZCount", params);
    }

    /**
     * 获取文章的收藏次数
     * @param params
     *  type: wx_article_type 中的id
     *  article_id: 需要查询的文章的id
     * @return
     * @throws Exception
     */
    public static long queryShCCount(Bean params) throws Exception {
        return RapidDao.selectOneByXml("COMMON.queryShCCount", params);
    }

    /**
     *  获取一条wx_article_meta 记录
     * @param params
     *  type: wx_article_type 中的id
     *  article_id: 需要查询的文章的id
     *  openid: openid
     * @return result
     *
     * @throws Exception
     */
    public static Bean queryArticleMetaInfo(Bean params) throws Exception {
        Bean result = new Bean();
        result.put("article_id", params.getStr("article_id"));

//        String openid = params.getStr("openid");
        Bean hybd = RapidDao.selectOneByXml("HYBD.queryHybdByOpenid", params);
        if (hybd == null) {
            result.put("isfavorites", false);
            result.put("isup", false);
        } else {
            params.put("u_id", hybd.getInt("uid"));
            Bean meta = RapidDao.selectOneByXml("COMMON.queryArticleMeta", params);
            if (meta == null) {
                result.put("isfavorites", false);
                result.put("isup", false);
            } else {
                result.put("isfavorites", meta.getBoolean("isfavorites"));
                result.put("isup", meta.getBoolean("isup"));
            }

        }
        long upCount = RapidDao.selectOneByXml("COMMON.queryDZCount", params);
        long favoritesCount = RapidDao.selectOneByXml("COMMON.queryShCCount", params);
        result.put("upCount", upCount);
        result.put("favoritesCount", favoritesCount);
        return result;
    }

    /**
     * 收藏操作
     * @param params
     *  type: wx_article_type 中的id
     *  article_id: 需要查询的文章的id
     *  openid: 用户id
     * @return
     */
    public static Bean insertShCInfo(Bean params) throws Exception {
        Bean result = new Bean();
//        String openid = params.getStr("openid");
        Bean hybd = RapidDao.selectOneByXml("HYBD.queryHybdByOpenid", params);
        if(hybd == null) {
            result.set("result", false);
            result.set("msg", "未绑定会员，无法收藏");
            return result;
        }
        params.put("u_id", hybd.getInt("uid"));
        Bean meta = RapidDao.selectOneByXml("COMMON.queryArticleMeta", params);
        if (meta == null) {
            // 如果没有记录 添加一条记录，并设置收藏(isfavorites)为1 点赞(isup)为0
            meta = new Bean();
            meta.set("article_id", params.getStr("article_id"));
            meta.set("u_id", params.getInt("u_id"));
            meta.set("type", params.getStr("type"));
            meta.set("isfavorites", 1);
            meta.set("isup", 0);
            meta.set("createdate", new Date());
            RapidDao.insertByXml("COMMON.inertArticleMeta", meta);
        } else {
            // 如果有记录 且还没有收藏 修改记录
            if (!meta.getBoolean("isfavorites")) {
                Bean meta1 = new Bean();
                meta1.set("article_id", meta.getStr("article_id"));
                meta1.set("u_id", meta.getInt("u_id"));
                meta1.set("type", meta.getInt("type"));
                meta1.set("isfavorites", 1);
                meta1.set("isup", meta.getBoolean("isup") ? 1 : 0);
                meta1.set("createdate", new Date());
                RapidDao.updateByXml("COMMON.updateArticleMeta", meta1);
            }
        }
        result.set("result", true);
        result.set("msg", "收藏成功");
        return result;
    }

    /**
     * 取消收藏
     * @param params
     * @return
     * @throws Exception
     */
    public static Bean delShCInfo(Bean params) throws Exception {
        Bean result = new Bean();
        String openid = params.getStr("openid");
        Bean hybd = RapidDao.selectOneByXml("HYBD.queryHybdByOpenid", params);
        if(hybd == null) {
            result.set("result", false);
            result.set("msg", "未绑定会员，操作失败");
            return result;
        }
        params.put("u_id", hybd.getInt("uid"));
        Bean meta = RapidDao.selectOneByXml("COMMON.queryArticleMeta", params);
        if (meta == null) {
            result.set("result", false);
            result.set("msg", "操作失败");
            return result;
        }
        if (meta.getBoolean("isfavorites")) {
            Bean meta1 = new Bean();
            meta1.set("article_id", meta.getStr("article_id"));
            meta1.set("u_id", meta.getInt("u_id"));
            meta1.set("type", meta.getInt("type"));
            meta1.set("isfavorites", 0);
            meta1.set("isup", meta.getBoolean("isup") ? 1 : 0);
            meta1.set("createdate", new Date());
            RapidDao.updateByXml("COMMON.updateArticleMeta", meta1);
        }
        result.set("result", true);
        result.set("msg", "取消收藏成功");
        return result;
    }

    /**
     * 点赞操作
     * @param params
     * @return
     * @throws Exception
     */
    public static Bean dzArticle(Bean params) throws Exception{
        Bean result = new Bean();
        String openid = params.getStr("openid");
        Bean hybd = RapidDao.selectOneByXml("HYBD.queryHybdByOpenid", params);
        if(hybd == null) {
            result.set("result", false);
            result.set("msg", "未绑定会员，无法点赞");
            return result;
        }
        params.put("u_id", hybd.getInt("uid"));
        Bean meta = RapidDao.selectOneByXml("COMMON.queryArticleMeta", params);
        if (meta == null) {
            // 如果没有记录 添加一条记录，并设置点赞(isup)为1 收藏(isfavorites)为0
            meta = new Bean();
            meta.set("article_id", params.getStr("article_id"));
            meta.set("u_id", params.getInt("u_id"));
            meta.set("type", params.getStr("type"));
            meta.set("isfavorites", 0);
            meta.set("isup", 1);
            RapidDao.insertByXml("COMMON.inertArticleMeta", meta);
        } else {
            // 如果有记录 且还没有点赞 修改记录
            if (!meta.getBoolean("isup")) {
                Bean meta1 = new Bean();
                meta1.set("article_id", meta.getStr("article_id"));
                meta1.set("u_id", meta.getInt("u_id"));
                meta1.set("type", meta.getInt("type"));
                meta1.set("isfavorites", meta.getBoolean("isfavorites") ? 1 : 0);
                meta1.set("isup", 1);
                RapidDao.updateByXml("COMMON.updateArticleMeta", meta1);
            }
        }
        result.set("result", true);
        result.set("msg", "点赞成功");
        return result;
    }

    /**
     * 取消点赞
     * @param params
     * @return
     * @throws Exception
     */
    public static Bean qxdzArticle(Bean params) throws Exception{
        Bean result = new Bean();
        String openid = params.getStr("openid");
        Bean hybd = RapidDao.selectOneByXml("HYBD.queryHybdByOpenid", params);
        if(hybd == null) {
            result.set("result", false);
            result.set("msg", "未绑定会员，操作失败");
            return result;
        }
        params.put("u_id", hybd.getInt("uid"));
        Bean meta = RapidDao.selectOneByXml("COMMON.queryArticleMeta", params);
        if (meta == null) {
            result.set("result", false);
            result.set("msg", "操作失败");
            return result;
        }
        if (meta.getBoolean("isup")) {
            Bean meta1 = new Bean();
            meta1.set("article_id", meta.getStr("article_id"));
            meta1.set("u_id", meta.getInt("u_id"));
            meta1.set("type", meta.getInt("type"));
            meta1.set("isfavorites", meta.getBoolean("isfavorites") ? 1 : 0);
            meta1.set("isup", 0);
            RapidDao.updateByXml("COMMON.updateArticleMeta", meta1);
        }
        result.set("result", true);
        result.set("msg", "取消点赞成功");
        return result;
    }

    /**
     * 获取评论列表
     * @param params
     *  article_id 文章的id
     *  type 类型
     *  pageNum 页码
     *  pageSize 每页条数
     * @return
     *  data: 数据
     *  pageNum 页码
     *  pageSize 每页条数
     *  total 总条数
     *  totalPage 总页数
     * @throws Exception
     */
    public static Bean queryCommentPage(Bean params) throws Exception {
        int pageNum = Integer.parseInt(params.getStr("pageNum"));
        int pageSize = Integer.parseInt(params.getStr("pageSize"));
        pageNum = (pageNum <= 1) ? 1 : pageNum;

        int total = RapidDao.selectOneByXml("COMMON.queryCommontPageCount", params);
        int totalPage = (total % pageSize == 0) ? (total / pageSize) : ((total / pageSize) + 1);
//        pageNum = (pageNum > totalPage) ? totalPage : pageNum;
        params.set("start", (pageNum - 1) * pageSize);
        List<Bean> list = RapidDao.selectListByXml("COMMON.queryCommontPageList", params);

        Bean result = new Bean();
        result.set("pageNum", pageNum);
        result.set("pageSize", pageSize);
        result.set("total", total);
        result.set("data", list);
        result.set("totalPage", totalPage);
        result.set("result", true);
        return result;
    }

    /**
     * 添加评论
     * @param params
     * @return
     * @throws Exception
     */
    public static Bean insertCommentInfo(Bean params) throws Exception {
        Bean result = new Bean();
        String openid = params.getStr("openid");
        Bean hybd = RapidDao.selectOneByXml("HYBD.queryHybdByOpenid", params);
        if (hybd == null) {
            result.set("result", false);
            result.set("msg", "评论失败，未绑定用户");
            return result;
        }
        params.set("u_id", hybd.getInt("uid"));
        params.set("yxbz", 1);
        RapidDao.insertByXml("COMMON.insertCommentINfo", params);

        result.set("result", true);
        result.set("msg", "评论成功");
        return result;
    }

    public static Bean querySearchList(Bean params) {
        Bean result = new Bean();
        try {
            int pageNum = Integer.parseInt(params.getStr("pageNum"));
            int pageSize = Integer.parseInt(params.getStr("pageSize"));
            pageNum = (pageNum <= 1) ? 1 : pageNum;
            params.set("start", (pageNum - 1) * pageSize);

            int total = RapidDao.selectOneByXml("COMMON.queryCountSearchList", params);
            int totalPage = (total % pageSize == 0) ? (total / pageSize) : ((total / pageSize) + 1);
            List<Bean> list = RapidDao.selectListByXml("COMMON.querySearchList", params);

            result.set("pageNum", pageNum);
            result.set("pageSize", pageSize);
            result.set("total", total);
            result.set("data", list);
            result.set("totalPage", totalPage);
            result.set("result", true);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            result.set("result", false);
            result.set("msg", "搜索失败");
            return result;
        }
    }

    public static List<Bean> queryFwhList() throws Exception {
        return RapidDao.selectListByXml("COMMON.queryFwhList");
    }

    public static FWH queryFwhByKey(String wx_key) throws Exception {
        FWH fwh = new FWH();
        Bean bean = RapidDao.selectOneByXml("COMMON.qyerFwhByKey", wx_key);
        fwh.setKey(bean.getStr("key"));
        fwh.setAppid(bean.getStr("appid"));
        fwh.setName(bean.getStr("name"));
        fwh.setAppsecret(bean.getStr("appsecret"));
        fwh.setYsid(bean.getStr("ysid"));
        fwh.setJiekou_token(bean.getStr("jiekou_token"));
        fwh.setEwm_src(bean.getStr("ewm_src"));
        return fwh;
    }

    @MyBatisDbConn
    public  List<Bean> queryFwhList1() throws Exception {
        return RapidDao.selectListByXml("COMMON.queryFwhList");
    }

}
