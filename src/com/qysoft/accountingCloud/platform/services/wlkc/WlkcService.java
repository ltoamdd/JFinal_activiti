package com.qysoft.accountingCloud.platform.services.wlkc;

import java.util.List;
import java.util.Map;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

public class WlkcService {
	public static Bean findYhbdByOpenid(String openid) throws Exception{
		Bean bean = RapidDao.selectOneByXml("WLKC.findYhbdByOpenid", openid);
		return bean;
	}
	/**
	 * @param map
	 * @return
	 * @throws Exception
	 * 网路课程，课程相关课程的查询
	 */
	public static Bean findWlkcXgkc(Bean map) throws Exception{
		Bean result = new Bean();
		String xgnr=map.get("hot_label")==null?"":map.get("hot_label").toString();
		if(!xgnr.equals("")){
			String[] xgnrs=xgnr.split(",");
			map.put("hot_label", xgnrs);
			List<Map<String,Object>> listXgnl=RapidDao.selectListByXml("WLKC.findAticleXgkc", map);
			result.put("list", listXgnl);
		}
		return result;
	}
}
