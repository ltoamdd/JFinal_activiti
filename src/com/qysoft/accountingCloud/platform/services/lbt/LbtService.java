package com.qysoft.accountingCloud.platform.services.lbt;

import java.util.List;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

public class LbtService {
	public static List<Bean> queryLbt() throws  Exception{
        List<Bean> result= RapidDao.selectListByXml("LBT.queryLbt");
        return result;
    }
}
