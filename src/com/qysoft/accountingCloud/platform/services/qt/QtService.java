package com.qysoft.accountingCloud.platform.services.qt;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;
import com.qysoft.rapid.exceptions.BizException;

import java.util.List;

/**
 * 客户群体Service
 */
public class QtService {

    /*
     * 客户群体普通会员openid
     */
    public static List<String> selectQtpthy() throws Exception {
        List<String> openidList = RapidDao.selectListByXml("QT.selectQtpthy");
        if (null == openidList) {
            throw new BizException("普通会员openid不存在");
        }
        return openidList;
    }

    /*
     * 客户群体银卡会员openid
     */
    public static List<String> selectQtykhy() throws Exception {
        List<String> openidList = RapidDao.selectListByXml("QT.selectQtykhy");
        if (null == openidList) {
            throw new BizException("银卡会员openid不存在");
        }
        return openidList;
    }

    /*
     * 客户群体金卡会员openid
     */
    public static List<String> selectQtjkhy() throws Exception {
        List<String> openidList = RapidDao.selectListByXml("QT.selectQtjkhy");
        if (null == openidList) {
            throw new BizException("金卡会员openid不存在");
        }
        return openidList;
    }

    /*
     * 查询所有群体名称
     */
    public static List<String> selectQtName() throws Exception {
        List<String> qt = RapidDao.selectListByXml("QT.selectQtName");
        if (null == qt) {
            throw new BizException("群体名称不存在");
        }
        return qt;
    }

    /*
     * 查询所有客户群体信息
     */
    public static List<Bean> selectAllQt() throws Exception {
        List<Bean> qt =  RapidDao.selectListByXml("QT.selectAllQt");
        if (null == qt) {
            throw new BizException("客户群体不存在");
        }
        return qt;
    }


}
