package com.qysoft.accountingCloud.platform.services.viewlog;

import java.util.Map;

import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

public class ViewlogService {
	public static void addViewLog(Bean params) throws Exception{
		String openid=params.get("openid")==null?"":params.get("openid").toString();
		String cid=params.get("course_cid")==null?"":params.get("course_cid").toString();
		if(!cid.equals("ts")){
			Map<String,Object> map=RapidDao.selectOneByXml("VIEWLOG.queryKcLs", params);
			params.put("teacher", map.get("teacher"));
			params.put("kc_createtime", map.get("kc_createtime"));//课程的创建时间
		}else{
			Map<String,Object> map=RapidDao.selectOneByXml("VIEWLOG.queryTsKccreatetime", params);
			params.put("kc_createtime", map.get("kc_createtime"));//课程的创建时间
		}
		//根据openid获取用户信息
		RapidDao.insertByXml("VIEWLOG.addViewLog",params);
	}
	public static Integer queryYhdjByOpenid(Bean params) throws Exception{
		Integer yhdj=RapidDao.selectOneByXml("VIEWLOG.queryUserByOpenid", params);
		return yhdj;
	}
}
