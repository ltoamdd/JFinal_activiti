package com.qysoft.accountingCloud.platform.config;

import com.jfinal.config.Routes;
import com.jfinal.template.Engine;
import com.qysoft.accountingCloud.platform.actions.chat.ChatAction;
import com.qysoft.accountingCloud.platform.actions.cskc.CskcAction;
import com.qysoft.accountingCloud.platform.actions.common.CommonAction;
import com.qysoft.accountingCloud.platform.actions.hyfw.WLKCAction;
import com.qysoft.accountingCloud.platform.actions.hyfw.ZBKCAction;
import com.qysoft.accountingCloud.platform.actions.lbt.LbtAction;
import com.qysoft.accountingCloud.platform.actions.shouyerjsc.ShouyeRjscAction;
import com.qysoft.accountingCloud.platform.actions.shouyewkt.ShouyeWktAction;
import com.qysoft.accountingCloud.platform.actions.shouyezbkc.ShouyeZbkcAction;
import com.qysoft.accountingCloud.platform.actions.viewlog.ViewlogAction;
import com.qysoft.accountingCloud.platform.actions.wlkc.KcbWlkcAction;

import com.qysoft.accountingCloud.platform.actions.cszx.CszxAction;
import com.qysoft.accountingCloud.platform.actions.dlfw.DlfwDmAction;
import com.qysoft.accountingCloud.platform.actions.dlfw.DljzAction;
import com.qysoft.accountingCloud.platform.actions.dlfw.GszcAction;
import com.qysoft.accountingCloud.platform.actions.dlfw.KptgAction;
import com.qysoft.accountingCloud.platform.actions.wode.HYBDAction;
import com.qysoft.accountingCloud.platform.actions.ztxl.ZtxlAction;
import com.qysoft.rapid.consts.RapidConsts;
import com.qysoft.rapid.core.Rapid;
import com.qysoft.rapid.core.RapidConifg;

public class CloudConfig extends RapidConifg {
    @Override
    public void configAction(Routes me) {
        me.add("/common", CommonAction.class);
        //跳转财税课程首页
        me.add("/cskc", CskcAction.class);
        //轮播图
        me.add("/lbt", LbtAction.class);
        //网络课程暂时的
        me.add("/wlkc", KcbWlkcAction.class);
        //财税咨询
        me.add("/cszx", CszxAction.class);
        //听税视频观看记录
        me.add("/viewlog", ViewlogAction.class);
        //专题系列
        me.add("/ztxl", ZtxlAction.class);
        //网络课程--里面有晓红说税
        me.add("/hyfw/wlkc", WLKCAction.class);
        //首页软件实操的跳转
        me.add("/shouyerjsc", ShouyeRjscAction.class);
        //首页微课堂的跳转
        me.add("/shouyewkt", ShouyeWktAction.class);
        //首页网络直播课程的跳转
        me.add("/shouyezbkc", ShouyeZbkcAction.class);
        //直播课程
        me.add("/hyfw/zbkc", ZBKCAction.class);
        //互动 郭伟2018/02/26
        me.add("/chat/chatHis", ChatAction.class);
        me.add("/wode/hybd", HYBDAction.class); //会员绑定

        me.add("/dlfw/dljz", DljzAction.class);
        me.add("/dlfw/kptg", KptgAction.class);
        me.add("/dlfw/gszc", GszcAction.class);
        me.add("/dlfw/dm", DlfwDmAction.class);

    }

    @Override
    public int configDbTool() {
        return RapidConsts.DB_TOOL_MYBATIS;
    }

    @Override
    public int configDbType() {
        return RapidConsts.DB_TYPE_MYSQL;
    }

    @Override
    public String configErrorMSg() {
        return "系统出错，请联系管理员！";
    }

    public static void main(String[] args) throws Exception {
        Rapid.getRapidInstance().start("WebRoot", 8888, "/qywxdyh", 5);

    }

    @Override
    public void configEngine(Engine engine) {

    }
}
