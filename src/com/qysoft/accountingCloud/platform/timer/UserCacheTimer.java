package com.qysoft.accountingCloud.platform.timer;

import com.jfinal.plugin.ehcache.CacheKit;
import com.qysoft.rapid.dao.mybatis.RapidDao;
import com.qysoft.rapid.domain.Bean;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by shenjinxiang on 2018-03-08.
 */
public class UserCacheTimer implements Runnable  {
    @Override
    public void run() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    List<Bean> list = RapidDao.selectListByXml("HYBDTEST.queryAllUserForTest");
                    CacheKit.put("userlist", "data", list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1000 * 60 * 60 * 24);
    }
}
