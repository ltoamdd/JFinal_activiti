package com.qysoft.accountingCloud.platform.timer;

import com.jfinal.aop.Enhancer;
import com.qysoft.accountingCloud.platform.services.cache.CacheService;
import com.qysoft.rapid.consts.RapidConsts;
import com.qysoft.rapid.plugin.mybatis.MyBatisPlugin;
import com.qysoft.rapid.plugin.mybatis.MyBatisSessionManager;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.ibatis.session.SqlSessionFactory;

/**
 * 每日听税列表信息缓存
 * Created by shenjinxiang on 2018-03-02.
 */
public class MrtsCacheTimer implements Runnable {


    @Override
    public void run() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    CacheService.mrtsCache();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1000 * 60 * 20);
    }
}
