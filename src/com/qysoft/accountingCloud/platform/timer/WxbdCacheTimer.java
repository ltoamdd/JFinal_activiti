package com.qysoft.accountingCloud.platform.timer;


import com.qysoft.accountingCloud.platform.services.cache.CacheService;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 用户绑定信息缓存
 * Created by shenjinxiang on 2018-03-02.
 */
public class WxbdCacheTimer implements Runnable {

    @Override
    public void run() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    CacheService.wxbdCache();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1000 * 60 * 60 * 2);
    }

}
