package com.qysoft.accountingCloud.platform.timer;

import com.jfinal.aop.Enhancer;
import com.qysoft.accountingCloud.platform.services.cache.CacheService;
import com.qysoft.rapid.consts.RapidConsts;
import com.qysoft.rapid.plugin.mybatis.MyBatisSessionManager;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 直播课程缓存
 * Created by shenjinxiang on 2018-03-02.
 */
public class ZbkcCacheTimer implements Runnable {

    @Override
    public void run() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
//                    MyBatisSessionManager.setSession(RapidConsts.getDEFAULT_DBSOURCE_KEY(),true);
                    CacheService.zbkcCache();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1000 * 60 * 60 * 20);
    }
}
