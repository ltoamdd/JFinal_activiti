package com.qysoft.accountingCloud.platform.entity;

/**
 * Created by shenjinxiang on 2017-06-28.
 */
public class FWH {

    private String key;
    private String name;
    private String appid;
    private String local;
    private String appsecret;
    private String ysid;
    private String jiekou_token;
    private String ewm_src;

    private String access_token = "";
    private long access_timestamp = 0;
    private String jsapi_ticket = "";
    private long jsapi_ticket_timestamp = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYsid() {
        return ysid;
    }

    public void setYsid(String ysid) {
        this.ysid = ysid;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getAppsecret() {
        return appsecret;
    }

    public void setAppsecret(String appsecret) {
        this.appsecret = appsecret;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public long getAccess_timestamp() {
        return access_timestamp;
    }

    public void setAccess_timestamp(long access_timestamp) {
        this.access_timestamp = access_timestamp;
    }

    public String getJsapi_ticket() {
        return jsapi_ticket;
    }

    public void setJsapi_ticket(String jsapi_ticket) {
        this.jsapi_ticket = jsapi_ticket;
    }

    public long getJsapi_ticket_timestamp() {
        return jsapi_ticket_timestamp;
    }

    public void setJsapi_ticket_timestamp(long jsapi_ticket_timestamp) {
        this.jsapi_ticket_timestamp = jsapi_ticket_timestamp;
    }

    public String getJiekou_token() {
        return jiekou_token;
    }

    public void setJiekou_token(String jiekou_token) {
        this.jiekou_token = jiekou_token;
    }

    public String getEwm_src() {
        return ewm_src;
    }

    public void setEwm_src(String ewm_src) {
        this.ewm_src = ewm_src;
    }

}
