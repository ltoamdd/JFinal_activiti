<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <style>
        .cszx_con section h6 span {
            color: #b4b4b4;
            width: 50%;
            display: inline-block;
        }
        .cszx_con section h6 em {
            float: right;
        }
    </style>
    <title>财税咨询</title>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var ztid = "${ztid}";
        var wx_key = '${wx_key}';
        var openid = '${openid}';
    </script>
    <base target="_self">
    <%--<jsp:include page="/WEB-INF/pages/common/baidutongji.jsp"/>--%>
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<div class="main clearfix">
    <form id="searchFrom_cszx" action="<%=ctx%>/common/search" method="get">
        <input type="hidden" id="openid" name="openid" value="${openid}">
        <input type="hidden" id="wx_key" name="wx_key" value="${wx_key}">
        <div class="cszx_search"><input type="text" name="search" id="search" placeholder="搜索" value=""/></div>
    </form>
    <div class="cszx_nav">
        <a href="javascript:toForwordZjzx()" class="zxzj">咨询专家</a>
        <a href="javascript:toForwordMyzx()" class="myzx">我的咨询</a>
    </div>
    <div class="cszx_tit">
        <h4 style="color: #333333;" onclick="javascript:toForwordZjzxNew()">最新</h4>
        <p style="color: #8a8a8a;" onclick="javascript:toForwordZjzxRm()">大家都在问</p>
    </div>
<div class="cszx_con">
    <%--
        <section>
            <a href="#">
                <h4>五险一金如何在个人所得税前扣除？</h4>
                <p>公司3年都未兑现经营层绩效年薪，每月只领取基薪，是否可以每月不扣除五险一金，等兑现绩效薪时扣除呢？</p></a>
                <h6><span>1小时前</span><em>看过18</em><a href="#">4</a></h6>
        </section>
    --%>
</div>
</div><!--main-->F
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/TouchSlide.1.1.source.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>
    TouchSlide({ slideCell:"#banner_slides",titCell:'.bannerSlide_foncus',mainCell:'.banner_slideContainer',autoPage:'<span></span>',effect:'leftLoop',delayTime:600,interTime:5000,autoPlay:true,titOnClassName:'active' });
</script>
</body>
</html>
