<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <title>财税咨询</title>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        <%--var ztid = "${ztid}";--%>
        var userid = "${userinfo.uid}";
        var nickname = "${userinfo.contacts}";
        var wx_key = "${wx_key}";
        var openid = '${openid}';
    </script>
    <base target="_self">
    <style>
        .wzx-box{margin: 0;padding: 0;display: none;padding-top: 3rem;}
        .wzx-box .wzx-img{display: block;margin:0 auto;width:2.2rem;}
    </style>
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
    <div class="wzx-box">
        <img class="wzx-img" src="<%=ctx%>/static/images/mobile/nomessage.png" />
    </div>
    <input type="hidden" id="openid" value="${openid}">
    <div class="myzx_main clearfix">
        <div class="myzx">
            <%--<section><a href="#">
                <h4>五险一金如何在个人所得税前扣除？</h4>
                <p><span>1小时前</span>请等待解答</p>
            </a></section>
            <section><a href="#">
                <h4>出售自己使用过的汽车如何进行账务核算？</h4>
                <p><span>2017-01-02</span>已解答</p>
            </a></section>
            <section><a href="#">
                <h4>五险一金如何在个人所得税前扣除？</h4>
                <p><span>2017-01-02</span>已解答</p>
            </a></section>
            <section><a href="#">
                <h4>出售自己使用过的汽车如何进行账务核算？</h4>
                <p><span>2017-01-02</span>已解答</p>
            </a></section>
            <section><a href="#">
                <h4>五险一金如何在个人所得税前扣除？</h4>
                <p><span>2017-01-02</span>已解答</p>
            </a></section>--%>
        </div>
    </div><!--main-->
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/TouchSlide.1.1.source.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>
    TouchSlide({ slideCell:"#banner_slides",titCell:'.bannerSlide_foncus',mainCell:'.banner_slideContainer',autoPage:'<span></span>',effect:'leftLoop',delayTime:600,interTime:5000,autoPlay:true,titOnClassName:'active' });
</script>
</body>
</html>
