<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <title>财税咨询</title>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var ztid = "${ztid}";
        var wx_key = "${wx_key}";
        var openid = '${openid}';
    </script>
    <base target="_self">
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<input type="hidden" id="openid" value="${openid}">
<div class="cszx_detail_main clearfix">
    <div class="cszx_detail">
        <div class="cszx_detail_ask">
            <h4><span>问:</span>${cszxInfo.content}</h4>
            <p>提问时间: ${cszxInfo.replytime1}</p>
        </div>
        <c:forEach items="${reCszxList}" var="reCszx">
            <div class="cszx_detail_ans">
                <img src="<%=ctx%>/static/images/mobile/cszx_zjhf.png" alt="" />
                <div class="cszx_detail_con">
                    <h4>${reCszx.content}</h4>
                    <p>回答时间: ${reCszx.replytime1}</p>
                </div>
            </div>
        </c:forEach>
        <%--<div class="cszx_detail_ans">--%>
            <%--<img src="<%=ctx%>/static/images/mobile/cszx_zjhf.png" alt="" />--%>
            <%--<div class="cszx_detail_con">--%>
                <%--<h4>根据国家税务总局关于发布房地产开发企业销售自行开发的房地产项目增值税征收管理暂行办法的公告（国家税务总局公告2016年第18号)的规定，一般纳税人销售自行开发的房地产项目，其2016年4月30日前收取并已向主管地税机关申报缴纳营业税的预收款未开具营业税发票的，可以开具增值税普通发票，不得开具增值税专用发票。</h4>--%>
                <%--<p>回答时间: 2017-02-25</p>--%>
            <%--</div>--%>
        <%--</div>--%>
        <%--<div class="cszx_detail_ans">--%>
            <%--<img src="<%=ctx%>/static/images/mobile/cszx_zjhf.png" alt="" />--%>
            <%--<div class="cszx_detail_con">--%>
                <%--<h4>根据国家税务总局关于发布房地产开发企业销售自行开发的房地产项目增值税征收管理暂行办法的公告（国家税务总局公告2016年第18号)的规定，一般纳税人销售自行开发的房地产项目，其2016年4月30日前收取并已向主管地税机关申报缴纳营业税的预收款未开具营业税发票的，可以开具增值税普通发票，不得开具增值税专用发票。</h4>--%>
                <%--<p>回答时间: 2017-02-25</p>--%>
            <%--</div>--%>
        <%--</div>--%>
        <%--<div class="cszx_detail_ans">--%>
            <%--<img src="<%=ctx%>/static/images/mobile/cszx_zjhf.png" alt="" />--%>
            <%--<div class="cszx_detail_con">--%>
                <%--<h4>根据国家税务总局关于发布房地产开发企业销售自行开发的房地产项目增值税征收管理暂行办法的公告（国家税务总局公告2016年第18号)的规定，一般纳税人销售自行开发的房地产项目，其2016年4月30日前收取并已向主管地税机关申报缴纳营业税的预收款未开具营业税发票的，可以开具增值税普通发票，不得开具增值税专用发票。</h4>--%>
                <%--<p>回答时间: 2017-02-25</p>--%>
            <%--</div>--%>
        <%--</div>--%>
    </div>
</div><!--main-->
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/TouchSlide.1.1.source.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>
    TouchSlide({ slideCell:"#banner_slides",titCell:'.bannerSlide_foncus',mainCell:'.banner_slideContainer',autoPage:'<span></span>',effect:'leftLoop',delayTime:600,interTime:5000,autoPlay:true,titOnClassName:'active' });
</script>
</body>
</html>
