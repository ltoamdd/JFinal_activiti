<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <title>财税咨询</title>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = '${wx_key}';
        var openid = '${openid}';
        <%--var ztid = "${ztid}";--%>
    </script>
    <base target="_self">
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<form id="fbForm" action="<%=ctx%>/cszx/cszx_addzxzj" method="post">
<input type="hidden" id="openid" name="openid" value="${openid}">
<input type="hidden" id="uid" name="uid" value="${userinfo.uid}">
<input type="hidden" id="typename" name="typename" value="财务处理">
<div class="main clearfix">
    <div class="cszx_zxzj">
        <select id="type" name="type" style="width: 100%;height: 40px;margin-bottom:0.2rem;" onchange="changeSelect(this)">
            <option value="684">财务处理</option>
            <option value="685">报表填报</option>
            <option value="689">综合类</option>
            <option value="687">小税种</option>
            <option value="101">个人所得税</option>
            <option value="688">出口退税</option>
            <option value="686">流转税</option>
            <option value="100">企业所得税</option>
        </select>

        <textarea name="content" rows="6" wrap="hard" placeholder="详细描述你的问题，将有利于获得更有参考价值答案。"></textarea>
        <%--<div class="cszx_file"><img src="../images/mobile/cszx_file.png" alt="" /><p>可上传图片</p></div>--%>
        <%--<input type="file" id="" name="" accept="image/*;capture=camera" />--%>
        <%--<div id="div_img"></div>--%>
        <button type="button" onclick="fbcszx()">发布</button>
        <%--<a href="#">查看免责声明&nbsp;></a>--%>
    </div>
</div><!--main-->
</form>
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.form.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/TouchSlide.1.1.source.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>
    TouchSlide({ slideCell:"#banner_slides",titCell:'.bannerSlide_foncus',mainCell:'.banner_slideContainer',autoPage:'<span></span>',effect:'leftLoop',delayTime:600,interTime:5000,autoPlay:true,titOnClassName:'active' });

    function changeSelect(sel){
        var _sel = sel;
        document.getElementById("typename").value=_sel.selectedOptions[0].innerText;
    }

</script>
</body>
</html>
