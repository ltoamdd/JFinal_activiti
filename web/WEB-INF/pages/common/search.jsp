<%--
    auther： 闫霄宇
    time  ： 2017-09-01
    page  ： 搜索页面
--%>

<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <base target="_self">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">

    <title>搜索</title>

    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>

    <%String ctx = request.getContextPath();%>
    <script>
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
    </script>

    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>

    <style>
        body {
            background: #eaedf1
        }
        .hyfw_search_main {
            background: #fff;
            border-bottom:1px solid #E6E6E6;
            overflow: hidden;
            height: 49px;
            top: 0;
        }
        .hyfw_search {
            width:auto;
            margin:10px;
        }
        #search {
            background: url(../static/images/mobile/icon_search.png) 0.3rem center no-repeat #EAEBED;
            background-size: 0.3rem 0.3rem;
            height: 30px;
            color:#333
        }
        .hyfw_search button{
            background: no-repeat;
            font-size: 0.25rem;
            color: #50CE51;
            outline: none;
        }
        .hottag {
            position: relative;
            top: 50px;
            margin: 0.2rem 0.42rem 0;
            font-size: 0.3rem;
        }
        .hottag p {
            color: #949494;
            margin-bottom: 0.2rem;
        }
        .hottag li {
            float: left;
            background: #E6E6E6;
            margin-right: 0.2rem;
            margin-bottom:0.2rem;
        }
        .hottag li a {
            display: block;
            padding:0.2rem 0.3rem;
            color: #333333;
        }
    </style>
</head>
<body>

    <div class="hyfw_search_main">
        <div class="hyfw_search clearfix">
            <input type="hidden" id="openid" name="openid" value="${openid}">
            <input type="text" name="search" id="search" value="${search}" placeholder="课程 | 课程标题 | 热门标签 | 行业" />
            <button>取消</button>
        </div>
    </div>

    <div class="hottag">
        <p>热门标签</p>
        <ul class="clear">
            <li><a href="">政策解读</a></li>
            <li><a href="">往期专题</a></li>
            <li><a href="">专题会计实操</a></li>
            <li><a href="">会计准则与实务报表</a></li>
            <li><a href="">往期专题</a></li>
            <li><a href="">税收优惠</a></li>
            <li><a href="">申报表</a></li>
            <li><a href="">财税报表解读</a></li>
        </ul>
    </div>

        <%--<div id="articleList" class="hyfw_jd">--%>
        <%--</div>--%>


    <%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
    <%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
</body>
</html>