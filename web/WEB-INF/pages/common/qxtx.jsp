<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page import="com.qysoft.csfw.platform.entity.FWH" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <title>提示信息</title>
    <%
        String ctx = request.getContextPath();
        FWH fwh = (FWH) request.getSession().getAttribute("fwh");
        String key = fwh==null?"csthyfw":fwh.getKey();

    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = '${wx_key}';
    </script>
    <base target="_self">
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<input type="hidden" id="openid" value="${openid}" />
<input type="hidden" id="errorCode" value="${errorCode}" />
<input type="hidden" id="backUrl" value="${backUrl}" />
<div class="main clearfix">
    <div class="qxts_main">
        <div id="qxts_title" class="qxts_tit"><p>系统将在<span id="time">10</span>秒后跳转，如长时间未反应，请点击<a id="backBtn" href="#">此处</a>。</p></div>
        <div class="qxts">
            <h4>尊敬的用户您好：</h4>
            <p id="msg">您的权限尚且无法访问该页面，请及时联系您的客户经理进行升级！</p>
            <% if("nmhxhyfw".equals(key)) { %>
                <h6>详情请拨打：<a href="tel:04713281778">0471-3281778</a></h6>
            <% } else if("gh_74acb48d4fda".equals(key)) { %>
            	<h6>详情请拨打：<a href="tel:02584537752">025-84537752</a></h6>
            <% } else if("gh_a35069f56858".equals(key)) { %>
        		<h6>详情请拨打：<a href="tel:4007008895">4007008895-8</a></h6>
            <% } else if("gh_f8963ae972b7".equals(key)) { %>
                <h6>详情请拨打：<a href="tel:09513915627">0951-3915627</a></h6>
        	<% }else {%>
                <h6>详情请拨打：<a href="tel:4006644360">4006644360</a></h6>

            <% } %>
        </div>
    </div>
</div><!--main-->
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
</body>
</html>