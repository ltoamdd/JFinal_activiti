<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>会员绑定</title>
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <base target="_self">
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
        var openid = "${openid}";
    </script>
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<div class="main clearfix">
    <input type="hidden" id="openid" name="openid" value="${openid}" />
    <input type="hidden" id="wx_key" name="wx_key" value="${wx_key}" />
    <input type="hidden" id="toUrl" name="toUrl" value="${toUrl}" />
    <%--<input type="hidden" id="articleIdVal" name="articleIdVal" value="${articleIdVal}" />--%>
    <%--<input type="hidden" id="articleIdName" name="articleIdName" value="${articleIdName}" />--%>

    <%-- 税干学苑微信号 不显示顶部图片 --%>
    <c:if test="${wx_key != 'gh_18a9a687b51a' }" >
        <div class="hybd_banner"><a href="#"><img src="<%=ctx%>/static/images/mobile/hybd_banner.png" alt="" /></a></div>
    </c:if>
    <div class="hybd_main">
        <ul>
            <%--<li>
                <p>纳税人识别号</p>
                <input type="text" class="hybd-input" id="nsrsbh" name="nsrsbh" placeholder="请输入纳税人识别号" />
                <span class="input_reset" id="input_reset1"></span>
            </li>--%>
            <li>
                <c:choose>
                    <c:when test="${wx_key != 'gh_18a9a687b51a' }" >
                        <p>纳税人识别号|会员卡号|手机号</p>
                        <input type="text" class="hybd-input" id="username" name="username" placeholder="纳税人识别号|会员卡号|手机号" />
                    </c:when>
                    <c:otherwise>
                        <p>账号</p>
                        <input type="text" class="hybd-input" id="username" name="username" placeholder="账号" />
                    </c:otherwise>
                </c:choose>
                <span class="input_reset" id="input_reset2"></span>
            </li>
            <li>
                <p>密码</p>
                <input type="password" class="hybd-input" id="password" name="password" placeholder="密码" />
                <span class="input_reset" id="input_reset3"></span>
            </li>
        </ul>
        <br>
        <br>
        <%--<h6><span>*</span>为方便您快捷的享受<c:if test="${wx_key != 'gh_18a9a687b51a' }">财税通</c:if>会员专享活动，请绑定会员账号!</h6>--%>
        <a href="javascript:saveHybd();" class="hybd_btn">提交绑定</a>
        <h6 style="text-align:center; font-size: 0.3rem">忘记会员登录信息，请联系客户经理查询</h6>
    </div>
</div><!--main-->
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
</body>
</html>
