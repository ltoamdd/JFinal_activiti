<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <title>会员特权</title>
    <base target="_self">
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
    </script>
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<input type="hidden" id="openid" name="openid" value="${openid}" />
<div class="main clearfix">
    <div class="hytqlx">
        <!-----------------导航-------------------->
        <div class="hytqlx_tit clearfix">
            <ul>
                <li class="active"><h2>会员特权</h2></li>
                <li><h2>会员类型</h2></li>
                <span></span>
            </ul>
        </div>
        <!-----------------正文-------------------->
        <div class="hytqlx_con clearfix">
            <div class="hytqlx_con1">
                <img src="<%=ctx%>/static/images/mobile/hytq1.png" alt="" />
                <img src="<%=ctx%>/static/images/mobile/hytq2.png" alt="" style="margin-top:0.2rem;"/>
                <img src="<%=ctx%>/static/images/mobile/hytq3.png" alt="" />
                <img src="<%=ctx%>/static/images/mobile/hytq4.png" alt="" />
                <img src="<%=ctx%>/static/images/mobile/hytq5.png" alt="" />
                <img src="<%=ctx%>/static/images/mobile/hytq6.png" alt="" />
                <img src="<%=ctx%>/static/images/mobile/hytq7.png" alt="" />
                <img src="<%=ctx%>/static/images/mobile/hytq8.png" alt="" />
            </div>
            <div class="hytqlx_con2">
                <c:choose>
                    <c:when test="${wx_key != 'gh_f8963ae972b7' }" >
                        <img src="<%=ctx%>/static/images/mobile/hylx_pt.png" alt="" />
                        <img src="<%=ctx%>/static/images/mobile/hylx_yk.png" alt="" />
                        <img src="<%=ctx%>/static/images/mobile/hylx_jk.png" alt="" />
                        <img src="<%=ctx%>/static/images/mobile/hylx_bjk.png" alt="" />
                        <img src="<%=ctx%>/static/images/mobile/hylx_zsk.png" alt="" />
                    </c:when>
                    <c:otherwise>
                        <img src="<%=ctx%>/static/images/mobile/hylx_pt_nx.jpg" alt="" />
                        <img src="<%=ctx%>/static/images/mobile/hylx_yk_nx.jpg" alt="" />
                        <img src="<%=ctx%>/static/images/mobile/hylx_jk_nx.jpg" alt="" />
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <%--<div class="hytqlx_bot">
            <div class="hytqlx_bot_zshy"><a href="javascript:void(0);">赠送好友</a></div>
            <div class="hytqlx_bot_kthy"><a href="javascript:kthy();">开通会员</a></div>
        </div>--%>
    </div><!--hytqlx-->
</div><!--main-->

<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
</body>
</html>