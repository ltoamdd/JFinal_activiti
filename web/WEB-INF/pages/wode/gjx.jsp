<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>工具箱</title>
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <base target="_self">
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
    </script>
    <base target="_self">
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<input type="hidden" id="openid" value="${openid}" />
<div class="gjx_main clearfix">
    <div class="gjx">
        <ul>
            <li><a href="javascript:toGjPage(49);">
                <img src="<%=ctx%>/static/images/mobile/gjx_swgj.png" alt="" />
                <p>实务工具</p>
            </a></li>
            <li><a href="javascript:toGjPage(50);">
                <img src="<%=ctx%>/static/images/mobile/gjx_fghb.png" alt="" />
                <p>法规汇编</p>
            </a></li>
            <li><a href="javascript:toGjPage(51);">
                <img src="<%=ctx%>/static/images/mobile/gjx_dyjh.png" alt="" />
                <p>答疑解惑</p>
            </a></li>
            <li><a href="http://www.sx-n-tax.gov.cn/xydjcx/">
                <img src="<%=ctx%>/static/images/mobile/gjx_gsfpcx.png" alt="" />
                <p>信用级别查询</p>
            </a></li>
            <%--<li><a href="http://218.26.181.88/SxhtWeix/mobile/fpcx/gs/fpcx.jsp">
                <img src="<%=ctx%>/static/images/mobile/gjx_gsfpcx.png" alt="" />
                <p>国税发票查询</p>
            </a></li>
            <li><a href="http://218.26.181.88/SxhtWeix/mobile/fpcx/ds/fpcx.jsp">
                <img src="<%=ctx%>/static/images/mobile/gjx_dsfpcx.png" alt="" />
                <p>地税发票查询</p>
            </a></li>--%>
            <li><a href="http://www.gerensuodeshui.cn/">
                <img src="<%=ctx%>/static/images/mobile/gjx_xbgsjsq.png" alt="" />
                <p>新版个税计算器</p>
            </a></li>
            <%--<li><a href="http://brccalc.duapp.com/tools/287.php">
                <img src="<%=ctx%>/static/images/mobile/gjx_wxyjjsq.png" alt="" />
                <p>五险一金计算器</p>
            </a></li>--%>
            <li><a href="http://baidu.kuaidi100.com/index2.html?bd_user=0&bd_sig=7215b099855fb4735a9f1b5d6f286fd7&canvas_pos=platform">
                <img src="<%=ctx%>/static/images/mobile/gjx_kdwlcx.png" alt="" />
                <p>快递物流查询</p>
            </a></li>
            <%--<li><a href="http://www.lyh8.cn/jsq/shuifei/yhs.htm?bd_user=0&bd_sig=4df5b04ac894e5836cd7d89ae4ee8a24&canvas_pos=platform">
                <img src="<%=ctx%>/static/images/mobile/gjx_yhsjsq.png" alt="" />
                <p>印花税计算器</p>
            </a></li>--%>
        </ul>
    </div>
</div><!--main-->

<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
</body>
</html>