<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>会员解绑</title>
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <%--<script type="text/javascript" src="../js/mobile/jquery.js"></script>--%>
    <%--<script type="text/javascript" src="../js/mobile/index.js"></script>--%>
    <base target="_self">
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
        var openid = "${openid}";
    </script>
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<div class="main clearfix">
    <input type="hidden" id="openid" name="openid" value="${hybd.openid}" />
    <input type="hidden" id="wx_key" name="wx_key" value="${wx_key}" />
    <input type="hidden" id="uid" name="uid" value="${hybd.uid}" />
    <c:if test="${wx_key != 'gh_18a9a687b51a' }" >
        <div class="hybd_banner"><a href="#"><img src="<%=ctx%>/static/images/mobile/hybd_banner.png" alt="" /></a></div>
    </c:if>
    <div class="hybd_main">
        <ul>
            <li>
                <p>纳税人识别号</p>
                <input type="text" id="nsrsbh" name="nsrsbh" value="${hybd.taxpayer}" placeholder="请输入纳税人识别号" readonly>
            </li>
            <li>
                <p>会员卡号</p>
                <input type="text" id="username" name="username" value="${hybd.username}" placeholder="请输入会员卡号" readonly>
            </li>
        </ul>
        <a href="javascript:jchybd();" class="hyjb_btn">解绑会员</a>
    </div>
</div><!--main-->
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
</body>
</html>
