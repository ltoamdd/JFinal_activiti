<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>${title}</title>
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <base target="_self">
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
    </script>
    <base target="_self">
    <style>
        .hyfw_jd{
            padding: 0.4rem 0.4rem 0.2rem;
        }
    </style>
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<input type="hidden" id="openid" value="${openid}" />
<input type="hidden" id="cid" value="${cid}" />
<div class="main clearfix">
    <div class="hyfw_jd" id="dataList">
        <%--<section>
            <a href="#">
                <h4>关于《国家税务总局国土资源关于落实资源改革优惠政策若干事项的公告》的解读</h4>
                <p>2017-02-01
                    <span>阅读890</span>
                </p>
            </a>
        </section>
        <section>
            <a href="#">
                <h4>关于《国家税务总局国土资源关于落实资源改革优惠政策若干事项的公告》的解读</h4>
                <p>2017-02-01
                    <span>阅读890</span>
                </p>
            </a>
        </section>--%>
    </div>
</div>
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
</body>
</html>