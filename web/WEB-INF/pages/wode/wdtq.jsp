<%--
  Created by IntelliJ IDEA.
  User: WangPengfei
  Date: 2017/3/17
  Time: 16:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>我的特权</title>
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <base target="_self">
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
    </script>
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
    <input type="hidden" id="openid" name="openid" value="${openid}" />
    <div class="hyxx_main clearfix">
        <c:if test="${isHybd}" >
            <div class="hyxx_basic">
                <img src="<%=ctx%>/static/images/mobile/hyxx_tx.png" alt="" />
                <div class="hyxx_xx"><h4>${bean.contacts}</h4>
                    <c:choose>
                        <c:when test="${bean.cha > 0}" >
                            <p>会员还有<span>${bean.cha}</span>天到期</p>
                        </c:when>
                        <c:otherwise>
                            <p><span>会员已到期</span></p>
                        </c:otherwise>
                    </c:choose>
                        <%--<a href="javascript:ljxf();">立即续费</a>--%>
                </div>
            </div>
            <div class="hyxx_date"><p>${bean.gname}有效期至: ${bean.endtime}</p></div>
        </c:if>

        <div class="hytq">
            <div class="hytq_tit"><h4>会员特权</h4><a href="javascript:gdtq();">更多特权</a></div>
            <div class="hytq_con">
                <ul>
                    <li><a href="javascript:void(0);" class="hytq_ssfxfk">涉税风险防控</a></li>
                    <li><a href="javascript:void(0);" class="hytq_gqkc">高清课程</a></li>
                    <li><a href="javascript:void(0);" class="hytq_zcjd">政策解读</a></li>
                    <li><a href="javascript:void(0);" class="hytq_zjdy">专家答疑</a></li>
                    <li><a href="javascript:void(0);" class="hytq_swal">实务案例</a></li>
                    <li><a href="javascript:void(0);" class="hytq_wfw">微服务</a></li>
                    <li><a href="javascript:void(0);" class="hytq_zcfg">政策法规</a></li>
                    <li><a href="javascript:void(0);" class="hytq_jpqk">精品期刊</a></li>
                </ul>
            </div>
        </div>
    </div>
    <%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
    <%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
    <%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
    <%=RenderHelper.includedAutoJavascript(request) %>
</body>
</html>
