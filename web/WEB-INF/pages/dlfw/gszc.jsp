<%@ page import="com.qysoft.rapid.utils.RenderHelper" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    />
    <title>工商注册</title>

    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var openid = "${openid}";
    </script>
    <%=RenderHelper.includedStyle(request, "/static/dlfw/dljz.min.css") %>
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/dlfw/area/LArea.css") %>
</head>

<body>
<input type="hidden" id="provinceId" name="provinceId" >
<input type="hidden" id="areaId" name="areaId" >
<input type="hidden" id="zclxId" name="zclxId" >
<img src="<%=ctx%>/static/dlfw/images/banner.jpg" alt="" id="banner">
<div id="dj">
    <div class="select active">
        <span>所属区域：</span>
        <input type="text" id="qymc" placeholder="选择注册区域" readonly="readonly">
    </div>
    <div class="select active">
        <span>注册类型：</span>
        <input type="text" id="zclx" placeholder="选择服务类型" readonly="readonly">
    </div>
    <button class="button button-block button-blue button-radius" onclick="xiyibu();">下一步</button>

</div>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/jquery-3.2.1.min.js") %>
<%=RenderHelper.includedJavascript(request, "/static/dlfw/dzfw.min.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedJavascript(request, "/static/dlfw/area/LArea.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>
    $("#qymc").focus(function(){
        $(this).blur();
    })
</script>
</body>

</html>