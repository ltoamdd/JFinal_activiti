<%@ page import="com.qysoft.rapid.utils.RenderHelper" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    />
    <title>开票托管</title>

    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>"
        var openid = '${openid}';
    </script>
    <%=RenderHelper.includedStyle(request, "/static/dlfw/dljz.min.css") %>
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/dlfw/area/LArea.css") %>
    <style>
        #dj>img {
            width: 4.5rem;
        }
        .button-groups>.button:not(:last-child):after{
            background: transparent;
        }
        #dj>img {
            margin: 0.3rem auto;
        }
        #dj .select.active.no-icon:after {
            content: '';
        }
    </style>
</head>

<body>
<input type="hidden" id="areaId" name="areaId" >
<input type="hidden" id="provinceId" name="provinceId" >
<div id="dj">
    <div class="select active">
        <span>所属区域：</span>
        <input type="text" id="qymc" placeholder="选择注册区域" readonly/>
    </div>
    <div class="select active no-icon">
        <span>服务类型：</span>
        <span>开票托管</span>
    </div>

    <img id="imgpath" src="<%=ctx%>/static/dlfw/images/img-kptg.jpg" alt="">
</div>


<div class="button-groups" id="dj-groups" >
    <button class="button button-info" id="qian">500元起/年</button>
    <button class="button button-blue" onclick="zixun();"><span class="iconfont icon-dianhua"></span>立即咨询</button>
</div>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/jquery-3.2.1.min.js") %>
<%=RenderHelper.includedJavascript(request, "/static/dlfw/dzfw.min.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedJavascript(request, "/static/dlfw/area/LArea.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>
    $("#qymc").focus(function(){
        $(this).blur();
    })
</script>
</body>

</html>