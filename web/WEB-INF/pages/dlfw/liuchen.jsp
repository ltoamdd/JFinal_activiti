<%@ page import="com.qysoft.rapid.utils.RenderHelper" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="singlePage">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    />
    <title>注册</title>

    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var openid = '${openid}';
    </script>
    <%=RenderHelper.includedStyle(request, "/static/dlfw/dljz.min.css") %>
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
</head>

<body>
<img src="<%=ctx%>/static/dlfw/images/banner.jpg" alt="" id="banner">
<div id="regedit-list">
    <ul>
        <li>
            <p>办理流程</p>
            <div><img src="<%=ctx%>/static/dlfw/images/img.jpg" alt=""></div>
        </li>
        <li>
            <p>准备资料</p>
            <div>
                <ul id="ziliao-list">
                    <li>1、公司名称</li>
                    <li>2、注册地址房产证及房主身份证复印件</li>
                    <li>3、全体股东身份证原件 </li>
                    <li>4、全体股东出资比例</li>
                    <li>5、公司经营范围</li>
                </ul>
            </div>
        </li>
    </ul>



</div>
<a class="button button-block button-blue button-fixed-bottom" href="tel:${phone}">
    <span class="iconfont icon-dianhua"></span>
    <span>免费预约代理注册</span>
</a>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/jquery-3.2.1.min.js") %>
<%=RenderHelper.includedJavascript(request, "/static/dlfw/dzfw.min.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
</body>

</html>