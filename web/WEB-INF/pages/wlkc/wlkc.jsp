<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%
	String ctx = request.getContextPath();
%>
<script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
        var MFKC="${mfkc}";
        var XSMF="${xsmf}";
        var STKC="${stkc}";
</script>
<title>网络课程</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<%=RenderHelper.includedStyle(request, "/static/kcb/stylesheet/index.css")%>
<%=RenderHelper.includedStyle(request, "/static/kcb/font/iconfont.css")%>
<%=RenderHelper.includedStyle(request, "/static/kcb/javascript/swiper/dist/css/swiper.min.css")%>
<%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css")%>
<%=RenderHelper.includedStyle(request, "/static/css/base.css")%>
<%=RenderHelper.includedStyle(request, "/static/css/csfw.css")%>
<style>
.list-search>ul {
	padding:0 0.15rem;
}
.list-search>ul>li {
	width:25%;
	flex:none;
}
a[data-type='list'] {
	display: block;
	position: relative;
	text-align: center;
	overflow: hidden;
}

a[data-type='list']:after {
	content: "";
	display: block;
	clear: both;
}


a[data-type='list']>span {
	font-size: 0.28rem;
	text-overflow: ellipsis;
	white-space: nowrap;
	overflow: hidden;
	display: inherit;
}


.list-search>ul>li>.overflow-list>ul>li.active {
	/* background-color: #ccc; */
	
}

.list-search>ul>li>.overflow-list>ul>li.active>a{
	color: red;
}
.list-main {
    position: absolute;
    top: 101px;
}
.nav-list {
    position: fixed;
    width: 100%;
    background-color: #fff;
    z-index: 1;
    border-bottom: 1px solid #E6E6E6;
}
.list-search {
     top: 51px;
    position: fixed;
    border-bottom: 1px solid #E6E6E6;
    background-color: #fff;
    z-index: 3;
    width: 100%;
}
</style>
</head>
<body>
	<input type="hidden" id="openid" name="openid" value="${openid}">
	<input type="hidden" id="dj" name="dj" value="${dj}">
	<nav class="nav-list">
		<a href="javascript:backShuye();" class="pull-left"> <span
			class="iconfont icon-zuojiantou"></span>
		</a> <a href="javascript:openidChaxun();" class="pull-right"> <span
			class="iconfont icon-sousuo"></span>
		</a>
		<p class="nav-title">网络课程</p>
	</nav>
	<section class="list-search">
		<ul>
			<li><a href="javascript:shuizhong('sz');" data-type='list'><span class="iconfont icon-jiantou3">税种</span></a>
				<div id="divsz" class="overflow-list">
					<ul >
						<li><a href="javascript:initWlkcDataSz('0', '税种');">全部</a></li>
						<c:forEach items="${kindsList}" var="kinds">
							<c:if test="${kinds.kindsvalue == 'wlkc_shuiz'}">
								<c:forEach items="${kinds.fllist}" var="fllist"
									varStatus="status">
									<li><a
										href="javascript:initWlkcDataSz('${fllist.realvalue}', '${fllist.keyname}');">${fllist.keyname}</a></li>
								</c:forEach>
							</c:if>
						</c:forEach>
					</ul>
				</div>
				</li>
			<li><a href="javascript:zhpx();" data-type='list'><span class="iconfont icon-jiantou3">最新</span></a>
				<div id="zhpx" class="overflow-list">
					<ul>
						<li class='active'><a href="javascript:initWlkcDataZhpx('zr', '最热');" >最热</a></li>
						<li><a href="javascript:initWlkcDataZhpx('zx', '最新');">最新</a></li>
					</ul>
				</div></li>
			<li>
			<c:if test="${dj=='4'}">
				<a href="javascript:hyqx();" data-type='list' ><span class="iconfont icon-jiantou3">金卡会员</span></a>
			</c:if>
			<c:if test="${dj=='2'}">
				<a href="javascript:hyqx();" data-type='list'><span class="iconfont icon-jiantou3">普通会员</span></a>
			</c:if>
			<c:if test="${dj=='3'}">
				<a href="javascript:hyqx();" data-type='list'><span class="iconfont icon-jiantou3">银卡会员</span></a>
			</c:if>
			<c:if test="${dj=='0'}">
				<a href="javascript:hyqx();" data-type='list'><span class="iconfont icon-jiantou3">免费课程</span></a>
			</c:if>
				<div id="hyqx" class="overflow-list">
					<ul>
						<li ><a href="javascript:initWlkcDataHyqx('', '会员权限','yhqx');">全部</a></li>
						<c:forEach items="${kindsList}" var="kinds">
							<c:if test="${kinds.kindsvalue == 'dj'}">
								<c:forEach items="${kinds.fllist}" var="fllist"
									varStatus="status">
									<c:choose>
										<c:when test="${fllist.realvalue==dj}">
											<li class='active'><a 
										href="javascript:initWlkcDataHyqx('${fllist.realvalue}', '${fllist.keyname}','yhqx');">${fllist.keyname}</a></li>
										</c:when>
										
										<c:otherwise>
											<li><a 
										href="javascript:initWlkcDataHyqx('${fllist.realvalue}', '${fllist.keyname}','yhqx');">${fllist.keyname}</a></li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:if>
						</c:forEach>
					</ul>
				</div>
			</li>
			<li>
			<a id="collapse"  data-type='list'>
				<span id="qb" class="iconfont icon-jiantou3">全部</span>
			</a>
			</li>
		</ul>
	</section>
	<ul class="list-main" id="ztxkul"></ul>
	<div class="nomsg">
		<span>没有更多内容</span>
	</div>
	<div id="panel-left" class="panel panel-left">
		<div class="panel-body">
			<dl id="fenlei-list">
				<c:forEach items="${kindsList}" var="kinds">
					<dt class="active">
						<p class="pull-left">${kinds.kindsname}</p>
						<i class="pull-right iconfont icon-jiantou"></i>
					</dt>
					<dd>
						<ul data-value="${kinds.kindsvalue}">
							<c:forEach items="${kinds.fllist}" var="fllist"
								varStatus="status">
								<li
									<c:if test="${kinds.fllistCount<=status.count}">class="noBorder"</c:if>
									data-value="${fllist.realvalue}"
									data-keyname="${fllist.keyname}">
									<a href="javascript:void(0);">${fllist.keyname}</a></li>
							</c:forEach>
						</ul>
					</dd>
				</c:forEach>
			</dl>
		</div>
		<div class="panel-footer">
			<button id="reset-search">
				<i class="iconfont icon-zhongzhi"></i>重置
			</button>
			<button id="closed-search" style="background-color: red;color:white;">
				<i class="iconfont icon-guanbi1"></i>确定
			</button>
		</div>
	</div>
	<div id="panel-right" class="panel panel-right">
		<div class="panel-body">
			<section class="searchbar">
				<div class="serachbar-tools">
					<i class="iconfont icon-sousuo" onclick="sousuo();"></i> 
					<input  type="text" placeholder=" 搜索从这里开始......" id="search_title" />
				</div>
				<button class="btn" id="closed-btn">取消</button>
			</section>

			<h3>热门搜索</h3>
			<ul class="hot-list">
				<c:forEach items="${kindsList}" var="kinds">
					<c:if test="${kinds.kindsvalue == 'wlkc_rem'}">
						<c:forEach items="${kinds.fllist}" var="fllist" varStatus="status">
							<li><a
								href="javascript:searchAsRem('${fllist.realvalue}', '${fllist.keyname}');">${fllist.keyname}</a></li>
						</c:forEach>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	</div>
	<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
	<script>
		var winWidth = window.innerWidth;
		var size = (winWidth / 750) * 100;
		document.documentElement.style.fontSize = (size < 100 ? size : 100)
				+ 'px';
	</script>
	<script type="text/javascript">
		function openidChaxun() {
			$("#panel-right").addClass('active');
			$("#list,body").css({
				'height' : $(window).height(),
				'overflow' : 'hidden'
			});
		}
		$(function() {
			$("#collapse").click(function() {
				$("body").css('overflow','hidden')
				$("#divsz").slideUp();
				$("#zhpx").slideUp();
				$("#hyqx").slideUp();
				$("#panel-left").addClass('active');
				$("#list,body").css({
					'height' : $(window).height(),
					'overflow' : 'hidden'
				});

			});
			$("#closed-btn").click(function() {
				$("#panel-right").removeClass('active');
				$("#list,body").css({
					'height' : 'auto',
					'overflow' : 'auto',
					'overflow-x' : 'hidden'
				});
			});
			$("#search_title").keypress(function(e) {
				console.log(e);
				console.log($(this).val());
				if (e.keyCode == 13) {
					postData.dj = 0;
					postData.pageNum = 1;
					postData.start = 0;
					postData.search_title = $(this).val();
					postData.hot_label = '';
					initWlkcList();
					$("#panel-right").removeClass('active');
				}
			})
			$("#reset-search").click(function() {
				$("#panel-left a").css('color', '#333');
				$("#qb").text("全部");
				postData.pageNum = 1;
				postData.start = 0;
				postData.dj = 0;
				postData.hot_label=0;
			});

			$("#closed-search").click(function() {
				$("#panel-left").removeClass('active');
				$("#list,body").css({
					'height' : 'auto',
					'overflow' : 'auto'
				});
				initWlkcList();
			});
			
			$("#closed-btn").click(function() {
				$("#panel-right").removeClass('active');
				$("#list,body").css({
					'height' : 'auto',
					'overflow' : 'auto',
					'overflow-x' : 'hidden'
				});
			});
			//收起来放开
			$("#panel-left").on('click', 'dt', function() {
				if ($(this).hasClass('active')) {
					$(this).removeClass('active')
				} else {
					$(this).addClass('active');
				}
			});

			$("#panel-left").on('click', 'a', function() {
				/* $("#panel-left").removeClass('active');
				$("#list,body").css({
					'height' : 'auto',
					'overflow' : 'auto'
				}); */

				$("#panel-left a").css('color', '#333');
				$(this).css('color', 'red');
 
				var _li = $(this).parent();
				var _ul = $(this).parent().parent();

				postData.pageNum = 1;
				postData.start = 0;
				if (_ul.attr("data-value") == "dj") {
					postData.hot_label = 0;
					var _rankDj = _li.attr("data-value");
					postData.dj = _rankDj;
					$("#hyqx").prev().children('span:first').text("会员权限")
					$("#hyqx").children().children('li').removeClass("active");
				} else {
					var _rankrealValue = _li.attr("data-value");
					postData.dj = "";
					postData.hot_label = _rankrealValue + '';
				}
				if(_ul.attr("data-value") == "wlkc_shuiz"){
					$("#divsz").prev().children('span:first').text("税种")
					$("#divsz").children().children('li').removeClass("active");
				}
				if(_ul.attr("data-value") == "wlkc_hangy"){
					$("#divsz").prev().children('span:first').text("税种")
					$("#divsz").children().children('li').removeClass("active");
				}
				if(_ul.attr("data-value") == "wlkc_rem"){
					$("#divsz").prev().children('span:first').text("税种")
					$("#divsz").children().children('li').removeClass("active");
				}
				var data_keyname = _li.attr("data-keyname");
				$("#qb").text(data_keyname);
			});
		});
	</script>
</body>
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js")%>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js")%>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js")%>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/commonIndex.js")%>
<%=RenderHelper.includedAutoJavascript(request) %>
</html>
