<%--
  Created by IntelliJ IDEA.
  User: WangPengfei
  Date: 2017/3/9
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/kcb/stylesheet/index.css") %>
    <%=RenderHelper.includedStyle(request, "/static/kcb/font/iconfont.css") %>
    <%=RenderHelper.includedStyle(request, "/static/kcb/javascript/swiper/dist/css/swiper.min.css") %>
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>

    <title>直播课程</title>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
        var openid = "${openid}";
    </script>
    <base target="_self">
    <style>
        .nomessage {
            text-align: center;
            margin-top: 20px;
            font-size: 29px;
        }
        .zbkc_list_tit ul li h2 {
        	margin:0;
        	width:auto;
        	text-align: center;
        }
    </style>
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
    <style>
        .zbkc_list_tit ul li h2 {
        	margin:0;
        	width:auto;
        	text-align: center;
        	font: 0.28rem/50px "Microsoft Yahei";
        }
        .zbkc_list_tit {
        height:50px}
        .zbkc_list_tit span {
        	height:50px
        }
        .list-main a>img {
        	width:2.65rem;
        	height:1.7rem
        }
    </style>
</head>
<body>
<input type="hidden" id="openid" name="openid" value="${openid}">
<nav class="nav-list">
	<a href="javascript:backShuye();" class="pull-left"> 
	<span class="iconfont icon-zuojiantou"></span>
	</a> 
	<!-- <a href="javascript:openidChaxun();" class="pull-right"> 
	<span class="iconfont icon-sousuo"></span>
	</a> -->
	<p class="nav-title">直播预告</p>
</nav>
	
<div class="main clearfix">
    <div class="zbkc_list">
        <div class="zbkc_list_tit">
            <ul>
                <li class="active"><h2>直播预告</h2></li>
                <li onclick="toZbkc_hk()"><h2>历史直播</h2></li>
            </ul>
            <span></span>
        </div>
        <ul class="list-main" id="ztxkul">
		</ul>
    </div>
</div><!--main-->
</body>
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/jquery.fancyspinbox.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/commonIndex.js")%>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
</html>
