<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: WangPengfei
  Date: 2017/3/9
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/csfw.css") %>
    <%=RenderHelper.includedStyle(request, "/static/iconfont/iconfont.css") %>
        <%=RenderHelper.includedStyle(request, "/static/kcb/stylesheet/index.css") %>
    <%=RenderHelper.includedStyle(request, "/static/kcb/font/iconfont.css") %>
    <%=RenderHelper.includedStyle(request, "/static/kcb/javascript/swiper/dist/css/swiper.min.css") %>
    <title>税控实操</title>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
        var openid = "${openid}";
    </script>
    <base target="_self">


    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
    <style>
        .nav-flex {display: flex}
        .nav-title-flex {flex: 1;text-align: center;color:#b4a0a0;position: relative}
        .nav-title-flex.active {color:#386EDC}
        .nav-title-flex span {background: #fff;}
        .nav-title-flex.active span{ background: #F5B450;}
        .nav-list{
        	top:0;
        	width: 100%;
        	position:fixed;
        	background: #fff;
        	z-index: 10;
        }
        .nav{
        	top:51px;
        }
        #wlkc_list{
        	top:101px;
        }
    </style>
</head>
<body>
	<nav class="nav-list">
		<a href="javascript:backShuye();" class="pull-left"> <span
			class="iconfont icon-zuojiantou"></span>
		</a> 
		<!-- <a href="javascript:openidChaxun();" class="pull-right"> <span
			class="iconfont icon-sousuo"></span>
		</a> -->
		<p class="nav-title">税控实操</p>
</nav>
    <nav class="nav nav-flex">
        <p class="nav-title-flex active"  id="wssb" onclick="rjsc_wssb()">网上申报<span class="title-line"></span></p>
        <p class="nav-title-flex " id="skkp" onclick="rjsc_skkp()">税控开票<span class="title-line"></span></p>
        <p class="nav-title-flex" id="rzgx" onclick="rjsc_rzgx()">认证.勾选<span class="title-line"></span></p>
    </nav>
    <div  id="wlkc_list">
        <input type="hidden" id="openid" name="openid" value="${openid}">
         <!-- <div class="wlkc_list_con" id="wlkc_list_con"></div> -->
           <ul class="list-main" id="ztxkul"></ul>
    </div>

</body>
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/commonIndex.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>

    $(function(){
        $(".nav-title-flex").click(function(){
            $(this).addClass('active').siblings().removeClass('active');
        })
    })
</script>

</html>
