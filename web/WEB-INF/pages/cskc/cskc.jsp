<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%
        String ctx = request.getContextPath();
    	%>
<script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key="${wx_key}";
        var openid="${openid}";
    	</script>
<title>财税课程</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<%=RenderHelper.includedStyle(request, "/static/kcb/stylesheet/index.css") %>
<%=RenderHelper.includedStyle(request, "/static/kcb/font/iconfont.css") %>
<%=RenderHelper.includedStyle(request, "/static/kcb/javascript/swiper/dist/css/swiper.min.css") %>
<style type="text/css">
.overflow-push li>a .info {
	margin-top: 8px;
}

.nav-search {
	background-color: #fff;
}

.nav-search .search-group input {
	background-color: #ddd;
}

.nav-search .search-group {
	background-color: #ddd;
}
  
</style>
</head>
<script type="text/javascript">
	//点击热门课程
	</script>
<body>
	<input type="hidden" id="openid" name="openid" value="${openid}">
	<input type="hidden" id="pageRmkc" name="pageRmkc" value="1">
	<input type="hidden" id="allPageRmkc" name="pageRmkc" value="1">
	<input type="hidden" id="pageZxkc" name="pageZxkc" value="1">
	<input type="hidden" id="allPageZxkc" name="pageRmkc" value="1">
	<!-- 搜索条 -->
	<nav class="nav-search">
		<div class="search-group">
			<input style="padding-left: 20px;" type="text" name="titleName"
				id="titleName" placeholder="搜索课程" /> <span
				class="iconfont icon-sousuo" onclick="sousuo();"></span>
		</div>
	</nav>
	<!-- 滑动切换 -->
	<div class="swiper-container">
		<div class="swiper-wrapper">
			<!-- <div class="swiper-slide"><a href="javascript:hdqh('slide1');"><img id="slide1" /></a></div>
	            <div class="swiper-slide"><a href="javascript:hdqh('slide2');"><img id="slide2" /></a></div> -->
			<!--  <div class="swiper-slide"><a href="#"><img id="slide3" /></a></div> -->
		</div>
		<div class="swiper-pagination"></div>
	</div>
	<!-- 分类接口 -->
	<div class="typeenter">
		<div>
			<a href="javascript:wlkc('ztxl');"> <img
				src="static/kcb/image/typeenter/typeenter-1.jpg" alt="" />
				<p>专题系列</p>
			</a>
		</div>
		<div>
			<a href="javascript:wlkc('wlkc');"> <img
				src="static/kcb/image/typeenter/typeenter-2.jpg" alt="" />
				<p>网络课程</p>
			</a>
		</div>
		<div>
			<a href="javascript:wlkc('zbkc');"> <img
				src="static/kcb/image/typeenter/typeenter-3.jpg" alt="" />
				<p>直播课程</p>
			</a>
		</div>

		<c:if test="${wx_key=='csthyfw'}">
			<div>
				<a href="javascript:wlkc('sksc');"> <img
					src="static/kcb/image/typeenter/typeenter-4.jpg" alt="" />
					<p>税控实操</p>
				</a>
			</div>
		</c:if>

		<div>
			<a href="javascript:wlkc('xhss');"> <img
				src="static/kcb/image/typeenter/typeenter-5.jpg" alt="" />
				<p>微课堂</p>
			</a>
		</div>
	</div>
	<!-- 信息切换 -->
	<div class="tabs">
		<div class="tabs-head">
			<span onclick="onlickRmkc();">推荐课程</span> 
				<span onclick="onlickZxkc();">最新课程</span>
				<span onclick="onlickMfkc();">免费课程</span>
		</div>
		<div class="tabs-main">
			<div class="tabs-list" id="first">
				<div class="overflow-push">
					<ul id="rmkcul">
					</ul>
					<!-- <div class="loadmor">
    						<span>下拉显示更多</span>
    					</div> -->
				</div>
			</div>
			<div class="tabs-list" id="second">
				<div class="overflow-push">
					<ul id="zxkcul">
						<!-- <li>
    							<a href="">
    								<div><img src="static/kcb/image/test.jpg" alt="" /><span>开票软件如何升级</span></div>
    								<div class="info"><span>1人</span><span>2017-07-16</span></div>
    							</a>
    						</li>
    						-->
					</ul>
					<!-- <div class="loadmor">
    						<span>下拉显示更多</span>
    					</div> -->
				</div>
			</div>
			<div  class="tabs-list" id="three">
				<div class="overflow-push">
					<ul id="mfkcul">
						
					</ul>
				</div>
			</div>
		
		</div>
	</div>
	<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
	<script src="static/kcb/javascript/swiper/dist/js/swiper.min.js"></script>
	<script src="static/kcb/javascript/iscroll.js"></script>
	<script src="static/kcb/javascript/tabs.js"></script>
	<script>
	    	var winWidth =  window.innerWidth;
            var size = (winWidth / 750) * 100;
            document.documentElement.style.fontSize = (size < 100 ? size : 100) + 'px' 
	    	
		 
		    
		  /*   loadresrfe('#first',function(){
		    	initRmkc();
		    })
		    
		    loadresrfe('#second',function(){
		    	initZxkc();
		    }) */
		    tabs();
	    </script>
	<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
	<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js")%>
	<%=RenderHelper.includedJavascript(request, "/static/js/mobile/commonIndex.js")%>
	<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js")%>
	<%=RenderHelper.includedAutoJavascript(request) %>
</body>
</html>
