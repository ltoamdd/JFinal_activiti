<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<%
        String ctx = request.getContextPath();
    	%>
    	<script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
        var openid = "${openid}";
    	</script>
		<title>${kcbbt}</title>
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<%=RenderHelper.includedStyle(request, "/static/kcb/stylesheet/index.css") %>
    	<%=RenderHelper.includedStyle(request, "/static/kcb/font/iconfont.css") %>
    	<%=RenderHelper.includedStyle(request, "/static/kcb/javascript/swiper/dist/css/swiper.min.css") %>
		<style>
			a[data-type='list']{
				display: block;
			}
			a[data-type='list']:after {
				content:"";
				display: block;
				clear:both;
			}
			a[data-type='list'] > span{
				float:left;
			}
			a[data-type='list'] > span:nth-child(1){
				width:65px;
				text-align: right;
				text-overflow: ellipsis;
			    white-space: nowrap;
			    overflow: hidden;
			}
			.nav-list {
   				background-color: #fff;
    			position: fixed;
			    z-index: 3;
			    width: 100%;
			}
			.list-main {
		        top: 50px;
		    	position: absolute;
			}
		</style>
	</head>
	<body>
		<input type="hidden" id="openid" name="openid" value="${openid}">
		<input type="hidden" id="kcbid" name="kcbid" value="${kcbid}">
		<nav class="nav-list">
		<a href="javascript:backShuye();" class="pull-left"> <span
			class="iconfont icon-zuojiantou"></span>
		</a> <a href="javascript:openidChaxun();" class="pull-right"> <span
			class="iconfont icon-sousuo"></span>
		</a>
		<p class="nav-title">${kcbbt}</p>
	</nav>
		<ul class="list-main" id="ztxkul">
		</ul>
		<div class="nomsg">
			<span>没有更多内容</span>
		</div>
		
		<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
		<script>
			var winWidth =  window.innerWidth;
            var size = (winWidth / 750) * 100;
            document.documentElement.style.fontSize = (size < 100 ? size : 100) + 'px';
		</script>
	</body>
	<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
	<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
	<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
	<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js")%>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/commonIndex.js")%>
	<%=RenderHelper.includedAutoJavascript(request) %>	
</html>
