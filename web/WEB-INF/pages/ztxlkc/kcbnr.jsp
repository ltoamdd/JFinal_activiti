<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page import="com.qysoft.accountingCloud.platform.entity.FWH" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html>
<html class="kc">
	<head>
		<%
        String ctx = request.getContextPath();
    	%>
		<meta charset="UTF-8">
		<title>课程详情</title>
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    	<meta name="apple-mobile-web-app-capable" content="yes">
    	<meta name="apple-mobile-web-app-status-bar-style" content="black">
    	<meta name="format-detection" content="telephone=no">
    	
		 <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
		 <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    	<%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
		<%=RenderHelper.includedStyle(request, "/static/wkt/public/stylesheet/index.css")%>
		<%=RenderHelper.includedStyle(request, "/static/wkt/public/font/iconfont.css")%>
		<%=RenderHelper.includedStyle(request, "/static/wkt/public/font3/iconfont.css")%>
    	
   <!--  <script type="text/javascript" src="http://cdn.aodianyun.com/lss/aodianplay/player.js"></script> -->
    
    <script type="text/javascript" src="http://cdn.aodianyun.com/mps_s/v1/hlsplayer.js"></script>
    
    <style type="text/css">
    
    .video {
    height:auto;
    overflow: hidden;
    background: #fff; 
}
	.footbar>div>p>a .mdd{
     color: #0099E1; 
	}
	.dd {
		position: absolute;
		width: 100%;
		height: 100%;
		z-index: 19891016
	}
	.ss {
		position: absolute;
		width: 100%;
		height:100px;
		z-index: 19891017
	}
    </style>
    	 <%
    	 String ctx1 = request.getContextPath();
         FWH fwh = (FWH) request.getSession().getAttribute("fwh");
         String key = fwh==null?"csthyfw":fwh.getKey();

        %>
        <script type="text/javascript">
        	var ctx = "<%=ctx1%>";
	        var articleId = "${id}";
	        var isGetWxSignature = true;
	        var fwh = '${fwh}';
	        var wx_key = '${wx_key}';
	        function getImgByPath(path) {
	            if (!path) {
	                return "";
	            }
	            if (path.indexOf("http://") == 0 || path.indexOf("https://") == 0) {
	                return path;
	            }
	            if (path.indexOf("/") == 0) {
	                return "http://mobile.csfw360.com" + path;
	            }
	        }
       
     </script>
	</head>
	 
     <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
	<body class="kc">
      <input type="hidden" id="errorCode" value="${errorCode}" />
      <input type="hidden" id="backUrl" value="${backUrl}" />
	<input type="hidden" id="openid" name="openid" value="${openid}">
	<input type="hidden" id="wx_key" name="wx_key" value="${wx_key}">
	<input type="hidden" id="id" name="id" value="">
	<input type="hidden" id="kcbid" name="kcbid" value="${kcbid}">
	<input type="hidden" id="hot_label" name="hot_label" value="${videoInfo.hot_label}">
	<input type="hidden" id="wlkc_id" name="wlkc_id" value="${videoInfo.id}">
	<input type="hidden" id="wlkc_cid" name="wlkc_cid" value="${videoInfo.cid}">
	<input type="hidden" id="title"  value="${videoInfo.title}">
	<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
	  <nav class="nav-list">
		<a href="javascript:backShuye();" class="pull-left"> <span
			class="iconfont icon-zuojiantou"></span>
		</a> 
		<p class="nav-title">课程详情</p>
	</nav>
	<section class="video">
		<div id="kcbkcPlayer" style="text-align:center;"></div>
		<script type="text/javascript">
	 var w = "100%";//视频宽度
     var h = "auto";//视频高度
     //var url = "http://13541.long-vod.cdn.aodianyun.com/u/13541/m3u8/adaptive/4839f3642b2972e883cf025cb9606011.m3u8";//视频地址
     var url = "${videoInfo.url}";//视频地址
     var urls = url.split(",");
     var flag = false;
     urls.forEach(function(item) {
         if(item.indexOf("http://") != -1) {
             url = item;
             flag = true;
         }
     });

     if(!flag){
         alert("视频源有误，请联系管理员");
     }
     var image = getImgByPath('${videoInfo.img}');//封面图片
     /**
     
     var objectPlayer=new aodianPlayer({
         container:'kcbkcPlayer',//播放器容器ID，必要参数
//         rtmpUrl: url,//控制台开通的APP rtmp地址，必要参数
         hlsUrl: url,//控制台开通的APP hls地址，必要参数
         / /以下为可选参数
         width: w,//播放器宽度，可用数字、百分比等
         height: h,//播放器高度，可用数字、百分比等
         autostart: false,//是否自动播放，默认为false
         controlbardisplay: 'enable',//是否显示控制栏，值为：disable、enable默认为disable。
         adveDeAddr: image,//封面图片链接
         adveWidth: w,//封面图宽度
         adveHeight: h//封面图高度
         //adveReAddr: ''//封面图点击链接
     });**/
     var currenttime=0;
     var totalfiletime=0;
     var objectPlayer=new mpsPlayer({
    	    container:'kcbkcPlayer',//播放器容器ID，必要参数
    	    uin: '13541',//用户ID
    	    appId: 'fNNB63H3mmbUXSJi',//播放实例ID
    	    url: url,//控制台开通的hls/mp4地址
    	    width: w,//播放器宽度，可用数字、百分比等
    	    height: h,//播放器高度，可用数字、百分比等
    	    autostart: false,//是否自动播放，默认为false
    	    controlbardisplay: 'enable',//是否显示控制栏，值为：disable、enable默认为disable。
    	    isclickplay: true,//是否单击播放，默认为false
    	    isfullscreen: true,//是否双击全屏，默认为true
    	    mobilefullscreen: true,//移动端是否全屏，默认为false
    	    coverImg:image,
    	    enablehtml5: false,//是否优先使用H5播放器，默认为false
    	    isloadcount: 1,//网络波动卡顿loading图标显示(默认1s后)
    	    pauseCallback:function(){
    	    	 currenttime=objectPlayer.currenttime();
    	    	 totalfiletime=objectPlayer.totalfiletime();
    	    },
    	 	 playCallback:function(fn){
    	 		currenttime=objectPlayer.currenttime();
    	 		if(Number(currenttime)>=10){
    	 			var code2=$("#errorCode").val();
    				if(code2==1||code2==2||code2==3){
                        objectPlayer.stopPlay();
    				}
    			}
    	     },
    	     onReady:function(){
    	         setTimeout(function(){
    	             objectPlayer.addPlayerCallback('exitFullScreen',function(){//退出全屏时触发
    	     	 		var bb=doBb();
    	     			var cutime=objectPlayer.currenttime();
    	     			if(bb==1){//手机端
    	     				if(cutime){
    	     					var gktime=getTotlaTime(cutime);
    	     					//来一个10秒钟的时间
    	     					if(Number(gktime)>=300){
    	     						var code1=$("#errorCode").val();
    	     						if(code1==1||code1==2||code1==3){
    	     							openQxtx();	
    	     						}
    	     					}
    	     				}
    	     			}else{//pc端
    	     				if(cutime>=300){
    	     					var code1=$("#errorCode").val();
    	     					if(code1==1||code1==2||code1==3){
    	     						openQxtx();
    	     					}
    	     				}
    	     			}
    	     			
    	            	 	
    	             });

    	         },2000); 
    	      }
    	});
    	/* rtmpUrl与hlsUrl同时存在时播放器优先加载rtmp*/
    	/* 以下为MPS支持的事件 */
    	/* objectPlayer.startPlay();//播放 */
    	/* objectPlayer.pausePlay();//暂停 */
    	/* objectPlayer.stopPlay();//停止*/
    	/* objectPlayer.closeConnect();//断开连接 */
    	/* objectPlayer.setMute(true);//静音或恢复音量，参数为true|false */
    	/* objectPlayer.setVolume(volume);//设置音量，参数为0-100数字 */
    	/* objectPlayer.setFullScreenMode(1);//设置全屏模式,1代表按比例撑满至全屏,2代表铺满全屏,3代表视频原始大小,默认值为1。手机不支持 */
        </script>
		</section>
		
		<section id="toggle">
		
			<div class="title" style="border-top: 6px solid #EAEDF1;">
				<p>${videoInfo.title}</p>
				<div class="info">
					<span>${videoInfo.click}人已学习</span>
					<span>${videoInfo.createtime1}</span>
				</div>
			</div>
			
			<!-- 信息切换 -->
			<div class="tabs">
	    		<div class="tabs-head">
	    			
	    			<c:choose>
	    				<c:when test="${bofangstatus=='ztxl'}">
	    					<span>目录</span>
	    				</c:when>
	    				<c:otherwise>
	    					<span>目录</span>
	    				</c:otherwise>
	    			</c:choose>
	    			<span>详情</span>
	    			<c:choose>
	    				<c:when test="${bofangstatus=='ztxl'}">
	    					
	    				</c:when>
	    				<c:otherwise>
	    					<span>相关课程</span> 
	    				</c:otherwise>
	    			</c:choose>
	    			
	    			
	    		</div>
	    		<div class="tabs-main">
	    			<div class="tabs-list" >
	    				<ul class="kc-list">
	    					<c:forEach items="${kcmlList}" var="kcml" varStatus="status">
	    					
	    					<c:choose>
	    						<c:when test="${kcml.id==videoInfo.id}">
	    						<li class='active'>
		        					<a href="javascript:toZbkcDetail('${kcml.id}')">
		    							<span style="color:#FF9900;">0${status.count}</span>
		    							<span style="color:#FF9900;">${kcml.title}</span>
		    							<!-- <span>09分</span> -->
		    						</a>
	    						</li>
	    						</c:when>
	    						<c:otherwise>
	    							<li>
		        					<a href="javascript:toZbkcDetail('${kcml.id}')">
		    							<span >0${status.count}</span>
		    							<span >${kcml.title}</span>
		    							<!-- <span>09分</span> -->
		    						</a>
	    						</li>
	    						</c:otherwise>
	    					</c:choose>
	        				
	        			</c:forEach>
	    				</ul>
	    			</div>
	    			<div class="tabs-list">
	    				<div class="kc-des">
	    					<h1>课程简介</h1>
	    					<p>${videoInfo.info}</p>
	    					<img style="display: block;margin: 5px auto;width: 95%;" src="<%=ctx %>${ewm_src}">
	    				</div>
	    				<div class="kc-say">
	    					<h1><span class="iconfont icon-yonghu"></span>用户评论</h1>
	    					<ul id="comment_ul">
	    					</ul>
	    				</div>
	    			</div>
	    			<!-- 相关课程 -->
	    			<div class="tabs-list">
						<ul class="list-main" id="ztxkul"></ul>
	    			</div>
	    		</div>
	    	</div>
		</section>
    	<section class="footbar">
    	
    		<div id="shoucBtn"  onclick="javascript:shoucang();">
    			<span class="iconfont icon-heart" ></span>
    			<p><i></i>收藏</p>
    		</div>
    
    		<div id="dzBtn" onclick="javascript:dianzan();">
    			<span class="iconfont icon-zan"   ></span>
    			<p><i></i><p id="upCount">0</p></p>
    		</div>
    		<div onclick="javascript:pinglun();">
    			<span class="iconfont icon-pinglun" ></span>
    			<p><a  class="flexbox-item nth3"><i></i><p>评论</p></a></p>
    		</div>
    	</section>
    	
   <div class="main clearfix" id="qxtx" style="display: none">
    <div class="qxts_main">
        <div class="qxts">
            <h4>尊敬的用户您好：</h4>
            <p id="msg">您的权限尚且无法访问该页面，请及时联系您的客户经理进行升级！</p>
            <% if("nmhxhyfw".equals(key)) { %>
                <h6>详情请拨打：<a href="tel:04713281778">0471-3281778</a></h6>
            <% } else if("gh_74acb48d4fda".equals(key)) { %>
            	<h6>详情请拨打：<a href="tel:02584537752">025-84537752</a></h6>
            <% } else if("gh_a35069f56858".equals(key)) { %>
        		<h6>详情请拨打：<a href="tel:4007008895">4007008895-8</a></h6>
            <% } else if("gh_f8963ae972b7".equals(key)) { %>
                <h6>详情请拨打：<a href="tel:09513915627">0951-3915627</a></h6>
        	<% }else {%>
                <h6>详情请拨打：<a href="tel:4006644360">4006644360</a></h6>

            <% } %>
        </div>
    </div>
</div>
	<%=RenderHelper.includedJavascript(request, "/static/js/mobile/jquery.fancyspinbox.js") %>
	<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js" ></script>
	<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
 	<%=RenderHelper.includedJavascript(request, "/static/kcb/javascript/tabs.js")%>
	<%=RenderHelper.includedAutoJavascript(request) %>
	</body>
    	<script type="text/javascript">
    	 	window.onload =function(){
    			tabs();
        		//tabsMain();
        		//页面高度
    			var viewHeight = document.documentElement.clientHeight;
        		//激活内容距离顶部的高度
    			var offsetTop = document.querySelector('.kc-list li.active').getBoundingClientRect().top;
        		//视屏描述的高度
    			var titleHeight = document.querySelector('.title').offsetHeight;
        		//顶部导航条高度
        		var topBarHeight = document.querySelector('.nav-list').offsetHeight ? document.querySelector('.nav-list').offsetHeight :0;
        		//底部功能条的高度
        		var footBarHeight = document.querySelector('.footbar').offsetHeight ? document.querySelector('.footbar').offsetHeight :0;
    			//视屏高度
    			var videoHeight = document.querySelector('.video').offsetHeight ? document.querySelector('.video').offsetHeight : document.querySelector('.video').offsetWidth /1.8;
        		//滚动容器高度
        		var _toggleHeight = document.querySelector('#toggle');
        		_toggleHeight.style.height = viewHeight - topBarHeight -videoHeight - footBarHeight +"px";
        		_toggleHeight.style.overflow="auto";
    			var isScroll = offsetTop - titleHeight - videoHeight;
    			document.querySelector("#toggle").scrollTop = document.querySelector('.kc-list li.active').getBoundingClientRect().top - topBarHeight -videoHeight +document.querySelector("#toggle").scrollTop;
    		} 
    		 var _fxUrl = "http://mobile.csfw360.com/sharpContent/renderSharpContentPage?id=${videoInfo.id}&modelName=article&isVedio=true&wx_key=${fwh.key}";
    		 youqingPY("${videoInfo.title}","财税通，您身边的税务专家",_fxUrl,"http://mobile.csfw360.com/common/getFileByPath?path=${videoInfo.img}");
    	</script>
    
</html>
