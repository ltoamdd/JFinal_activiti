<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/wkt/public/stylesheet/index.css")%>
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <title>每日听税</title>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
    </script>
    <base target="_self">
    <style>
        .mrts_list_tit a.mrts_list_sx {
            background: url(<%=ctx%>/static/images/mobile/mrts_list_sx.png) 0rem center no-repeat;
            background-size: 0.3rem 0.3rem;
        }
        .mrts_list_tit a.mrts_list_jx {
            background: url(<%=ctx%>/static/images/mobile/mrts_list_jx.png) 0rem center no-repeat;
            background-size: 0.3rem 0.3rem;
        }
    </style>
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
    <%--<jsp:include page="/WEB-INF/pages/common/baidutongji.jsp"/>--%>
</head>
<body>
<input type="hidden" id="openid" value="${openid}" />
<div class="main clearfix">
    <%--<div class="mrts_rec">
        <a href="javascript:void(0);">
            <div class="mrts_rec_l"><img src="<%=ctx%>/static/images/mobile/tingshuibanner.png" alt="" /></div>
            <div class="mrts_rec_r">
                <h4>每日听税</h4>
                <p>简介：每日给大家分享一个财税新政、财税实操或者财税筹划技巧。</p>
                <h6 id="tsCount"></h6>
            </div>
        </a>
    </div>--%>
        <section class="list-search" >
            <ul id="tsTabs">
            </ul>

        </section>
    <div class="mrts_list">
        <div class="mrts_list_tit"><h2>已更新<span id="listLength"></span>期</h2><a id="mrts_px" class="mrts_list_sx" href="javascript:paixu();">排序</a></div>
        <div id="tsList" class="mrts_list_con">
            <!--
            <section>
                <a href="#">
                    <h4>房地产企业纳税义务税实操或者税实操</h4>
                    <p>11小时前<span>890人听过</span></p>
                </a>
            </section>
            -->
        </div>
    </div>
</div><!--main-->
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
</body>
</html>
