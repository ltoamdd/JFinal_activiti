<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: WangPengfei
  Date: 2017/3/9
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/live_chat.css") %>
    <%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
    <%=RenderHelper.includedStyle(request, "/static/kcb/stylesheet/index.css")%>
    <%=RenderHelper.includedStyle(request, "/static/kcb/font/iconfont.css")%>
    <%=RenderHelper.includedStyle(request, "/static/wlzbfont/iconfont.css")%>
    <%=RenderHelper.includedStyle(request, "/static/kcb/javascript/swiper/dist/css/swiper.min.css")%>
    <script type="text/javascript" src="http://cdn.aodianyun.com/lss/aodianplay/player.js"></script>
    <script type="text/javascript" src="https://cdn.aodianyun.com/mps_s/v1/hlsplayer.js"></script>
    <script type="text/javascript" src="http://cdn.aodianyun.com/dms/rop_client.js"></script>
    <title>课程详情</title>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var articleId = "${id}";
        var isGetWxSignature = true;
        var fwh = '${fwh}';
        var wx_key = '${wx_key}';
        var zb_classtime = '${videoInfo.classtime}';
        var zb_source = '${videoInfo.source}';
        var click='${videoInfo.click}';
        var openid='${openid}';
        function getImgByPath(path) {
            if (!path) {
                return "";
            }
            if (path.indexOf("http://") == 0 || path.indexOf("https://") == 0) {
                return path;
            }
            if (path.indexOf("/") == 0) {
                return "http://mobile.csfw360.com" + path;
            }
        }
    </script>
    <base target="_self">
    <style>
        #djsContent {
            position: absolute;
            width: 100%;
            z-index: 19891000;
            top: 0px;
            background-image: url("<%=ctx%>/static/images/mobile/livetimebag.png");
            background-size: 100% 100%;
        }

        #djsContent .djs-title {
            margin-top: 1.1rem;
            font-size: 0.35rem;
            text-align: center;
            color: #fff;
        }

        #djsContent #timeContent {
            margin-top: 0.8rem;
            font-size: 0.35rem;
            text-align: center;
            color: #fff;
        }

        #timeContent .time_span {
            padding: 0.15rem;
            background-image: url("<%=ctx%>/static/images/mobile/livetimenum.png");
            background-size: 100% 100%;
        }

        .speak-list li div p {
            background-color: #f0f0f0;
        }

        .zbkc_video_con p, .zbkc_video_con div {
            padding: 0;
        }
    </style>
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp" %>
</head>
<body>
<%--<nav class="nav-list">--%>
<%--<a href="javascript:backShuye();" class="pull-left"> <span--%>
<%--class="iconfont icon-zuojiantou"></span>--%>
<%--</a>--%>
<%--<!-- <a href="javascript:openidChaxun();" class="pull-right"> <span--%>
<%--class="iconfont icon-sousuo"></span>--%>
<%--</a> -->--%>
<%--<p class="nav-title">课程详情</p>--%>
<%--</nav>--%>
<input type="hidden" id="openid" name="openid" value="${openid}">
<div class="main clearfix">
    <!--------------视频--------------->
    <div class="zbkc_video">
        <%--<img src="<%=ctx%>/static/images/mobile/zbsp_test.png" alt="" />--%>
        <div id="videoPlayer" style="text-align:center;"></div>
        <script type="text/javascript">
            var w = "100%";//视频宽度
            var h = "auto";//视频高度
            //var url = "http://13541.long-vod.cdn.aodianyun.com/u/13541/m3u8/adaptive/4839f3642b2972e883cf025cb9606011.m3u8";//视频地址
            var url = "${videoInfo.url}";//视频地址
            var urls = url.split(",");
            var flag = false;
            urls.forEach(function (item) {
                if (item.indexOf("http://") != -1) {
                    url = item;
                    flag = true;
                }
            });

            if (!flag) {
                alert("视频源有误，请联系管理员");
            }
            var image = getImgByPath('${videoInfo.img}');//封面图片
                        var objectPlayer = new aodianPlayer({
                            container: 'videoPlayer',//播放器容器ID，必要参数
            //                rtmpUrl: url,//控制台开通的APP rtmp地址，必要参数
                            hlsUrl: url,//控制台开通的APP hls地址，必要参数
                            /* 以下为可选参数*/
                            width: w,//播放器宽度，可用数字、百分比等
                            height: h,//播放器高度，可用数字、百分比等
                            autostart: false,//是否自动播放，默认为false
                            controlbardisplay: 'enable',//是否显示控制栏，值为：disable、enable默认为disable。
                            adveDeAddr: image,//封面图片链接
                            adveWidth: w,//封面图宽度
                            adveHeight: h,//封面图高度
                            //adveReAddr: ''//封面图点击链接
//                            isclickplay: false,//是否单击播放，默认为false
//                            isfullscreen: true,//是否双击全屏，默认为true
//                            mobilefullscreen: false,//移动端是否全屏，默认为false
//                            enablehtml5: true,//是否优先使用H5播放器，默认为false
//                            isloadcount: 1//网络波动卡顿loading图标显示(默认1s后)
                        });


            /*var objectPlayer=new aodianPlayer({
             container:'play',//播放器容器ID，必要参数
             rtmpUrl: 'rtmp://13541.lssplay.aodianyun.com/xcs/stream',//控制台开通的APP rtmp地址，必要参数
             hlsUrl: '',//控制台开通的APP hls地址，必要参数
             /!* 以下为可选参数*!/
             width: '905',//播放器宽度，可用数字、百分比等
             height: '554',//播放器高度，可用数字、百分比等
             autostart: true,//是否自动播放，默认为false
             bufferlength: '1',//视频缓冲时间，默认为3秒。hls不支持！手机端不支持
             maxbufferlength: '2',//最大视频缓冲时间，默认为2秒。hls不支持！手机端不支持
             stretching: '1',//设置全屏模式,1代表按比例撑满至全屏,2代表铺满全屏,3代表视频原始大小,默认值为1。hls初始设置不支持，手机端不支持
             controlbardisplay: 'enable',//是否显示控制栏，值为：disable、enable默认为disable。
             //adveDeAddr: '',//封面图片链接
             //adveWidth: 320,//封面图宽度
             //adveHeight: 240,//封面图高度
             //adveReAddr: ''//封面图点击链接
             });*/
//            var objectPlayer = new mpsPlayer({
//                container: 'videoPlayer',//播放器容器ID，必要参数
//                uin: '13541',//用户ID
//                appId: 'fNNB63H3mmbUXSJi',//播放实例ID
//                hlsUrl: url,//控制台开通的APP hls地址
//                rtmpUrl: '',//控制台开通的APP rtmp地址
//                flvUrl: '',//flv地址
//                width: w,//播放器宽度，可用数字、百分比等
//                height: h,//播放器高度，可用数字、百分比等
//                autostart: true,//是否自动播放，默认为false
//                controlbardisplay: 'enable',//是否显示控制栏，值为：disable、enable默认为disable。
//                isclickplay: false,//是否单击播放，默认为false
//                isfullscreen: true,//是否双击全屏，默认为true
//                mobilefullscreen: false,//移动端是否全屏，默认为false
//                enablehtml5: true,//是否优先使用H5播放器，默认为false
//                isloadcount: 1//网络波动卡顿loading图标显示(默认1s后)
//
////                adveDeAddr: image,//封面图片链接
////                adveWidth: w,//封面图宽度
////                adveHeight: h,//封面图高度
//
//            });

        </script>


        <div id="djsContent" style="display: none;">
            <div class="djs-title">距离开播还有</div>
            <div id="timeContent">
                <span id="djsDate" class="time_span">01</span>
                <span>天</span>
                <span id="djsHour" class="time_span">21</span>
                <span>时</span>
                <span id="djsMinute" class="time_span">00</span>
                <span>分</span>
                <span id="djsSecond" class="time_span">44</span>
                <span>秒</span>
            </div>
        </div>

        <section id="status">
            <span class="status-icon-end">LIVE</span>
            <span class="status-text-end">正在直播中......</span>
        </section>

        <div id="tabs" class="tabs">
            <div class="tabs-header">
                <span><i class="iconfont icon-jianjie"></i>简介</span>
                <span><i class="iconfont icon-hudong"></i>互动</span>
            </div>
            <%--<div class="tabs-items">--%>
            <%--<div class="tabs-item">--%>
            <%--<div id="zkbc_video_con" class="zbkc_video_con">--%>
            <%--<h4>${videoInfo.title}</h4>--%>
            <%--<h6>${videoInfo.createtime1}&nbsp;&nbsp;&nbsp;&nbsp;时长: ${videoInfo.setp}<span>${videoInfo.click}次播放</span>--%>
            <%--</h6>--%>
            <%--<div>${videoInfo.content}</div>--%>
            <%--<img src="<%=ctx%>${ewm_src}" alt=""/>--%>

            <%--</div><!--main-->--%>
            <%--</div>--%>
            <%--<div class="tabs-item speak-item">--%>
            <%--<input type="hidden" id="id_pubkey" name="openid" value="${chatModel.pubkey}">--%>
            <%--<input type="hidden" id="id_subkey" name="openid" value="${chatModel.subkey}">--%>
            <%--<input type="hidden" id="id_client" name="openid" value="${chatModel.clientid}">--%>
            <%--<input type="hidden" id="idgroup" name="openid" value="${chatModel.topicId}">--%>
            <%--<input type="hidden" id="id_client_context" name="openid" value="${chatModel.clientContext}">--%>
            <%--<ul class="speak-list" id="chatul">--%>
            <%--</ul>--%>

            <%--</div>--%>
            <%--</div>--%>
        </div>

        <div id="tab_jianji" style="height: 5.5rem; overflow-y: auto;">

            <div id="zkbc_video_con" class="zbkc_video_con">
                <h4>${videoInfo.title}</h4>
                <h6>${videoInfo.classtime}&nbsp;&nbsp;&nbsp;&nbsp;时长: ${videoInfo.setp}
                	<span></span>
                </h6>
                <div>${videoInfo.content}</div>
                <%--<img src="<%=ctx%>${ewm_src}" alt=""/>--%>

            </div><!--main-->

            <!--------------评论--------------->
            <div class="zbkc_comment">
                <div class="zbkc_comment_tit clearfix"><h4>用户评论</h4></div>
                <div class="zbkc_commen_con">
                    <ul id="comment_ul">
                        <li>
                            <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                            <p>讲得很好。</p>
                        </li>
                        <li>
                            <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                            <p>好的。</p>
                        </li>
                        <li>
                            <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                            <p>讲得很好。</p>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <!--------------底部--------------->
        <%--<div class="article_footer">
            <div class="footer_return"><a href="javascript:window.history.go(-1);"><img src="<%=ctx%>/static/images/mobile/icon_return.png" alt="" /></a></div>
            <div class="footer_box">
                <a href="javascript:shoucang();" id="shoucBtn" class="article_sc"><p>收藏</p></a>
                <a href="javascript:dianzan();" id="dzBtn" class="article_dz"><p id="upCount">150</p></a>
                <a href="javascript:pinglun();" class="article_pl"><p>评论</p></a>
                <a href="javascript:void(1);" class="article_fx"><p>分享</p></a>
            </div>
        </div>--%>
        <footer class="flexbox" id="breif_footer">
            <ul>
                <li><a href="javascript:shoucang();" id="shoucBtn" class="flexbox-item nth1"><i></i>
                    <p>收藏</p></a></li>
                <li><a href="javascript:dianzan();" id="dzBtn" class="flexbox-item nth2"><i></i>
                    <p id="upCount">150</p></a></li>
                <li><a href="javascript:pinglun();" class="flexbox-item nth3"><i></i>
                    <p>评论</p></a></li>
                <%--<li><a href="javascript:void(1);" class="flexbox-item nth5"><i></i><p>分享</p></a></li>--%>
            </ul>
        </footer>

        <input type="hidden" id="binded_flag" name="binded_flag" value="${hasBinded}">
        <input type="hidden" id="id_pubkey" name="pubkey" value="${chatModel.pubkey}">
        <input type="hidden" id="id_subkey" name="subkey" value="${chatModel.subkey}">
        <input type="hidden" id="id_client" name="clientid" value="${chatModel.clientid}">
        <input type="hidden" id="idgroup" name="topicId" value="${chatModel.topicId}">
        <input type="hidden" id="id_client_context" name="clientContext" value="${chatModel.clientContext}">
        <ul class="speak-list" id="chatul"
            style="margin-bottom: 0.7rem; height:5.5rem; overflow: auto;background-color:#fff">
        </ul>
        <footer id="chat_footer">
            <section id="form">
                <input type="text" id="idtext">
                <button onclick="Publish()">发送</button>
            </section>
        </footer>


</body>

<%=RenderHelper.includedJavascript(request, "/static/js/mobile/jquery.fancyspinbox.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/TouchSlide.1.1.source.js") %>
<%--<%=RenderHelper.includedJavascript(request, "/static/js/mobile/index.js") %>--%>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>
    //    $('#my-menu1').fancyspinbox();
    //    $('#my-menu2').fancyspinbox();
    //    $('#my-menu3').fancyspinbox();
</script>

</html>

<script>
    /**
     * 分享功能
     * @param title
     * @param desc
     * @param link
     * @param imgUrl
     */
     var _fxUrl = "http://mobile.csfw360.com/sharpContent/renderSharpContentPage?id=${videoInfo.id}&modelName=article&isVedio=true&wx_key=${fwh.key}";
     youqingPY("${videoInfo.title}", "财税通，您身边的税务专家", _fxUrl, "http://mobile.csfw360.com/common/getFileByPath?path=${videoInfo.img}");
</script>
