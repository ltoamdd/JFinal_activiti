<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: WangPengfei
  Date: 2017/3/9
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <%=RenderHelper.includedStyle(request, "/static/kcb/stylesheet/index.css")%>
<%=RenderHelper.includedStyle(request, "/static/kcb/font/iconfont.css")%>
<%=RenderHelper.includedStyle(request, "/static/kcb/javascript/swiper/dist/css/swiper.min.css")%>
    <%
        String ctx = request.getContextPath();
    %>
    <!--新增样式begin-->
    <style>
        .video_img{width:100%;height:auto;display:block;margin:0 auto;}

        .video_nav{width:100%;height:0.85rem;border-bottom:1px #e6e6e6 solid;display:-moz-box;display:-webkit-box; display:box;background:#fff;}
        .video_nav_left{position:relative;overflow:hidden;-moz-box-flex:1;-webkit-box-flex:1;box-flex:1;}
        .video_nav_list{}
        .video_nav_list ul{position:relative;white-space:nowrap;font-size:0;}
        .video_nav_list ul li{width:2.5rem;height:0.6rem;margin:0.2rem 0.35rem 0rem 0.6rem;display:inline-block;}
        .video_nav_list ul li a{width:100%;height:100%;font:0.34rem/0.6rem "Microsoft Yahei";color:#333333;display:block;text-align:center;}
        .video_nav_list ul li a.active{color:#2c94da;border-bottom:3px #2c94da solid;}

        .video_list{background:#fff;overflow:hidden;}
        .video_list_con{padding-bottom:0.4rem;}
        .video_list_con section{border-top:1px #e6e6e6 solid;padding:0.2rem 0.4rem 0.2rem 1rem;background:url(<%=ctx%>/static/images/mobile/newstart.png) 5% center no-repeat;background-size:0.4rem 0.4rem;}
        .video_list_con section h4{width:6rem;font:0.28rem/0.48rem "Microsoft Yahei";color:#333333;overflow : hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp:2;-webkit-box-orient: vertical;}
        .video_list_con section.active{ background-image:url(<%=ctx%>/static/images/mobile/newon.png);}
        .video_list_con section.active h4{color:#FF9900;}
        /*.video_intro,.video_list{margin-bottom:0.85rem;}*/
    </style>
    <!--新增样式End-->
    <%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
    <script type="text/javascript" src="http://cdn.aodianyun.com/lss/aodianplay/player.js"></script>
    <title>课程详情</title>

    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var articleId = "${id}";
        var isGetWxSignature = true;
        var fwh = '${fwh}';
        var wx_key = '${wx_key}';
        function getImgByPath(path) {
            if (!path) {
                return "";
            }
            if (path.indexOf("http://") == 0 || path.indexOf("https://") == 0) {
                return path;
            }
            if (path.indexOf("/") == 0) {
                return "http://mobile.csfw360.com" + path;
            }
        }

    </script>
    <base target="_self">
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<nav class="nav-list">
		<a href="javascript:backShuye();" class="pull-left"> <span
			class="iconfont icon-zuojiantou"></span>
		</a> <a href="javascript:openidChaxun();" class="pull-right"> <span
			class="iconfont icon-sousuo"></span>
		</a>
		<p class="nav-title">课程详情</p>
</nav>
<form id="zbkc_detail_form" action="<%=ctx%>/hyfw/zbkc/zbkcDetail" method="post">
<input type="hidden" id="openid" name="openid" value="${openid}">
<input type="hidden" id="id" name="id" value="">
</form>
<div class="main clearfix">
    <!--------------视频--------------->
    <div class="zbkc_video">
        <%--<img src="<%=ctx%>/static/images/mobile/zbsp_test.png" alt="" />--%>
        <div id="videoPlayer" style="text-align:center;"></div>
        <script type="text/javascript">
            var w = "100%";//视频宽度
            var h = "auto";//视频高度
            //var url = "http://13541.long-vod.cdn.aodianyun.com/u/13541/m3u8/adaptive/4839f3642b2972e883cf025cb9606011.m3u8";//视频地址
            var url = "${videoInfo.url}";//视频地址
            var urls = url.split(",");
            var flag = false;
            urls.forEach(function(item) {
                if(item.indexOf("http://") != -1) {
                    url = item;
                    flag = true;
                }
            });

            if(!flag){
                alert("视频源有误，请联系管理员");
            }
            var image = getImgByPath('${videoInfo.img}');//封面图片
            var objectPlayer=new aodianPlayer({
                container:'videoPlayer',//播放器容器ID，必要参数
//                rtmpUrl: url,//控制台开通的APP rtmp地址，必要参数
                hlsUrl: url,//控制台开通的APP hls地址，必要参数
                /* 以下为可选参数*/
                width: w,//播放器宽度，可用数字、百分比等
                height: h,//播放器高度，可用数字、百分比等
                autostart: false,//是否自动播放，默认为false
                controlbardisplay: 'enable',//是否显示控制栏，值为：disable、enable默认为disable。
                adveDeAddr: image,//封面图片链接
                adveWidth: w,//封面图宽度
                adveHeight: h//封面图高度
                //adveReAddr: ''//封面图点击链接
            });


            /*var objectPlayer=new aodianPlayer({
                container:'play',//播放器容器ID，必要参数
                rtmpUrl: 'rtmp://13541.lssplay.aodianyun.com/xcs/stream',//控制台开通的APP rtmp地址，必要参数
                hlsUrl: '',//控制台开通的APP hls地址，必要参数
                /!* 以下为可选参数*!/
                width: '905',//播放器宽度，可用数字、百分比等
                height: '554',//播放器高度，可用数字、百分比等
                autostart: true,//是否自动播放，默认为false
                bufferlength: '1',//视频缓冲时间，默认为3秒。hls不支持！手机端不支持
                maxbufferlength: '2',//最大视频缓冲时间，默认为2秒。hls不支持！手机端不支持
                stretching: '1',//设置全屏模式,1代表按比例撑满至全屏,2代表铺满全屏,3代表视频原始大小,默认值为1。hls初始设置不支持，手机端不支持
                controlbardisplay: 'enable',//是否显示控制栏，值为：disable、enable默认为disable。
                //adveDeAddr: '',//封面图片链接
                //adveWidth: 320,//封面图宽度
                //adveHeight: 240,//封面图高度
                //adveReAddr: ''//封面图点击链接
            });*/


        </script>

            <!--新增切换标签Begin-->
            <div class="video_nav">
                <div class="video_nav_left">
                    <div class="video_nav_list">
                        <ul>
                            <li><a href="#" id="video_intro_tab" class="active">介绍</a></li>
                            <li><a href="#" id="video_list_tab">目录</a></li>
                            <li><a href="#" id="video_list_tab">相关</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--新增切换标签End-->
            <!--新增介绍divBegin-->
            <div id="video_intro" class="video_intro">
                <!--新增介绍divEnd-->

        <div class="zbkc_video_con">
            <h4>${videoInfo.title}</h4>
            <h6>${videoInfo.createtime1}&nbsp;&nbsp;&nbsp;&nbsp;时长: ${videoInfo.setp}<span>${videoInfo.click}次播放</span></h6>
            <div>${videoInfo.content}</div>
             <img src="<%=ctx%>${ewm_src}" alt="" />
        </div>
    <%--</div>--%>
    <!--------------评论--------------->
    <div class="zbkc_comment">
        <div class="zbkc_comment_tit clearfix"><h4>用户评论</h4></div>
        <div class="zbkc_commen_con">
            <ul id="comment_ul">
                <li>
                    <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                    <p>讲得很好。</p>
                </li>
                <li>
                    <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                    <p>好的。</p>
                </li>
                <li>
                    <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                    <p>讲得很好。</p>
                </li>
            </ul>
        </div>
    </div>
    <!--新增介绍div对应结尾符Begin-->
    </div>
    <!--新增介绍div对应结尾符End-->
<!--新增目录列表Begin-->
<div id="video_list" class="video_list" style="display:none;">
    <div class="video_list_con">

        <c:forEach items="${kcmlList}" var="kcml">

            <section <c:if test="${kcml.id == videoInfo.id}">class="active" </c:if> ><a href="javascript:toZbkcDetail('${kcml.id}')"><h4>${kcml.title}</h4></a></section>
        </c:forEach>
        <%--<section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section class="active"><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程</h4></a></section>
        <section><a href="#"><h4>房地产企业纳税义务实操或者税实操课程实操或者税实操课程end</h4></a></section>--%>

    </div>
</div>
<!--新增目录列表End-->
</div></div><!--main-->
<!--------------底部--------------->
<%--<div class="article_footer">
    <div class="footer_return"><a href="javascript:window.history.go(-1);"><img src="<%=ctx%>/static/images/mobile/icon_return.png" alt="" /></a></div>
    <div class="footer_box">
        <a href="javascript:shoucang();" id="shoucBtn" class="article_sc"><p>收藏</p></a>
        <a href="javascript:dianzan();" id="dzBtn" class="article_dz"><p id="upCount">150</p></a>
        <a href="javascript:pinglun();" class="article_pl"><p>评论</p></a>
        <a href="javascript:void(1);" class="article_fx"><p>分享</p></a>
    </div>
</div>--%>
<footer class="flexbox">
    <ul>
        <li><a href="javascript:shoucang();" id="shoucBtn" class="flexbox-item nth1"><i></i><p>收藏</p></a></li>
        <li><a href="javascript:dianzan();" id="dzBtn" class="flexbox-item nth2"><i></i><p id="upCount">150</p></a></li>
        <li><a href="javascript:pinglun();" class="flexbox-item nth3"><i></i><p>评论</p></a></li>
        <%--<li><a href="javascript:void(1);" class="flexbox-item nth5"><i></i><p>分享</p></a></li>--%>
    </ul>
</footer>
</body>

<%=RenderHelper.includedJavascript(request, "/static/js/mobile/jquery.fancyspinbox.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/TouchSlide.1.1.source.js") %>
<%--<%=RenderHelper.includedJavascript(request, "/static/js/mobile/index.js") %>--%>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js" ></script>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>
//    $('#my-menu1').fancyspinbox();
//    $('#my-menu2').fancyspinbox();
//    $('#my-menu3').fancyspinbox();
</script>

</html>
<script type="text/javascript">
    //新增切换js
    $(function(){
        $('#video_intro_tab').click(function(){
            $('.video_nav a').removeClass('active');
            $(this).addClass('active');
            $('#video_intro').show();
            $('#video_list').hide();
        });
        $('#video_list_tab').click(function(){
            $('.video_nav a').removeClass('active');
            $(this).addClass('active');
            $('#video_intro').hide();
            $('#video_list').show();
        });
    })
</script>
<script>
    /**
     * 分享功能
     * @param title
     * @param desc
     * @param link
     * @param imgUrl
     */
     var _fxUrl = "http://mobile.csfw360.com/sharpContent/renderSharpContentPage?id=${videoInfo.id}&modelName=article&isVedio=true&wx_key=${fwh.key}";
     youqingPY("${videoInfo.title}","财税通，您身边的税务专家",_fxUrl,"http://mobile.csfw360.com/common/getFileByPath?path=${videoInfo.img}");
</script>
