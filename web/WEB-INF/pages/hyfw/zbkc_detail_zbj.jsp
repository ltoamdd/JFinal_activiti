<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: WangPengfei
  Date: 2017/3/9
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <%=RenderHelper.includedStyle(request, "/static/plugin/countdown/jquery.countdown.css")%>



    <%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
    <script type="text/javascript" src="http://cdn.aodianyun.com/lss/aodianplay/player.js"></script>
    <title><c:if test="${videoInfo.cid==12}">直播课程</c:if><c:if test="${videoInfo.cid==32}">网络课程</c:if></title>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var articleId = "${id}";
        var wx_key = "${wx_key}";
        function getImgByPath(path) {
            if (!path) {
                return "";
            }
            if (path.indexOf("http://") == 0 || path.indexOf("https://") == 0) {
                return path;
            }
            if (path.indexOf("/") == 0) {
                return "http://mobile.csfw360.com" + path;
            }
        }

    </script>
    <base target="_self">
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<input type="hidden" id="openid" name="openid" value="${openid}">
<div class="main clearfix">
    <!--------------视频--------------->
    <div class="zbkc_video">
        <%--<img src="<%=ctx%>/static/images/mobile/zbsp_test.png" alt="" />--%>
        <div id="videoPlayer" style="text-align:center;"></div>
        <script type="text/javascript">
            var w = "100%";//视频宽度
            var h = "auto";//视频高度
            //var url = "http://13541.long-vod.cdn.aodianyun.com/u/13541/m3u8/adaptive/4839f3642b2972e883cf025cb9606011.m3u8";//视频地址
            var url = "${videoInfo.url}";//视频地址
            var urls = url.split(",");
            var flag = false;
            urls.forEach(function(item) {
                if(item.indexOf("http://") != -1) {
                    url = item;
                    flag = true;
                }
            });
//            if(!flag){
//                alert("视频源有误，请联系管理员");
//            }
            var image = getImgByPath(${videoInfo.img});//封面图片
            var objectPlayer=new aodianPlayer({
                container:'videoPlayer',//播放器容器ID，必要参数
//                rtmpUrl: url,//控制台开通的APP rtmp地址，必要参数
                hlsUrl: url,//控制台开通的APP hls地址，必要参数
                /* 以下为可选参数*/
                width: w,//播放器宽度，可用数字、百分比等
                height: h,//播放器高度，可用数字、百分比等
                autostart: false,//是否自动播放，默认为false
                controlbardisplay: 'enable',//是否显示控制栏，值为：disable、enable默认为disable。
                adveDeAddr: image,//封面图片链接
                adveWidth: w,//封面图宽度
                adveHeight: h,//封面图高度
                //adveReAddr: ''//封面图点击链接
            });


                /*var objectPlayer=new aodianPlayer({
                 container:'play',//播放器容器ID，必要参数
                 rtmpUrl: 'rtmp://13541.lssplay.aodianyun.com/xcs/stream',//控制台开通的APP rtmp地址，必要参数
                 hlsUrl: '',//控制台开通的APP hls地址，必要参数
                 /!* 以下为可选参数*!/
                 width: '905',//播放器宽度，可用数字、百分比等
                 height: '554',//播放器高度，可用数字、百分比等
                 autostart: true,//是否自动播放，默认为false
                 bufferlength: '1',//视频缓冲时间，默认为3秒。hls不支持！手机端不支持
                 maxbufferlength: '2',//最大视频缓冲时间，默认为2秒。hls不支持！手机端不支持
                 stretching: '1',//设置全屏模式,1代表按比例撑满至全屏,2代表铺满全屏,3代表视频原始大小,默认值为1。hls初始设置不支持，手机端不支持
                 controlbardisplay: 'enable',//是否显示控制栏，值为：disable、enable默认为disable。
                 //adveDeAddr: '',//封面图片链接
                 //adveWidth: 320,//封面图宽度
                 //adveHeight: 240,//封面图高度
                 //adveReAddr: ''//封面图点击链接
                 });*/


            </script>

        <div class="zbkc_video_con">
            <h4>${videoInfo.title}</h4>
            <h6>${videoInfo.createtime1}&nbsp;&nbsp;&nbsp;&nbsp;时长: ${videoInfo.setp}<span>${videoInfo.click}次播放</span></h6>
            <div>${videoInfo.content}</div>
        </div>
    </div>
    <!--------------评论--------------->
    <div class="zbkc_comment">
        <%--<div class="zbkc_comment_tit clearfix"><h4>用户评论</h4></div>
        <div class="zbkc_commen_con">
            <ul id="comment_ul">
                <li>
                    <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                    <p>讲得很好。</p>
                </li>
                <li>
                    <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                    <p>好的。</p>
                </li>
                <li>
                    <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                    <p>讲得很好。</p>
                </li>
            </ul>
        </div>--%>
    </div>
</div><!--main-->
<!--------------底部--------------->
<%--<div class="article_footer">
    <div class="footer_return"><a href="javascript:window.history.go(-1);"><img src="<%=ctx%>/static/images/mobile/icon_return.png" alt="" /></a></div>
    <div class="footer_box">
        <a href="javascript:shoucang();" id="shoucBtn" class="article_sc"><p>收藏</p></a>
        <a href="javascript:dianzan();" id="dzBtn" class="article_dz"><p id="upCount">150</p></a>
        <a href="javascript:pinglun();" class="article_pl"><p>评论</p></a>
        <a href="javascript:void(1);" class="article_fx"><p>分享</p></a>
    </div>
</div>--%>
<footer class="flexbox">
    <ul>
        <li><a href="javascript:shoucang();" id="shoucBtn" class="flexbox-item nth1"><i></i><p>收藏</p></a></li>
        <li><a href="javascript:dianzan();" id="dzBtn" class="flexbox-item nth2"><i></i><p id="upCount">150</p></a></li>
        <li><a href="javascript:pinglun();" class="flexbox-item nth3"><i></i><p>评论</p></a></li>
        <%--<li><a href="javascript:void(1);" class="flexbox-item nth5"><i></i><p>分享</p></a></li>--%>
    </ul>
</footer>
</body>

<%=RenderHelper.includedJavascript(request, "/static/js/mobile/jquery.fancyspinbox.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/TouchSlide.1.1.source.js") %>
<%--<%=RenderHelper.includedJavascript(request, "/static/js/mobile/index.js") %>--%>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/index.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/countdown/jquery.countdown.js")%>
<script>
//    $('#my-menu1').fancyspinbox();
//    $('#my-menu2').fancyspinbox();
//    $('#my-menu3').fancyspinbox();
    $(function(){
        var source = '${videoInfo.source}';

        var time = new Date('${videoInfo.classtime} ${videoInfo.source}').getTime();//1495677600;
        var note = $("#note"),
            ts = new Date(time*1000),
            newYear = true;
        if((new Date()) > ts){
            //The new year is here! Count towards something else.
            //Notice the *1000 at the end - time must be in milliseconds
            ts = (new Date()).getTime() + 10*24*60*60*1000;

            newYear = false;
        }

        $("#countdown").countdown({
            timestamp :ts,
            callback : function(days,hours,minutes,seconds){
                var message = "";

                message += days + " day" + (days == 1 ? '':'s' ) + ", ";
                message += hours + " hour" + (hours == 1 ? '':'s' ) + ", ";
                message += minutes + " minute" + (minutes == 1 ? '':'s' ) + " and ";
                message += seconds + " second" + (seconds == 1 ? '':'s' ) + " <br/> ";

                if(newYear){
                    message += "left until the new year!";
                }else{
                    message += "left to 10 days from now!";
                }

                note.html(message);

            }
        });
    });
</script>
</html>
