<%--
  Created by IntelliJ IDEA.
  User: WangPengfei
  Date: 2017/3/9
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <title>网络课程</title>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
    </script>
    <base target="_self">
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<div class="main clearfix">
    <input type="hidden" id="openid" name="openid" value="${openid}">
    <div class="wlkc_list">
        <%--<div class="wlkc_list_djbd"><p>绑定会员账户，方便您学习专属课程，<a onclick="djbdhy();">点击绑定 >></a></p></div>--%>
        <div class="container">
            <select id="my-menu1">
                <option value="" selected>全部</option>
                <option value="1">普卡课程</option>
                <option value="2">银卡课程</option>
                <option value="3">高端课程</option>
            </select>
            <select id="my-menu2">
                <option value="1" selected>热门</option>
                <option value="2">最新</option>
            </select>
            <select id="my-menu3">
                <option value="0" selected>分类</option>
                <option value="1">2016汇算清缴</option>
                <option value="2">营改增</option>
                <option value="3">增值税</option>
                <option value="4">企业所得税</option>
                <option value="5">个人所得税</option>
                <option value="6">实务政策</option>
            </select>
        </div>



        <div class="wlkc_list_con" id="aa">
           <%-- <section>
                <a href="#">
                    <img src="<%=ctx%>/static/images/mobile/zbkc_test1.png" alt="" />
                    <div class="txt_area">
                        <h4>判断企业税负轻判断企业税负轻重要看三方重要看三方</h4>
                        <p>2月16日   15:00</p>
                        <h6>银卡专享</h6>
                    </div>
                </a>
            </section>

            <section>
                <a href="#">
                    <img src="<%=ctx%>/static/images/mobile/zbkc_test2.png" alt="" />
                    <div class="txt_area">
                        <h4>会计人，培训费出新规！</h4>
                        <p>2月16日   15:00</p>
                        <h6>银卡专享</h6>
                    </div>
                </a>
            </section>

            <section>
                <a href="#">
                    <img src="<%=ctx%>/static/images/mobile/zbkc_test3.png" alt="" />
                    <div class="txt_area">
                        <h4>税负波动原因多，政策加力</h4>
                        <p>2月16日   15:00</p>
                        <h6>高端专享</h6>
                    </div>
                </a>
            </section>

            <section>
                <a href="#">
                    <img src="<%=ctx%>/static/images/mobile/zbkc_test4.png" alt="" />
                    <div class="txt_area">
                        <h4>税负波动原因多，政策加力</h4>
                        <p>2月16日   15:00</p>
                        <h6>高端专享</h6>
                    </div>
                </a>
            </section>

            <section>
                <a href="#">
                    <img src="<%=ctx%>/static/images/mobile/zbkc_test5.png" alt="" />
                    <div class="txt_area">
                        <h4>4步查出增值税发票的真假</h4>
                        <p>2月16日   15:00</p>
                        <h6>高端专享</h6>
                    </div>
                </a>
            </section>--%>
        </div>
    </div>

</div>
</body>
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/jquery.fancyspinbox.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/commonIndex.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>
    $('#my-menu1').fancyspinbox();
    $('#my-menu2').fancyspinbox();
    $('#my-menu3').fancyspinbox();
</script>
</html>
