<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var isGetWxSignature = true;
        var fwh = '${fwh}';
        var wx_key = '${wx_key}';
        var ts_content = '${ts_content}';
    </script>
    <base target="_self">
    <title>每日听税</title>
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
</head>
<body>
<div class="main clearfix">
    <!--------------音频--------------->
    <div class="mrts_audio">
        <input type="hidden" id="openid" name="openid" value="${ts.openid}">
        <input type="hidden" id="id" name="id" value="${ts.id}" />
        <input type="hidden" id="image" name="image" value="${ts.image}" />
        <input type="hidden" id="audio_src" name="audio_src" value="${ts.audio_src}" />
        <input type="hidden" id="file_host" name="file_host" value="${file_host}" />
        <input type="hidden" id="ewm_src" name="ewm_src" value="${ewm_src}" />
        <input type="hidden" id="title" name="title" value="${ts.title}" />

        <h2 id="audio_title">${ts.title}</h2>
        <img id="audio_img" src="<%=ctx%>/static/images/mobile/ltsf_1.jpg" alt="" />

        <div class="mrts_time">
           <!--   <span id="dqsj" class="ksc">0:00</span>
            <span id="shc" class="zsc">缓冲..</span>  -->
            <span id="dqsj" class="ksc">3:30</span>
            <span id="shc" class="zsc">6:50</span> 
        </div>
        <div class="progressBar" id="progressBar">
            <div id="bab"></div>
            <div id="bar"></div>
        </div>
        <div class="mrts_menu">
            <a href="javascript:toTsList();" class="cd"></a>
            <a id="ktBtn" href="javascript:void(0);" class="kt"></a>
            <%--<a href="#" class="zt"></a>--%>
            <a id="bfkzBtn" href="javascript:void(0);" class="bf"></a>
            <a id="kjBtn" href="javascript:void(0);" class="kj"></a>
            <a id="dzBtn" href="javascript:dianzan();" class="dz"></a>
        </div>
        <%--<audio controls="controls" src="<%=ctx %>/hyfw/ts/queryAudioById?id=${ts.id}" style="width:100%;display:block; margin-top:20px;"></audio>--%>

    </div>
    <!--------------正文--------------->
    <div class="mrts_detail">
        <div class="mrts_detail_tit"><h4>每日听税</h4></div>
        <div class="mrts_detail_con">
            <h4 id="mrts_title">${ts.title}</h4>
            <h6>时长 <span id="shc_info" style="float:none;">缓冲..</span>  <%--${ts.cjsj}--%> <span>${ts.playTimes + 1234}次播放</span></h6>
            <%--<div id="ts_info" class="ts_info">${ts.info}</div>--%>
            <%--<input type="hidden" id="tsContent" value="${ts.content}" />--%>
            <div id="ts_content" class="ts_content" style="/*display:none;*/">${ts_content}</div>
            <a id="showContentBtn" href="javascript:showContent();"><img src="<%=ctx%>/static/images/mobile/mrts_ckqw.png" alt="" /></a>
        </div>

    </div>
    <div class="zbkc_comment">
        <div class="zbkc_comment_tit clearfix"><h4>用户评论</h4></div>
        <div class="zbkc_commen_con">
            <ul id="comment_ul">
                <li>
                    <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                    <p>讲得很好。</p>
                </li>
                <li>
                    <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                    <p>好的。</p>
                </li>
                <li>
                    <h6>匿名用户<span>01-19&nbsp;&nbsp;&nbsp;&nbsp;11:10</span></h6>
                    <p>讲得很好。</p>
                </li>
            </ul>
        </div>
    </div>
</div><!--main-->
<!--------------底部--------------->
<footer class="flexbox">
    <ul>
        <li><a href="javascript:shoucang();" id="shoucBtn" class="flexbox-item nth1"><i></i><p>收藏</p></a></li>
        <li><a href="javascript:dianzan();" id="dzBtn1" class="flexbox-item nth2"><i></i><p id="upCount">150</p></a></li>
        <li><a href="javascript:pinglun();" class="flexbox-item nth3"><i></i><p>评论</p></a></li>
        <%--<li><a href="javascript:tsYouQing();" class="flexbox-item nth4"><i></i><p>邀请听</p></a></li>--%>
    </ul>
</footer>

<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquerySession.js") %>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js" ></script>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>
    /**
     * 分享功能
     * @param title
     * @param desc
     * @param link
     * @param imgUrl
     */
     /* var _fxUrl = "http://mobile.csfw360.com/sharpContent/renderSharpContentPage?id=${ts.id}&modelName=ts&isVedio=false&wxopenid=${ts.openid}&wx_key=${fwh.key}"; */
     var _fxUrl = "http://mobile.csfw360.com/sharpContent/renderSharpContentPage?id=${ts.id}&modelName=ts&isVedio=false&wx_key=${fwh.key}";
     youqingPY("${ts.title}","财税通，您身边的税务专家",_fxUrl,"http://mobile.csfw360.com/static/images/mobile/ltsf_1.jpg");
</script>
</body>
</html>
