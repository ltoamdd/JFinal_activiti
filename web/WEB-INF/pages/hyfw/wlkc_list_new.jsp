<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: WangPengfei
  Date: 2017/3/9
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
<%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
<%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
<%=RenderHelper.includedStyle(request, "/static/css/csfw.css") %>
<%=RenderHelper.includedStyle(request, "/static/iconfont/iconfont.css") %>
<%=RenderHelper.includedStyle(request, "/static/kcb/stylesheet/index.css") %>
<%=RenderHelper.includedStyle(request, "/static/kcb/font/iconfont.css") %>
<%=RenderHelper.includedStyle(request, "/static/kcb/javascript/swiper/dist/css/swiper.min.css") %>
<title>网络课程</title>
<%
        String ctx = request.getContextPath();
    %>
<script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
    </script>
<base target="_self">
<%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
<style type="text/css">

	


</style>
</head>
<body>
	<nav class="nav" style="position: absolute;">
		<div class="pull-left">
			<i class="iconfont icon-1mulu" id="collapse"></i>
		</div>
		<div class="pull-right">
			<i class="iconfont icon-sousuo" id="search"></i>
		</div>
		<h1 class="nav-title" id="wlkc-title">
			全部<span class="title-line"></span>
		</h1>
	</nav>
	<div class="container" id="wlkc_list">
		<input type="hidden" id="openid" name="openid" value="${openid}">
		<%--<div class="wlkc_list">--%>
		<%--<div class="serach">--%>
		<%--<div class="cszx_search padding-20">--%>
		<%--<form id="searchFrom_cszx" action="<%=ctx%>/common/search" method="get">--%>
		<%----%>
		<%--<input type="text" name="search" id="search" placeholder="搜索财税问题">--%>
		<%--</form>--%>
		<%--</div>--%>
		<%--</div>--%>
		<!-- <div class="wlkc_list_con" id="wlkc_list_con"></div> -->
		 <ul class="list-main" id="ztxkul">
		</ul>
	</div>

	</div>


	<div id="panel-left" class="panel panel-left">
		<div class="panel-body">
			<dl id="fenlei-list">
				<c:forEach items="${kindsList}" var="kinds">
					<dt class="active">
						<p class="pull-left">${kinds.kindsname}</p>
						<i class="pull-right iconfont icon-jiantou"></i>
					</dt>
					<dd>
						<ul data-value="${kinds.kindsvalue}">
							<c:forEach items="${kinds.fllist}" var="fllist"
								varStatus="status">
								<li
									<c:if test="${kinds.fllistCount<=status.count}">class="noBorder"</c:if>
									data-value="${fllist.realvalue}"
									data-keyname="${fllist.keyname}"><a
									href="javascript:void(0);">${fllist.keyname}</a></li>
							</c:forEach>
						</ul>
					</dd>
				</c:forEach>
			</dl>
		</div>
		<div class="panel-footer">
			<button id="reset-search">
				<i class="iconfont icon-zhongzhi"></i>重置条件
			</button>
			<button id="closed-search">
				<i class="iconfont icon-guanbi1"></i>取消搜索
			</button>
		</div>
	</div>


	<div id="panel-right" class="panel panel-right">
		<div class="panel-body">
			<section class="searchbar">
				<div class="serachbar-tools">
					<i class="iconfont icon-sousuo"></i> <input type="text"
						placeholder="搜索从这里开始......" id="search_title" />
				</div>
				<button class="btn" id="closed-btn">取消</button>
			</section>

			<h3>热门搜索</h3>
			<ul class="hot-list">
				<c:forEach items="${kindsList}" var="kinds">
					<c:if test="${kinds.kindsvalue == 'wlkc_rem'}">
						<c:forEach items="${kinds.fllist}" var="fllist" varStatus="status">
							<li><a
								href="javascript:searchAsRem('${fllist.realvalue}', '${fllist.keyname}');">${fllist.keyname}</a></li>
						</c:forEach>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	</div>



</body>
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/commonIndex.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>

<script type="text/javascript">
    $(function(){

        $("#reset-search").click(function(){
            $("#panel-left a").css('color','#333');
        });


        $("#closed-search").click(function(){
            $("#panel-left").removeClass('active');
            $("#list,body").css({'height':'auto','overflow':'auto'});
        });





        $("#collapse").click(function(){

            $("#panel-left").addClass('active');
            $("#list,body").css({'height':$(window).height(),'overflow':'hidden'});

        });

        $("#search").click(function(){

            $("#panel-right").addClass('active');
            $("#list,body").css({'height':$(window).height(),'overflow':'hidden'});

        });

        $("#closed-btn").click(function(){
            $("#panel-right").removeClass('active');
            $("#list,body").css({'height':'auto','overflow':'auto','overflow-x':'hidden'});
        });

        $("#panel-left").on('click','dt',function(){
            if($(this).hasClass('active')){
                $(this).removeClass('active')
            }else{
                $(this).addClass('active');
            }
        });


        $("#panel-left").on('click','a',function(){


            $("#panel-left").removeClass('active');
            $("#list,body").css({'height':'auto','overflow':'auto'});


            $("#panel-left a").css('color','#333');
            $(this).css('color','orange');

            var _li = $(this).parent();
            var _ul = $(this).parent().parent();

            postData.pageNum = 1;
            postData.start = 0;
            if(_ul.attr("data-value")=="dj"){
                postData.hot_label = 0;
                var _rankDj = _li.attr("data-value");
                postData.dj = _rankDj;
            }else{
                var _rankrealValue = _li.attr("data-value");
                postData.dj=0;
                postData.hot_label = _rankrealValue+'';
            }

            initWlkcList();

            var data_keyname = _li.attr("data-keyname");
            $("#wlkc-title").text(data_keyname);
        });
    })
</script>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
</html>
