<%--
  Created by IntelliJ IDEA.
  User: WangPengfei
  Date: 2017/3/9
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.qysoft.rapid.utils.RenderHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <%=RenderHelper.includedStyle(request, "/static/plugin/mobile/layer_mobile/need/layer.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/base.css") %>
    <%=RenderHelper.includedStyle(request, "/static/css/css.css") %>
     <%=RenderHelper.includedStyle(request, "/static/kcb/stylesheet/index.css") %>
    <%=RenderHelper.includedStyle(request, "/static/kcb/font/iconfont.css") %>
    <%=RenderHelper.includedStyle(request, "/static/kcb/javascript/swiper/dist/css/swiper.min.css") %>
    <title>微课堂</title>
    <%
        String ctx = request.getContextPath();
    %>
    <script type="text/javascript">
        var ctx = "<%=ctx%>";
        var wx_key = "${wx_key}";
        var openid = "${openid}";
    </script>
    <base target="_self">
    <%@include file="/WEB-INF/pages/common/baidutongji.jsp"%>
    <style type="text/css">
    	.nav-list {
       	position: fixed;
    	width: 100%;
    	z-index: 1;
    	background-color: #fff;
		}
		.main{
			margin-top: 51px;
		}
    </style>
</head>
<body>
<nav class="nav-list">
		<a href="javascript:backShuye();" class="pull-left"> <span
			class="iconfont icon-zuojiantou"></span>
		</a> 
		<!-- <a href="javascript:openidChaxun();" class="pull-right"> <span
			class="iconfont icon-sousuo"></span>
		</a> -->
		<p class="nav-title">微课堂</p>
</nav>
<div class="main clearfix">
    <input type="hidden" id="openid" name="openid" value="${openid}">
    <div class="wlkc_list">
        <%--<div class="wlkc_list_djbd"><p>绑定会员账户，方便您学习专属课程，<a onclick="djbdhy();">点击绑定 >></a></p></div>--%>
        <%--<div class="container">
            <select id="my-menu1">
                <option value="" selected>全部</option>
                <option value="1">普卡课程</option>
                <option value="2">银卡课程</option>
                <option value="3">高端课程</option>
            </select>
            <select id="my-menu2">
                <option value="1" selected>热门</option>
                <option value="2">最新</option>
            </select>
            <select id="my-menu3">
                <option value="0" selected>分类</option>
                <option value="1">2016汇算清缴</option>
                <option value="2">营改增</option>
                <option value="3">增值税</option>
                <option value="4">企业所得税</option>
                <option value="5">个人所得税</option>
                <option value="6">实务政策</option>
            </select>
        </div>--%>
       <!--  <div class="wlkc_list_con" id="aa"></div> -->
       <ul class="list-main" id="ztxkul"></ul>
    </div>

</div>
</body>
<%=RenderHelper.includedJavascript(request, "/static/plugin/jquery.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/jquery.fancyspinbox.js") %>
<%=RenderHelper.includedJavascript(request, "/static/plugin/mobile/layer_mobile/layer.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/common.js") %>
<%=RenderHelper.includedJavascript(request, "/static/js/mobile/commonIndex.js") %>
<%=RenderHelper.includedAutoJavascript(request) %>
<script>
    $('#my-menu1').fancyspinbox();
    $('#my-menu2').fancyspinbox();
    $('#my-menu3').fancyspinbox();
</script>
</html>
