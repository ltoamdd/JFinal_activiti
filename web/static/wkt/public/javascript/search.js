function listSearch(callback){
	var winHeight = window.innerHeight;
	 
	$('.overflow-list').css('height',winHeight);
	
	$('.list-search').on('click',"a[data-type='list']",function(){
		var _this = $(this);
		
		if(_this.next().is(':visible')){
			$("body").css('overflow','auto')
			_this.next().slideUp()
		}else{
			$("body").css('overflow','hidden')
			_this.next().slideDown().parent().siblings().children('.overflow-list').hide()
		}
	})
	
	$('.list-search').on('click','.overflow-list a',function(e){
		$("body").css('overflow','auto');
		$(this).parents('.overflow-list').slideUp();
		callback();
		e.preventDefault()
	})
}
