/*
 * 作者 ：yanxiaoyu
 * 优化 :  否
 * 封装 ： 否
 * 
 * */

function tabs(){
	var _index = $('.tabs-head').children('span.active').index();
	
	if(_index){
		$('.tabs-head').children('span:first').addClass('active');
		$('.tabs-main').children('.tabs-list:first').show()
	}else{
		$('.tabs-main').children('.tabs-list').eq(_index).show()
	}
	
	$('.tabs').on('click','.tabs-head span',function(){
		var _index = $(this).index();
		$(this).addClass('active').siblings().removeClass('active');
		$('.tabs-main').children('.tabs-list').eq(_index).show().siblings().hide()
		
	})
}

function tabsMain(){
	var winHeight = window.innerHeight;
	
	$('.tabs-main').css('height',winHeight-200-76-41-51)
}