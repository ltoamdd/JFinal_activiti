/**
 * Created by Administrator on 2017/3/9.
 */
// 设置页面viewport && 基准rem
(function(e,t){var i=document,n=window;var l=i.documentElement;var a,r;var d,s=document.createElement("style");var o;function m(){var i=l.getBoundingClientRect().width;if(!t){t=540}if(i>t){i=t}var n=i*100/e;s.innerHTML="html{font-size:"+n+"px;}"}a=i.querySelector('meta[name="viewport"]');r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no";if(a){a.setAttribute("content",r)}else{a=i.createElement("meta");a.setAttribute("name","viewport");a.setAttribute("content",r);if(l.firstElementChild){l.firstElementChild.appendChild(a)}else{var c=i.createElement("div");c.appendChild(a);i.write(c.innerHTML);c=null}}m();if(l.firstElementChild){l.firstElementChild.appendChild(s)}else{var c=i.createElement("div");c.appendChild(s);i.write(c.innerHTML);c=null}n.addEventListener("resize",function(){clearTimeout(o);o=setTimeout(m,300)},false);n.addEventListener("pageshow",function(e){if(e.persisted){clearTimeout(o);o=setTimeout(m,300)}},false);if(i.readyState==="complete"){i.body.style.fontSize="16px"}else{i.addEventListener("DOMContentLoaded",function(e){i.body.style.fontSize="16px"},false)}})(750,750);
// console.log(ctx);
// console.log("----------------------------");
var postData = {
	    pageNum : 1,
	    dj:$("#dj").val(),
	    pageSize: 15,
	    start: (1 - 1) * 15,
	    hot_label:0,
	    sort: 1,
    	wx_key:wx_key,
	    openid:$("#openid").val(),
	    zhpx:'zx'
}

$(function() {
    initWlkcList();
    initEvent();
    listSearch()
});


//初始化列表
function initWlkcList(){
	//debugger;
    doService(ctx + '/hyfw/wlkc/queryAllWlkc', postData, function(json) {
    	$("#ztxkul").empty();
    	console.info(postData);
        postData.total = json.total;
        initWlkcBlock(json.list);
    });
}

//初始化网络课程单元块模块
function initWlkcBlock(data) {
    if (data.length <= 0) {
        //debugger;
        qyMsg("数据已加载完成");
    }
    console.info(data);
    for(var i=0;i<data.length;i++){
        var str="<li>" +
        		" <a href=\"javascript:wlkcDetail('"+data[i].id+"','" + data[i].cid + "','article')\">" +
        				" <img src='" + getImgByPath(data[i].img) +"' alt='' /> " +
        					" <section> ";
        					if(data[i].rank==0){
        						str+=" <span class='badge badge-gary' style='font-size:12px'>"+showRank(data[i])+"</span> ";
        					}else{
        						str+=" <span class='badge badge-orange' style='font-size:12px'>"+showRank(data[i])+"</span> ";
        					}
        					str+=" <p>"+data[i].title+"</p> " +
        					" <div class='info'><span>"+kcclick(data[i])+"人</span><span>"+data[i].createtime1+"</span></div> " +
        					" </section> " +
        					" </a> " +
        					" </li>";
        $("#ztxkul").append(str);
        remSize();
    }
}
function wlkcDetail(id,cid,nrlx) {//csrd/csrdZtDetailIndex?articleId=1&openid=undefined#
    window.location.href = ctx + getURLByLx(id,nrlx,cid);//'/csrd/csrdZtDetailIndex?articleId=' + id + '&openid=' + $("#openid").val();
}
function getURLByLx(id, lx, cid, pid) {
    if (lx === 'ts') {
        return "/hyfw/ts/tsDetail?id=" + id + "&wx_key=" + wx_key;
    } else if (lx === 'article') {
        switch (cid){
            case '32'://点播网络课程
            return "/wlkc/wlkcDetail?id=" + id + "&wx_key=" + wx_key+"&bofangstatus=wlkc";
            break;
        }
        switch (cid){
        case '33'://点播网络课程
            return "/wlkc/wlkcDetail?id=" + id + "&wx_key=" + wx_key+"&bofangstatus=wlkc";
            break;
        }
    }
    return "";
}
function showRank(data){
    //debugger;
    if(data.rank == 2){
        return "普通专享";
    }else if(data.rank == 3){
        return "银卡专享";
    }else if(data.rank == 4){
        return "高端专享";
    }else if (data.rank == 0) {
		if(data.status_sfmf){
			if(data.status_sfmf=="1"){
				return "免费课程";
			}else if(data.status_sfmf=="2"){
				return "限时课程";
			}else if(data.status_sfmf=="3"){
				return "试听课程";
			}
		}else{
			return "免费课程";
		}
	}
}
function kcclick(data){
	console.info("点击数"+data.click);
	if(data.click==undefined){
		return 0;
	}else{
		return data.click;
	}
}

function initEvent() {
    /**
     * 滚到到浏览器底部以后 加载下一页数据
     */

    $(window).scroll(function() {
        //debugger;
        // window.pageYOffset 滚动条位置
        // window.innerHeight 视口尺寸
        // document.body.scrollHeight 文档高度
        if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
            if (postData.pageNum * postData.pageSize < postData.total) {
                //initWlkcList();
                loadWlkcList();
            }
        }
    });


    $('#my-menu1').change(function () {
        //alert(121212);
        var rankDj = $(this).children('option:selected').val();
        postData.pageNum = 1;
        postData.start = 0;
        if(rankDj == 1){
            postData.dj = 2;

        }if(rankDj == 2){
            postData.dj = 3;
        }
        if(rankDj == 3){
            postData.dj = 4;
        }

        if(rankDj == 0){
            postData.dj = 0;
        }
        initWlkcList();
    });

    $('#my-menu2').change(function () {
        var zxZr = $(this).children('option:selected').val();//最新最热切换
        postData.pageNum = 1;
        postData.start = 0;
        if(zxZr == 1){
            postData.sort = 1;
        }else{
            postData.sort = -1;
        }
        initWlkcList();
    });

}


function loadWlkcList() {
    postData.pageNum += 1;
    postData.start = (postData.pageNum - 1) * postData.pageSize;
    doService(ctx + '/hyfw/wlkc/queryAllWlkc', postData, function(json) {
        postData.total = json.total;
        initWlkcBlock(json.list);
    });
}

function showWlkcDetail(id) {
    console.log(id);
}

function  djbdhy() {
    //alert(111);
    doService(ctx + '/wode/hybd/queryHybd', {openid: $("#openid").val(), wx_key: wx_key}, function(json) {
        //debugger
        console.log(json);
        if(json.result) {
            window.location.href = ctx + '/hyfw/wlkc?wx_key=' + wx_key;
            $(".wlkc_list_djbd").hide();
        } else {
            toHybd();
            $(".wlkc_list_djbd").show();
        }
    });

}
function shuizhong(){
	var winHeight = window.innerHeight;
	$('.overflow-list').css('height',winHeight);
	if($("#divsz").is(':visible')){
		$("body").css('overflow','auto')
		$("#divsz").slideUp();
	}else{
		$("body").css('overflow','hidden')
		$("#divsz").slideDown().parent().siblings().children('.overflow-list').hide()
	}
}
function hyqx(){
	var winHeight = window.innerHeight;
	$('.overflow-list').css('height',winHeight);
	if($("#hyqx").is(':visible')){
		$("body").css('overflow','auto')
		$("#hyqx").slideUp();
	}else{
		$("body").css('overflow','hidden')
		$("#hyqx").slideDown().parent().siblings().children('.overflow-list').hide()
	}
}
function zhpx(){
	var winHeight = window.innerHeight;
	$('.overflow-list').css('height',winHeight);
	if($("#zhpx").is(':visible')){
		$("body").css('overflow','auto')
		$("#zhpx").slideUp();
	}else{
		$("body").css('overflow','hidden')
		$("#zhpx").slideDown().parent().siblings().children('.overflow-list').hide()
	}
}
function initWlkcDataSz(val, text){
	$("body").css('overflow','auto');
	//$("#divsz").slideUp();
	$("#divsz").prev().children('span:first').text(text)
	searchSz(val,text);
	queding("sz");
}

function initWlkcDataZhpx(val, text){
	$("#zhpx").prev().children('span:first').text(text);
	searchZxzr(val,text);
	queding("zxzr");
}
function initWlkcDataHyqx(val, text,yhqx){
	$("body").css('overflow','auto');
	//$("#hyqx").slideUp();
	$("#hyqx").prev().children('span:first').text(text)
	searchHyqx(val,text);
	queding(yhqx);
}
function searchSz(val, text) {
    postData.pageNum = 1;
    postData.start = 0;
    postData.hot_label = val;
    $("#panel-right").removeClass('active');
    $("#wlkc-title").text(text);
}
function searchZxzr(val, text) {
    postData.pageNum = 1;
    postData.start = 0;
    postData.zhpx = val;
    $("#panel-right").removeClass('active');
    $("#wlkc-title").text(text);
}

function searchHyqx(val, text) {
	if(val=="mf"){
		postData.dj = 0;
		postData.status_sfmf=1;
	}else if(val=="xs"){
		postData.dj = 0;
		postData.status_sfmf=2;
		
	}else if(val=="st"){
		postData.dj = 0;
		postData.status_sfmf=3;
	}else{
		postData.dj = val;
        postData.status_sfmf="";
	}
    postData.pageNum = 1;
    postData.start = 0;
    $("#panel-right").removeClass('active');
    $("#wlkc-title").text(text);
}
function shuizhong(){
	var winHeight = window.innerHeight;
	$('.overflow-list').css('height',winHeight);
	if($("#divsz").is(':visible')){
		$("body").css('overflow','auto')
		$("#divsz").slideUp();
	}else{
		$("body").css('overflow','hidden')
		$("#divsz").slideDown().parent().siblings().children('.overflow-list').hide()
	}
}


//跳转到课程包里面的课程列表
function kcbkc(id,kcid) {
	var url="/ztxl/kcindex?kcbid="+id+"&kcid="+kcid+"&wx_key="+wx_key;
    window.location.href = ctx + url;
}
function searchAsRem(val, text) {
    postData.dj = 0;
    postData.pageNum = 1;
    postData.start = 0;
    postData.hot_label = val;
    initWlkcList();
    $("#panel-right").removeClass('active');
    $("#wlkc-title").text(text);
}
function chongzhi(a){
	postData.pageNum = 1;
	postData.start = 0;
	if(a=='sz'){
		$("#divsz").children().children('li').removeClass("active");
		postData.hot_label = 0;
		$("#divsz").prev().children('span:first').text("税种")
	}
	if(a=='zxzr'){
		$("#zhpx").children().children('li').removeClass("active");
		postData.zhpx = 'zx';
		$("#zhpx").prev().children('span:first').text("最新")

	}
	if(a=='yhqx'){
		$("#hyqx").children().children('li').removeClass("active");
		postData.dj = 0;
		$("#hyqx").prev().children('span:first').text("会员权限")

	}
}
function queding(a){
	if(a=='sz'){
		$("body").css('overflow','auto');
		$("#divsz").slideUp();
		initWlkcList();
		var qb=$("#qb").text();
		if(qb=="普卡会员"||qb=="银卡会员"||qb=="金卡会员"){
		}else{
			$("#qb").text("全部");
			$("#panel-left a").removeAttr("style");
			$(this).removeAttr("style");;
		}
		
	}
	if(a=='zxzr'){
		$("body").css('overflow','auto');
		$("#zhpx").slideUp();
		initWlkcList();
	}
	if(a=='yhqx'){
		$("body").css('overflow','auto');
		$("#hyqx").slideUp();
		initWlkcList();
		var qb=$("#qb").text();
		if(qb=="普卡会员"||qb=="银卡会员"||qb=="金卡会员"){
			$("#qb").text("全部");
			$("#panel-left a").removeAttr("style");;
			$(this).removeAttr("style");;
		}
	}
}
function listSearch(){
	
var winHeight = window.innerHeight;
 

$('.list-search').on('click','.overflow-list a',function(e){
	$(this).parent('li').addClass("active");
	$(this).parent('li').siblings().removeClass("active");
})
}
function backShuye(){
    window.history.go(-1);
}
function sousuo(){
	postData.dj = 0;
	postData.pageNum = 1;
	postData.start = 0;
	postData.search_title = $("#search_title").val();
	postData.hot_label = '';
	initWlkcList();
	$("#panel-right").removeClass('active');
	
}
function remSize(){
	var maxWidht = $(window).width();
	if(maxWidht >340){
		$('.list-main section').css('font-size','0.28rem')
		$('.info>span:nth-child(1)').css({
			 "background-size":"0.3rem 0.3rem",
		})
	}else{
		$('.list-main section').css('font-size','0.22rem')
		$('.info>span:nth-child(1)').css({
			 "background-size":"0.22rem 0.22rem",
	    	 "padding-left":"0.22rem"
		})
	}
}
