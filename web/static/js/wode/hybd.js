// 设置页面viewport && 基准rem
(function(e,t){var i=document,n=window;var l=i.documentElement;var a,r;var d,s=document.createElement("style");var o;function m(){var i=l.getBoundingClientRect().width;if(!t){t=540}if(i>t){i=t}var n=i*100/e;s.innerHTML="html{font-size:"+n+"px;}"}a=i.querySelector('meta[name="viewport"]');r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no";if(a){a.setAttribute("content",r)}else{a=i.createElement("meta");a.setAttribute("name","viewport");a.setAttribute("content",r);if(l.firstElementChild){l.firstElementChild.appendChild(a)}else{var c=i.createElement("div");c.appendChild(a);i.write(c.innerHTML);c=null}}m();if(l.firstElementChild){l.firstElementChild.appendChild(s)}else{var c=i.createElement("div");c.appendChild(s);i.write(c.innerHTML);c=null}n.addEventListener("resize",function(){clearTimeout(o);o=setTimeout(m,300)},false);n.addEventListener("pageshow",function(e){if(e.persisted){clearTimeout(o);o=setTimeout(m,300)}},false);if(i.readyState==="complete"){i.body.style.fontSize="16px"}else{i.addEventListener("DOMContentLoaded",function(e){i.body.style.fontSize="16px"},false)}})(750,750);
$(function() {
    initEvent();
});

function initEvent() {
    $(".hybd-input").on('keyup', function() {
        if($(this).val().length > 0) {
            $(this).next().css({'visibility': "visible"});
        } else {
            $(this).next().css({'visibility': "hidden"});
        }
    });
    $(".input_reset").on('click', function() {
        $(this).prev().val('');
    });
}

/**
 * 提交绑定
 */
function saveHybd() {
   //  debugger
   //  if ($("#toUrl").val()) {
   //     window.location.href = ctx + $("#toUrl").val();
   // }
    qyConfirm("确定要执行绑定操作吗？", function(val) {
        if (val) {
            var openid = $("#openid").val();
            var nsrsbh = $("#nsrsbh").val();
            var username = $("#username").val();
            var password = $("#password").val();
            // if ($.trim(nsrsbh) == '' || $.trim(username) == '' || $.trim(password) == '') {
            if ($.trim(username) == '' || $.trim(password) == '') {
                qyMsg('请输入绑定信息');
                return;
            }
            var data = {
                openid: openid,
                nsrsbh: nsrsbh,
                wx_key: wx_key,
                username: username,
                password: password
            }
            doService(ctx + '/wode/hybd/hybd', data, function(json) {
               if (json.result) {
                   qyMsg("绑定成功", function() {
                       if ($("#toUrl").val()) {
                           // window.location.href = $("#toUrl").val() + '?openid=' + openid + '&' + $("#articleIdName").val() + '=' + $("#articleIdVal").val();
                           var toUrl = $("#toUrl").val();
                           if (toUrl.indexOf(ctx) >= 0) {
                               window.location.href = $("#toUrl").val();
                           } else {
                               window.location.href = ctx + $("#toUrl").val();
                           }
                       } else {
                           window.location.href = ctx + "/wode/hybd?wx_key=" + wx_key + '&v=' + Math.random();
                       }
                   });

               } else {
                   qyMsg(json.msg);
               }
            });
        }
    });
}


// document.getElementById("username").addEventListener("keyup", function() {
//     if (this.value.length > 0) {
//         document.getElementById("input_reset1").style.visibility = "visible";
//         document.getElementById("input_reset1").onclick = function() {
//             document.getElementById("username").value = "";
//         }
//     } else {
//         document.getElementById("input_reset1").style.visibility = "hidden";
//     }
// });
//
// document.getElementById("usermember").addEventListener("keyup", function() {
//     if (this.value.length > 0) {
//         document.getElementById("input_reset2").style.visibility = "visible";
//         document.getElementById("input_reset2").onclick = function() {
//             document.getElementById("usermember").value = "";
//         }
//     } else {
//         document.getElementById("input_reset2").style.visibility = "hidden";
//     }
// });
//
// document.getElementById("password").addEventListener("keyup", function() {
//     if (this.value.length > 0) {
//         document.getElementById("input_reset3").style.visibility = "visible";
//         document.getElementById("input_reset3").onclick = function() {
//             document.getElementById("password").value = "";
//         }
//     } else {
//         document.getElementById("input_reset3").style.visibility = "hidden";
//     }
// });