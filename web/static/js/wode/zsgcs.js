// 设置页面viewport && 基准rem
(function(e,t){var i=document,n=window;var l=i.documentElement;var a,r;var d,s=document.createElement("style");var o;function m(){var i=l.getBoundingClientRect().width;if(!t){t=540}if(i>t){i=t}var n=i*100/e;s.innerHTML="html{font-size:"+n+"px;}"}a=i.querySelector('meta[name="viewport"]');r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no";if(a){a.setAttribute("content",r)}else{a=i.createElement("meta");a.setAttribute("name","viewport");a.setAttribute("content",r);if(l.firstElementChild){l.firstElementChild.appendChild(a)}else{var c=i.createElement("div");c.appendChild(a);i.write(c.innerHTML);c=null}}m();if(l.firstElementChild){l.firstElementChild.appendChild(s)}else{var c=i.createElement("div");c.appendChild(s);i.write(c.innerHTML);c=null}n.addEventListener("resize",function(){clearTimeout(o);o=setTimeout(m,300)},false);n.addEventListener("pageshow",function(e){if(e.persisted){clearTimeout(o);o=setTimeout(m,300)}},false);if(i.readyState==="complete"){i.body.style.fontSize="16px"}else{i.addEventListener("DOMContentLoaded",function(e){i.body.style.fontSize="16px"},false)}})(750,750);
$(function() {
    var hyxx = JSON.parse(hyxxJson);
    if (hyxx.khjl_xingm) {
        $("#xingm_title").text("我是您的客户经理" + hyxx.khjl_xingm + "，竭诚为您服务！");

        $("#xingm").text(hyxx.khjl_xingm);
    } else {
        $("#xingm_title").text("暂时未找到匹配的客户经理，请联系：");
        $("#xingm").remove();
    }
    if (hyxx.khjl_lxdh) {
        $("#lxdh").html(hyxx.khjl_lxdh + '<a href="tel:' + hyxx.khjl_lxdh + '">一键拔号</a>');
    } else {
        $("#lxdh").html('0351-2148571<a href="tel:0351-2148571">一键拔号</a>');
    }

});