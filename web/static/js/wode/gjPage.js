// 设置页面viewport && 基准rem
(function(e,t){var i=document,n=window;var l=i.documentElement;var a,r;var d,s=document.createElement("style");var o;function m(){var i=l.getBoundingClientRect().width;if(!t){t=540}if(i>t){i=t}var n=i*100/e;s.innerHTML="html{font-size:"+n+"px;}"}a=i.querySelector('meta[name="viewport"]');r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no";if(a){a.setAttribute("content",r)}else{a=i.createElement("meta");a.setAttribute("name","viewport");a.setAttribute("content",r);if(l.firstElementChild){l.firstElementChild.appendChild(a)}else{var c=i.createElement("div");c.appendChild(a);i.write(c.innerHTML);c=null}}m();if(l.firstElementChild){l.firstElementChild.appendChild(s)}else{var c=i.createElement("div");c.appendChild(s);i.write(c.innerHTML);c=null}n.addEventListener("resize",function(){clearTimeout(o);o=setTimeout(m,300)},false);n.addEventListener("pageshow",function(e){if(e.persisted){clearTimeout(o);o=setTimeout(m,300)}},false);if(i.readyState==="complete"){i.body.style.fontSize="16px"}else{i.addEventListener("DOMContentLoaded",function(e){i.body.style.fontSize="16px"},false)}})(750,750);
function Page() {
    this.pageNum = 1;
    this.pageSize = 15;
}
$(function() {
    var page = new Page();
    loadDataList(page);

    $(window).scroll(function() {
        if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
            loadDataList(page);
        }
    });

});

function loadDataList(page) {
    var postData = {
        pageNum: page.pageNum,
        pageSize: page.pageSize,
        cid: $("#cid").val(),
        wx_key: wx_key,
        openid: $("#openid").val()
    };
    doService(ctx + "/wode/gjx/queryGjDataList", postData, function(json) {
        console.log(json.pageNum);
        if (json.result) {
            var data = json.data;
            if (page.pageNum == 1) {
                $("#dataList").empty();
                if (data.length <= 0) {
                    $("#dataList").append($("<section>暂无数据</section>"));
                }
            } else {
                if (data.length <= 0) {
                    qyMsg("数据已加载完成");
                }
            }
            page.total = json.total;
            page.totalPage = json.totalPage;
            if(page.pageNum <= page.totalPage) {
                page.pageNum = json.pageNum + 1;
            }
            data.forEach(function(item) {
                var _html = "<section>"
                        + "<a href=\"javascript:showDetail('" + item.id + "')\">"
                        + "<h4>第" + item.step + "期：" + item.title + "</h4>"
                        + "<p>" + item.classtime
                        + "<span>阅读：" + item.click + "</span>"
                        + "</p>"
                        + "</a>"
                    '</section>';
                $("#dataList").append($(_html));
            });
        }
    });
}

function showDetail(id) {
    var url = getURLByLx(id, 'article', $("#cid").val());
    window.location.href = ctx + url;
}