$(function(){
	headerMenu("#user");
	headerMenu("#handle");
	fold();
	nav();
	maxHeight();
})

$(window).resize(function(){
	maxHeight();
})

function headerMenu(obj){
	
	$(obj).click(function(){
		
		if($(this).hasClass('active')){
			
			$(this).removeClass('active');
			
		}else{
			
			$(this).addClass('active');
			
		}
		
		
	});
	
	$(document).click(function(e){
		
		
		if( $(e.target).parents('#handle').length==0 ){
			$("#handle").removeClass();
		}
		if($(e.target).parents('#user').length ==0 ){
			$("#user").removeClass();
		}
	}); 
}


function fold(){
	$("#fold").click(function(){
		
		if($("nav").hasClass('active')){
			
			$("nav").removeClass('active');
			
		}else{
			$("nav").addClass('active');
			
		}
		
	})
}

function nav (){
	$("#nav a").click(function(){
		var href = $(this).attr('href');
		if(href&&href!=""&&href!="#"){ 
			$("#nav a").removeClass('on_active');
			$(this).addClass('on_active');
			var _text = $(this).find('.title').text();
			var _url = ctx+$(this).attr('href');
			var _zyid = $(this).attr('zyid');
			if( !$(".tabs_header span[zyid='"+_zyid+"']").length){
				var _header = "<span zyid='"+_zyid+"' class='active'>" + _text +  "<i></i></span>";
				var _container = "<iframe zyid='"+_zyid+"' src='"+_url+"'></iframe>"
				$(".tabs_header").children().removeClass('active').end().append(_header);
				$('.tabs_container').children().hide().end().append(_container);
				bindContextMenu(_zyid);
			}else{
				$(".tabs_header span[zyid='"+_zyid+"']:eq(0)").trigger("click");			
			}
		}else{      
			if($(this).parents('dt').length){
				if($(this).parents('dt').hasClass('active')){
					$(this).parents('dt').removeClass('active').nextUntil('dt').hide();
				}else{
					$(this).parents('dt').addClass('active').nextUntil('dt').show();
				}
			}
		}
		return false;
	});
	$('.tabs_header').on('click','span',function(){
		var _index = $(this).index();
		$(this).addClass('active').siblings('span').removeClass('active');
		$('.tabs_container iframe').eq(_index).show().siblings().hide();
	});
	
	
	$('.tabs_header').on('click','i',function(){
		
		var _index = $(this).parent().index();
		
		var _activeIndex =  _index -1;
		
		$(this).parent().remove();
		$('.tabs_container iframe').eq(_index).remove();
		
		if(!$(".tabs_header .active").length){
			$('.tabs_header span').eq(_activeIndex).addClass('active');
			$('.tabs_container iframe').eq(_activeIndex).show();
		}
	});
	
	
	
}

function maxHeight(){
	$("#tabs").height($("#main").height())
}

function bindContextMenu(zyid){
	$(".tabs_header span[zyid='"+zyid+"']:eq(0)").contextMenu('myMenu1', {
    	menuStyle: {
        },
        itemStyle : {
        	'border':'0px',
        	'font':"12px/20px '微软雅黑'"
        },
        itemHoverStyle: {
        	'border':'0px',
        	'font':"12px/20px '微软雅黑'"
        },
      	bindings: {
	        'closeCurrent': function(t) {
	        	$(".tabs_header span[zyid='"+zyid+"'] i:eq(0)").trigger('click');
	        },
	        'closeOther': function(t) {
	        	$(".tabs_header span[zyid!='"+zyid+"'] i").trigger('click');
	        },
	        'closeAll': function(t) {
	        	$(".tabs_header span[zyid!='00'] i").trigger('click');
	        },
	        'refreshCurrent': function(t) {
	        	$(".tabs_container iframe[zyid='"+zyid+"']").get(0).contentWindow.location.reload(true);
	        }
      	}
    });
}