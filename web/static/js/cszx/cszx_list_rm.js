/**
 * Created by huangwei on 2017/3/20.
 */
// 设置页面viewport && 基准rem
var currentIndexDiv = 0;
(function(e,t){ var i=document,n=window; var l=i.documentElement; var a,r; var d,s=document.createElement("style"); var o; function m(){ var i=l.getBoundingClientRect().width; if(!t){ t=540 } if(i>t){ i=t } var n=i*100/e; s.innerHTML="html{font-size:"+n+"px;}" } a=i.querySelector('meta[name="viewport"]'); r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no"; if(a){ a.setAttribute("content",r) }else{ a=i.createElement("meta"); a.setAttribute("name","viewport"); a.setAttribute("content",r); if(l.firstElementChild){ l.firstElementChild.appendChild(a) }else{ var c=i.createElement("div"); c.appendChild(a); i.write(c.innerHTML); c=null } } m(); if(l.firstElementChild){ l.firstElementChild.appendChild(s) }else{ var c=i.createElement("div"); c.appendChild(s); i.write(c.innerHTML); c=null } n.addEventListener("resize",function(){ clearTimeout(o); o=setTimeout(m,300) },false); n.addEventListener("pageshow",function(e){ if(e.persisted){ clearTimeout(o); o=setTimeout(m,300) } },false); if(i.readyState==="complete"){ i.body.style.fontSize="16px" }else{ i.addEventListener("DOMContentLoaded",function(e){ i.body.style.fontSize="16px" },false) } })(750,750);
console.log(ctx+"-----------------------------");
var postData = {
    pageNum : 0,
    pageSize: 15,
    start: (1 - 1) * 15,
    sort: 1,
    repid:0,
    sqlid:"reply",
    orderType:"click",
    openid:$("#openid").val()
};
var artPostData= [];
$(function() {
    initEvent();
    loadArtList();
});


/**
 * 添加列表数据dom
 * @param data
 */
function insertArticList(data) {
    /**
     *
     <section>
     <a href="#"><h4>五险一金如何在个人所得税前扣除？</h4>
     <p>公司3年都未兑现经营层绩效年薪，每月只领取基薪，是否可以每月不扣除五险一金，等兑现绩效薪时扣除呢？</p></a>
     <h6><span>1小时前</span><em>看过18</em><a href="#">4</a></h6>
     </section>
     */
    for(var i = 0; i < data.length; i++) {
        var _html = "<section>"
                + "<a href=\"javascript:showDetail('" + data[i].id + "')\">"
                + '<h4>' + data[i].title + '</h4>'
                +'<p>' + data[i].content + '</p></a>'
                +'<h6><span>' + data[i].replytime1 + '</span><em>' + data[i].click + '人阅读过</em></h6>'//<a href="#">4</a>
                +'</section>';
                // + '<div class="txt_area">'
                // + "<h4>" + data[i].ztname + "</h4>"
                // + "<p>" + data[i].ztinfo + "</p>"
                // + "</div>"
                // + "</a>"
                // + "</section>";
        $('.cszx_con').append($(_html));
    }
}

/**
 * 排序
 */
function paixu() {
    postData.pageNum = 1;
    postData.start = 0;
    postData.sort = -postData.sort;
    initTsList();
}

function initEvent() {

    /**
     * 滚到到浏览器底部以后 加载下一页数据
     */
    $(window).scroll(function() {
        // window.pageYOffset 滚动条位置
        // window.innerHeight 视口尺寸
        // document.body.scrollHeight 文档高度
        if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
            if (postData.pageNum * postData.pageSize < postData.total) {
                // qyAlert("正在加载新数据");
                // loadTsList();
                loadArtList();
            }else{
                qyMsg("数据已加载完成");
            }
        }
    });
}


function loadArtList() {
    postData.pageNum += 1;
    postData.start = (postData.pageNum - 1) * postData.pageSize;
    doService(ctx + '/common/extQuery1', postData, function(json) {

        postData.total = json.totalRow;
        insertArticList(json.list);
    });
}


/**
 * 跳转到专题详情页面
 * @param id
 */
function showDetail(id) {
    window.location.href = ctx + '/cszx/cszxDetailIndex?id=' + id + '&openid=' + openid;
}


/**
 * 插入文章列表
 */
function initArtList(){
    doService(ctx + '/common/extQuery1', postData, function(json) {
        postData.total = json.totalRow;
        insertArticList(json.list);
    });
}

function zjzx(){
    postData = {
        pageNum : 0,
        pageSize: 8,
        start: (1 - 1) * 8,
        sort: 1,
        pid:0,
        repid:0,
        sqlid:"reply",
        openid:$("#openid").val()
    };
    loadArtList();
}

function myzx(){
    postData = {
        pageNum : 0,
        pageSize: 8,
        start: (1 - 1) * 8,
        sort: 1,
        repid:0,
        sqlid:"reply",
        openid:$("#openid").val()
    };
    loadArtList();
}

function zjzx_new(){
    postData = {
        pageNum : 0,
        pageSize: 8,
        start: (1 - 1) * 8,
        sort: 1,
        repid:0,
        sqlid:"reply",
        openid:$("#openid").val()
    };
    loadArtList();
}

function toForwordZjzx(id) {
    window.location.href = ctx + '/cszx/cszxZxzj?openid=' + openid;
}

function toForwordZjzxNew(id) {
    window.location.href = ctx + '/cszx/cszxListNew?openid=' + openid;
}

function toForwordMyzx(id) {
    window.location.href = ctx + '/cszx/cszxListMine?openid=' + openid;
}

function toForwordZjzxRm(id) {
    window.location.href = ctx + '/cszx/cszxListRm?openid=' + openid;
}