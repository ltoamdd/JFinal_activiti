$(function() {
    initArea();
    zclx();
});
function initArea(){
    doService(ctx + '/dlfw/dm/queryAllXzqy', function(data) {
        if(data&&data.length>0){
            new MobileSelect({
                trigger: '#qymc',
                wheels: [
                    {data: data}
                ],
                keyMap:{id:'id', val:'name', childs:'child'},
                callback:function(index,data){
                    if(data[1]){
                        $("#qymc").val(data[0].name+','+data[1].name)
                        $("#areaId").val(data[0].id+','+data[1].id);
                    }else{
                        $("#qymc").val(data[0].name)
                        $("#areaId").val(data[0].id);
                    }
                }
            });

        }
    });
}
function zclx(){//注册类型

    doService(ctx + '/dlfw/dm/queryAllZclx', function(data) {
        if(data&&data.length>0){
            new MobileSelect({
                trigger: '#zclx',
                wheels: [{data: data}],
                callback:function(index,data){
                    $("#zclx").val(data[0].val);
                    $("#zclxId").val(data[0].id);
                }
            });
        }
    });
}
function xiyibu(){//点击下一步
    var areaStr=$("#areaId").val();
    if(areaStr==""){
        qyMsg("请选择地区");
        return;
    }
    if (areaStr.split(",").length < 2) {
        qyMsg("请选择详细的注册区域");
        return;
    }
    if($("#zclxId").val()==""){
        qyMsg("请选择注册类型");
        return;
    }
    var areaStrValue;
    var provinceValue;
    var areas=areaStr.split(",")[1];
    if(areas){
        areaStrValue=areaStr.split(",")[1];
        provinceValue=areaStr.split(",")[0];
    }else{
        areaStrValue=0;
        provinceValue=areaStr.split(",")[0];
    }
    console.info(areaStrValue+","+provinceValue+","+$("#zclxId").val());

    doService(ctx + '/dlfw/gszc/addGszc', {
        openid: openid,
        zclx_bm: $("#zclxId").val(),
        areaId: areaStrValue,
        provinceId: provinceValue
    }, function(data) {
        if (data) {
            data = data.replace(/\D+/g, '');
        } else {
            data = "03518286418";
        }

        var url=ctx+"/dlfw/gszc/xiayibu?provinceId="+provinceValue + '&phone=' + data;
        window.location.href=url;
    });

}


