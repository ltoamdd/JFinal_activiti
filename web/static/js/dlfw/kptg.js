$(function() {
    initArea();//加载区域
});

function initArea() {
    doService(ctx + '/dlfw/dm/queryAllXzqy', function(data) {
        if(data&&data.length>0){
            new MobileSelect({
                trigger: '#qymc',
                wheels: [
                    {data: data}
                ],
                keyMap:{id:'id', val:'name', childs:'child'},
                callback:function(index,data){
                    if(data[1]){
                        $("#qymc").val(data[0].name+','+data[1].name)
                        $("#areaId").val(data[0].id+','+data[1].id);
                    }else{
                        $("#qymc").val(data[0].name)
                        $("#areaId").val(data[0].id);
                    }
                }
            });

        }
    });
}

function zixun() {
    var areaStr=$("#areaId").val();
    if($("#areaId").val()==""){
        qyMsg("请选择注册区域");
        return;
    }
    if (areaStr.split(",").length < 2) {
        qyMsg("请选择详细的注册区域");
        return;
    }
    var areaStrValue;
    var provinceValue;
    var areas=areaStr.split(",")[1];
    if(areas){
        areaStrValue=areaStr.split(",")[1];
        provinceValue=areaStr.split(",")[0];
    }else{
        areaStrValue=0;
        provinceValue=areaStr.split(",")[0];
    }
    doService(ctx + '/dlfw/kptg/addDljz', {
        openid: openid,
        areaId: areaStrValue,
        provinceId: provinceValue
    }, function(data) {
        if (data) {
            data = data.replace(/\D+/g, '');
            window.location.href = "tel:"+data;
        } else {
            window.location.href = "tel:03518286418";
        }
    });
}