var kxlx = "rmkc";
$(function() {
	//热门课程加载
	initRmkcList();
	//加载最新课程
	initZxkcList();
	//加载免费课程
	initMfkcList();
	//轮播图加载
	lbtquery();
	initEvent();
});
var rmkcData = {
	pageNum : 1,
	pageSize : 15,
	start : (1 - 1) * 15,
	dj : 0,
	hot_label : 0,
	sort : 1,
	openid : $("#openid").val(),
	titleName : "",
    wx_key:wx_key,
}
var zxkcData = {
	pageNum : 1,
	pageSize : 15,
	start : (1 - 1) * 15,
	dj : 0,
	hot_label : 0,
	sort : 1,
	openid : $("#openid").val(),
	titleName : "",
    wx_key:wx_key,
}
var mfkcData = {
		pageNum : 1,
		pageSize : 15,
		start : (1 - 1) * 15,
		dj : 0,
		hot_label : 0,
		sort : 1,
		openid : $("#openid").val(),
		titleName : "",
    	wx_key:wx_key,
	}
//初始化最热列表
function initRmkcList() {
	doService(ctx + "/cskc/findKcbRmkcList", rmkcData, function(json) {
		$("#rmkcul").empty();
		rmkcData.total = json.total;
		console.info("热门课程的数量" + json.total);
		initRmkc(json.list);
	});
}
//初始化最新课程列表
function initZxkcList() {
	doService(ctx + "/cskc/findKcbZxkcList", zxkcData, function(json) {
		$("#zxkcul").empty();
		zxkcData.total = json.total;
		console.info("最新课程的数量" + json.total);
		console.info(json.list);
		initZxkc(json.list);
	});
}
//初始化免费课程列表
function initMfkcList() {
	doService(ctx + "/cskc/findKcbMfkcList", mfkcData, function(json) {
		$("#mfkcul").empty();
		mfkcData.total = json.total;
		console.info("免费课程的数量" + json.total);
		console.info(json.list);
		initMfkc(json.list);
	});
}
//加载热门课程
function initRmkc(data) {
	if (data.length <= 0) {
		//debugger;
		qyMsg("数据已加载完成");
	}
	for (var i = 0; i < data.length; i++) {
		var str = "<li><a href=\"javascript:wlkcDetail('" + data[i].id + "','"
		+ data[i].cid + "','article')\">" + "<div><img src='" +getImgByPath(data[i].img)
		+ "' alt='' /></div>" + "<div class='info'>";

		if (data[i].rank == 0) {
			str += "<span   style='font-size: 13px;line-height:18px;height:18px;display:none' >"
				+ "</span><span  style='font-size:13px;color:#F47017;line-height:18px;height:18px;'>"
				+ showRank(data[i]) + "</span>";
		} else {
			str += "<span style='font-size: 13px;line-height:18px;height:18px;display:none' >"
				+ "</span><span  style='font-size:13px;color:#F47017;line-height:18px;height:18px;'>"
				+ showRank(data[i]) + "</span>";
		}

		str += "</div></a></li>";
		$("#rmkcul").append(str);
		remSize();
	}
}
//加载最新课程
function initZxkc(data) {
	if (data.length <= 0) {
		//debugger;
		qyMsg("数据已加载完成");
	}
	for (var i = 0; i < data.length; i++) {
		var str = '<li><a href=\'javascript:wlkcDetail("' + data[i].id + '","'
				+ data[i].cid + '","article")\'>' + '<div><img src="' + getImgByPath(data[i].img)
				+ '" alt="" /></div>' + '<div class="info">';
		if (data[i].rank == 0) {
			str += '<span style="font-size: 13px;line-height:18px;height:18px;">'
					+ data[i].click
					+ '人</span><span  style="font-size:13px;color:#F47017;line-height:18px;height:18px;">'
					+ showRank(data[i]) + '</span>';
		} else {
			str += '<span style="font-size: 13px;line-height:18px;height:18px;">'
					+ data[i].click
					+ '人</span><span  style="font-size:13px;color:#F47017;line-height:18px;height:18px;">'
					+ showRank(data[i]) + '</span>';
		}
		//str+='<span>'+data[i].click+'人</span><span>'+data[i].createtime1+'</span>';

		str += '</div></a></li>';
		$("#zxkcul").append(str);
		remSize();
	}
}
//加载免费课程
function initMfkc(data) {
	if (data.length <= 0) {
		//debugger;
		qyMsg("数据已加载完成");
	}
	for (var i = 0; i < data.length; i++) {
		var str = '<li><a href=\'javascript:wlkcDetail("' + data[i].id + '","'
				+ data[i].cid + '","article")\'>' + '<div><img src="' + getImgByPath(data[i].img)
				+ '" alt="" /></div>' + '<div class="info">';
		if (data[i].rank == 0) {
			str += '<span style="font-size: 13px;line-height:18px;height:18px;">'
					+ data[i].click
					+ '人</span><span  style="font-size:13px;color:#666;line-height:18px;height:18px;">'
					+ showRank(data[i]) + '</span>';
		} else {
			str += '<span style="font-size: 13px;line-height:18px;height:18px;">'
					+ data[i].click
					+ '人</span><span  style="font-size:13px;color:#F47017;line-height:18px;height:18px;">'
					+ showRank(data[i]) + '</span>';
		}
		//str+='<span>'+data[i].click+'人</span><span>'+data[i].createtime1+'</span>';

		str += '</div></a></li>';
		$("#mfkcul").append(str);
		remSize();
	}
}
function showRank(data) {
	//debugger;
	if (data.rank == 2) {
		return "普通专享";
	} else if (data.rank == 3) {
		return "银卡专享";
	} else if (data.rank == 4) {
		return "高端专享";
	} else if (data.rank == 0) {
		if(data.status_sfmf){
			if(data.status_sfmf=="1"){
				return "免费课程";
			}else if(data.status_sfmf=="2"){
				return "限时课程";
			}else if(data.status_sfmf=="3"){
				return "试听课程";
			}
		}else{
			return "免费课程";
		}
	}
}
//网络课程内容跳转
function wlkcDetail(id, cid, nrlx) {
	window.location.href = ctx + getWlkcUrl(id, nrlx, cid);
}
function getWlkcUrl(id, lx, cid, pid) {
	return "/wlkc/wlkcDetail?id=" + id + "&openid=" + openid+"&wx_key="+wx_key;
}
function lbtquery() {
    doService(ctx + "/lbt", function(json) {
        for (var i = 0; i < json.length; i++) {
            if (json[i].tpljdz) {
                var str = "<div class='swiper-slide'>"
                    + " <a href=\"javascript:hdqh('" + json[i].tpljdz
                    + "')\">" + " <img id='slide" + (i + 1) + "' src='"
                    + getImgByPath(json[i].picurl) + "' alt='' /></a></div>";
                $(".swiper-wrapper").append(str);
            }
        }
        var swiper = new Swiper('.swiper-container', {
            pagination : '.swiper-pagination',
            paginationClickable : true,
            spaceBetween : 30,
            centeredSlides : true,
            autoplay : 2500,
            autoplayDisableOnInteraction : false
        });
    });
}
function showNrxxDetail(id, cid, nrlx) {
	window.location.href = ctx + getUrl(id, nrlx, cid);
}
/**
 * 获取详情页面的action地址
 * @param id
 * @param lx
 * @param pid
 * @param cid
 * @returns {*}
 */
function getUrl(id, lx, cid, pid) {
	switch (cid) {
	case 12://直播
		return "/hyfw/zbkc/zbkcDetail?id=" + id + "&openid="
				+ openid+"&wx_key="+ wx_key;
		break;
	case 32://点播网络课程
		return "/hyfw/zbkc/zbkcDetail?id=" + id + "&openid="
				+ openid+"&wx_key="+ wx_key;
		break;
	case 33://试听课程
		return "/hyfw/zbkc/zbkcDetail?id=" + id + "&openid="
				+ openid+"&wx_key="+ wx_key;
		break;
	}
	return "";
}
//网络课程跳转链接
function wlkc(fwlx) {
	if (fwlx == "ztxl") {
		window.location.href = ctx + "/ztxl?openid=" + openid+"&wx_key="+ wx_key;
	}
	if (fwlx == "zbkc") {
		window.location.href = ctx + "/shouyezbkc?openid=" + openid+"&wx_key="+ wx_key;
	}
	if (fwlx == "wlkc") {
		window.location.href = ctx + "/wlkc?openid=" + openid+"&wx_key="+ wx_key;
	}
	if (fwlx == "sksc") {
		window.location.href = ctx + "/shouyerjsc?openid=" + openid+"&wx_key="+ wx_key;
	}
	//微课堂也就是晓红说税
	if (fwlx == "xhss") {
		window.location.href = ctx + "/shouyewkt/wlkc_XhssIndexPage?openid="
				+ openid+"&wx_key="+ wx_key;
	}

}
//点击获取热门课程
function onlickRmkc() {
	kxlx = "rmkc";
}
//点击执行最新课程
function onlickZxkc() {
	kxlx = "zxkc";
}
//免费课程
function onlickMfkc(){
	kxlx = "mfkc";
}
function initEvent() {
	/**
	 * 滚到到浏览器底部以后 加载下一页数据
	 */

	$(window)
			.scroll(
					function() {
						//debugger;
						// window.pageYOffset 滚动条位置
						// window.innerHeight 视口尺寸
						// document.body.scrollHeight 文档高度
						if (kxlx == "rmkc") {
							if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
								if (rmkcData.pageNum * rmkcData.pageSize < rmkcData.total) {
									loadWlkcRmkcList();
								} else {
									qyMsg("数据已加载完成");
								}
							}
						} else if(kxlx=="zxkc"){
							if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
								if (zxkcData.pageNum * zxkcData.pageSize < zxkcData.total) {
									loadWlkcZxkcList();
								} else {
									qyMsg("数据已加载完成");
								}
							}
						}else if(kxlx=="mfkc"){
							if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
								if (mfkcData.pageNum * mfkcData.pageSize < mfkcData.total) {
									loadMfkcList();
								} else {
									qyMsg("数据已加载完成");
								}
							}
						}

					});
}
//屏幕滚动的时候加载热门课程
function loadWlkcRmkcList() {
	rmkcData.pageNum += 1;
	rmkcData.start = (rmkcData.pageNum - 1) * rmkcData.pageSize;
	doService(ctx + '/cskc/findKcbRmkcList', rmkcData, function(json) {
		rmkcData.total = json.total;
		initRmkc(json.list);
	});
}
//屏幕滚动的时候加载最新课程
function loadWlkcZxkcList() {
	zxkcData.pageNum += 1;
	zxkcData.start = (zxkcData.pageNum - 1) * zxkcData.pageSize;
	doService(ctx + '/cskc/findKcbZxkcList', zxkcData, function(json) {
		zxkcData.total = json.total;
		initZxkc(json.list);
	});
}
//屏幕滚动的时候加载免费课程
function loadMfkcList() {
	mfkcData.pageNum += 1;
	mfkcData.start = (mfkcData.pageNum - 1) * mfkcData.pageSize;
	doService(ctx + '/cskc/findKcbMfkcList', mfkcData, function(json) {
		mfkcData.total = json.total;
		initMfkc(json.list);
	});
}
function sousuo() {
	var titleName = $("#titleName").val();
	if (kxlx == "rmkc") {
		rmkcData.pageNum = 1;
		rmkcData.start = 0;
		rmkcData.titleName = titleName;
		initRmkcList();
	} else if (kxlx == "zxkc") {
		zxkcData.pageNum = 1;
		zxkcData.start = 0;
		zxkcData.titleName = titleName;
		initZxkcList();
	}else if(kxlx == "mfkc"){
		mfkcData.pageNum = 1;
		mfkcData.start = 0;
		mfkcData.titleName = titleName;
		initMfkcList();
	}

}
function remSize() {
	var maxWidht = $(window).width();
	if (maxWidht > 340) {
		$('.list-main section').css('font-size', '0.28rem')
		$('.info>span:nth-child(1)').css({
			"background-size" : "0.3rem 0.3rem",
		})
	} else {
		$('.list-main section').css('font-size', '0.22rem')
		$('.info>span:nth-child(1)').css({
			"background-size" : "0.22rem 0.22rem",
			"padding-left" : "0.22rem"
		})
	}
}
function hdqh(path) {
	if (path == "zbkc") {
		initZbkcList();
	} else {

		var index = path.indexOf("/");
		if (index == 0) {
			window.location.href = ctx + path + "?openid=" + openid+"&wx_key="+ wx_key;
		} else if (index == -1) {
			window.location.href = path + "?openid=" + openid+"&wx_key="+ wx_key;
		}
	}

}
var postData = {
	pageNum : 1,
	pageSize : 15,
	start : (1 - 1) * 15,
	dj : 0,
	sort : 1,
	did : 1,
	openid : $("#openid").val()
}
function initZbkcList() {
	doService(ctx + '/shouyezbkc/queryAllZbkc', postData, function(json) {
		postData.total = json.total;
		var data = json.list;
		for (var i = 0; i < data.length; i++) {
			window.location.href = ctx + "/hyfw/zbkc/zbkcDetail?id="
					+ data[i].id + "&openid=" + openid+"&wx_key="+ wx_key;
		}
	});
}
