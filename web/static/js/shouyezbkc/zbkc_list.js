/**
 * Created by Administrator on 2017/3/9.
 */
// 设置页面viewport && 基准rem
(function(e,t){var i=document,n=window;var l=i.documentElement;var a,r;var d,s=document.createElement("style");var o;function m(){var i=l.getBoundingClientRect().width;if(!t){t=540}if(i>t){i=t}var n=i*100/e;s.innerHTML="html{font-size:"+n+"px;}"}a=i.querySelector('meta[name="viewport"]');r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no";if(a){a.setAttribute("content",r)}else{a=i.createElement("meta");a.setAttribute("name","viewport");a.setAttribute("content",r);if(l.firstElementChild){l.firstElementChild.appendChild(a)}else{var c=i.createElement("div");c.appendChild(a);i.write(c.innerHTML);c=null}}m();if(l.firstElementChild){l.firstElementChild.appendChild(s)}else{var c=i.createElement("div");c.appendChild(s);i.write(c.innerHTML);c=null}n.addEventListener("resize",function(){clearTimeout(o);o=setTimeout(m,300)},false);n.addEventListener("pageshow",function(e){if(e.persisted){clearTimeout(o);o=setTimeout(m,300)}},false);if(i.readyState==="complete"){i.body.style.fontSize="16px"}else{i.addEventListener("DOMContentLoaded",function(e){i.body.style.fontSize="16px"},false)}})(750,750);
// console.log(ctx);
// console.log("----------------------------");
var postData = {
    pageNum : 1,
    pageSize: 15,
    start: (1 - 1) * 15,
    dj:0,
    sort: 1,
    did:1,
    wx_key: wx_key,
    openid:$("#openid").val()
}

$(function() {
    initZbkcList();
});


//初始化列表
function initZbkcList(){

    doService(ctx + '/hyfw/zbkc/queryAllZbkcYg', postData, function(json) {
        postData.total = json.total;
        $("#ztxkul").empty();
        initZbkcBlock(json);
    });
}


function getBeginEndInfo(zbkc) {

    var classtime = zbkc.classtime;
    var source = zbkc.source;

    var z_year = parseInt(classtime.substring(0, 4));
    var z_month = parseInt(classtime.substring(5, 7)) - 1;
    var z_date = parseInt(classtime.substring(8));
    var z_start_hour = parseInt(source.substring(0,2));
    var z_start_minute = parseInt(source.substring(3,5));
    var z_end_hour = parseInt(source.substring(6,8));
    var z_end_minute = parseInt(source.substring(9));

    var start = new Date();
    start.setFullYear(z_year);
    start.setMonth(z_month);
    start.setDate(z_date);
    start.setHours(z_start_hour);
    start.setMinutes(z_start_minute);
    start.setSeconds(0);

    var end = new Date();
    end.setFullYear(z_year);
    end.setMonth(z_month);
    end.setDate(z_date);
    end.setHours(z_end_hour);
    end.setMinutes(z_end_minute);
    end.setSeconds(0);

    var current = new Date().getTime();
    var startTime = start.getTime();
    var endTime = end.getTime();

    if (current < startTime) {
        return { begin: 0, end: 0 };
    } else {
        if (current < endTime) {
            return { begin: 1, end: 0 };
        } else {
            return { begin: 1, end: 1 };
        }
    }
}


//初始化网络课程单元块模块
function initZbkcBlock(json) {
    var data = json.list;

    data.forEach(function (item) {
        var timeStatus = getBeginEndInfo(item);
        var fwh = item.fwh;
        if (fwh && fwh.indexOf(json.wx_key) >= 0 && timeStatus.end == 0) {
            var _li = "<li>" +
                "<a href=\"javascript:showZbygNrxxDetail('" + item.id + "','" + item.cid + "','article')\">" +
                "<img src='" + getImgByPath(item.img) + "' alt='' /> " +
                "<section>" +
                ((wx_key == 'gh_18a9a687b51a') ? "" : showRankHtml(item)) +
                "<p>" + item.title + "</p>" +
                "<div class='info'>" +
                "<span>" + (timeStatus.begin == 1 ? "正在直播" : "") + "</span>" +
                "<span>" + item.classtime + "</span>" +
                "</div> " +
                "</section>" +
                "</a></li>";
            $("#ztxkul").append($(_li));

        }
    });
    if ($("#ztxkul li").size() <= 0) {
        $('#ztxkul').append($('<div class="nomessage"><img src="' + ctx + '/static/images/mobile/default_no_data.png" width="50%" /></div>'));
    }
}

function showZbygNrxxDetail(id, cid, lx) {
    window.location.href = ctx + "/hyfw/zbkc/zbkcygDetail?id=" + id + "&wx_key=" + wx_key+"&openid="+openid;
}

function kcclick(data){
	console.info("点击数"+data.click);
	if(data.click==undefined){
		return 0;
	}else{
		return data.click;
	}
}


function showRankHtml(data) {
    if (data.rank == 0) {
        return "<div class='badge badge-orange' style='font-size:12px'>" + showRank(data) + "</div>";
    } else {
        return "<div class='badge badge-orange' style='font-size:12px'>" + showRank(data) + "</div>";
    }
}

function showRank(data){
    if(data.rank==null){
        return "限时免费";
    }else if(data.rank == 2){
        return "普通专享";
    }else if(data.rank == 3){
        return "银卡专享";
    }else if(data.rank == 4){
        return "高端专享";
    }else if(data.rank == 0){
        return "限时免费";
    }else{
        return "限时免费";
    }
}


function showZbkcDetail(id) {
    window.location.href = ctx + '/shouyezbkc/zbkcDetail?wx_key=' + wx_key+"&id="+id;
    console.log(id);
}

function toZbkc_hk(){
    window.location.href = ctx + '/shouyezbkc/zbkcHkList?wx_key=' + wx_key;
}
function backShuye(){
	window.history.go(-1);
}
function remSize(){
	var maxWidht = $(window).width();
	if(maxWidht >340){
		$('.list-main section').css('font-size','0.28rem')
		$('.info>span:nth-child(1)').css({
			 "background-size":"0.3rem 0.3rem",
		})
	}else{
		$('.list-main section').css('font-size','0.22rem')
		$('.info>span:nth-child(1)').css({
			 "background-size":"0.22rem 0.22rem",
	    	 "padding-left":"0.22rem"
		})
	}
}