/**
 * Created by Administrator on 2017/3/9.
 */
// 设置页面viewport && 基准rem
$(function(){
	var maxWidht = $(window).width();
	$("html").css({
		fontSize:(maxWidht / 720) *100
	})
})
// console.log(ctx);
// console.log("----------------------------");
var postData = {
    pageNum : 1,
    pageSize: 15,
    start: (1 - 1) * 15,
    dj:0,
    sort: 1,
    did:0,
    wx_key: wx_key,
    openid:$("#openid").val()
}

$(function() {
    initZbkcList();
    initEvent();
});


//初始化列表
function initZbkcList(){

    console.log(ctx);
    doService(ctx + '/shouyezbkc/queryAllZbkc', postData, function(json) {
        postData.total = json.total;
        $("#ztxkul").empty();
        initZbkcBlock(json.list);
    });
}

//初始化网络课程单元块模块
function initZbkcBlock(data) {
    if (data.length <= 0) {
        qyMsg("数据已加载完成");
    }
    for(var i=0;i<data.length;i++){
       /* var _html = "<section>"
            + "    <a   href=\"javascript:showZbkcDetail('" + data[i].id + "')\"    >   "
            // +'<div class="zbhf">直播回放</div>'
            + "<img src="+ctx+"/common/getFileByPath?path="+data[i].img+" alt='' />"
            + "<div class='txt_area'>"
            + "<h4>"+data[i].title+"</h4> "
            + "<p>"+data[i].classtime+"  "+data[i].source+"</p>"
            + "<h6>"+showRank(data[i])+"</h6>"
            + "</div>"
            + "</a>"
            + "</section>";
        $('.zbkc_list_con1').append($(_html));*/
    	
    	var str="<li>" +
		" <a href=\"javascript:showNrxxDetail('"+data[i].id+"','" + data[i].cid + "','article')\">" +
				" <img src='" + getImgByPath(data[i].img) + "' alt='' /> " +
					" <section> ";
    				if(data[i].rank==0){
    					str+="<span class='badge badge-gary'>"+showRank(data[i])+"</span> ";
    				}else{
    					str+="<span class='badge badge-orange'>"+showRank(data[i])+"</span> ";
    				}
					str+="<p>"+data[i].title+"</p> " +
					" <div class='info'><span>"+kcclick(data[i])+"人</span><span>"+data[i].classtime+"</span></div> " +
					" </section> " +
					" </a> " +
					" </li>";
        $("#ztxkul").append(str);
        remSize();
    }
}
function kcclick(data){
	console.info("点击数"+data.click);
	if(data.click==undefined){
		return 0;
	}else{
		return data.click;
	}
}

function showRank(data){
    if(data.rank==null){
        return "免费课程";
    }else if(data.rank == 2){
        return "普卡及以上会员专享";
    }else if(data.rank == 3){
        return "银卡及以上会员专享";
    }else if(data.rank == 4){
        return "金卡专享";
    }else if(data.rank == 0){
        return "免费课程";
    }else{
        return "免费课程";
    }
}


function initEvent() {
    /**
     * 滚到到浏览器底部以后 加载下一页数据
     */

    $(window).scroll(function() {
        //debugger;
        // window.pageYOffset 滚动条位置
        // window.innerHeight 视口尺寸
        // document.body.scrollHeight 文档高度
        if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
            if (postData.pageNum * postData.pageSize < postData.total) {
                //initZbkcList();
                loadZbkcList();
            }else{
                qyMsg("数据已加载完成");
            }
        }
    });

}


function loadZbkcList() {
    postData.pageNum += 1;
    postData.start = (postData.pageNum - 1) * postData.pageSize;
    doService(ctx + '/shouyezbkc/queryAllZbkc', postData, function(json) {
        postData.total = json.total;
        initZbkcBlock(json.list);
    });
}

function showZbkcDetail(id) {
    window.location.href = ctx + '/shouyezbkc/zbkcDetail?wx_key=' + wx_key+"&id="+id;
    console.log(id);
}
function toZbkc(){
    window.location.href = ctx + '/shouyezbkc/zbkcList?wx_key=' + wx_key;
}
function backShuye(){
	window.history.go(-1);
}

function remSize(){
	var maxWidht = $(window).width();
	if(maxWidht >340){
		$('.list-main section').css('font-size','0.25rem')
		$('.info>span:nth-child(1)').css({
			 "background-size":"0.3rem 0.3rem",
		})
	}else{
		$('.list-main section').css('font-size','0.22rem')
		$('.info>span:nth-child(1)').css({
			 "background-size":"0.22rem 0.22rem",
	    	 "padding-left":"0.22rem"
		})
	}
}