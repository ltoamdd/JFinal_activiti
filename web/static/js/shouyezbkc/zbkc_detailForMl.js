/**
 * Created by huangwei on 2017/3/20.
 */
// var tsMeta = new ArticleMeta($("#openid").val(), $("#id").val(), 2, metaInfo);
var tsMeta = new ArticleMeta($("#openid").val(), articleId, 2, metaInfo);
// 设置页面viewport && 基准rem
var currentIndexDiv = 0;
(function(e,t){ var i=document,n=window; var l=i.documentElement; var a,r; var d,s=document.createElement("style"); var o; function m(){ var i=l.getBoundingClientRect().width; if(!t){ t=540 } if(i>t){ i=t } var n=i*100/e; s.innerHTML="html{font-size:"+n+"px;}" } a=i.querySelector('meta[name="viewport"]'); r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no"; if(a){ a.setAttribute("content",r) }else{ a=i.createElement("meta"); a.setAttribute("name","viewport"); a.setAttribute("content",r); if(l.firstElementChild){ l.firstElementChild.appendChild(a) }else{ var c=i.createElement("div"); c.appendChild(a); i.write(c.innerHTML); c=null } } m(); if(l.firstElementChild){ l.firstElementChild.appendChild(s) }else{ var c=i.createElement("div"); c.appendChild(s); i.write(c.innerHTML); c=null } n.addEventListener("resize",function(){ clearTimeout(o); o=setTimeout(m,300) },false); n.addEventListener("pageshow",function(e){ if(e.persisted){ clearTimeout(o); o=setTimeout(m,300) } },false); if(i.readyState==="complete"){ i.body.style.fontSize="16px" }else{ i.addEventListener("DOMContentLoaded",function(e){ i.body.style.fontSize="16px" },false) } })(750,750);
$(function () {
    // init();
    // initAudio(ctx + '/temp/00001.mp3');
    // 获取收藏点赞状态和数量
    tsMeta.queryArticleMetaInfo();

    // 评论列表
    tsMeta.queryPinglunData();

    // 滚动加载评论
    $(window).scroll(function() {
        if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
            tsMeta.queryPinglunData();
        }
    });
});

/**
 * 收藏和点赞默认的回调函数
 * @param json
 */
function metaInfo(json) {
    if(json.isup) {
        $("#dzBtn").addClass('active');
    }else {
        $("#dzBtn").removeClass('active');
    }
    if(json.isfavorites) {
        $("#shoucBtn").addClass("active");
    } else {
        $("#shoucBtn").removeClass("active");
    }
    $("#upCount").text(json.upCount);
}

/**
 * 点击收藏按钮，用于收藏或取消收藏
 */
function shoucang() {
    if (!$("#shoucBtn").hasClass('active')) {
        tsMeta.shcArticle();
    } else {
        tsMeta.qxshcArticle();
    }
}

/**
 * 点击点赞按钮，用于点赞或取消点赞
 */
function dianzan() {
    if(!$("#dzBtn").hasClass('active')) {
        tsMeta.dzArticle();
    } else {
        tsMeta.qxdzArticle();
    }
}

/**
 * 评论，弹出评论窗口
 */
function pinglun() {
    tsMeta.tcPinglun();
}

/**
 * 提交评论内容
 */
function tjpinglun() {
    tsMeta.tjPinglun();
}

/**
 * 跳转导其他直播课程
 */
function toZbkcDetail(id){
    // debugger;
    // var _form =  $("<form></form>");
    // _form.attr("action","");
    // _form.attr("method","post");
    // _form.append($("<input type='hidden' value=''>"));
    // $(_form).commit();
    window.location.href = ctx+'/hyfw/zbkc/zbkcDetail?id='+id+"&wx_key="+wx_key;
}
function backShuye(){
	window.history.back(-1); 
}
