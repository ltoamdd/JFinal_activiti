/**
 * Created by Administrator on 2017/3/9.
 */
// 设置页面viewport && 基准rem
var cid=280;
(function(e,t){var i=document,n=window;var l=i.documentElement;var a,r;var d,s=document.createElement("style");var o;function m(){var i=l.getBoundingClientRect().width;if(!t){t=540}if(i>t){i=t}var n=i*100/e;s.innerHTML="html{font-size:"+n+"px;}"}a=i.querySelector('meta[name="viewport"]');r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no";if(a){a.setAttribute("content",r)}else{a=i.createElement("meta");a.setAttribute("name","viewport");a.setAttribute("content",r);if(l.firstElementChild){l.firstElementChild.appendChild(a)}else{var c=i.createElement("div");c.appendChild(a);i.write(c.innerHTML);c=null}}m();if(l.firstElementChild){l.firstElementChild.appendChild(s)}else{var c=i.createElement("div");c.appendChild(s);i.write(c.innerHTML);c=null}n.addEventListener("resize",function(){clearTimeout(o);o=setTimeout(m,300)},false);n.addEventListener("pageshow",function(e){if(e.persisted){clearTimeout(o);o=setTimeout(m,300)}},false);if(i.readyState==="complete"){i.body.style.fontSize="16px"}else{i.addEventListener("DOMContentLoaded",function(e){i.body.style.fontSize="16px"},false)}})(750,750);
// console.log(ctx);
// console.log("----------------------------");
var postData = {
    pageNum : 1,
    pageSize: 15,
    start: (1 - 1) * 15,
    dj:0,
    hot_label:0,
    wx_key: wx_key,
    sort: 1,
    openid:$("#openid").val()
}

$(function() {
    initWlkcList();
    initEvent();
});


//初始化列表
function initWlkcList(){

    console.log(ctx);
    doService(ctx + '/shouyerjsc/queryRjsc?cid='+cid, postData, function(json) {
        postData.total = json.total;
        $("#wlkc_list_con").empty();
        initWlkcBlock(json.list);
    });
}

//初始化网络课程单元块模块
function initWlkcBlock(data) {
 	$("#ztxkul").empty();
    if (data.length <= 0 && postData.pageNum == 1) {
        qyMsg("暂无数据");
        return;
    }
    if (data.length <= 0) {
        qyMsg("数据已加载完成");
        return;
    }
    for(var i=0;i<data.length;i++){
    	var str="<li>" +
		" <a href=\"javascript:wlkcDetail('"+data[i].id+"')\">" +
				" <img src='" + getImgByPath(data[i].img) +"' alt='' /> " +
					" <section> ";
    				if(data[i].rank==0){
    					str+=" <span class='badge badge-gary' style='font-size:12px;'>"+showRank(data[i])+"</span> ";
    				}else{
    					str+=" <span class='badge badge-orange' style='font-size:12px;'>"+showRank(data[i])+"</span> ";
    				}
					str+=" <p>"+data[i].title+"</p> " +
					" <div class='info'><span>"+kcclick(data[i])+"人</span><span>"+data[i].createtime1+"</span></div> " +
					" </section> " +
					" </a> " +
					" </li>";
        $("#ztxkul").append(str);
        remSize();
    }
}
function kcclick(data){
	console.info("点击数"+data.click);
	if(data.click==undefined){
		return 0;
	}else{
		return data.click;
	}
}
function wlkcDetail(id){
    var url=ctx+"/wlkc/wlkcDetail?id=" + id + "&openid=" + openid+"&wx_key="+wx_key;
    window.location.href=url;
}
function showRank(data){
    if(data.rank == 2){
        return "普通专享";
    }else if(data.rank == 3){
        return "银卡专享";
    }else if(data.rank == 4){
        return "高端专享";
    }
    return "免费课程";
}


function initEvent() {
    /**
     * 滚到到浏览器底部以后 加载下一页数据
     */

    $(window).scroll(function() {
        // window.pageYOffset 滚动条位置
        // window.innerHeight 视口尺寸
        // document.body.scrollHeight 文档高度
        if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
            if (postData.pageNum * postData.pageSize < postData.total) {
                //initWlkcList();
                loadWlkcList();
            }else{
                qyMsg("数据已加载完成");
            }
        }
    });
}


function loadWlkcList() {
    postData.pageNum += 1;
    postData.start = (postData.pageNum - 1) * postData.pageSize;
    doService(ctx + '/hyfw/rjscAction/queryRjsc?cid='+cid, postData, function(json) {
        postData.total = json.total;
        initWlkcBlock(json.list);
    });
}

function showWlkcDetail(id) {
    console.log(id);
}

function  djbdhy() {
    doService(ctx + '/wode/hybd/queryHybd', {openid: $("#openid").val()}, function(json) {
        console.log(json);
        if(json.result) {
            window.location.href = ctx + '/hyfw/wlkc?wx_key=' + wx_key;
            $(".wlkc_list_djbd").hide();
        } else {
            toHybd();
            $(".wlkc_list_djbd").show();
        }
    });

}


function searchAsRem(val, text) {
    postData.dj = 0;
    postData.pageNum = 1;
    postData.start = 0;
    postData.hot_label = val;
    initWlkcList();
    $("#panel-right").removeClass('active');
    $("#wlkc-title").text(text);
}
//网上申报
function rjsc_wssb(){
	console.log(ctx);
	cid=280;
    doService(ctx + '/shouyerjsc/queryRjsc?cid='+cid, postData, function(json) {
        postData.total = json.total;
        $("#wlkc_list_con").empty();
        initWlkcBlock(json.list);
    });
}
//税控开票
function rjsc_skkp(){
	cid=278
	console.log(ctx);
    doService(ctx + '/shouyerjsc/queryRjsc?cid='+cid, postData, function(json) {
        postData.total = json.total;
        $("#wlkc_list_con").empty();
        initWlkcBlock(json.list);
    });
}
//认证勾选
function rjsc_rzgx(){
	cid=279;
    doService(ctx + '/shouyerjsc/queryRjsc?cid='+cid, postData, function(json) {
        postData.total = json.total;
        $("#wlkc_list_con").empty();
        initWlkcBlock(json.list);
    });
}
function backShuye(){
	window.history.back(-1); 
}
function remSize(){
	var maxWidht = $(window).width();
	if(maxWidht >340){
		$('.list-main section').css('font-size','0.28rem')
		$('.info>span:nth-child(1)').css({
			 "background-size":"0.3rem 0.3rem",
		})
	}else{
		$('.list-main section').css('font-size','0.22rem')
		$('.info>span:nth-child(1)').css({
			 "background-size":"0.22rem 0.22rem",
	    	 "padding-left":"0.22rem"
		})
	}
}


