function qyLoading(msg) {
    var index;
    if (msg) {
        index = layer.open({
            type: 2,
            shadeClose: false
        });
    } else {
        index = layer.open({
            type: 2,
            content: msg,
            shadeClose: false
        });
    }
    return index;

}

/**
 * layer提示框
 * @param msg 提示信息
 * @param time 提示信息显示时间， 默认为2秒
 */
function qyMsg(msg, fn) {
    layer.open({
        content: msg,
        skin: 'msg',
        time: 2,
        end: function() {
            if (typeof fn === "function") {
                fn();
            }
        }
    });
}

/**
 * layer警告框
 * @param msg 警告信息
 * @param fn 可选参数 点击确定以后的回调函数
 */
function qyAlert(msg, fn) {
    layer.open({
        content: msg,
        btn: '确定',
        shadeClose: false,
        yes: function(index) {
            layer.close(index);
            if (typeof fn === 'function') {
                fn();
            }
        }
    });
}

/**
 * layer询问框
 * @param msg 询问信息
 * @param fn 点击确定或取消后的回调函数，点击确定以后回调函数的第一个参数值为true，点击取消以后回调函数的第一个参数值为false
 */
function qyConfirm(msg, fn) {
    layer.open({
        content: msg,
        btn: ['确定', '取消'],
        yes: function(index) {
            layer.close(index);
            fn(true);
        },
        no: function() {
            fn(false);
        }
    });
}

/**
 * ajax请求
 * @param url 请求地址 必填参数
 * @param data 参数 可选参数
 * @param callback 回调函数 必填参数
 */
function doService(url, data, callback) {
    var _data = arguments.length >= 3 ? arguments[1] : {};
    var _callback = arguments.length >= 3 ? arguments[2] : arguments[1];
    $.ajax({
        url: url,
        type: 'POST',
        data: _data,
        dataType: "json",
        success: function (json) {
            if(json.success){
                _callback(json.message);
            } else {
                qyMsg(json.message);
            }
        }
    });
}

$.fn.getFormJson = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


/**
 *
 * @param openid
 * @param article_id  文章、听税、课程的id
 * @param type 对应wx_article_type中的主键id
 * @param callbcak 可选参数，设置默认的回调函数
 * @constructor
 */
function ArticleMeta(wx_key,openid, article_id, type, callback) {
    if (!openid) {
        qyMsg("请绑定用户");
        return;
    }
    this.openid = openid;
    this.article_id = article_id;
    this.type = type;
    this.wx_key=wx_key;
    if (typeof callback === 'function') {
        this.default_callback = callback;
    }
    this.default_pageSize = 8;
    this.pageNum = 1;
    this.default_pl_id = 'comment_ul';
}

/**
 * 获取收藏点赞信息
 *
 * @param callback 回调函数 json
 * article_id 文章、听税、课程的id
 * favoritesCount 收藏次数
 * upCount 点赞次数
 * isfavorites 当前用户是否收藏
 * isup 当前用户是否点赞
 *
 */
ArticleMeta.prototype.queryArticleMetaInfo = function(callback) {
    if (!this.openid) {
        qyMsg("请绑定用户");
        return;
    }
    var data = {
        article_id: this.article_id,
        type: this.type,
        wx_key:this.wx_key,
        openid: this.openid
    };
    if (typeof callback === "function" && typeof this.default_callback === 'function') {
        this.default_callback = callback;
    }
    var cb = this.default_callback;
    doService(ctx + '/common/queryArticleMetaInfo', data, function(json) {
        if (typeof callback === 'function') {
            callback(json);
        } else {
            if (typeof cb === 'function') {
                cb(json);
            }
        }

    });
};

/**
 * 收藏
 * @param callback
 */
ArticleMeta.prototype.shcArticle = function(callback) {
    if (!this.openid) {
        qyMsg("请绑定用户");
        return;
    }
    var data = {
        article_id: this.article_id,
        type: this.type,
        wx_key:this.wx_key,
        openid: this.openid
    };
    var cb = this.default_callback;
    var that = this;
    doService(ctx + '/common/shCArticle', data, function(json) {
        if (typeof callback === 'function') {
            callback(json);
        } else {
            if (typeof cb === "function") {
                if (json.result) {
                    that.queryArticleMetaInfo(cb);
                } else {
                    if (json.msg) {
                        qyMsg(json.msg);
                    }
                }
            }
        }
    });
};

/**
 * 取消收藏
 * @param callback
 */
ArticleMeta.prototype.qxshcArticle = function(callback) {
    if (!this.openid) {
        qyMsg("请绑定用户");
        return;
    }
    var data = {
        article_id: this.article_id,
        type: this.type,
        wx_key:this.wx_key,
        openid: this.openid
    };
    var cb = this.default_callback;
    var that = this;
    doService(ctx + '/common/qxShCArticle', data, function(json) {
        if (typeof callback === 'function') {
            callback(json);
        } else {
            if (typeof cb === "function") {
                if (json.result) {
                    that.queryArticleMetaInfo(cb);
                } else {
                    if (json.msg) {
                        qyMsg(json.msg);
                    }
                }
            }
        }
    });
};

/**
 * 点赞
 * @param callback
 */
ArticleMeta.prototype.dzArticle = function (callback) {
    if (!this.openid) {
        qyMsg("请绑定用户");
        return;
    }
    var data = {
        article_id: this.article_id,
        type: this.type,
        wx_key:this.wx_key,
        openid: this.openid
    };
    var that = this;
    var cb = this.default_callback;
    doService(ctx + '/common/dzArticle', data, function(json) {
        if (typeof callback === 'function') {
            callback(json);
        } else {
            if (typeof cb === "function") {
                if (json.result) {
                    that.queryArticleMetaInfo(cb);
                } else {
                    if (json.msg) {
                        qyMsg(json.msg);
                    }
                }
            }
        }
    });
};

/**
 * 取消点赞
 * @param callback
 */
ArticleMeta.prototype.qxdzArticle = function (callback) {
    if (!this.openid) {
        qyMsg("请绑定用户");
        return;
    }
    var data = {
        article_id: this.article_id,
        type: this.type,
        wx_key:this.wx_key,
        openid: this.openid
    };
    var that = this;
    var cb = this.default_callback;
    doService(ctx + '/common/qxdzArticle', data, function(json) {
        if (typeof callback === 'function') {
            callback(json);
        } else {
            if (typeof cb === "function") {
                if (json.result) {
                    that.queryArticleMetaInfo(cb);
                } else {
                    if (json.msg) {
                        qyMsg(json.msg);
                    }
                }
            }
        }
    });
};

/**
 * 弹出评论窗口
 */
ArticleMeta.prototype.tcPinglun = function() {
    if (!this.openid) {
        qyMsg("请绑定用户");
        return;
    }
    this.plIndex = PL.tcPl();
};

/**
 * 关闭评论窗口
 */
ArticleMeta.prototype.gbPinglun = function() {
    layer.close(this.plIndex);
};

/**
 * 提交评论
 * @param callback
 */
ArticleMeta.prototype.tjPinglun = function() {
    if (!this.openid) {
        qyMsg("请绑定用户");
        return;
    }
    var data = {
        article_id: this.article_id,
        type: this.type,
        openid: this.openid,
        wx_key:this.wx_key,
        content: PL.getPlContent(),
        star_num: PL.getPlStarNum()
    };
    if ($.trim(data.content) == '') {
        qyMsg("请输入评论内容");
        return;
    }
    var that = this;
    layer.closeAll();
    doService(ctx + '/common/tjPinglun', data, function(json) {
        if (json.result) {
            qyMsg(json.msg);
            that.resetPlPageNum();
            that.queryPinglunData();
        } else {
            if (json.msg) {
                qyMsg(json.msg);
            }
        }
    });
};

ArticleMeta.prototype.resetPlPageNum = function() {
    this.pageNum = 1;
};

ArticleMeta.prototype.setPlUlId = function(id) {
    this.default_pl_id = id;
};

ArticleMeta.prototype.setPlPageNum = function(pageNum) {
    this.pageNum = pageNum;
};

/**
 * 获取评论列表
 * @param callback
 */
ArticleMeta.prototype.queryPinglunData = function() {
    if (!this.openid) {
        qyMsg("请绑定用户");
        return;
    }
    var data = {
        article_id: this.article_id,
        type: this.type,
        pageNum: this.pageNum,
        pageSize: this.default_pageSize,
        wx_key:this.wx_key,
        openid:this.openid
    };
    var that = this;
    doService(ctx + '/common/queryPinglunData', data, function(json) {
        if (json.result) {
            var data = json.data;
            if (that.pageNum == 1) {
                $("#" + that.default_pl_id).empty();
                if (data.length <= 0) {
                    $("#" + that.default_pl_id).append($("<li>暂无评论</li>"));
                }
            } else {
                if (data.length <= 0) {
                    // qyMsg("评论已加载完成");
                }
            }
            that.total = json.total;
            that.totalPage = json.totalPage;
            if(that.pageNum <= that.totalPage) {
                that.pageNum = json.pageNum + 1;
            }

            data.forEach(function(item) {
                var _html = '<li>' +
                    '<h6>' + ((item.contacts&&(item.contacts!=0&&item.contacts!='')) ? item.contacts : ('****'+item.username.substring(4,item.username.length))) + '<span>' + item.createTime.substr(0, 10) + '&nbsp;&nbsp;' + item.createTime.substr(11, 5) + '</span></h6>' +
                    '<p>' + item.content + '</p>' +
                    '</li>';
                
                var str="<li><div>" +
                		" <img src='"+ctx+"/static/kcb/image/user-head.png'/>"+ 
                		" <span>"+((item.contacts&&(item.contacts!=0&&item.contacts!='')) ? item.contacts : ('****'+item.username.substring(4,item.username.length)))+"</span><span>"+item.createTime.substr(0, 10) + '&nbsp;&nbsp;' + item.createTime.substr(11, 5)+"</span></div> <p>"+item.content+"</p></li>";
                $("#" + that.default_pl_id).append($(_html));
            });
        }
    });
};

/**
 * 评论相关对象
 * @type {{html: string, clickStar: PL.clickStar, getPlStarNum: PL.getPlStarNum, getPlContent: PL.getPlContent, tcPl: PL.tcPl}}
 */
var PL =  {
    html: '<div  class="showbox">'+
        '<div id="star">'+
        '<ul>'+
        '<li onclick="PL.clickStar(0)" class="plstar" ><a href="javascript:void(0);">1</a></li>'+
        '<li onclick="PL.clickStar(1)" class="plstar" ><a href="javascript:void(0);">2</a></li>'+
        '<li onclick="PL.clickStar(2)" class="plstar" ><a href="javascript:void(0);">3</a></li>'+
        '<li onclick="PL.clickStar(3)" class="plstar" ><a href="javascript:void(0);">4</a></li>'+
        '<li onclick="PL.clickStar(4)" class="plstar" ><a href="javascript:void(0);">5</a></li>'+
        '</ul>'+
        '<h6>选择评星</h6>'+
        '</div>'+
        '<div class="mainlist">'+
        '<textarea id="plContent" name="textbox" wrap="hard" placeholder="我来说两句（评论最多500字）" ></textarea>'+
        '<button  type="button" onclick="tjpinglun()" class="btn_tj">提  交</button>'+
        '</div>'+
        '</div>',

    clickStar: function(num) {
    	console.info("执行到了这儿"+num);
        for (var i = 0; i <= 4; i++) {
            if (i <= num) {
                $(".plstar").eq(i).addClass('on');
            } else {
                $(".plstar").eq(i).removeClass('on');
            }
        }
    },
    getPlStarNum: function() {
        return $(".plstar.on").size();
    },
    getPlContent: function() {
        return $("#plContent").val();
    },
    tcPl: function () {
        var index = layer.open({
            type: 1,
            content: PL.html
        });
        $("#plContent").keyup(function() {
            if($("#plContent").val().length > 500) {
                $("#plContent").val($("#plContent").val().substring(0, 500));
            }
        });
        return index;
    }
};


/**
 * 跳转至会员绑定页面
 * @param url
 */
function toHybd(url) {
    url = url ? url : (window.location.pathname + window.location.search);
    qyConfirm("未绑定会员，确定要绑定会员吗？", function(val){
        if(val) {
            url = encodeURIComponent(url);
            window.location.href = ctx + '/wode/hybd?wx_key=' + wx_key + "&toUrl=" + url;
        }
    });
}

$("#search").keyup(function(e){
    if(e.keyCode==13){
        $("#searchFrom1").form().submit();
    }else{
        return false;
    }
});


/**
 * 获取详情页面的action地址
 * @param id
 * @param lx
 * @param pid
 * @param cid
 * @returns {*}
 */
function getURLByLx(id, lx, cid, pid) {
    if (lx === 'ts') {
        return "/hyfw/ts/tsDetail?id=" + id + "&wx_key=" + wx_key;
    } else if (lx === 'article') {
        switch (cid){
            case '12'://直播
                // return "/hyfw/zbkc/zbkcDetail?id=" + id + "&wx_key=" + wx_key;
                return "/wlkc/wlkcDetail?id=" + id + "&wx_key=" + wx_key + '&bofangstatus=zbkc';
                break;
            case '32'://点播网络课程
                return "/hyfw/zbkc/zbkcDetail?id=" + id + "&wx_key=" + wx_key;
                break;
            case '33'://试听课程
                return "/hyfw/zbkc/zbkcDetail?id=" + id + "&wx_key=" + wx_key;
                break;
            case '16'://专家谈风险
                return "/hyfw/zbkc/zbkcDetail?id=" + id + "&wx_key=" + wx_key;
                break;
            case '278'://税控开票
                return "/hyfw/zbkc/zbkcDetail?id=" + id + "&wx_key=" + wx_key;
                break;
            case '279'://认证-勾选
                return "/hyfw/zbkc/zbkcDetail?id=" + id + "&wx_key=" + wx_key;
                break;
            case '280'://网上申报
                return "/hyfw/zbkc/zbkcDetail?id=" + id + "&wx_key=" + wx_key;
                break;
            case '16'://专家谈风险
                return "/hyfw/zbkc/zbkcDetail?id=" + id + "&wx_key=" + wx_key;
                break;
            case '23'://专题内容页
                return "/csrd/csrdZtDetailIndex?articleId=" + id + "&wx_key=" + wx_key;
                break;
            case '23ZtIndex'://专题内容页
                return "/csrd/csrdZtIndex?articleId=" + id + "&wx_key=" + wx_key;
                break;
            case '21'://期刊
                return "/hyfw/more/toArticleDetail?articleId=" + id + "&wx_key=" + wx_key;
                break;
            default:
                return "/article/articleDetailIndex?articleId=" + id + "&wx_key=" + wx_key;
//ctx + url + '?qkUrl=' + qkUrl + '&openid='+ openid + '&articleId='+id;
                // + " <a href=\"javascript:intoQkDetail('/hyfw/more/toArticleDetail','" + data[i].url + "','" + data[i].id + "')\">" + "在线阅读" + "</a>"
        }
    }
    return "";
}

function toForwordZjzx(id) {
    window.location.href = ctx + '/cszx/cszxZxzj?wx_key=' + wx_key;
}

function toForwordZjzxNew(id) {
    window.location.href = ctx + '/cszx/cszxListNew?wx_key=' + wx_key;
}

function toForwordMyzx(id) {
    window.location.href = ctx + '/cszx/cszxListMine?wx_key=' + wx_key;
}

function toForwordZjzxRm(id) {
    window.location.href = ctx + '/cszx/cszxListRm?wx_key=' + wx_key;
    window.location.href = ctx + '/cszx/cszxZxzj?openid=' + openid;
}

function toForwordZjzxNew(id) {
    window.location.href = ctx + '/cszx/cszxListNew?openid=' + openid;
}

function toForwordMyzx(id) {
    window.location.href = ctx + '/cszx/cszxListMine?openid=' + openid;
}

function toForwordZjzxRm(id) {
    window.location.href = ctx + '/cszx/cszxListRm?openid=' + openid;
}

function getContentInfo(content) {
    var text = $('<div>' + content + '</div>').text();
    return text.substring(0, 100) + "...";
}

function getContentHtml(content) {
    return $('<div>' + content + '</div>');
}

$(function() {
    if(typeof(isGetWxSignature)!="undefined" && isGetWxSignature == true && typeof(fwh)!="undefined" && fwh!= ''){

        var data1 = {path1: window.location.href};
        doService(ctx+"/common/getWxSignature", data1, function(json) {
            console.log(json);
            wx.config({
                debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                appId: json.appid, // 必填，公众号的唯一标识
                timestamp:json.timestamp, // 必填，生成签名的时间戳
                nonceStr: json.noncestr, // 必填，生成签名的随机串
                signature: json.signature, // 必填，签名，见附录1
                jsApiList: [
                    // 所有要调用的 API 都要加到这个列表中
                    'checkJsApi',
                    'openLocation',
                    'getLocation',
                    'onMenuShareTimeline',
                    'onMenuShareAppMessage',
                    'onMenuShareQQ',
                    'onMenuShareWeibo'
                ] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
            });

        });
    }
});

/**
 * 分享功能
 * @param title
 * @param desc
 * @param link
 * @param imgUrl
 */
function youqingPY(title,desc,_link,imgUrl) {
    var reg = /openid=[^\?\&]+[\&]?/
    var action = window.location.pathname.replace(ctx+'/','') + window.location.search.replace(reg, '');
    var link = window.location.host + "/common/wxApi?action=" + encodeURIComponent(action);

    // wx.onMenuShareAppMessage({
    //     title: '互联网之子 方倍工作室',
    //     desc: '在长大的过程中，我才慢慢发现，我身边的所有事，别人跟我说的所有事，那些所谓本来如此，注定如此的事，它们其实没有非得如此，事情是可以改变的。更重要的是，有些事既然错了，那就该做出改变。',
    //     link: 'http://mobile.csfw360.com/csfw_weix/hyfw/ts/tsDetail?id=93fa42af-1e68-11e7-9c79-02004c4f4f50&openid=ox1Ltt68yPAOG-oCa3x_CJO7i-Qk',
    //     imgUrl: 'http://img3.douban.com/view/movie_poster_cover/spst/public/p2166127561.jpg',
    //     trigger: function (res) {
    //         alert('用户点击发送给朋友');
    //     },
    //     success: function (res) {
    //         alert('已分享');
    //     },
    //     cancel: function (res) {
    //         alert('已取消');
    //     },
    //     fail: function (res) {
    //         alert(JSON.stringify(res));
    //     }
    // });


        // wx.onMenuShareTimeline({
        //     'title': '互联网之子 方倍工作室',
        //     'desc': '在长大的过程中，我才慢慢发现，我身边的所有事，别人跟我说的所有事，那些所谓本来如此，注定如此的事，它们其实没有非得如此，事情是可以改变的。更重要的是，有些事既然错了，那就该做出改变。',
        //     'link': 'http://movie.douban.com/subject/25785114/',
        //     'imgUrl': 'http://img3.douban.com/view/movie_poster_cover/spst/public/p2166127561.jpg',
        //     trigger: function (res) {
        //         alert('用户点击发送给朋友');
        //     },
        //     success: function (res) {
        //         alert('已分享');
        //     },
        //     cancel: function (res) {
        //         alert('已取消');
        //     },
        //     fail: function (res) {
        //         alert(JSON.stringify(res));
        //     }
        // });
    // });

    //获取openid的用户信息判断用户是否登录

    wx.ready(function(){
        //alert("微信验证OK");
        // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。

        // 获取“分享给朋友”按钮点击状态及自定义分享内容接口
        wx.onMenuShareAppMessage({
            title: title, // 分享标题
            desc: desc, // 分享描述
            link: _link, // 分享链接
            imgUrl: imgUrl, // 分享图标
            type: '', // 分享类型,music、video或link，不填默认为link
            dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
            success: function () {
                // 用户确认分享后执行的回调函数
                alert("分享给朋友成功");
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
                alert("分享给朋友失败");
            }
        });

        // 2.2 监听“分享到朋友圈”按钮点击、自定义分享内容及分享结果接口
        wx.onMenuShareTimeline({
            title: title, // 分享标题
            desc: desc, // 分享描述
            link: _link, // 分享链接
            imgUrl: imgUrl, // 分享图标
            type: '', // 分享类型,music、video或link，不填默认为link
            dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
            trigger: function (res) {
                // alert('用户点击分享到朋友圈');
            },
            success: function (res) {
                alert('已分享');
            },
            cancel: function (res) {
                alert('已取消');
            },
            fail: function (res) {
                // alert(JSON.stringify(res));
            }
        });

        // 2.3 监听“分享到QQ”按钮点击、自定义分享内容及分享结果接口
        wx.onMenuShareQQ({
            title: title, // 分享标题
            desc: desc, // 分享描述
            link: _link, // 分享链接
            imgUrl: imgUrl, // 分享图标
            type: '', // 分享类型,music、video或link，不填默认为link
            dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
            trigger: function (res) {
                // alert('用户点击分享到QQ');
            },
            complete: function (res) {
                alert(JSON.stringify(res));
            },
            success: function (res) {
                alert('已分享');
            },
            cancel: function (res) {
                alert('已取消');
            },
            fail: function (res) {
                // alert(JSON.stringify(res));
            }
        });

        // 2.4 监听“分享到微博”按钮点击、自定义分享内容及分享结果接口
        wx.onMenuShareWeibo({
            title: title, // 分享标题
            desc: desc, // 分享描述
            link: _link, // 分享链接
            imgUrl: imgUrl, // 分享图标
            type: '', // 分享类型,music、video或link，不填默认为link
            dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
            trigger: function (res) {
                // alert('用户点击分享到微博');
            },
            complete: function (res) {
                alert(JSON.stringify(res));
            },
            success: function (res) {
                alert('已分享');
            },
            cancel: function (res) {
                alert('已取消');
            },
            fail: function (res) {
                // alert(JSON.stringify(res));
            }
        });

        /*wx.hideOptionMenu();

        wx.showMenuItems({
            menuList: [
                'menuItem:share:qq', // 分享qq
                'menuItem:share:timeline', // 分享到朋友圈
                'menuItem:share:weiboApp' // 分享微博
            ],
            success: function (res) {
                // alert('已显示“阅读模式”，“分享到朋友圈”，“复制链接”等按钮');
            },
            fail: function (res) {
                // alert(JSON.stringify(res));
            }
        });*/
    });
}

function getImgByPath(path) {
    if (!path) {
        return "";
    }
    if (path.indexOf("http://") == 0 || path.indexOf("https://") == 0) {
        return path;
    }
    if (path.indexOf("/") == 0) {
        return "http://mobile.csfw360.com" + path;
    }
}