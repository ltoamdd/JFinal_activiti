// 设置页面viewport && 基准rem
(function(e,t){var i=document,n=window;var l=i.documentElement;var a,r;var d,s=document.createElement("style");var o;function m(){var i=l.getBoundingClientRect().width;if(!t){t=540}if(i>t){i=t}var n=i*100/e;s.innerHTML="html{font-size:"+n+"px;}"}a=i.querySelector('meta[name="viewport"]');r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no";if(a){a.setAttribute("content",r)}else{a=i.createElement("meta");a.setAttribute("name","viewport");a.setAttribute("content",r);if(l.firstElementChild){l.firstElementChild.appendChild(a)}else{var c=i.createElement("div");c.appendChild(a);i.write(c.innerHTML);c=null}}m();if(l.firstElementChild){l.firstElementChild.appendChild(s)}else{var c=i.createElement("div");c.appendChild(s);i.write(c.innerHTML);c=null}n.addEventListener("resize",function(){clearTimeout(o);o=setTimeout(m,300)},false);n.addEventListener("pageshow",function(e){if(e.persisted){clearTimeout(o);o=setTimeout(m,300)}},false);if(i.readyState==="complete"){i.body.style.fontSize="16px"}else{i.addEventListener("DOMContentLoaded",function(e){i.body.style.fontSize="16px"},false)}})(750,750);


//直播课程列表页 切换
$(function () {
   $('.zbkc_list_tit ul li').click(function(){
        //获得当前被点击的元素索引值
        var Index = $(this).index();
		//给菜单添加选择样式
	    $(this).addClass('active').siblings().removeClass('active');
		//显示对应的div
		$('.zbkc_list_con').children('div').eq(Index).show().siblings('div').hide();
   });
   
   //财税热点专题 切换
   $('.csrd_tit ul li').click(function(){
        //获得当前被点击的元素索引值
        var Index = $(this).index();
		//给菜单添加选择样式
	    $(this).addClass('active').siblings().removeClass('active');
		//显示对应的div
		$('.csrd_con').children('div').eq(Index).show().siblings('div').hide();

   });
   
   //会员服务-实务 切换
   $('.hyfw_sw_tit ul li').click(function(){
        //获得当前被点击的元素索引值
        var Index = $(this).index();
		//给菜单添加选择样式
	    $(this).addClass('active').siblings().removeClass('active');
		//显示对应的div
		$('.hyfw_sw_con').children('div').eq(Index).show().siblings('div').hide();
   });
   
   //会员特权类型 切换
   $('.hytqlx_tit ul li').click(function(){
        //获得当前被点击的元素索引值
        var Index = $(this).index();
		//给菜单添加选择样式
	    $(this).addClass('active').siblings().removeClass('active');
		//显示对应的div
		$('.hytqlx_con').children('div').eq(Index).show().siblings('div').hide();
   });
   
   
   //会员服务导航滑动
	var fl_w=$(".hyfw_nav_list").width();
	var flb_w=$(".hyfw_nav_left").width();
	$(".hyfw_nav_list").on('touchstart', function (e) {
		var touch1 = e.originalEvent.targetTouches[0];
		x1 = touch1.pageX;
		y1 = touch1.pageY;
		ty_left = parseInt($(this).css("left"));
	});
	$(".hyfw_nav_list").on('touchmove', function (e) {
		var touch2 = e.originalEvent.targetTouches[0];
		var x2 = touch2.pageX;
		var y2 = touch2.pageY;
		if(ty_left + x2 - x1>=0){
			$(this).css("left", 0);
		}else if(ty_left + x2 - x1<=flb_w-fl_w){
			$(this).css("left", flb_w-fl_w);
		}else{
			$(this).css("left", ty_left + x2 - x1);
		}
		if(Math.abs(y2-y1)>0){
			e.preventDefault();
		}
	});


});


//会员服务法规 下拉
var menuids=["hyfw_fg"] //Enter id(s) of SuckerTree UL menus, separated by commas

function buildsubmenus(){
for (var i=0; i<menuids.length; i++){
  var ultags=document.getElementById(menuids[i]).getElementsByTagName("ul")
    for (var t=0; t<ultags.length; t++){
    //ultags[t].parentNode.getElementsByTagName("a")[0].className="subfolderstyle"
    ultags[t].parentNode.onmouseover=function(){
    this.getElementsByTagName("ul")[0].style.display="block"
    }
    ultags[t].parentNode.onmouseout=function(){
    this.getElementsByTagName("ul")[0].style.display="none"
    }
    }
  }
}

if (window.addEventListener)
window.addEventListener("load", buildsubmenus, false)
else if (window.attachEvent)
window.attachEvent("onload", buildsubmenus)


//音频播放进度条
function progressBar(time){
	//初始化js进度条
	$("#bar").css("width","0px");
	//进度条的速度，越小越快
	var len = $("#progressBar").width();
	$("#bar").animate({ 
		width: len
		},time*1000);
		
	$("#bab").css("left","0px");
	//进度条的速度，越小越快
	var lem = $("#progressBar").width()-8;
	$("#bab").animate({ 
		left: lem
		},time*1000);
// 			  bar = setInterval(function(){
// 			   nowWidth = parseInt($("#bar").width());
// 			   //宽度要不能大于进度条的总宽度
// 			   if(nowWidth<=$("#progressBar").css("width")){
// 			    barWidth = (nowWidth + 1)+"px";
// 			    $("#bar").css("width",barWidth);
// 			   }else{
// 			    //进度条读满后，停止
// 			    clearInterval(bar);
// 			   } 
// 			  },speed);
}

// 我要评论
$(document).ready(function(){
	$(".showdiv").click(function(){
		var box = "150" ;
		var h =document.body.clientHeight;
		var rw =$(window).width()/2-box;
		$(".showbox").animate({top:'4rem',opacity:'show',width:'6rem',height:'5rem',right:rw},500);
		$("#zhezhao").css({
			display:"block",height:$(document).height()
		});
		return false;
	});
	$(".showbox .close").click(function(){
		$(this).parents(".showbox").animate({top:0,opacity: 'hide',width:0,height:0,right:0},500);
		$("#zhezhao").css("display","none");
	});
});


//选择评星
window.onload = function ()
{
 var oStar = document.getElementById("star");
 var aLi = oStar.getElementsByTagName("li");
 var oUl = oStar.getElementsByTagName("ul")[0];
 var oSpan = oStar.getElementsByTagName("span")[1];
 var oP = oStar.getElementsByTagName("p")[0];
 var i = iScore = iStar = 0;
 for (i = 1; i <= aLi.length; i++)
 {
  aLi[i - 1].index = i;
  //鼠标移过显示分数
  aLi[i - 1].onmouseover = function ()
  {
   fnPoint(this.index);
   //浮动层显示
   oP.style.display = "none";
   //计算浮动层位置
   oP.style.left = oUl.offsetLeft + this.index * this.offsetWidth - 104 + "px";
   //匹配浮动层文字内容
   oP.innerHTML = "<em><b>" + this.index + "</b> 分 " + aMsg[this.index - 1].match(/(.+)\|/)[1] + "</em>" + aMsg[this.index - 1].match(/\|(.+)/)[1]
  };
  //鼠标离开后恢复上次评分
  aLi[i - 1].onmouseout = function ()
  {
   fnPoint();
   //关闭浮动层
   oP.style.display = "none"
  };
  //点击后进行评分处理
  aLi[i - 1].onclick = function ()
  {
   iStar = this.index;
   oP.style.display = "none";
   oSpan.innerHTML = "<strong>" + (this.index) + " 分</strong> (" + aMsg[this.index - 1].match(/\|(.+)/)[1] + ")"
  }
 }
 //评分处理
 function fnPoint(iArg)
 {
  //分数赋值
  iScore = iArg || iStar;
  for (i = 0; i < aLi.length; i++) aLi[i].className = i < iScore ? "on" : "";
 }
};


















