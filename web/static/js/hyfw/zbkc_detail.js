/**
 * Created by huangwei on 2017/3/20.
 */
// var tsMeta = new ArticleMeta($("#openid").val(), $("#id").val(), 2, metaInfo);
var tsMeta = new ArticleMeta(wx_key,$("#openid").val(), articleId, 2, metaInfo);
// 设置页面viewport && 基准rem
var currentIndexDiv = 0;
(function (e, t) {
    var i = document, n = window;
    var l = i.documentElement;
    var a, r;
    var d, s = document.createElement("style");
    var o;

    function m() {
        var i = l.getBoundingClientRect().width;
        if (!t) {
            t = 540
        }
        if (i > t) {
            i = t
        }
        var n = i * 100 / e;
        s.innerHTML = "html{font-size:" + n + "px;}"
    }

    a = i.querySelector('meta[name="viewport"]');
    r = "width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no";
    if (a) {
        a.setAttribute("content", r)
    } else {
        a = i.createElement("meta");
        a.setAttribute("name", "viewport");
        a.setAttribute("content", r);
        if (l.firstElementChild) {
            l.firstElementChild.appendChild(a)
        } else {
            var c = i.createElement("div");
            c.appendChild(a);
            i.write(c.innerHTML);
            c = null
        }
    }
    m();
    if (l.firstElementChild) {
        l.firstElementChild.appendChild(s)
    } else {
        var c = i.createElement("div");
        c.appendChild(s);
        i.write(c.innerHTML);
        c = null
    }
    n.addEventListener("resize", function () {
        clearTimeout(o);
        o = setTimeout(m, 300)
    }, false);
    n.addEventListener("pageshow", function (e) {
        if (e.persisted) {
            clearTimeout(o);
            o = setTimeout(m, 300)
        }
    }, false);
    if (i.readyState === "complete") {
        i.body.style.fontSize = "16px"
    } else {
        i.addEventListener("DOMContentLoaded", function (e) {
            i.body.style.fontSize = "16px"
        }, false)
    }
})(750, 750);
$(function () {
    tabs();
    // init();
    // initAudio(ctx + '/temp/00001.mp3');
    // 获取收藏点赞状态和数量
    tsMeta.queryArticleMetaInfo();

    // 评论列表
    tsMeta.queryPinglunData();

    // 滚动加载评论
    $(window).scroll(function () {
        if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
            tsMeta.queryPinglunData();
        }
    });

    initDjs();

    initStatus();

    $("#chat_footer").hide();
    $("#idtext").focus(function(){
        // $("#chat_footer").css("position","absolute");
    });
    $("#idtext").blur(function(){
        // $("#chat_footer").css("position","fixed");
    });

    $("#chatul").hide();
    $("#chatul").scrollbarHide;

    //登陆成功
    ROP.On("enter_suc",
        function () {
            ShowMsg("加入成功");
            OnJoin();
        })
//重连中
    ROP.On("reconnect",
        function () {
            ShowMsg("reconnect:");
        })
//离线状态，之后会重连
    ROP.On("offline",
        function (err) {
            ShowMsg("offline:" + err);
        })
//登陆失败
    ROP.On("enter_fail",
        function (err) {
            ShowMsg("EnterFail:" + err);
        })
//收到消息
    ROP.On("publish_data",
        function (data, topic) {
            var name = getClientName(data);

            addList(data, name);
        })
//彻底断线了
    ROP.On("losed",
        function () {
            ShowMsg("Losed");
        })

    OnEnter();

});


function initDjs() {
    var w = $(document.body).width();
    var h = w / 1.88;
    $("#djsContent").height(h);
    var startTime = getStartTime().getTime();
    var endTime = getEndTime().getTime();

    var timer = setInterval(function () {
        $("#djsContent").height($("#videoPlayer").height());
        initStatus();
        var currentTime = new Date().getTime();
        if (currentTime < startTime) {
            $("#djsContent").show();
            showDjsNumber(startTime - currentTime);
        } else if (currentTime < endTime) {  // 正在直播
            $("#djsContent").remove();
            // clearInterval(timer);
        }
    }, 500);

    function showDjsNumber(time) {
        var result = getDjsNumbers(time);
        $("#djsDate").text(result.date);
        $("#djsHour").text(result.hour);
        $("#djsMinute").text(result.minute);
        $("#djsSecond").text(result.second);
    }

    function getDjsNumbers(times) {
        var d = Math.floor(times / 1000 / 60 / 60 / 24);
        var h = Math.floor(times / 1000 / 60 / 60 % 24);
        var m = Math.floor(times / 1000 / 60 % 60);
        var s = Math.floor(times / 1000 % 60);

        return {
            date: format(d),
            hour: format(h),
            minute: format(m),
            second: format(s)
        };

    }

    function format(n) {
        return (n > 9) ? ('' + n) : ('0' + n);
    }

}


function initStatus() {
    var startTime = getStartTime().getTime();
    var endTime = getEndTime().getTime();
    var current = new Date().getTime();
    if (startTime > current) {  // 未开始
        $("#status .status-icon-end").text('LIVE').css("background-color", '#C6222E');
        $("#status .status-text-end").text('直播预告中...').css("color", '#26BD3D');
        $("#zkbc_video_con").children("h6").children("span").text("0次播放");
    } else if (current < endTime) {  // 正在直播
        $("#status .status-icon-end").text('LIVE').css("background-color", '#C6222E');
        $("#status .status-text-end").text('正在直播中...').css("color", '#26BD3D');
        $("#zkbc_video_con").children("h6").children("span").text(click+"次播放");
    } else {  // 回放
        $("#status .status-icon-end").text('REPLAY').css("background-color", '#F79400');
        $("#status .status-text-end").text('直播已结束，查看回放').css("color", '#a1a1a1');
    }
}

function getStartTime() {
    var reg = /\d+/g
    var dArr = zb_classtime.match(reg);
    var tArr = zb_source.match(reg);

    var startDate = new Date();
    startDate.setFullYear(parseInt(dArr[0]));
    startDate.setMonth(parseInt(dArr[1]) - 1);
    startDate.setDate(parseInt(dArr[2]));
    startDate.setHours(parseInt(tArr[0]));
    startDate.setMinutes(parseInt(tArr[1]));
    startDate.setSeconds(0);
    return startDate;
}


function getEndTime() {
    var reg = /\d+/g
    var dArr = zb_classtime.match(reg);
    var tArr = zb_source.match(reg);

    var endDate = new Date();
    endDate.setFullYear(parseInt(dArr[0]));
    endDate.setMonth(parseInt(dArr[1]) - 1);
    endDate.setDate(parseInt(dArr[2]));
    endDate.setHours(parseInt(tArr[2]));
    endDate.setMinutes(parseInt(tArr[3]));
    endDate.setSeconds(0);
    return endDate;
}

/**
 * 收藏和点赞默认的回调函数
 * @param json
 */
function metaInfo(json) {
    if (json.isup) {
        $("#dzBtn").addClass('active');
    } else {
        $("#dzBtn").removeClass('active');
    }
    if (json.isfavorites) {
        $("#shoucBtn").addClass("active");
    } else {
        $("#shoucBtn").removeClass("active");
    }
    $("#upCount").text(json.upCount);
}

/**
 * 点击收藏按钮，用于收藏或取消收藏
 */
function shoucang() {
    if (!$("#shoucBtn").hasClass('active')) {
        tsMeta.shcArticle();
    } else {
        tsMeta.qxshcArticle();
    }
}

/**
 * 点击点赞按钮，用于点赞或取消点赞
 */
function dianzan() {
    if (!$("#dzBtn").hasClass('active')) {
        tsMeta.dzArticle();
    } else {
        tsMeta.qxdzArticle();
    }
}

/**
 * 评论，弹出评论窗口
 */
function pinglun() {
    tsMeta.tcPinglun();
}

/**
 * 提交评论内容
 */
function tjpinglun() {
    tsMeta.tjPinglun();
}
function backShuye() {
    window.history.back(-1);
}

function ShowMsg(str) {
    $("#chat_show").val(str + '\n' + $("#chat_show").val());
}
function Publish() {
    if ($("#idtext").val() != "" && $.trim($("#idtext").val()).length != 0) {
        // if($("#binded_flag").val() == 0){
        ROP.Publish("[" + $("#id_client_context").val() + "]" + $("#idtext").val(), $("#idgroup").val());
        submitChat();
        // }else{
        //     qyMsg("请绑定用户。");
        // }

    } else {
        qyMsg("请输入内容后发送。");
        // $("#idtext").focus();
    }

}
function OnEnter() {
    ROP.Enter($("#id_pubkey").val(), $("#id_subkey").val(), $("#id_client").val());
}
function OnJoin() {
    ROP.Subscribe($("#idgroup").val());
    initChatList();
}
function OnUnJoin() {
    ROP.UnSubscribe($("#idgroup").val());
}
function Clear() {
    $("#chat_show").attr("value", "");
}

//提交聊天记录
function submitChat() {

    var submit_chat_postData = {
        topicId: $("#idgroup").val(),
        userid: $("#id_client").val(),
        contactName: $("#id_client_context").val(),
        content: "[" + $("#id_client_context").val() + "]" + $("#idtext").val()
    };

    doService(ctx + '/chat/chatHis/saveChat', submit_chat_postData, function (json) {

        $("#idtext").val("");
    });
}


function addList(data, name) {
    var str = "<li>"
        + "<img src=" + ctx + "/static/images/person_head_new.png\>"
        + "<div>"
        + "<span>" + name + "</span>"
        + "<p>" + data.replace("[" + name + "]", "") + "</p>"
        + "</div>"
        + "</li>"
    $("#chatul").append(str);

    var list_length = $("#chatul").children("li").length;

    var li_height = $("#chatul").children("li").innerHeight()

    $("#chatul").scrollTop(li_height * list_length);

    $("#idtext").val("");
    // $("#idtext").focus();
}

function initList(data) {
    for (var i = 0; i < data.length; i++) {
        var chatid = data[i].id;
        var clientname = getClientName(data[i].content);
        var str = "<li>"
            + "<img src=" + ctx + "/static/images/person_head_new.png\>"
            + "<div>"
            + "<span>" + clientname + "</span>"
            + "<p>"  + data[i].content.replace("[" + clientname + "]", "") +  "</p>"
            + "</div>"
            + "</li>"

        +"</li>";
        $("#chatul").append(str);
    }

    // alert($("#chatid_"+data[data.length-1].id).innerHeight());
    $("#chatul").scrollTop($("#chatid_" + data[0].id).innerHeight() * data.length);


}

//初始化列表
function initChatList() {

    var chat_his_postData = {
        topicId: $("#idgroup").val(),
        pageSize: 20
    };

    doService(ctx + '/chat/chatHis/queryChatHistory', chat_his_postData, function (json) {
        // if(!json.list||json.list.length == 0){
        //     //没有记录时
        //     $('.zbkc_list_con1').append($('<div class="nomessage"><H1>暂无信息</H1></div>'));
        // }
        // $("#chatul").empty();

        initList(json.data);
    });
}


function getClientName(str) {
    var name;
    var reg = /\[(.*?)\]/gi;
    var tmp = str.match(reg);
    if (tmp) {
        for (var i = 0; i < tmp.length; i++) {
            // alert(tmp[i]); // 保留中括号
            // alert(tmp[i].replace(reg, "$1")); // 不保留中括号
            name = tmp[i].replace(reg, "$1");
        }
    } else {
        // alert("no match.");
    }
    return name;
}


function tabs() {
    var $index = $("#tabs>.tabs-header>span.active").index() > -1 ? $("#tab>.tabs-header>span.active").index() : 0;
    $("#tabs>.tabs-header>span").eq($index).addClass('active');
    $("#tabs>.tabs-items>.tabs-item").eq($index).show();

    $("#tabs>.tabs-header>span").click(function () {
        var $index = $(this).index();
        if (!$(this).hasClass('active')) {
            $(this).addClass('active').siblings().removeClass();
            // $("#tabs>.tabs-items>.tabs-item").eq($index).show().siblings().hide();
            if ($index == 0) { //简介
                $("#tab_jianji").show();
                $("#zkbc_video_con").show();
                $(".zbkc_comment").show();
                $(".breif_footer").show();
                $("#chatul").hide();
                $("#chat_footer").hide();
            } else { //互动
                $("#tab_jianji").hide();
                $("#zkbc_video_con").hide();
                $(".zbkc_comment").hide();
                $(".breif_footer").hide();
                $("#chatul").show();
                $("#chat_footer").show();

            }
        }
        ;
    })
}

