// 设置页面viewport && 基准rem
(function(e,t){ var i=document,n=window; var l=i.documentElement; var a,r; var d,s=document.createElement("style"); var o; function m(){ var i=l.getBoundingClientRect().width; if(!t){ t=540 } if(i>t){ i=t } var n=i*100/e; s.innerHTML="html{font-size:"+n+"px;}" } a=i.querySelector('meta[name="viewport"]'); r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no"; if(a){ a.setAttribute("content",r) }else{ a=i.createElement("meta"); a.setAttribute("name","viewport"); a.setAttribute("content",r); if(l.firstElementChild){ l.firstElementChild.appendChild(a) }else{ var c=i.createElement("div"); c.appendChild(a); i.write(c.innerHTML); c=null } } m(); if(l.firstElementChild){ l.firstElementChild.appendChild(s) }else{ var c=i.createElement("div"); c.appendChild(s); i.write(c.innerHTML); c=null } n.addEventListener("resize",function(){ clearTimeout(o); o=setTimeout(m,300) },false); n.addEventListener("pageshow",function(e){ if(e.persisted){ clearTimeout(o); o=setTimeout(m,300) } },false); if(i.readyState==="complete"){ i.body.style.fontSize="16px" }else{ i.addEventListener("DOMContentLoaded",function(e){ i.body.style.fontSize="16px" },false) } })(750,750);
var DEFAULTBFCS = 1234;
// var tsPage = {
//     pageNum: 1,
//     pageSize: 15,
//     sort: 1
// }
var sort = 1;
var tsData;

$(function() {
    loadTsTabs();
    // loadTsList();
    initEvent();
});

function loadTsTabs() {
    doService(ctx + '/hyfw/ts/queryTsTabs', {openid: $("#openid").val(), wx_key: wx_key}, function(json) {
        if(json) {
            $("#tsTabs").empty();
            json.forEach(function(item) {
                var _li = '<li>' +
                    '<a href="javascript:void(0)" tab="' + item.id + '" ><span>' + item.name + '</span></a>' +
                '</li>';
                $("#tsTabs").append($(_li));
            });
            $("#tsTabs").find("a").eq(0).addClass("active");
            loadTsData();
        }
    });
}

function loadTsData() {
    var postData = {
        openid: $("#openid").val(),
        wx_key: wx_key,
        tab: $("#tsTabs .active").attr("tab")
    };
    doService(ctx + '/hyfw/ts/queryTsDataList', postData, function(json) {
        if (json.result) {
            tsData = json;
            showTsData();
        }
    });
}

function showTsData() {
    var data = tsData.data;
    $("#tsList").empty();
    $("#listLength").html(tsData.total);
    // $("#tsCount").html("栏目总播放次数：" + (tsData.totalPlayTimes + tsData.total * DEFAULTBFCS) + "次");
    if (data.length <= 0) {
        $("#tsList").append($("<div>暂无数据</div>"));
    }
    for(var i = 0; i < data.length; i++) {
        var _html = "<section>"
            + "<a href=\"javascript:showTsDetail('" + data[i].id + "')\">"
            + "<h4>" + data[i].title + "</h4>"
            + "<p>" + data[i].createTime.substr(0, 10) + "<span>"
            + (data[i].playTimes + DEFAULTBFCS) + "次播放</span></p>"
            + "</a>"
            + "</section>";
        $("#tsList").append($(_html));
    }
}

/**
 * 初始化加载列表
 */
// function loadTsList() {
//     var postData = {
//         openid: $("#openid").val(),
//         pageNum: tsPage.pageNum,
//         pageSize: tsPage.pageSize,
//         sort: tsPage.sort,
//     };
//     doService(ctx + '/hyfw/ts/queryTsDataList', postData, function(json) {
//         if (json.result) {
//             var data = json.data;
//             $("#listLength").html(json.total);
//             $("#tsCount").html("栏目总播放次数：" + (json.count + json.total * DEFAULTBFCS) + "次");
//             if (tsPage.pageNum == 1) {
//                 $("#tsList").empty();
//                 if (data.length <= 0) {
//                     $("#tsList").append($("<section>暂无数据</section>"));
//                 }
//             } else {
//                 if (data.length <= 0) {
//                     qyMsg("数据已加载完成");
//                 }
//             }
//             tsPage.total = json.total;
//             tsPage.totalPage = json.totalPage;
//             if(tsPage.pageNum <= tsPage.totalPage) {
//                 tsPage.pageNum = json.pageNum + 1;
//             }
//             for(var i = 0; i < data.length; i++) {
//                 var _html = "<section>"
//                     + "<a href=\"javascript:showTsDetail('" + data[i].id + "')\">"
//                     + "<h4>" + data[i].title + "</h4>"
//                     + "<p>" + data[i].createTime.substr(0, 10) + "<span>"
//                     + (data[i].playTimes + DEFAULTBFCS) + "次播放</span></p>"
//                     + "</a>"
//                     + "</section>";
//                 $("#tsList").append($(_html));
//             }
//         }
//     });
// }


/**
 * 排序
 */
function paixu() {
    sort = - sort;
    if($("#mrts_px").hasClass("mrts_list_sx")) {
        $("#mrts_px").removeClass("mrts_list_sx").addClass("mrts_list_jx");
    } else {
        $("#mrts_px").removeClass("mrts_list_jx").addClass("mrts_list_sx");
    }
    tsData.data.reverse();
    showTsData();
}

function initEvent() {

    /**
     * 滚到到浏览器底部以后 加载下一页数据
     */
    // $(window).scroll(function() {
    //     // window.pageYOffset 滚动条位置
    //     // window.innerHeight 视口尺寸
    //     // document.body.scrollHeight 文档高度
    //     if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
    //         loadTsList();
    //     }
    // });

    $("body").on('click', "#tsTabs a", function() {
        $("#tsTabs a").removeClass("active");
        $(this).addClass("active");
        loadTsData();
    });
}


/**
 * 跳转到听税详情页面
 * @param id
 */
function showTsDetail(id) {
    window.location.href = ctx + '/hyfw/ts/tsDetail?id=' + id + '&wx_key=' + wx_key;
    // doService(ctx + '/wode/hybd/queryHybd', {openid: $("#openid").val()}, function(json) {
    //     console.log(json);
    //     if(json.result) {
    //         window.location.href = ctx + '/hyfw/ts/tsDetail?id=' + id + '&openid=' + $("#openid").val();
    //     } else {
    //         toHybd();
    //     }
    // });
}

