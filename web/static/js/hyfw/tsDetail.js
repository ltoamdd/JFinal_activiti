// 设置页面viewport && 基准rem
var tsMeta = new ArticleMeta(wx_key,$("#openid").val(), $("#id").val(), 1, metaInfo);
(function(e,t){ var i=document,n=window; var l=i.documentElement; var a,r; var d,s=document.createElement("style"); var o; function m(){ var i=l.getBoundingClientRect().width; if(!t){ t=540 } if(i>t){ i=t } var n=i*100/e; s.innerHTML="html{font-size:"+n+"px;}" } a=i.querySelector('meta[name="viewport"]'); r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no"; if(a){ a.setAttribute("content",r) }else{ a=i.createElement("meta"); a.setAttribute("name","viewport"); a.setAttribute("content",r); if(l.firstElementChild){ l.firstElementChild.appendChild(a) }else{ var c=i.createElement("div"); c.appendChild(a); i.write(c.innerHTML); c=null } } m(); if(l.firstElementChild){ l.firstElementChild.appendChild(s) }else{ var c=i.createElement("div"); c.appendChild(s); i.write(c.innerHTML); c=null } n.addEventListener("resize",function(){ clearTimeout(o); o=setTimeout(m,300) },false); n.addEventListener("pageshow",function(e){ if(e.persisted){ clearTimeout(o); o=setTimeout(m,300) } },false); if(i.readyState==="complete"){ i.body.style.fontSize="16px" }else{ i.addEventListener("DOMContentLoaded",function(e){ i.body.style.fontSize="16px" },false) } })(750,750);
$(function () {
    init();
    // 获取收藏点赞状态和数量
    tsMeta.queryArticleMetaInfo();

    // 评论列表
    tsMeta.queryPinglunData();

    // 滚动加载评论
    $(window).scroll(function() {
        if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
            tsMeta.queryPinglunData();
        }
    });
//    window.addEventListener("popstate", function(e) {  
//        alert("我监听到了浏览器的返回按钮事件啦");//根据自己的需求实现自己的功能  
//    }, false);  
});
/**
 * 收藏和点赞默认的回调函数
 * @param json
 */
function metaInfo(json) {
    if(json.isup) {
        $(".dz").addClass('active');
        $("#dzBtn1").addClass('active');
    }else {
        $(".dz").removeClass('active');
        $("#dzBtn1").removeClass('active');
    }
    if(json.isfavorites) {
        $("#shoucBtn").addClass("active");
    } else {
        $("#shoucBtn").removeClass("active");
    }
    $("#upCount").text(json.upCount);
}

/**
 * 点击收藏按钮，用于收藏或取消收藏
 */
function shoucang() {
    if (!$("#shoucBtn").hasClass('active')) {
        tsMeta.shcArticle();
    } else {
        tsMeta.qxshcArticle();
    }
}

/**
 * 点击点赞按钮，用于点赞或取消点赞
 */
function dianzan() {
    if(!$("#dzBtn").hasClass('active')) {
        tsMeta.dzArticle();
    } else {
        tsMeta.qxdzArticle();
    }
}

/**
 * 评论，弹出评论窗口
 */
function pinglun() {
    tsMeta.tcPinglun();
}

/**
 * 提交评论内容
 */
function tjpinglun() {
    tsMeta.tjPinglun();
}

/**
 * 跳转至听税列表页面
 */
function toTsList() {
	tkjl();
    window.location.href = ctx + '/hyfw/ts?wx_key=' + wx_key;
}

function init() {
    setInfo ();
    // var audio_src = ctx + "/hyfw/ts/queryAudioById?id=" + $("#id").val() + "&openid=" + $('#openid').val();
    var audio_src = $("#file_host").val()  + $("#audio_src").val();
    initAudio(audio_src);
    if ($("#image").val()) {
        // var img_src = ctx + "/hyfw/ts/queryAudioImgById?id=" + $("#id").val() + "&openid=" + $('#openid').val();
        var img_src = $("#file_host").val() + $("#image").val();
        $("#audio_img").attr("src", img_src);
    }
    // 引用temp文件夹下的方式
    // doService(ctx + "/hyfw/ts/queryTsSrcsInfo", {id: $("#id").val()}, function (json) {
    //     if (json.result) {
    //         if (json.audio_src) {
    //             initAudio(ctx + "/temp/" + json.audio_src);
    //         }
    //     }
    // });



}

function initAudio(src) {
    var step, step1;
    var audio = document.createElement("audio");
    var timer;
    audio.src = src;

    // 开始加载数据
    $(audio).on('loadstart', function () {
        console.log("audio.paused: " + audio.paused);
        console.log('loadstart');
    });

    // 元数据加载完毕
    $(audio).on("loadedmetadata", function() {
        console.log("audio.paused: " + audio.paused);
        var duration = audio.duration.toFixed(0);
        step = $("#progressBar").width() / duration;
        step1 = ($("#progressBar").width() - 8) / duration;
        // qyAlert("时长：" + audio.duration + "   格式化后的时间:" + second2Minute(duration));
        $("#shc").html(second2Minute(duration));
        $("#shc_info").html(second2Minute(duration));
    });

    $(audio).on('loadeddata', function() {
        console.log("audio.paused: " + audio.paused);
        console.log('首次加载完毕');
    });

    // 可以播放
    $(audio).on("canplay", function () {
        console.log('设置播放按钮可用');
        // canPlay = true;
        // layer.close(loadIndex);
    });

    $(audio).on('ended', function() {
       console.log('播放结束');
       $("#bfkzBtn").removeClass().addClass('bf');
    });

    // 发送错误
    $(audio).on("error", function(e) {
        console.log('error');
    });

    // currentTime属性发生变化时触发
    $(audio).on("timeupdate", function () {
        $("#bab").css({'left': step1 * audio.currentTime + 'px'});
        $("#bar").width(step * audio.currentTime);
        $("#dqsj").html(second2Minute(audio.currentTime.toFixed(0)));
    });

    // 调整播放位置
    $("#progressBar").click(function(e) {
        var box = $("#bar")[0].getBoundingClientRect();
        audio.currentTime = (e.clientX - box.left) / step;
    });

    // 拖动播放位置
    $("#progressBar").bind('touchmove',function(e){
        var box = $("#bar")[0].getBoundingClientRect();
        audio.currentTime = (e.originalEvent.changedTouches[0].clientX - box.left) / step;
    });

    // 快退
    $("#ktBtn").bind('touchstart', function () {
        timer = setInterval(function () {
            audio.currentTime -= 1;
        }, 50);
    });
    $("#ktBtn").click(function(){
        audio.currentTime -= 10;
    });

    // 快进
    $("#kjBtn").bind('touchstart', function () {
        timer = setInterval(function () {
            audio.currentTime += 1;
        }, 50);
    });
    $("#kjBtn").click(function(){
        audio.currentTime += 10;
    });

    // 停止快进和快退
    $(window).bind('touchend', function() {
        clearInterval(timer);
    });

    // 播放或暂停
    $("#bfkzBtn").on('click', bfkzClick);

    function bfkzClick() {
        // qyAlert(canPlay);
        if (audio.paused) {
            audio.play();
            if(!audio.paused) {
                $("#bfkzBtn").removeClass().addClass('zt');
            }
        } else {
            audio.pause();
            if(audio.paused) {
                $("#bfkzBtn").removeClass().addClass('bf');
            }
        }
    }

}

// 秒 转 时:分:秒 121秒->2:01
function second2Minute(second) {
    var time = parseInt(second);
    if (time < 60) {
        return '0' + ':' + ((time < 10) ? ('0' + time) : time );
    }
    var s = (time % 60);
    s = (s < 10) ? ('0' + s) : s;
    var m = Math.floor(time / 60);
    if (time < (60 * 60)) {
        return m + ':' + s;
    }
    var h = Math.floor(m / 60);
    m = m % 60;
    m = (m < 10) ? ('0' + m) : m;
    return h + ':' + m + ':' + s;
}

function setInfo() {
    $("#ts_content").html(getContentInfo(ts_content));
}

function showContent() {
    // $("#ts_info").hide();
    // $("#ts_content").show();
    // var content = $("#tsContent").val();
    $("#ts_content").empty();
    $("#ts_content").append(getContentHtml(ts_content));
    var ewm_src=$("#ewm_src").val();
    var str="<img style='width:100%;height:auto' src="+ctx+ewm_src+" alt=''/>";
    $("#ts_content").append(str);
    $("#showContentBtn").hide();
}

function tsYouQing() {

    // youqingPY({
    //     title: '每日听税',
    //     desc: '每日听税每日听税每日听税每日听税'
    // });
}
/**
 * 获取听课记录
 */
function tkjl(){
	var dqsj=$("#dqsj").text();
	if(dqsj=="0:00"){//说明没有听课，没有记录的必要
		return;
	}else{//听课了
		var course_total_time=getTotlaTime($("#shc").text());
		var course_gk_time=getTotlaTime($("#dqsj").text());
		var course_gk_bfb=getGkBfb(course_total_time,course_gk_time);
		var data={
				openid:$("#openid").val(),
				article_id:$("#id").val(),
				title:$("#title").val(),
				course_cid:'ts',
				wx_key:wx_key,
				course_total_time:course_total_time,
				course_gk_time:course_gk_time,
				course_gk_bfb:course_gk_bfb
			};
		console.info(data);
		var url=ctx+"/viewlog"
			doService(url,data,function(json){
				if(json.result){//数据提交
					
				}
			});
	}
	
}
function getTotlaTime(time){//获取课程总时长转化为--秒
	var s=time.substring(0,time.indexOf(":"));
	var e=time.substring(time.indexOf(":")+1,time.length);
	var z=Number(s)*60+Number(e)
	return z;
}
function getGkBfb(course_total_time,course_gk_time){//获取听课时长百分比
	console.info(course_total_time);
	console.info(course_gk_time);
	var e=course_gk_time/course_total_time;
	if(e<=0.15){
		bfb="10%";
		console.info("10%");
	}else if(e>=0.15&&e<0.25){
		bfb="20%";
		console.info("20%");
	}else if(e>=0.25&&e<0.35){
		bfb="30%";
		console.info("30%");
	}else if(e>=0.35&&e<0.45){
		bfb="40%";
		console.info("40%");
	}else if(e>=0.45&&e<0.55){
		bfb="50%";
		console.info("50%");
	}else if(e>=0.55&&e<0.65){
		bfb="60%";
		console.info("60%");
	}else if(e>=0.65&&e<0.75){
		bfb="70%";
		console.info("70%");
	}else if(e>=0.75&&e<0.85){
		bfb="80%";
		console.info("80%");
	}else if(e>=0.85&&e<0.95){
		bfb="90%";
		console.info("90%");
	}else if(e>=0.95){
		bfb="100%";
		console.info("100%");
	}
	console.info(bfb);
	return bfb;
}
$(function(){  
	     pushHistory();  
	     window.addEventListener("popstate", function(e) {  
	         tkjl();
	         window.location.href = ctx + '/hyfw/ts?wx_key=' + wx_key;
	  }, false);  
	 function pushHistory() {  
          var state = {  
              title: "title",  
              url: "#"  
         };  
         window.history.pushState(state, "title", "#");
	  }  
	       
});  