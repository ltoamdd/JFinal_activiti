/**
 * Created by Administrator on 2017/3/9.
 */
// 设置页面viewport && 基准rem
(function(e,t){var i=document,n=window;var l=i.documentElement;var a,r;var d,s=document.createElement("style");var o;function m(){var i=l.getBoundingClientRect().width;if(!t){t=540}if(i>t){i=t}var n=i*100/e;s.innerHTML="html{font-size:"+n+"px;}"}a=i.querySelector('meta[name="viewport"]');r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no";if(a){a.setAttribute("content",r)}else{a=i.createElement("meta");a.setAttribute("name","viewport");a.setAttribute("content",r);if(l.firstElementChild){l.firstElementChild.appendChild(a)}else{var c=i.createElement("div");c.appendChild(a);i.write(c.innerHTML);c=null}}m();if(l.firstElementChild){l.firstElementChild.appendChild(s)}else{var c=i.createElement("div");c.appendChild(s);i.write(c.innerHTML);c=null}n.addEventListener("resize",function(){clearTimeout(o);o=setTimeout(m,300)},false);n.addEventListener("pageshow",function(e){if(e.persisted){clearTimeout(o);o=setTimeout(m,300)}},false);if(i.readyState==="complete"){i.body.style.fontSize="16px"}else{i.addEventListener("DOMContentLoaded",function(e){i.body.style.fontSize="16px"},false)}})(750,750);
// console.log(ctx);
// console.log("----------------------------");
var postData = {
    pageNum : 1,
    pageSize: 15,
    start: (1 - 1) * 15,
    dj:0,
    sort: 1,
    wx_key: wx_key,
    openid:$("#openid").val()
}

$(function() {
    initWlkcList();
    initEvent();
});


//初始化列表
function initWlkcList(){

    console.log(ctx);
    doService(ctx + '/hyfw/wlkc/queryAllWlkc_Xhss', postData, function(json) {
        postData.total = json.total;
        $("#ztxkul").empty();
        initWlkcBlock(json.list);
    });
}

//初始化网络课程单元块模块
function initWlkcBlock(data) {
	//debugger;
    if (data.length <= 0) {
        qyMsg("数据已加载完成");
    }
    for(var i=0;i<data.length;i++){
    	var str="<li>" +
		" <a href=\"javascript:wlkcDetail('"+data[i].id+"','" + data[i].cid + "','article')\">" +
				" <img src='" + getImgByPath(data[i].img) +"' alt='' /> " +
					" <section> ";
                    if (wx_key != 'gh_18a9a687b51a' ) {
                        if(data[i].rank==0){
                            // str+=" <span class='badge badge-gary' style='font-size:12px;'>"+showRank(data[i])+"</span> ";
                            str+=" <span class='badge badge-orange' style='font-size:12px;'>"+showRank(data[i])+"</span> ";
                        }else{
                            str+=" <span class='badge badge-orange' style='font-size:12px;'>"+showRank(data[i])+"</span> ";
                        }
                    }
					str+=" <p>"+data[i].title+"</p> " +
					" <div class='info'><span>"+kcclick(data[i])+"人</span><span>"+data[i].createtime1+"</span></div> " +
					" </section> " +
					" </a> " +
					" </li>";
        $("#ztxkul").append(str);
    }
}
function wlkcDetail(id,cid,nrlx) {//csrd/csrdZtDetailIndex?articleId=1&openid=undefined#
    window.location.href = ctx + getURLByLx(id,nrlx,cid);//'/csrd/csrdZtDetailIndex?articleId=' + id + '&openid=' + $("#openid").val();
}
function getURLByLx(id, lx, cid, pid) {
    if (lx === 'ts') {
        return "/hyfw/ts/tsDetail?id=" + id + "&wx_key=" + wx_key;
    } else if (lx === 'article') {
        switch (cid){
            case '32'://点播网络课程
            return "/wlkc/wlkcDetail?id=" + id + "&wx_key=" + wx_key+"&bofangstatus=wlkc";
            break;
        }
        switch (cid){
        case '33'://点播网络课程
            return "/wlkc/wlkcDetail?id=" + id + "&wx_key=" + wx_key+"&bofangstatus=wlkc";
            break;
        }
    }
    return "";
}
function kcclick(data){
	console.info("点击数"+data.click);
	if(data.click==undefined){
		return 0;
	}else{
		return data.click;
	}
}

function showRank(data){
    //debugger;
    if(data.rank == 2){
        return "普通专享";
    }else if(data.rank == 3){
        return "银卡专享";
    }else if(data.rank == 4){
        return "高端专享";
    }else if(data.rank == 0){
    	return "限时免费";
    }
    
}


function initEvent() {
    /**
     * 滚到到浏览器底部以后 加载下一页数据
     */

    $(window).scroll(function() {
        //debugger;
        // window.pageYOffset 滚动条位置
        // window.innerHeight 视口尺寸
        // document.body.scrollHeight 文档高度
        if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
            if (postData.pageNum * postData.pageSize < postData.total) {
                //initWlkcList();
                loadWlkcList();
            }else{
                qyMsg("数据已加载完成");
            }
        }
    });


    $('#my-menu1').change(function () {
        //alert(121212);
        var rankDj = $(this).children('option:selected').val();
        postData.pageNum = 1;
        postData.start = 0;
        if(rankDj == 1){
            postData.dj = 2;

        }if(rankDj == 2){
            postData.dj = 3;
        }
        if(rankDj == 3){
            postData.dj = 4;
        }

        if(rankDj == 0){
            postData.dj = 0;
        }
        initWlkcList();
    });

    $('#my-menu2').change(function () {
        var zxZr = $(this).children('option:selected').val();//最新最热切换
        postData.pageNum = 1;
        postData.start = 0;
        if(zxZr == 1){
            postData.sort = 1;
        }else{
            postData.sort = -1;
        }
        initWlkcList();
    });

}


function loadWlkcList() {
    postData.pageNum += 1;
    postData.start = (postData.pageNum - 1) * postData.pageSize;
    doService(ctx + '/hyfw/wlkc/queryAllWlkc_Xhss', postData, function(json) {
        postData.total = json.total;
        initWlkcBlock(json.list);
    });
}

function showWlkcDetail(id) {
    console.log(id);
}

function  djbdhy() {
    //alert(111);
    doService(ctx + '/wode/hybd/queryHybd', {openid: $("#openid").val(), wx_key: wx_key}, function(json) {
        debugger
        console.log(json);
        if(json.result) {
            window.location.href = ctx + '/hyfw/wlkc?wx_key=' + wx_key;
            $(".wlkc_list_djbd").hide();
        } else {
            toHybd();
            $(".wlkc_list_djbd").show();
        }
    });

}
function backShuye(){
	window.history.back(-1); 
}