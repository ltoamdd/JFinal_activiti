/**
 * Created by huangwei on 2017/3/20.
 */
 var tsMeta = new ArticleMeta(wx_key,$("#openid").val(), $("#wlkc_id").val(), 2, metaInfo);

//var tsMeta = new ArticleMeta(wx_key, articleId, 2, metaInfo);
// 设置页面viewport && 基准rem
var currentIndexDiv = 0;
(function(e,t){ var i=document,n=window; var l=i.documentElement; var a,r; var d,s=document.createElement("style"); var o; function m(){ var i=l.getBoundingClientRect().width; if(!t){ t=540 } if(i>t){ i=t } var n=i*100/e; s.innerHTML="html{font-size:"+n+"px;}" } a=i.querySelector('meta[name="viewport"]'); r="width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no"; if(a){ a.setAttribute("content",r) }else{ a=i.createElement("meta"); a.setAttribute("name","viewport"); a.setAttribute("content",r); if(l.firstElementChild){ l.firstElementChild.appendChild(a) }else{ var c=i.createElement("div"); c.appendChild(a); i.write(c.innerHTML); c=null } } m(); if(l.firstElementChild){ l.firstElementChild.appendChild(s) }else{ var c=i.createElement("div"); c.appendChild(s); i.write(c.innerHTML); c=null } n.addEventListener("resize",function(){ clearTimeout(o); o=setTimeout(m,300) },false); n.addEventListener("pageshow",function(e){ if(e.persisted){ clearTimeout(o); o=setTimeout(m,300) } },false); if(i.readyState==="complete"){ i.body.style.fontSize="16px" }else{ i.addEventListener("DOMContentLoaded",function(e){ i.body.style.fontSize="16px" },false) } })(750,750);
$(function () {
    // init();
    // initAudio(ctx + '/temp/00001.mp3');
    // 获取收藏点赞状态和数量
    tsMeta.queryArticleMetaInfo();

    // 评论列表
    tsMeta.queryPinglunData();

    // 滚动加载评论
    $(window).scroll(function() {
        if (window.pageYOffset >= (document.body.scrollHeight - window.innerHeight)) {
            tsMeta.queryPinglunData();
        }
    });
    initWlkcList();
  //定时器
    setInterval(show1,500);
    initYs();
});
function initYs(){
	var errorObj = {
	        1: '请绑定会员',
	        2: '会员已到期',
	        3: '会员等级不够'
	    };
	    var openid = $("#openid").val();
	    console.info(openid);
	    var errorCode = $("#errorCode").val();
	    console.info(errorCode);
	    var backUrl = $("#backUrl").val();
	    if (errorCode == 1) {
	        $("#msg").html("您的权限尚且无法访问该页面，需要绑定会员信息，绑定会员<a href=\"javascript:hybd();\">点击这里</a>！");
	    } else if (errorCode == 2) {
	        $("#msg").text("您的会员服务已到期无法访问该页面，请及时联系您的客户经理！");
	    } else if (errorCode == 3) {
	    	$("#msg").text("您的会员权限无法访问该页面，请及时联系您的客户经理升级会员等级！");
	    }
}
function hybd() {
    var backUrl = $("#backUrl").val();
    window.location.href = ctx + "/wode/hybd?wx_key=" + wx_key + "&openid="+openid+"&toUrl=" + encodeURIComponent(backUrl);
}
var postData = {
	hot_label: $("#hot_label").val(),
	wlkc_id:$("#wlkc_id").val(),
	wx_key:wx_key
}
function initWlkcList(){
	doService(ctx + '/wlkc/queryWlkcXgkc', postData, function(json) {
		$("#ztxkul").empty();
		initWlkcBlock(json.list);
	});
}
function initWlkcBlock(data) {
	if(data!=null){
		for(var i=0;i<data.length;i++){
			var str="<li>" +
					" <a href=\"javascript:toZbkcDetail('"+data[i].id+"')\">" +
							" <img src='" + getImgByPath(data[i].img) +"' alt='' /> " +
								" <section> ";
								if(data[i].rank==0){
									str+=" <span class='badge badge-gary' style='font-size:12px'>"+showRank(data[i])+"</span> ";
								}else{
									str+=" <span class='badge badge-orange' style='font-size:12px'>"+showRank(data[i])+"</span> ";
								}
								str+=" <p>"+data[i].title+"</p> " +
								" <div class='info'><span>"+kcclick(data[i])+"人</span><span>"+data[i].createtime1+"</span></div> " +
								" </section> " +
								" </a> " +
								" </li>";
			$("#ztxkul").append(str);
		}
	}
}
function showRank(data){
    //debugger;
    if(data.rank == 2){
        return "普通专享";
    }else if(data.rank == 3){
        return "银卡专享";
    }else if(data.rank == 4){
        return "高端专享";
    }else if(data.rank == 0){
        return "免费课程";
    }
}
function kcclick(data){
	console.info("点击数"+data.click);
	if(data.click==undefined){
		return 0;
	}else{
		return data.click;
	}
}
/**
 * 收藏和点赞默认的回调函数
 * @param json
 */
function metaInfo(json) {
    if(json.isup) {
        $("#dzBtn").addClass('active');
    }else {
        $("#dzBtn").removeClass('active');
    }
    if(json.isfavorites) {
        $("#shoucBtn").addClass("active");
        $("#sc").addClass('mdd');
    } else {
        $("#shoucBtn").removeClass("active");
        $("#sc").removeClass('mdd');
    }
    $("#upCount").text(json.upCount);
}

/**
 * 点击收藏按钮，用于收藏或取消收藏
 */
function shoucang() {
    if (!$("#shoucBtn").hasClass('active')) {
        tsMeta.shcArticle();
    } else {
        tsMeta.qxshcArticle();
    }
}

/**
 * 点击点赞按钮，用于点赞或取消点赞
 */
function dianzan() {
    if(!$("#dzBtn").hasClass('active')) {
        tsMeta.dzArticle();
    } else {
        tsMeta.qxdzArticle();
    }
}

/**
 * 评论，弹出评论窗口
 */
function pinglun() {
    tsMeta.tcPinglun();
}

/**
 * 提交评论内容
 */
function tjpinglun() {
    tsMeta.tjPinglun();
}

/**
 * 跳转导其他直播课程
 */
function toZbkcDetail(id){
		window.location.href = ctx+'/wlkc/wlkcDetail?id='+id+"&wx_key="+wx_key+"&openid="+openid;

}
function backShuye(){
    window.history.go(-1);
	
}
/**
 * 获取听课记录
 */
function tkjl(){
	console.info("方法进来了看一线有没有获取到参数");
	if(currenttime==0){
		return;
	}
	var course_gk_time=getTotlaTime(currenttime);
	var course_total_time=getTotlaTime(totalfiletime);
	if(course_gk_time==0){//说明没有听课，没有记录的必要
		return;
	}else{//听课了
		var course_gk_bfb=getGkBfb(course_total_time,course_gk_time);
		var data={
				openid:$("#openid").val(),
				article_id:$("#wlkc_id").val(),
				title:$("#title").val(),
				wx_key:wx_key,
				course_cid:$("#wlkc_cid").val(),
				course_total_time:course_total_time,
				course_gk_time:course_gk_time,
				course_gk_bfb:course_gk_bfb
			};
		var url=ctx+"/viewlog"
			doService(url,data,function(json){
			});
	}
}
function getTotlaTime(time){//获取课程总时长转化为--秒
	if(time!=0){
        var s=time.substring(0,time.indexOf(":"));
        var e=time.substring(time.indexOf(":")+1,time.length);
        var z=Number(s)*60+Number(e)
        return z;
	}


	
}
function getGkBfb(course_total_time,course_gk_time){//获取听课时长百分比
	var e=course_gk_time/course_total_time;
	if(e<=0.15){
		bfb="10%";
		console.info("10%");
	}else if(e>=0.15&&e<0.25){
		bfb="20%";
		console.info("20%");
	}else if(e>=0.25&&e<0.35){
		bfb="30%";
		console.info("30%");
	}else if(e>=0.35&&e<0.45){
		bfb="40%";
		console.info("40%");
	}else if(e>=0.45&&e<0.55){
		bfb="50%";
		console.info("50%");
	}else if(e>=0.55&&e<0.65){
		bfb="60%";
		console.info("60%");
	}else if(e>=0.65&&e<0.75){
		bfb="70%";
		console.info("70%");
	}else if(e>=0.75&&e<0.85){
		bfb="80%";
		console.info("80%");
	}else if(e>=0.85&&e<0.95){
		bfb="90%";
		console.info("90%");
	}else if(e>=0.95){
		bfb="100%";
		console.info("100%");
	}
	console.info(bfb);
	return bfb;
}
//浏览器后退监听事件
$(function(){  
    pushHistory();  
    window.addEventListener("popstate", function(e) { 
        tkjl();
        window.history.go(-1);
 }, false);  
   function pushHistory() {  
	    var state = {  
	         title: "title",  
	         url: "#"  
	    };  
	    window.history.pushState(state, "title", "#");
  }  
      
}); 
function show1(){
	if(objectPlayer){
		var bb=doBb();
		var cutime=objectPlayer.currenttime();
		if(bb==1){//手机端
			if(cutime){
				var gktime=getTotlaTime(cutime);
				//来一个10秒钟的时间
				if(Number(gktime)>=300){
					var code1=$("#errorCode").val();
					if(code1==1||code1==2||code1==3){
                        objectPlayer.stopPlay();
					}
				}
			}
		}else{//pc端
			if(cutime>=300){
				var code1=$("#errorCode").val();
				if(code1==1||code1==2||code1==3){
					objectPlayer.stopPlay();
				}
			}
		}
		
		
	}
	
}
function openQxtx(){
	var aa=($("#qxtx").html());
		layer.open({
	    type: 1
	    ,content: aa
	    ,anim: 'up'
	    ,style: 'position:fixed; bottom:0; left:0; width: 100%; height: 400px; padding:10px 0; border:none;'
	 });
}
function doBb(){
	if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
	   return "1";//手机端
	}else{
		return "2";//pc端
	}
}
