/*
 * 作者 ：yanxiaoyu
 * 优化 :  否
 * 封装 ： 否
 * 
 * */
function loadresrfe(el,callback){
	var _hasTouch = 'ontouchstart' in window;  
	var changeHeight;  
	$(el).on('touchstart',function(e){
		 var even = typeof event == "undefined" ? e : event;  
        //保存当前鼠标Y坐标  
        _start = _hasTouch ? even.touches[0].pageY : even.pageY;  
	})
	
	$(el).on('touchmove',function(e){
        	 
    	var even = typeof event == "undefined" ? e : event;  
    	 //保存当前鼠标Y坐标  
        _end = _hasTouch ? even.touches[0].pageY : even.pageY;  
        changeHeight=_end - _start; 
        
        
        if(_start < _end){
        	$(this).children('.overflow-push').css('transform', 'translateY('+changeHeight+'px)')
        }
        even.preventDefault();  
        
	})
	
	$(el).on('touchend',function(e){
    	$(this).children('.overflow-push').css('transform', 'translateY(0)')
    	if(changeHeight >40){
    		callback();
    	}
    })
}